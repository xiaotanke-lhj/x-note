# 1、JUC 基础阶段

> JUC：是 JDK 中关于处理多线程的一个工具包，方便开发人员。

## 1.1、线程通信

> Lock 接口：为锁和等待条件提供的一个框架接口和类，手动加锁和释放锁。它有一下实现类 `ReentrantLock`、`ReentrantReadWriteLock`。

```java
// 使用 ReentrantLock 可重入锁实现线程数据同步 (卖票案例)
public class SaleTicket {
    private int tickets = 30;
    // 必须是同一个对象，不然使用的锁不一样
    private final Lock lock = new ReentrantLock();
    public boolean sale(){
        try {
            // 手动加锁
            lock.lock();
            if (this.tickets<=0){
                System.out.println("票卖光了~~~~");
                return false;
            }
            System.out.println(Thread.currentThread().getName() + "卖出第：" + (tickets--));
        }finally {
            // 手动解锁
            lock.unlock();
        }
        return true;
    }
}
```

> 线程通信 ( synchronized 版本)：消费者与生产者。

~~~java
public class Consumer {
    // 消费者进行消费
    public void consumer(AppleBuffer buffer, Object lock){
        synchronized (lock){
            try {
                List<Apple> list = buffer.getBuffer();
                // 判断盘子是否存在苹果，存在进行消费
                if (list.size()>0) {
                    Apple apple = list.get(0);
                    System.out.println(Thread.currentThread().getName() + "消费了id:" + apple.getId() + "，名称为：" + apple.getName());
                    list.remove(0);
                    Thread.sleep(1000);
                    // 唤醒生产者进行生产
                    lock.notifyAll();
                }else {
                    System.out.println("等待生产");
                    // 没有苹果就等待生产者生产
                    lock.wait();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}
~~~

~~~java
public class Product {
    // 生产者生产苹果
    public synchronized void product(AppleBuffer buffer, Object lock){
        synchronized (lock){
            try {
                List<Apple> list = buffer.getBuffer();
                int i = list.size();
                if (list.size() < buffer.getLength()){
                    // 进行生产
                    Apple apple = new Apple(String.valueOf(list.size() + 1), "苹果： " + UUID.randomUUID());
                    System.out.println(Thread.currentThread().getName() + "生产了id:" + apple.getId() + "，名称为：" + apple.getName());
                    list.add(apple);
                    Thread.sleep(1000);
                    lock.notifyAll();
                }else {
                    // 已经满了，就等消费者消费
                    System.out.println("等待消费");
                    lock.wait();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}
~~~

```java
public static void main(String[] args) {
    AppleBuffer buffer = new AppleBuffer(10, new LinkedList<>());

    Consumer consumer = new Consumer();
    Product product = new Product();
    // 注意需要使用同一个锁，不然线程之间通信会失败
    final Object lock = new Object();

    new Thread(()->{
        while (true){
            consumer.consumer(buffer, lock);
        }
    }, "消费者1").start();
    new Thread(()->{
        while (true){
            consumer.consumer(buffer, lock);
        }
    }, "消费者2").start();


    new Thread(()->{
        while (true){
            product.product(buffer, lock);
        }
    }, "生产者1").start();
    new Thread(()->{
        while (true){
            product.product(buffer, lock);
        }
    }, "生产者2").start();
}
```

==注意：在线程通信中需要使用同一个锁 (同步监视器)，如果不是同一个线程之间通信会失败。==

> 线程通信 ( Lock 版 )：

​		==wait()、 notify()、notifyAll() 只能使用在同步方法或者同步代码块中，在 Lock 锁中使用这些方法会报异常==。对于 Lock 锁的线程通信自己提供了对应方法。

~~~java
/**
 * lock 同步锁
 */
private final Lock lock = new ReentrantLock();
/**
 * 通过 lock 同步锁产生对应的线程同步类
 *      - await()：线程进入阻塞状态，类似 Object 中的 wait()
 *      - signal()：随机唤醒一个 Lock 阻塞线程，类似 notify()
 *      - signalAll()：唤醒所有 Lock 阻塞线程，类似 notifyAll()
 */
private final Condition condition = lock.newCondition();
~~~

```java
public class MetaBuffer {
    /**
     * lock 同步锁
     */
    private final Lock lock = new ReentrantLock();
    /**
     * 通过 lock 同步锁产生对应的线程同步类
     *      - await()：线程进入阻塞状态，类似 Object 中的 wait()
     *      - signal()：随机唤醒一个 Lock 阻塞线程，类似 notify()
     *      - signalAll()：唤醒所有 Lock 阻塞线程，类似 notifyAll()
     */
    private final Condition condition = lock.newCondition();

    private Integer length = 10;
    private List<Meta> buffer = new LinkedList<>();

    public void consumer(){
        try {
            lock.lock();
            if (buffer.size() > 0){
                // 进行消费
                Meta meta = buffer.get(0);
                System.out.println(Thread.currentThread().getName() + "吃了这块肉：" + meta.getName());
                buffer.remove(0);
                Thread.sleep(1000);
                condition.signalAll();
            }else {
                condition.await();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    public void product(){
        try {
            lock.lock();
            if (buffer.size() < length){
                Meta meta = new Meta();
                meta.setId(String.valueOf(buffer.size() + 1));
                meta.setName(UUID.randomUUID().toString());
                // 进行生产
                System.out.println(Thread.currentThread().getName() + "生产了这块肉：" + meta.getName());
                buffer.add(meta);
                Thread.sleep(1000);
                condition.signalAll();
            }else {
                condition.await();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }
}
```













## 1.2、集合线程安全

> Java 集合中 ArrayList、LinkedList、HashMap 都不是线程安全的集合，如果存在多个线程去操作同一个集合，就可能出现数据被覆盖、数据缺少、下标越界等等异常。

```java
public static void main(String[] args) {
    List<String> array = new ArrayList<>();

    // 开启 10 个线程往同一个集合中添加数据
    for (int i = 0; i < 30; i++){
        new Thread(()->{
            array.add(UUID.randomUUID().toString());
            System.out.println(array);
        }).start();
    }
}
```

![image-20230728134612525](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20230728134612525.png)

> Vector：是一个线程安全的集合列表，可以解决 List 线程不安全问题，因为它定义的方法大多数使用 `synchronized` 关键字修饰。

![image-20230728142534927](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20230728142534927.png)

> Collections 工具类：它提供了将集合变成线程安全集合的方法，`synchronizedList()`、`synchronizedMap()`、`synchronizedSet()`。

```java
public static void main(String[] args) {
    List<String> array = new ArrayList<>();
    
    // 转换成线程安全集合
    List<String> list = Collections.synchronizedList(array);
    
    // 开启 10 个线程往同一个集合中添加数据
    for (int i = 0; i < 10; i++){
        new Thread(()->{
            list.add(UUID.randomUUID().toString());
            System.out.println(list);
        }).start();
    }
}
```

> CopyOnWriteArrayList / Set：可以获取到线程安全的集合类，list 底层类似 ArrayList，set 底层就是 hashSet，采用写时复制技术，==读操作可以并发读取数据，进行写操作时会将数据进行拷贝一份，然后将新数据写入拷贝列表中，写入完成后然后将原来的列表替换掉==。

```java
public static void main(String[] args) {
    // 转换成线程安全集合
    List<String> list = new CopyOnWriteArrayList<>();

    // 开启 10 个线程往同一个集合中添加数据
    for (int i = 0; i < 10; i++){
        new Thread(()->{
            list.add(UUID.randomUUID().toString());
            System.out.println(list);
        }).start();
    }
}
```

> ConcurrentHashMap：解决 map 集合线程不安全问题。

```java
Map<String, String> map = new ConcurrentHashMap<>();
// 开启 10 个线程往同一个集合中添加数据
for (int i = 0; i < 30; i++){
    new Thread(()->{
        map.put(String.valueOf(UUID.randomUUID()), UUID.randomUUID().toString());
        System.out.println(map);
    }).start();
}
```

## 1.3、多线程锁

> synchronized 同步锁：是一种独占锁（Exclusive Lock），也称为互斥锁。在同一时间，只有一个线程可以持有该锁，其他线程必须等待锁的释放才能继续执行，它会有一个监视器，多线程中应该共用同步监视器。

- 同步代码块：可以自己指定晋监视器，是一个 Object 对象。

```java
private final Object object = new Object();
public void test1(){
    // 括号中就是同步监视器，由自己指定一般不能改变，并且在代码块执行wait和notify方法也需要同步监视器去调用
    synchronized(object){
        // object.wait();
        // object.notify();
    }
}
```

- 非静态同步方法：这种的同步监视是就是这个对象实例 this。

```java
// 非静态同步方法，同步监视是对象实例，多线程在执行这个方法时需要注意是否为同一个实例对象
public synchronized void test2(){
    
}
```

- 静态同步方法：这种方法的同步监视器是这个类的 Class 对象，默认同一个。

```java
public static synchronized void test3(){
    
}
```

> 公平锁与非公平锁：

- 非公平锁：线程进行资源抢占时一旦发现资源空闲就会立即抢占，不会进行排队操作，==效率较高，但是存在线程饿死情况：一个线程把所有任务执行完成。==
- 公平锁：线程在抢占资源时会先询问资源是否空闲，空闲就抢占，非空闲就会排队等待，==效率较非公平锁较低，但是解决了线程饿死情况。==

```java
// 构造器可以传入一个boolean值，true表示公平锁，false表示非公平锁，默认使用非公平锁
private final Lock lock = new ReentrantLock();
```

> 可重入锁：是一种支持线程重复获取锁的锁机制。在多线程编程中，当一个线程持有锁时，如果该线程再次==请求获取同一个锁==，它将能够成功获取，而不会被阻塞。==synchronized 和 lock 都是可重入锁==。

```java
public void test1(){
    // 外层获取锁
    synchronized (lock){
        System.out.println(Thread.currentThread().getName() + " 外层");
        // 中层再去获取同一把锁时,依然可以获取到,然后执行锁中的代码
        synchronized (lock){
            System.out.println(Thread.currentThread().getName() + " 中层");
            synchronized (lock){
                System.out.println(Thread.currentThread().getName() + " 内层");
            }
        }
    }
}
```

==注意：每个线程在持有锁时，会维护一个计数器，每次获取同一个锁时计数器加1，释放锁时计数器减1，当计数器为 0 时才会真正释放锁，所以在内层释放锁会外层锁没有影响，但是需要注意每一层上了锁就需要解锁，不然可能锁会一直在。==

> 死锁：两个或者两个以上线程执行过程中，因为资源抢夺而造成一种互相等待现象。

![image-20230731115038889](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20230731115038889.png)

~~~java
private final String sourceA = "A";
private final String sourceB = "B";

public void test1(){
    // 先请求资源A，再去获取资源B发现需要等待线程2释放资源，所有线程都会阻塞
    synchronized (sourceA){
        System.out.println(Thread.currentThread().getName() + " 获取到资源A");
        System.out.println(Thread.currentThread().getName() + " 等待获取到资源B");
        try{
            Thread.sleep(1000);
        }catch (Exception e){

        }
        synchronized (sourceB){
            System.out.println(Thread.currentThread().getName() + " 获取到资源B");
        }
    }
}

public void test3(){
    // 先请求资源B
    synchronized (sourceB){
        System.out.println(Thread.currentThread().getName() + " 获取到资源B");
        System.out.println(Thread.currentThread().getName() + " 等待获取到资源A");
        try{
            Thread.sleep(1000);
        }catch (Exception e){

        }
        synchronized (sourceA){
            System.out.println(Thread.currentThread().getName() + " 获取到资源A");
        }
    }
}
~~~

> 检测程序是否包含死锁？==使用 jps 和 jstack 命令进行检测==。

- jps：用于查看当前 Java 进程列表以及信息。
  - -q：仅显示进程 ID，而不显示主类名。
  - -l：显示完整主类名，包含包路径。
  - -m：显示传递给 Java 进程主类的参数。

![image-20230731135903666](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20230731135903666.png)

- jstack：用于生成 Java 应用程序的线程信息，包括每个线程的堆栈跟踪信息。`jstack pid`

![image-20230731140234099](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20230731140234099.png)

## 1.4、Callable 接口

> Callable 接口：一种创建多线程任务的一种方式，实现它的 call() 方法，这个方法拥有返回值当线程执行完成可以返回，并且也可以向上抛出异常信息。

```java
public class CallableTest implements Callable<Integer> {
    @Override
    public Integer call() throws Exception {
        Thread.sleep(5000);
        return 10;
    }
}
```

> Future 接口：表示一个可能还没有完成的异步任务的结果，可以对异步任务进行操作以及获取到异步任务执行完成后的执行结果，常用实现类 `FutureTask`。

```java
public static void main(String[] args) throws ExecutionException, InterruptedException {
    /**
     * 两种构造器：
     *  - 通过实现Callable接口
     *  - 通过Runnable接口，但是需要执行线程执行完后的返回结果
     */
    // FutureTask<Integer> task = new FutureTask<>(new CallableTest()); // callable方法
    FutureTask<Integer> task = new FutureTask<>(new RunnableTest(), 1000);

    new Thread(task, "线程一").start();
    System.out.println(task.get());
}
```

<font color=red>常用方法：</font>

- run()：主线程执行一次异步方法。
- get()：获取方法的执行结果，如果任务未完成会进入阻塞状态，直到任务完成。==异步任务只需要执行一次，后面在获取数据就不在需要计算了==。
- get(long time, TimeUnit unit)：可以指定任务完成超时时间，在指定时间未完成就抛出异常。
- isDone()：判断异步任务是否完成。
- cancel(boolean)：取消异步任务，参数是只指定当任务在运行时是否中断线程，true 中断，fasle 不中断。
- isCancel()：判断线程是否取消。

## 1.5、常用辅助类

> CountDownLatch 减少计数器：它可以让一个或者多个线程等待另外其它线程全部都完成后再进行操作，每完成一个线程计数就要减 1，直到减少到 0 线程就全部完成。

```java
public static void main(String[] args) throws InterruptedException {
    // 初始化对象，参数是需要完成线程个数
    CountDownLatch count = new CountDownLatch(3);
    
    // 开启三个线程
    new Thread(()->{
        try {
            Thread.sleep(2000);
            System.out.println("线程正在执行中");
            // 线程完成，计数器 - 1
            count.countDown();
        }catch (Exception e){}
    },"线程二").start();

    new Thread(()->{
        try {
            Thread.sleep(3000);
            System.out.println("线程正在执行中");
            // 线程完成，计数器 - 1
            count.countDown();
        }catch (Exception e){}
    },"线程一").start();

    new Thread(()->{
        try {
            Thread.sleep(4000);
            System.out.println("线程正在执行中");
            // 线程完成，计数器 - 1
            count.countDown();
        }catch (Exception e){}
    },"线程三").start();

    System.out.println("主线程等待子线程全部执行完成。。。。。。");
    count.await();
    System.out.println("子线程全部执行完成");
}
```

<font color=red>常用方法：</font>

- countDown()：当前计数器减 1，减少为 0 当前所有线程都执行完成。
- await()：当前线程进入阻塞状态，==等待计数器减少到 0 就会唤醒当前线程==。
- getCount()：获取当前计数器数。

> CyclicBarrier 循环栅栏：用于让一组线程等待其它线程完成一定任务后，多个线程需要等待彼此达到一个同步点后再一起执行。

```java
public static void main(String[] args) {
    /**
     * 参数一：指定到达同步线程数量
     * 参数二：runnable接口实现类，当同步线程数量达到指定数量后执行
     */
    CyclicBarrier barrier = new CyclicBarrier(3, ()->{
        System.out.println("所有线程同步了");
    });

    for (int i=0;i<3;i++){
        int finalI = i;
        new Thread(()->{
            System.out.println(Thread.currentThread().getName()+"全部行程同步前执行。。。");
            try {
                Thread.sleep((finalI +1)*1000);
                // 阻塞线程，只有同步线程达到指定数量会继续执行，也指定唤醒指定数量线程
                barrier.await();
            } catch (InterruptedException | BrokenBarrierException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName()+"全部线程同步执行。。。");
        },"线程 "+ i).start();
    }
}
```

<font color=red>注意：</font>与 `CountDownLatch` 不同，它是在执行过程中线程同步到一个点后继续再执行，`CyclicBarrier`可以被重复使用。当所有线程都到达屏障点后，`CyclicBarrier`会自动重置，可以继续使用。

> Semaphore 信号灯：线程在访问资源之前需要获取许可证，如果计数器大于0，则线程可以获得许可证，然后执行访问资源的操作；如果计数器为0，则线程需要等待，直到有其他线程释放许可证为止。

```java
public static void main(String[] args) {
    /**
     * 参数一：指定许可证的数量
     * 参数二：是否为公平抢夺许可证，默认false
    */
    Semaphore semaphore = new Semaphore(3, true);
    for (int i=0;i<5;i++){
        int finalI = i + 1;
        new Thread(()->{
            try {
                // 请求一个许可证，许可证减1
                semaphore.acquire();
                System.out.println(Thread.currentThread().getName() + "正在执行操作");
                Thread.sleep(finalI * 1000);
                System.out.println(Thread.currentThread().getName() + "执行完成");
                // 释放一个许可证，许可证加1
                semaphore.release();
            }catch (Exception e){}
            finally {
                semaphore.release();
            }
        }, "Thread " + i).start();
    }
}
```





















