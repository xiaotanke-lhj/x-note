# Jenkins笔记

> Jenkins：Jenkins是一款开源 CI&CD 软件，用于自动化各种任务，包括构建、测试和部署软件。Jenkins 支持各种运行方式，可通过系统包、Docker 或者通过一个独立的 Java 程序。

## 1、Jenkins 下载安装

[下载地址：https://www.jenkins.io/zh/download/](https://www.jenkins.io/zh/download/)，下载后是一个war，直接可以通过java -jar 命令运行启动。

![image-20221104000747678](https://gitee.com/xiaotanke-lhj/image/raw/master/csdn/image-20221104000747678.png)

后台运行命令：启动后直接就可以进行访问了，然后在`/root/.jenkins/secrets/initialAdminPassword`目录下查询初始密码。

~~~bash
# --httpPort=8000指定运行端口，后面配置日志输出目录
nohup java -jar jenkins.war --httpPort=8000 >jenkins.log 2>&1 &
~~~

<img src="https://gitee.com/xiaotanke-lhj/image/raw/master/csdn/image-20221104002016006.png" alt="image-20221104002016006" style="zoom: 33%;" />

![image-20221104143235773](https://gitee.com/xiaotanke-lhj/image/raw/master/csdn/image-20221104143235773.png)

## 2、Linux 配置Maven环境

[下载地址:https://maven.apache.org/download.cgi](https://maven.apache.org/download.cgi)，因为Jenkins构建项目需要Maven环境，下载完成后解压到服务器上。

![image-20221104142639955](https://gitee.com/xiaotanke-lhj/image/raw/master/csdn/image-20221104142639955.png)

> 配置Maven环境：

~~~bash
# 配置当前用户环境变量
vi ~/.bash_profile
 
# 在文件中添加如下命令，记得切换自己文件路径
export MAVEN_HOME=/home/work/apache-maven-3.6.3
export PATH=$MAVEN_HOME/bin:$PATH
 
# 立即生效
source ~/.bash_profile

# 查看 maven 版本
mvn -v
~~~

![image-20221104143753135](https://gitee.com/xiaotanke-lhj/image/raw/master/csdn/image-20221104143753135.png)

> 修改下载镜像：修改maven的settings.xm

~~~xml
<mirror>
	<id>alimaven</id>
	<name>aliyun maven</name>
	<url>http://maven.aliyun.com/nexus/content/groups/public/</url>
	<mirrorOf>central</mirrorOf>
</mirror>
~~~

![image-20221104152011831](https://gitee.com/xiaotanke-lhj/image/raw/master/csdn/image-20221104152011831.png)

![image-20221104152104584](https://gitee.com/xiaotanke-lhj/image/raw/master/csdn/image-20221104152104584.png)

## 3、Jenkins 基本使用

> Maven插件安装：用于打包发布Maven构建的项目。

![image-20221104150219613](https://gitee.com/xiaotanke-lhj/image/raw/master/csdn/image-20221104150219613.png)

![image-20221104150358919](https://gitee.com/xiaotanke-lhj/image/raw/master/csdn/image-20221104150358919.png)

> 创建一个Maven的item：

![image-20221104150715979](https://gitee.com/xiaotanke-lhj/image/raw/master/csdn/image-20221104150715979.png)

> 配置item：需要在Jenkins服务器上安装git，直接`yum install -y git`。

![image-20221104154424695](https://gitee.com/xiaotanke-lhj/image/raw/master/csdn/image-20221104154424695.png)

![image-20221104154526542](https://gitee.com/xiaotanke-lhj/image/raw/master/csdn/image-20221104154526542.png)

![image-20221104154805172](https://gitee.com/xiaotanke-lhj/image/raw/master/csdn/image-20221104154805172.png)

![image-20221104155115030](https://gitee.com/xiaotanke-lhj/image/raw/master/csdn/image-20221104155115030.png)

![image-20221104155357791](https://gitee.com/xiaotanke-lhj/image/raw/master/csdn/image-20221104155357791.png)

![image-20221104170053901](https://gitee.com/xiaotanke-lhj/image/raw/master/csdn/image-20221104170053901.png)

## 4、Jenkins之自动部署

​	当Jenkins打成jar后需要手动去启动jar，这时就需要自动化部署jar到服务器的操作。

### 4.1、自动部署到其它服务器

每次构建的时候，自动将构建好的jar传输到指定服务器上，然后自动运行命令进行启动服务。

> 安装publish ssh插件：

![image-20221104172454178](https://gitee.com/xiaotanke-lhj/image/raw/master/csdn/image-20221104172454178.png)

> 添加远程服务器：在系统全局配置里面进行添加，只有下载了publish ssh插件才有。

![image-20221104172749116](https://gitee.com/xiaotanke-lhj/image/raw/master/csdn/image-20221104172749116.png)

![image-20221104173348132](https://gitee.com/xiaotanke-lhj/image/raw/master/csdn/image-20221104173348132.png)

> 配置构建后续操作：就是将jar传输到指定服务器上，然后自动运行命令部署jar包。

![image-20221104173633853](https://gitee.com/xiaotanke-lhj/image/raw/master/csdn/image-20221104173633853.png)

![image-20221104174156651](https://gitee.com/xiaotanke-lhj/image/raw/master/csdn/image-20221104174156651.png)

![image-20221104224804959](https://gitee.com/xiaotanke-lhj/image/raw/master/csdn/image-20221104224804959.png)

### 4.2、publish ssh 超时机制

![image-20221105112751550](https://gitee.com/xiaotanke-lhj/image/raw/master/csdn/image-20221105112751550.png)

> 命令优化：`nohup java -jar /root/jars/*.jar  &`，这个命令有一点问题，他执行时会卡主，可能导致超时错误。

![image-20221105113853294](https://gitee.com/xiaotanke-lhj/image/raw/master/csdn/image-20221105113853294.png)

优化后的命令：`nohup java -jar /roor/jars/*.jar  >note.log 2>&1 &`

- nohup：这个命令是当前会话退出后进程依然能够运行。
- `>`：覆盖输入
- `>>`：追加输入
- 0：标准输入
- 1：标准输出
- 2：标准错误输出
- 最后一个&：表示进程在后台运行

### 4.3、Pre  Steps前置操作

> 前置操作：用于删除的文件并杀死之前启动的项目进程，防止再次启动导致端口号冲突。

![image-20221105145414559](https://gitee.com/xiaotanke-lhj/image/raw/master/csdn/image-20221105145414559.png)

![image-20221105145655995](https://gitee.com/xiaotanke-lhj/image/raw/master/csdn/image-20221105145655995.png)

> 编写shell脚本：

```shell
# 部署项目前清除脚本

# 查询进程号，awk：去参数，{printf $2}：第二个参数，就是进程号
pid=`ps -ef | grep modules-pay-01 | grep 'java -jar' | awk '{printf $2}'`

# 判断进行是否在启动，判断进程号是否为空
if [ -z $pid ];
  # 为空
  then echo "项目未启动"
  # 杀死进程
  else
    kill -9 $pid
    # 判断杀死成功
    checkPid=`ps -ef | grep -w $pid | grep 'java -jar'`
    if [ -z $checkPid ];
     # 杀死成功
     then
       echo "项目杀死成功"
     else
       echo "项目杀死失败"
    fi
fi
```

> Pre  Steps之前上传脚本文件并执行文件，来杀死之前的进程

![image-20221105164656195](https://gitee.com/xiaotanke-lhj/image/raw/master/csdn/image-20221105164656195.png)
