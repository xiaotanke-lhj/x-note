# XXL JOB 任务调度中心

> XXL-JOB是一个分布式任务调度平台，其核心设计目标是开发迅速、学习简单、轻量级、易扩展。现已开放源代码并接入多家公司线上产品线，开箱即用。

<font color=red>分布式集群下，任务调度的问题：</font>

- 多台机器下部署的定时任务如何保证任务不被重复执行。
- 在不重启项目下，如何动态去修改定时任务的时间。
- 部署定时任务的机器发生故障时，如何转移定时任务。
- 如何对定时任务进行监控管理。
- 业务量需求大时，如何对性能进行提高。

![image-20221110111058079](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20221110111058079.png)

## 1、XXL JOB 任务调度中心安装部署

[Gitee地址：https://gitee.com/xuxueli0323/xxl-job](https://gitee.com/xuxueli0323/xxl-job)，下载源码。

![image-20221110111301188](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20221110111301188.png)

> 修改配置文件：

```properties
### web
server.port=8080
server.servlet.context-path=/xxl-job-admin

### xxl-job, datasource
spring.datasource.url=jdbc:mysql://127.0.0.1:3306/xxl_job?useUnicode=true&characterEncoding=UTF-8&autoReconnect=true&serverTimezone=Asia/Shanghai
spring.datasource.username=root
spring.datasource.password=root_pwd
spring.datasource.driver-class-name=com.mysql.cj.jdbc.Driver

### xxl-job, email
spring.mail.host=smtp.qq.com
spring.mail.port=25
spring.mail.username=xxx@qq.com
spring.mail.from=xxx@qq.com
spring.mail.password=xxx
spring.mail.properties.mail.smtp.auth=true
spring.mail.properties.mail.smtp.starttls.enable=true
spring.mail.properties.mail.smtp.starttls.required=true
spring.mail.properties.mail.smtp.socketFactory.class=javax.net.ssl.SSLSocketFactory

### xxl-job, access token
xxl.job.accessToken=default_token
```

> 初始化数据库：在doc文件夹下有一个sql脚本，在数据库上执行就可以生成一个库。

![image-20221110111615700](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20221110111615700.png)

> Maven编译打包成一个jar：访问`http://127.0.0.1:8080/xxl-job-admin/`，打成的jar可以直接在Linux上进行部署。登录账号默认是admin和123456，可以修改。

![image-20221110112056052](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20221110112056052.png)

## 2、XXL  JOB 执行器

### 2.1、项目中执行器配置

> 引入依赖：

~~~xml
<dependency>
    <groupId>com.xuxueli</groupId>
    <artifactId>xxl-job-core</artifactId>
    <version>2.3.1</version>
</dependency>
~~~

> 配置执行器：

~~~yaml
xxl:
  job:
    admin:
      addresses: http://127.0.0.1:8080/xxl-job-admin
    # 【选填】任务中心每次调度任务请求是否携带token，非空携带
    accessToken:
    executor:
      # 【选填】执行器的名称，为空就关闭自动注册
      appname: xxl-job-test
      # 【选填】优先使用该配置作为注册地址，为空时使用内嵌服务 ”IP:PORT“
      address: http://127.0.0.1:8080/xxl-job-admin
      # 【选填】默认为空表示自动获取IP，多网卡时可手动设置指定IP，一般为空
      ip: ""
      # 【选填】执行器端口号 [选填]：小于等于0则自动获取；默认端口为9999，单机部署多个执行器时，注意要配置不同执行器端口；
      port: 0
      # 日志文件保存路径
      logpath: /data/applogs/xxl-job/jobhandler
      # 执行器日志文件保存天数 [选填] ： 过期日志自动清理, 限制值大于等于3时生效; 否则, 如-1, 关闭自动清理功能；
      logretentiondays: 30
~~~

> 编写配置类：

```java
@Configuration
public class XxlJobConfig {
    
    private Logger logger = LoggerFactory.getLogger(XxlJobConfig.class);

    @Value("${xxl.job.admin.addresses}")
    private String adminAddresses;
    @Value("${xxl.job.accessToken}")
    private String accessToken;
    @Value("${xxl.job.executor.appname}")
    private String appname;
    @Value("${xxl.job.executor.address}")
    private String address;
    @Value("${xxl.job.executor.ip}")
    private String ip;
    @Value("${xxl.job.executor.port}")
    private int port;
    @Value("${xxl.job.executor.logpath}")
    private String logPath;
    @Value("${xxl.job.executor.logretentiondays}")
    private int logRetentionDays;

    @Bean
    public XxlJobSpringExecutor xxlJobExecutor() {
        logger.info(">>>>>>>>>>> xxl-job config init.");
        XxlJobSpringExecutor xxlJobSpringExecutor = new XxlJobSpringExecutor();
        xxlJobSpringExecutor.setAdminAddresses(adminAddresses);
        xxlJobSpringExecutor.setAppname(appname);
        xxlJobSpringExecutor.setAddress(address);
        xxlJobSpringExecutor.setIp(ip);
        xxlJobSpringExecutor.setPort(port);
        xxlJobSpringExecutor.setAccessToken(accessToken);
        xxlJobSpringExecutor.setLogPath(logPath);
        xxlJobSpringExecutor.setLogRetentionDays(logRetentionDays);

        return xxlJobSpringExecutor;
    }
}
```

> 项目中新增一个调度任务：

```java
// 注入容器
@Component
public class TestScheduleTask {

    /**
     * 编写一个任务方法：
     *  - 使用@XxlJob注解标记：
     *        - value：任务名称
     *        - init：指定一个方法名，在任务初始化时会执行这个任务
     *        - destroy：指定一个方法名，在任务销毁时执行的一个方法
     */
    @XxlJob(value = "test",init = "init",destroy = "destroy")
    public void test(){
        System.out.println(LocalDateTime.now());

        /**
         * 任务结果：默认任务结果为 "成功" 状态，不需要主动设置；如有诉求，
         *      任务成功 XxlJobHelper.handleFail()
         *      任务失败 XxlJobHelper.handleSuccess()
         *      任务返回数据：XxlJobHelper.handleResult()
         */
        XxlJobHelper.handleSuccess("test任务执行成功");
    }


    public void init(){
        System.out.println("===== 任务正在初始化 =====");
    }

    public void destroy(){
        System.out.println("===== 任务正在销毁 ======");
    }
}
```

### 2.2、调度中心配置执行器及任务

> 新增一个执行器：

![image-20221110160858469](https://gitee.com/xiaotanke-lhj/image/raw/master/csdn/image-20221110160858469.png)

<font color=red>机器地址：</font>任务调度中心去执行调度任务的对应ip和端口号。

> 调度中心新增一个任务：

![image-20221110163500278](https://gitee.com/xiaotanke-lhj/image/raw/master/csdn/image-20221110163500278.png)

> 路由策略：

![image-20221110211221931](https://gitee.com/xiaotanke-lhj/image/raw/master/csdn/image-20221110211221931.png)

> 阻塞处理策略：

当任务调度发生阻塞时，单机串行是排队等待，丢弃后续调度是后面的任务直接丢弃，覆盖就是覆盖前一次任务。

![image-20221110211437567](https://gitee.com/xiaotanke-lhj/image/raw/master/csdn/image-20221110211437567.png)

> 运行模式：

![image-20221110211911776](https://gitee.com/xiaotanke-lhj/image/raw/master/csdn/image-20221110211911776.png)

> 其它属性：

- 子任务ID：当前任务执行完成后会去执行子任务，子任务可以配置多个。
- 任务超时时间：任务执行时间超过这个时间就自动失败。
- 失败重试次数：任务失败后继续继续执行的次数。

![image-20221110212035664](https://gitee.com/xiaotanke-lhj/image/raw/master/csdn/image-20221110212035664.png)

> 调度中心去调度多台机器：需要在执行器里面新增ip地址和端口号，然后根据选择的路由策略去调度。

![image-20221110215254981](https://gitee.com/xiaotanke-lhj/image/raw/master/csdn/image-20221110215254981.png)

### 2.3、 Cron 表达式

[Cron在线生成网站：https://cron.qqe2.com/](https://cron.qqe2.com/)

> Cron表达式：由若干数字、空格、符号按一定的规则，组成的一组字符串，从而表达时间的信息。格式：`{秒数} {分钟} {小时} {日期} {月份} {星期} {年份(可为空)}`

- `*`：表示每隔多少时间触发一次，例如每秒、每分钟、每天、每月、每年等。
- `,`：指定时间触发一次，例如：`0,15,40`，0秒、15秒、49秒各触发一次。
- `-`：表示一个范围，例如：`1-5`，每分钟的1秒到5秒，每隔1秒各触发一次。
- `/`：表示每隔多少时间执行一次，`5/20`，前面值是初始值，后面一个值是间隔时间，表示从5秒开始，每隔20秒执行一次。

<font color=red>日期特殊表达式：
</font>

- `L`：表示这个月的最后一天。
- `W`：在指定日期的这个月中，距离最近的工作日，如果指定日期是工作日，则距离为0。













