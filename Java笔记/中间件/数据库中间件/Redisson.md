​		`Redisson` 是一款在 `Redis` 基础上的 Java 内存数据网格。它提供了一系列分布式的 Java 常用对象，还提供了许多分布式服务。`Redisson` 提供了使用 `Redis` 的最简单和最便捷的方法。`Redisson` 的宗旨是促进使用者对 `Redis` 的关注分离，它的底层采用 Netty 框架。

~~~xml
<dependency>
    <groupId>org.redisson</groupId>
    <artifactId>redisson</artifactId>
    <version>3.18.1</version>
</dependency>
~~~

## 1、配置方法

### 1.1、单机版

> 程序配置：构建`Config`对象实例来实现的

```java
@Configuration
public class RedissonConfig {

    @Bean
    public RedissonClient config(){
        Config config = new Config();
        // 构建config对象，然后创建RedissonClient（单机版）
        config.useSingleServer().setAddress("redis://49.234.48.133:6379");
        return Redisson.create(config);
    }
}
```





