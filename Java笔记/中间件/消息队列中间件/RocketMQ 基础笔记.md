## 1、Rocket MQ 简介

### 1.1、MQ 简介

> MQ：消息队列，一套提供消息数据的生产、存储、消费全程的API的系统软件。

<font color=red>MQ 用途：
</font>

- 限流消峰：MQ 可以将系统超量的请求进行暂存，以便后期系统进行处理调度，从而避免请求的的丢失和系统被压垮。

![image-20221125185444191](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20221125185444191.png)

- 异步解耦：上游系统去调用下游系统时采用同步调用方式，系统的吞吐量会大大降低，且上下游系统的耦合度增加。一般会在上下游系统之间添加一个MQ，上游系统将消息数据给MQ 然后直接返回给用户，后面的所有操作由MQ 进行请求下游操作，如果失败了就进行重试。

![image-20221125190838306](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20221125190838306.png)

- 数据收集：分布式系统会产生大量的数据，例如业务日志、监控数据等。针对这些数据进行实时采集和处理，然后对数据进行分析操作，MQ 也可以完成这类操作。

### 1.2、Rocket MQ 简介

> RocketMQ：是阿里开源的一款云原生的消息、事件、流的实时数据处理平台，覆盖云边端一体化数据处理场景。

<font color=red>消息(message)：</font>消息系统所传输信息的载体，生产与消费的最小单位，每条消息必须属于一个主题。

<font color=red>主题(topic)：</font>表示一类消息的集合，每个主题包含多条消息，每条消息只能属于一个主题。==一个生产者可以发送多种主题的消息，而消费者只能订阅和消费一种主题消息。==

<font color=red>标签(tag)：</font>标签，在RocketMQ 发送给topic 的消息设置标签，用于区分同一主题下不同消息。

<font color=red>队列(Queue)：</font>存储消息的物理实体，一个topic 包含多个队列，每个队列中存放的就是topic 的消息，也成为topic 中的一个消息分区。==一个消费者（同一个消费者组）只能消费topic 中的一个队列，这样可以有多个消费者进行消费同一个topic 中的消息。==(消费者 ： 队列 ==> 1:n，队列 : 消费者 ==> 1:1)。一个队列只能被同一个消费者组的一个消费者消费，一个消费者可以消费不同的队列。

<font color=red>分片：</font>分片存放相应主题下的主机，每个分片中创建不同的队列。

![image-20221201150303840](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20221201150303840.png)

<font color=red>消息标识：</font>每一条消息都有自己的唯一标识，以便对消息的查询。标识一个有三个，两个MessageId、一个业务标识key，两个MessageId一个是生产者产生的，一个是主机生成的。

- 生产者MessageId：由生产端 (product) 产生，生成规则：`product IP + 进程Id + MessageClientIDSetter类的ClassLoader的hascode + 当前时间 + 自增`。这个id不容易重复，但是可能会重复。
- 主机MessageId：由Broker 生成，生成规则是：`broker IP + 队列消息的偏移量`，容易重复，因为队列的消息偏移量容易相同。
- key：用户指定的不重复业务标识。

## 2、Rocket MQ 基础理论

### 2.1、Topic 主题

> Topic：是消息传输和存储的顶层容器，是一个逻辑概念并不是真正的容器，==一般用于标识一类业务逻辑的消息==。

- <font color=red>消息分类隔离：</font>将不同的业务类型消息拆分成不同主题进行管理，通过 Topic 实现存储、订阅的隔离。
- <font color=red>定义数据身份和权限：</font>消息本来就是一个匿名身份，同一类消息使用相同的 Topic 来做身份识别和权限管理。

![image-20221211192937168](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20221211192937168.png)

==主题内是由多个 Queue 组成，这些 Queue 负责消息的存储、负载均衡能力，也在 Queue 中完成针对主题的所有约束和属性设置。==

> Topic 内部属性：

- <font color=red>Topic 名称：</font>标识主题，在集群中唯一，满足一定的命名条件。
- <font color=red>Queue 列表：</font>作为 Topic 的组成单元，一个 Topic 中包含过个 Queue，创建 Topic 时默认创建的 Queue 有16个，也可以自定义 Queue 数量。==一个 Topic 至少有一个 Queue。==
- <font color=red>消息类型：</font>Topic 所支持的消息类型，==每个 Topic 只支持一种消息类型。==
  1. 普通消息(Normal)：消息之间没有任何关联。
  2. 顺序消息(FIFO)：通过消息分组 MessageGroup 标记特定消息的先后顺序，保证消息的投递顺序。
  3. 定时/延时消息(Delay)：通过延时时间控制消息生产后不会立即被投递，而是在延时时间过后才会被消费者可见。
  4. 事务消息(Transaction)：支持分布式事务消息，支持应用数据库更新和消息调用的事务的一致性。

> 行为约束：发送和订阅消息时会对消息进行校验，若检验不成功，则请求会被拒绝。

- 消息类型必须和 Topic 定义的类型保持一致。
- Topic 中只支持一种消息类型，不允许多种类型的消息发送到一个 Topic 中。

### 2.2、Queue 队列





























### 1.3、生产者与消费者

![image-20221201152555424](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20221201152555424.png)

- <font color=red>producer 生产者：</font>负责消息的生产，产生消息后producer 通过MQ 的负载均衡将消息投递到对应的broker 主机，投递过程中支持快速失败并且低延迟。RocketMQ 中的生产者都是以生产者组的形式存在，生产者组是一类生产者的集合，一个生产者组只能发送同一类的topic 主题的消息，但是能生产多类的topic 主题的消息。
- <font color=red>consumer 消费者：</font>负责消息的消费，一个消费者会从broker 中获取消息，并对消息进行业务处理。RocketMQ 中的消费者都是以消费者组形式存在，一个消费者组只能消费同一topic 的消息，消费者组在获取消息时实现了==负载均衡==（队列的负载均衡，topic的多个队列会平均分配给消费者组中的所有消费者）和==容错==（某个消费者宕机后，它对应的队列也会重新分配给其它消费者）。

![image-20221201162302912](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20221201162302912.png)

<font color=red>注意事项：</font>

1. 一个队列只能被一个消费者组的一个消费者进行消费。
2. topic 中队列的数量应多于或者等于消费者组中消费者数量，不然会有空闲的消费者导致资源浪费。
3. 一个topic 类型的消息可以被多个消费者组进行消费。
4. 一个消费者组的消费者必须订阅==完全相同==的topic。

### 1.4、NameServer

> NameServer：是一个broker 和topic 路由的注册中心，支持broker 的动态注册与发现。

- broker 管理：接受broker 集群的注册信息并保存为路由信息的基本数据，提供心跳检测机制，检查broker 是否还存在。
- 路由信息管理：每个NameServer 都保存==broker 集群的整个路由信息==和用于==客户端查询的队列信息==，生产者与消费者通过NameServer 可以获取broker 集群信息，然后进行消息的投递和消费。

> 路由注册：broker 的信息会注册进NameServer 中。

- NameServer 一般以集群的形式存在，是无状态的，节点与节点之间是不进行任何通信的。
- 在broker 启动时，会和NameServer 集群的每一个节点建立一个长连接，发起注册请求，并且在NameServer 中维护一个broker 表。
- broker 为了让NameServer 认为服务正常，会每隔30s 向NameServer 发送心跳包，包含broker 的信息，NameServer 就会更新broker 的心跳检测时间。

> 路由剔除：

​	NameServer 中有一个定时任务，每隔10s 就会去扫描broker 表，查看每一个broker 的更新时间，如果时间超过120s，就是超时然后NameServer 就会将这个broker 信息删除掉。

> 路由发现：

​	RocketMQ 的路由发现是采取PULL 模型，当topic 发生变化时，NameServer 不会主动推送给客户端，而是客户端会定时从NameServer 上拉取最新的topic (默认30s 拉一次)，这种模型实时性较差，如果刚拉取完没发生变化，然后突然发生变化。

> 客户端的NameServer选择策略：在客户端连接NameServer集群时都会把所有的配置进去。

- 首先会产生一个随机数，然后余上NameServer 的数量，然后进行连接。
- 如果随机连接失败，就会采取轮询的方式去尝试连接每一个NameServer 。

### 1.5、Broker

​		Broker 主要是个消息中转角色，负责消息存储、转发消息。接收并存储生产者生产的消息，同时也为消费者拉取消息做好准备。在Broker 中存储着消息相关的元数据，包括消费者进行消费的进度偏移offset、主题、queue。`Remoting Module`就是整个Broker，在他下面包含了以下几个模块：

![image-20221206153638854](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20221206153638854.png)

- <font color=red>Client Manager：</font>客户端管理器，负责接收和解析Productor 和Consumer 的请求。
- <font color=red>Store Service：</font>存储服务，提供方便的API 接口，处理消息存储到物理硬盘和查询消息。
- <font color=red>HA Service：</font>高可用服务，负责通过Master 和Slaver 之间的数据同步。
- <font color=red>Index Service：</font>索引服务，根据用户指定的业务Key 进行快速查询消息。

> Rocket MQ 工作流程：

1. 启动NameServer， 它就开始监听端口，等待Broker 、Consumer、Producer的连接。
2. 启动Broker，Broker 会和所有的NameServer 建立一个长连接，然后每30s 向NameServer 发送一次心跳包。
3. 创建Topic，创建Topic 可以指定该Topic 需要存放在那些Broker 上，创建Topic 后也会将Topic 与 Broker 的关系写到NameServer 中
4. Producer 发送消息，启动时先和NameServer 集群中的一个建立长连接(先随机，然后轮询去连接)，然后从NameServer 中获取路信息，即当前发送的Topic 消息对应的Broker 地址(ip+Port)及Queue的映射关系。然后根据算法策略选择一个Queue，然后就向Broker 发送消息。在回去路由信息后，Productor 会把路由信息缓存到本地，然后每30s 会从NameServer 更新路由信息。
5. Consumer 消费消息，与Productor 类似，先和一台NameServer 建立长连接，然后根据订阅的Topic 去获取路由信息，然后通过算法策略获取到路由信息对应的queue，然后与对应的Broker 建立长连接并开始消费消息。Consumer 在获取到路由信息后，也会每30s 向NameServer 拉取一次最新的路由信息并且也会发一次心跳包确保Broker 是否在线。

### 1.6、Rocket MQ 官方约束

| 参数                         | 建议                                                         | 说明                                                         |
| ---------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Topic名称                    | 1-64个字符，字母、数字、下划线、短线、%，不能使用保留字      | 特殊字符可能会导致消息收发异常                               |
| ConsumerGroup名称            | ==和Topic类似==                                              |                                                              |
| ACL Credentials              | AK、SK、Token，仅支持字母、数字，不超过1024个字符            |                                                              |
| 请求超时时间                 | 客户端行为，默认3000毫秒                                     |                                                              |
| 消息大小                     | 不超过4MB                                                    | 消息传输应尽量压缩和控制负载大小，避免超大文件传输。若消息大小不满足限制要求，==可以尝试分割消息或使用OSS存储，用消息传输URL。== |
| 消息自定义属性               | 所有可见字符，属性的Key和Value总长度不超过16 KB，不允许使用保留属性作为自定义属性的Key。 保留属性Key |                                                              |
| MessageGroup                 | 所有可见字符。 长度建议：1~64字节。                          | 是顺序消息的分组标识。一般设置为需要保证顺序的一组消息标识，例如订单ID、用户ID等。 |
| 消息重发次数                 | 默认3次，无限制                                              | ==是客户端SDK内置的重试策略==，对应用不可见，建议取值不要过大。==如果消息达到最大重试次数后还未发送成功，建议业务侧做好兜底处理，保证消息可靠性。== |
| 消息消费重试次数             | 默认16次，无限制                                             | 重试次数过大容易造成系统压力过量增加。                       |
| 事务异常检查隔离             | 默认60秒                                                     | 事务异常检查间隔指的是，半事务消息因系统重启或异常情况导致没有提交，生产者客户端会按照该间隔时间进行事务状态回查。 间隔时长不建议设置过短，否则频繁的回查调用会影响系统性能。 |
| 半事务消息第一次回查时间     | 默认值：取值等于[事务异常检查间隔] * 最大限制：不超过1小时。 |                                                              |
| 半事务消息最大超时时长       | 默认值：4小时。 ==* 取值范围：不支持自定义修改==             | 生产者客户端会按照事务异常检查间隔时间进行回查，若超过半事务消息超时时长后没有返回结果，半事务消息将会被强制回滚。 您可以通过监控该指标避免异常事务。 |
| PushConsumer本地缓存         | 最大数量：1024条<br />最大缓存：64MB                         | 消费者类型为PushConsumer时，为提高消费者吞吐量和性能，客户端会在SDK本地缓存部分消息。缓存的消息的数量和大小应设置在系统内存允许的范围内。 |
| PushConsumer重试间隔时长     | *非顺序性投递：间隔时间阶梯变化，具体取值，请参见PushConsumer消费重试策略。<br/>*顺序性投递：3000毫秒。 |                                                              |
| PushConsumer消费并发度       | 默认值：20个线程。                                           |                                                              |
| 获取消息最大批次             | 默认值：32条。                                               | 消费者从服务端获取消息时，一次获取到最大消息条数。建议按照实际业务设置合理的参数值，一次获取消息数量过大容易在消费失败时造成大批量消息重复。 |
| SimpleConsumer最大不可见时间 | 默认值：用户必填参数，无默认值。<br/>取值范围建议：最小10秒；最大12小时。 | 消费不可见时间指的是消息处理+失败后重试间隔的总时长，建议设置时取值比实际需要耗费的时间稍微长一些。 |

## 2、Rocket MQ 安装

[下载地址:https://www.apache.org/dyn/closer.cgi?path=rocketmq/5.0.0/rocketmq-all-5.0.0-bin-release.zip](https://www.apache.org/dyn/closer.cgi?path=rocketmq/5.0.0/rocketmq-all-5.0.0-bin-release.zip)

![image-20221206193147515](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20221206193147515.png)

上传下载好的压缩包到服务器上并解压。

### 2.1、单机版安装

> 修改Rocket MQ 初始启动内存：默认它初始启动内存是4G，可以改下一点。

- 修改`runserver.sh`

![image-20221206194424741](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20221206194424741.png)

- 修改`broker.sh`

![image-20221206194534759](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20221206194534759.png)

> 启动命令：

~~~bash
# 启动NameServer，默认端口9876
nohup sh bin/mqnamesrv &
# 查看日志
 tail -f ~/logs/rocketmqlogs/namesrv.log
 
# 启动Broker和proxy，localhost:9876是NameServer的ip+port
# 5.x 版本下我们建议使用 Local 模式部署，即 Broker 和 Proxy 同进程部署。5.x 版本也支持 Broker 和 Proxy 分离部署以实现更灵活的集群能力。
nohup sh bin/mqbroker -n localhost:9876 --enable-proxy &
# 查看日志
tail -f ~/logs/rocketmqlogs/broker_default.log 
~~~

> 测试发送和接收消息：

~~~bash
# 设置一个环境变量，以便获取到NameServer的地址
export NAMESRV_ADDR=localhost:9876

# 测试发送消息
sh bin/tools.sh org.apache.rocketmq.example.quickstart.Producer

# 测试消费消息
sh bin/tools.sh org.apache.rocketmq.example.quickstart.Consumer
~~~

> SDK 测试收发消息：

1. 引入依赖：

```xml
<dependency>
    <groupId>org.apache.rocketmq</groupId>
    <artifactId>rocketmq-client-java</artifactId>
    <version>5.0.0</version>
</dependency>
```

2. 生产消息：

~~~java
private static String IP = "192.168.32.130:8081";
private static String Test_Topic = "TopicTest";

// 发送消息
public static void send() throws ClientException {
    ClientServiceProvider provider = ClientServiceProvider.loadService();
    ClientConfigurationBuilder builder = ClientConfiguration.newBuilder().setEndpoints(IP);
    ClientConfiguration configuration = builder.build();

    // 配置生产者
    Producer producer = provider.newProducerBuilder()
        .setTopics(Test_Topic)
        .setClientConfiguration(configuration)
        .build();

    // 配置发送的消息
    Message message = provider.newMessageBuilder()
        .setTopic(Test_Topic)
        .setKeys(UUID.randomUUID().toString())
        .setTag("test")
        .setBody("Hello RocketMQ".getBytes())
        .build();
    // 发送消息
    try {
        SendReceipt result = producer.send(message);
        System.out.println(result);
    } catch (ClientException e) {
        e.printStackTrace();
    }
}
~~~

3. 消费消息：

```java
private static String IP = "192.168.32.130:8081";
private static String Test_Topic = "TopicTest";

// 消费消息
public static void consumer() throws ClientException {
    ClientServiceProvider provider = ClientServiceProvider.loadService();
    ClientConfigurationBuilder builder = ClientConfiguration.newBuilder().setEndpoints(IP);
    ClientConfiguration configuration = builder.build();

    // 设置消费者组
    String tag = "*";
    // 配置消费者组订阅所有tag的消息
    FilterExpression filterExpression = new FilterExpression(tag, FilterExpressionType.TAG);
    //为消费者指定所属的消费者分组，Group需要提前创建。
    String consumerGroup = "test_consumer";
    provider.newPushConsumerBuilder()
            .setClientConfiguration(configuration)
            .setConsumerGroup(consumerGroup)
            .setSubscriptionExpressions(Collections.singletonMap(Test_Topic, filterExpression))
            .setMessageListener(messageView -> {
                //处理消息并返回消费结果。
                System.out.println("Consume message!!" + messageView);
                return ConsumeResult.SUCCESS;
            })
            .build();
}
```

> 关闭服务：

~~~bash
# 关闭broker
sh bin/mqshutdown broker
# 关闭NameServer  
sh bin/mqshutdown namesrv
~~~





































### 2.2、集群版安装

#### 2.2.1、集群结构

![image-20221206224400989](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20221206224400989.png)

​	因为NameServer 集群之间是无状态连接的，所以不会去搭建NameSever 集群。一般搭建的集群是Broker 集群，一般一个Master 有一个Slaver 模式。

> 刷盘策略和数据复制：

![image-20221206221318603](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20221206221318603.png)

<font color=red>刷盘：</font>指消息发送到broker 时，数据冲内存同步到磁盘中。

- 同步刷盘：请求数据到内存后，要等数据成功写到磁盘后才会返回消息写入成功。
- 异步刷盘：请求数据到内存后，直接返回消息写入成功，后面写入磁盘操作后续继续做。

<font color=red>复制：</font>指消息发送到Master 上，然后需要同步数据到Slaver 上，这个过程叫复制。

- 同步复制：消息写入到Master 中，需要等待数据成功同步到Slaver 上才会返回消息写入成功。
- 异步复制：消息写入到Master 中，直接返回消息写入成功，不会等待Slaver 数据同步成功。

==同步方式的数据安全性较高，但是有写入延迟，吞吐量会有影响。异步方式就是去掉了写入延迟，一般是把消息写入到PageCache，数量达到一定时就进行同步，这样可能会出现数据错误。==

> Broker 集群模式：

<font color=red>单Master：</font>只有一个Broker 主机，存在单点故障问题，一般在测试环境下使用。

<font color=red>多Master：</font>多台Broker 主机，它们都是Master ，没有Save。同一个Topic 主题的队列会平均分配到每一个Broker上。

- 优点：配置简单，==在磁盘阵列为RAID10时==，单台的Broker 宕机不可使用，但是其它主机也可以写入消息，宕机的机器消息不会丢失(同步刷盘不会丢失数据，但是一部刷盘可能会丢失部分数据)。
- 缺点：在单台Broker 宕机后，消费者不能进行读取宕机主机未被消费的消息，直到宕机主机恢复。

<font color=red>多Master多Slave：</font>Broker 集群中有多个Master，每一个Master 都有多个Slave（在磁盘阵列为RAID10时只需配置一个Slave）。Master 与Slave 之间是主备关系，Master 负责读写，而Slave 负责备份以及当主Master 宕机后自动切换成主Master。

- 异步复制：Master 与Slave 之间同步数据时可以采取异步方式，但是异步同步有延迟(毫秒)，这有可能在主Master 宕机后同步数据可能会丢失。
- 同步复制：Master 与Slave 同步数据时只有当Slave 同步成功才会返回给Master，然后Master 才会返回结果，这样消息的返回时间就会边长。这个有个问题，当Master 宕机后，Slave 不会自动切换成Master。

#### 2.2.2、集群搭建

> <font color=red>最佳实践：</font>在生产环境中，一般使用多台Master + 多台Slave + RAID10 磁盘阵列去搭建MQ 集群，这样能保证数据不丢失和高可用。下面使用两台机器搭建(一台：Master1+Slave2，一台：Master2+Slave1)。

1. 修改配置文件：修改安装名录下的`conf/`的文件，修改2主2从的异步复制配置。

![image-20221211143617826](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20221211143617826.png)

~~~properties
# 默认的集群组
brokerClusterName=DefaultCluster
# 当前这个Master 组的broker名称，这个组下的所有broker都是这个名称
brokerName=broker-a
# broker的id，0是master、1是slave
brokerId=0
# 每天凌晨4点删除过期消息存放的文件
deleteWhen=04
# 未发生更新的消息存放文件保留时间为48小时
fileReservedTime=48
# 复制策略是异步复制
brokerRole=ASYNC_MASTER
# 刷盘策略也是异步刷盘
flushDiskType=ASYNC_FLUSH
# 指定的NameServe 地址，多个使用英文逗号隔开
nameservAddr=192.168.32.130:9876;192.168.32.131:9867
# 监听端口，提供给consumer和producer，默认是10911
listenPort=10911
# 消息存放的相关路径，如果在一台主机上模拟多台MQ需要修改对应的文件，默认是在~/store
storePathRootDir=~/store-s
storePathCommitLog=~/store-s/commitlog
storePathConsumeQueue=~/store-s/consumequeue
storePathIndex=~/store-s/index
storeCheckpoint=~/store-s/checkpoint
abortFile=~/store-s/abort
~~~

2. 启动两台机器的NameServe 服务

~~~bash
nohup sh bin/mqnamesrv &
~~~

![image-20221211150119495](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20221211150119495.png)

3. 启动两台Master

~~~bash
# 需要指定启动对应的配置文件，-c就是指定配置文件
nohup sh bin/mqbroker -c conf/2m-2s-async/broker-xx.properties &
~~~

4. 启动两台Slave

~~~bash
# 需要指定启动对应的配置文件，-c就是指定配置文件
nohup sh bin/mqbroker -c conf/2m-2s-async/broker-xx.properties &
~~~

















































































































