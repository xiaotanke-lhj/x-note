# Netty 学习笔记

> Netty：一个异步、基于事件驱动的网络应用框架，用于快速开发高性能、高可用的网络 I/O 程序。

- 针对于 TCP 协议下，面向 Client 端的高并发应用。
- 本质是一个 NIO 框架，适用于服务器通讯相关的场景。
- 例如用于 RPC 的底层框架、游戏服务器之间通信组件、大数据领域。

## 1、BIO、NIO、AIO 

> I/O 模型：在 Java 中支持三种 I/O 模型，就是 BIO、NIO、AIO。

- BIO：==同步阻塞== IO 模型，服务器实现模式是一个连接就会创建一个线程，客户端有连接请求服务器就会创建一个线程去处理这个请求。
- NIO：==同步非阻塞== IO 模型，服务器实现模式是服务器一个线程可以处理多个连接请求，客户端发送连接请求会注册到多路复用器上，然后多路复用器会轮询到 IO 请求上进行处理。
- AIO：==异步非阻塞== IO 模型，引入异步通道采用 Proactor 模式，由底层操作系统完成后才会通知服务器启动线程去处理。

### 1.1、BIO 模型

![image-20230707101549447](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20230707101549447.png)

> 工作机制：

- 服务器端会创建 ServerSocket，客户端启动 Socket 与服务器进行通信，每个客户端的连接请求都会在服务器端创建新线程去处理。
- 客户端发出连接请求后，先咨询服务器是否有线程响应，如果没有就会等待或者被拒绝。
- 如果服务器有响应，客户端线程会等待连接请求结束后继续执行。

> 简单示例：

~~~java
public static void main(String[] args) throws Exception {
    // 采用多线程去处理客户端请求
    ExecutorService threadPool = Executors.newFixedThreadPool(10);
    // 创建一个服务器socket
    ServerSocket serverSocket = new ServerSocket(6666);
    System.out.println("服务器已经开启了，等待连接。。。。。");
    // 循环等待客户端端连接
    while (true){
        System.out.println("当前线程 -> "+Thread.currentThread().getName());
        final Socket socket = serverSocket.accept();
        System.out.println("连接到一个客户端----");
        // 使用线程池对客户端进行请求
        threadPool.execute(new Runnable() {
            public void run() {
                System.out.println("正在处理客户端请求。。。。");
                try {
                    Thread.sleep(TimeUnit.SECONDS.toSeconds(10));
                }catch (Exception e){
                    e.printStackTrace();
                }
                System.out.println("处理客户端线程 -> "+Thread.currentThread().getName());
                try(
                    InputStream inputStream = socket.getInputStream();
                    OutputStream outputStream = socket.getOutputStream()

                ) {
                    final byte[] bytes = new byte[1024];
                    // 循环的读取客户端发送的数据
                    while (true) {
                        System.out.println("read....");
                        int read = inputStream.read(bytes);
                        if (read != -1) {
                            System.out.println(new String(bytes, 0, read));//输出客户端发送的数据
                        } else {
                            break;
                        }
                        // 返回客户端信息
                        outputStream.write("你好呀客户端".getBytes(StandardCharsets.UTF_8));
                    }
                }catch (IOException e) {
                    e.printStackTrace();
                }
                System.out.println("客户端请求处理完成。。。。");
            }
        });
    }
}
~~~

> 注意：

- BIO 模型主要适用于连接数少且固定架构，这种需要服务器性能较高。
- 每个请求都会创建一个线程去处理，如果很多请求就会造成系统资源占用大。
- 连接建立后，如果当前线程没有数据可读，当前线程就会阻塞在 read 上，造成资源浪费。

### 1.2、NIO 模型

> NIO：它有三大核心模块，Channel(通道)、Buffer(缓冲区)、Selector(选择器)。它是面向缓冲区编程，数据会读取到一个稍后处理的缓冲区，需要时可以从缓冲区中前后移动。非阻塞模型是一个处理线程从某通道发出请求或读取数据，它只能获取到目前可用数据，如果没有可用数据就什么都不做，而不是像 BIO 保持线程阻塞，这个线程就可以去处理其他客户端请求。

<img src="https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20230707110717939.png" alt="image-20230707110717939" style="zoom:67%;" />

==注意：每个 Channel 都会对应一个 Buffer，每个 Selector 都会对应一个处理线程，而每个 Selector 可以切换到不同的 Channel 管道，具体切换到哪个 Channel 管道是由事件来决定的，例如管道连接事件、数据到达事件等等。==

#### 1.2.1、Buffer  缓冲区

> 本质是一个可读可写数据的内存块，缓冲区内置了一些机制能够跟踪、记录缓存区状态变化情况。Channel 从文件、网络中读取、写入数据都需要经过缓存区。

![image-20230707113629891](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20230707113629891.png)

> Buffer 类：是一个顶级抽象类，下面有很多实现类，例如：ByteBuffer、ShortBuffer、CharBuffer、IntBuffer、LongBuffer、DoubleBuffer、FloatBuffer。

~~~java
// Buffer 方法列表
/**
 * int capacity()：返回缓冲区的最大容量
 * int position()：返回下一个读写元素的下标，传一个参数可以设置新下标
 * int limit()：返回缓冲区当前终点，不能去极限终点进行操作，可以传入参数设置新极限
 * buffer mark()：在缓冲区的位置上设置标记，下一次获取从标记位开始
 * buffer reset()：将缓冲区位置重置为以前标记位
 * buffer clear()：清除缓冲区所有标记位，但是数据不会清除
 * buffer flip()：反转缓冲区数据
 * buffer rewind()：重绕缓冲区，相当于将位置重置
 * int remaining()：返回当前位置与极限之间的元素个数
 * boolean hasRemaining()：当前位置与极限之间是否还存在元素
 * boolean isReadOnly()：返回当前缓冲区是否只读
*/
~~~

~~~java
// 其它实现类部分方法
/**
 * buffer allocate(int)：创建指定初始容量的缓冲区
 * E get()：获取当前位置上元素，get之后position就会+1，也可以指定获取元素下标
 * buffer put(E)：向缓冲区写入元素，写入以后position+1，也可以指定写入的位置
 */
~~~

#### 1.2.2、Channel 管道

> 类似于数据流，但是流只能同时读或者同时写，而管道可以同时进行读写，可以实现异步读写数据。Channel 是个顶级接口，下面的实现类有FileChannel、DatagramChannel、ServerSocketChannel、SocketChannel。

~~~java
public static void main(String[] args) throws Exception {
    // 将数据通过管道写入到文件中
    FileOutputStream outputStream = new FileOutputStream("test.txt",true);
    // 获取管道
    FileChannel channel = outputStream.getChannel();
    String data = "hello，world!";
    // 将数据放入缓冲区
    ByteBuffer buffer = ByteBuffer.wrap(data.getBytes());
    // 使用管道将缓冲区数据写入文件
    channel.write(buffer);


    // 使用通道从文件中读取数据
    FileInputStream inputStream = new FileInputStream("test.txt");
    FileChannel channel1 = inputStream.getChannel();
    // 创建一个缓冲区来接受数据
    ByteBuffer buffer1 = ByteBuffer.allocate(1024);
    while (channel1.read(buffer1) != -1){
        System.out.println(new String(buffer1.array()));
    }

    // 拷贝文件
    FileInputStream stream1 = new FileInputStream("test.jpg");
    FileOutputStream stream2 = new FileOutputStream("test1.jpg");
    // 获取管道
    FileChannel fileChannel1 = stream1.getChannel();
    FileChannel fileChannel2 = stream2.getChannel();

    // 拷贝文件
    fileChannel2.transferFrom(fileChannel1, 0, fileChannel1.size());
}
~~~

#### 1.2.3、Selector 选择器(多路复用器)

> NIO 模型可以使用一个线程去处理多个客户端连接，就是使用到 Selector 选择器，它能够检测多个注册通道是否存在事件发生，可以针对每个事件进行对应的处理。

- 只有在连接真正有读写事件发生时，才会进行读写，并没有为每个连接创建一个线程。
- 避免了多线程之间的上下文切换导致系统开销。
- 线程从客户端 Socket 通道读写数据时，如果没有数据可用，这个线程就可以去处理其它客户端通道数据。

![image-20230707144208356](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20230707144208356.png)

~~~java
// Selector 部分方法列表
/**
 * int select()：阻塞，可以设置阻塞时间，不设置就一直阻塞，单位毫秒
 * selector wakeup()：唤醒当前选择器
 * int selectNow()：不阻塞，立即返回
 * Set<SelectorKey> selectedKeys()：获取所有的selectedKeys
*/
~~~

#### 1.2.4、NIO 非阻塞网络编程

![image-20230707152248403](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20230707152248403.png)

> 执行流程：

- 当客户端连接时，会通过 ServerSocketChannel 得到 SocketChannel。
- Selector 会监听 select 方法，并返回发生事件的通道个数。
- 将发生事件的管道注册到 Selector 上，一个 Selector 可以注册多个管道，注册之后返回一个 SelectionKey 和 Selector 关联。
- 然后通过 SelectionKey 反向获取 SocketChannel，进行业务处理。

> SelectionKey：表示 Selector 与网络通道的注册关系。

- OP_ACCEPT：值为16，表示有新的网络连接。
- OP_CONNECT：值为8，表示连接已经建立。
- OP_READ：值为1，表示读操作。
- OP_WRITE：值为4，表示写操作。

~~~java
// selectionKey 部分方法
/**
 * Selector selector()：获取关联的选择器
 * SelectableChannel channel()：获取关联到的管道
 * SelectionKey interestOps()：重新设置与选择器的注册关系
 * boolean isAcceptable()：是否可以连接
 * boolean isReadable()：是否可读
 * boolean isWritable()：是否可写
*/
~~~

> ServerSocketChannel：在服务器端监听客户端 Socket 连接。

~~~java
/**
 * ServerSocketChannel bind()：绑定服务器端端口号
 * ServerSocketChannel configureBlocking()：设置是否阻塞，true阻塞，false非阻塞
 * SocketChannel accept()：获取一个客户端连接
 * SelectionKey register(selector, ops)：注册进选择器并监听事件
 */
~~~

> SocketChannel：具体负责进行读写操作，把缓冲区数据写入通道或者将通道数据读取到缓冲区。

~~~java
/**
 * SocketChannel bind()：连接服务器的地址和端口号
 * SocketChannel configureBlocking()：设置是否阻塞，true阻塞，false非阻塞
 * boolean connect()：获取一个客户端连接
 * boolean finishConnect()：如果上面方法连接失败，则执行这个方法完成连接操作
 * int write(buffer)：把buffer数据写入管道
 * int read(buffer)：从管道中读取数据到buffer
 * SelectionKey register(selector, ops)：注册进选择器并监听事件
*/
~~~

> 多人聊天系统：

- 服务器端：

```java
public class ChatServer {

    private final int PORT = 9000;
    private Selector selector;
    private ServerSocketChannel serverSocketChannel;

    public ChatServer(){
        try {
            selector = Selector.open();
            serverSocketChannel = ServerSocketChannel.open();
            serverSocketChannel.bind(new InetSocketAddress(PORT));
            serverSocketChannel.configureBlocking(false);
            serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
            System.out.println("服务器启动成功！！！");
        }catch (Exception e){
            System.out.println("服务器启动失败！！！！");
        }
    }

    // 监听
    public void listen(){
        try {
            while (true){
                // 事件个数
                int select = selector.select();
                if (select > 0){
                    // 触发事件的所有key
                    Set<SelectionKey> keys = selector.selectedKeys();
                    Iterator<SelectionKey> iterator = keys.iterator();

                    while (iterator.hasNext()){
                        SelectionKey key = iterator.next();
                        if (key.isAcceptable()){ // 如果是连接事件
                            SocketChannel accept = serverSocketChannel.accept();
                            // 将连接通道注册到选择器中
                            accept.configureBlocking(false);
                            accept.register(selector, SelectionKey.OP_READ);
                            System.out.println(accept.getRemoteAddress() + " 上线了 ");
                        }
                        if (key.isReadable()) { // 如果是可读事件,读取数据并发送给其它客户端
                            SocketChannel channel = (SocketChannel) key.channel();
                            ByteBuffer buffer = ByteBuffer.allocate(1024);
                            int i = channel.read(buffer);
                            if (i>0){
                                System.out.println("获取到客户端消息 => " + new String(buffer.array()));
                                // 将消息发送给其它客户端
                                for (SelectionKey temp : selector.keys()) {
                                    Channel tempChannel = temp.channel();
                                    // 排除自己
                                    if (tempChannel instanceof SocketChannel && tempChannel != channel){
                                        ByteBuffer wrap = ByteBuffer.wrap(new String(buffer.array()).getBytes());
                                        ((SocketChannel) tempChannel).write(wrap);
                                    }
                                }
                            }
                        }
                        // 移除key
                        iterator.remove();

                    }
                }else {
                    System.out.println("等待客户端事件发生！！！");
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {

        }
    }
}
```

- 客户端：

```java
public class ChatClient {

    private final int PORT = 9000;
    private final String IP = "127.0.0.1";
    private Selector selector;
    private SocketChannel socketChannel;
    private String username;

    public ChatClient(){
        try {
            selector = Selector.open();
            socketChannel = SocketChannel.open();
            socketChannel.connect(new InetSocketAddress(IP,PORT));
            socketChannel.configureBlocking(false);
            socketChannel.register(selector, SelectionKey.OP_READ);
            username = socketChannel.getLocalAddress().toString().substring(1);
            System.out.println("客户端启动成功 -> " + username);
        }catch (Exception e){
            e.printStackTrace();
            System.out.println("客户端启动失败");
        }
    }

    public void sendMessage(String data) throws IOException {
        data  = username + "说：" + data;
        System.out.println(data);
        ByteBuffer wrap = ByteBuffer.wrap(data.getBytes());
        System.out.println(wrap.toString());
        socketChannel.write(wrap);
    }

    public void readMessage() throws IOException {
        int select = selector.select();
        if (select > 0){
            Iterator<SelectionKey> iterator = selector.selectedKeys().iterator();
            while (iterator.hasNext()){
                SelectionKey key = iterator.next();
                if (key.isReadable()){
                    SocketChannel channel = (SocketChannel) key.channel();
                    ByteBuffer buffer = ByteBuffer.allocate(1);

                    channel.read(buffer);
                    System.out.println(new String(buffer.array()));
                }
               iterator.remove();
            }
        }else {
            System.out.println("没有信息管道");
        }
    }
}
```

#### 1.2.5、NIO 之零拷贝

> 传统模式 IO：

![image-20230710100627632](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20230710100627632.png)

> mmap：通过内存映射将文件映射到内核缓冲区，用户空间也可以共享内核空间的数据。

![image-20230710100737261](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20230710100737261.png)

> sendFile：数据不用经过用户空间，直接从内核缓冲区进入到 SocketBuffer。

![image-20230710101038412](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20230710101038412.png)

<font color=red>注意：</font>

- mmap 适合小数据量读写，sendFile 适合大文件传输。
- mmap 需要4次上下文切换，3次数据拷贝；sendFile 需要3次上下文切换，最少2次数据拷贝。
- sendFile 可以利用 DMA 方式来减少 CPU 拷贝，而 mmap 必须将数据拷贝到 SocketBuffer 中。

## 2、Netty 高性能架构设计

- Netty 是对原 NIO 网络编程进行一个封装，使用原生的 NIO 编程比较繁琐并且需要掌握其它额外技能，开发难度较大。
- Netty 具有高性能、吞吐量高、低延迟、资源消耗较少等功能。
- 统一的 API 文档，详细的用户指南。

### 2.1、传统阻塞 IO 模型

- 每个客户端建立连接时需要独立线程完成数据处理，当并发量很大时就会创建很多线程占用很大系统资源。
- 连接创建后，如果当前线程暂时没有数据操作，那么这个线程就会阻塞，造成资源浪费。

<img src="https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20230710103616765.png" alt="image-20230710103616765" style="zoom:67%;" />

### 2.2、Reactor  模式

- 多个连接公用一个阻塞对象，程序也只需要德在一个阻塞对象等待，避免了传统 IO 中出现所有连接阻塞等待现象。
- 当某个连接有新数据处理时，操作系统通知程序，线程从阻塞状态返回进行业务处理。
- 基于线程池复用线程资源，不必为每个连接创建处理线程，某个连接处理完以后当前线程可以给其它连接进行处理。

![image-20230710105434590](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20230710105434590.png)

<img src="https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20230710105642325.png" alt="image-20230710105642325" style="zoom:67%;" />

- Reactor 模式，多个客户端同时传递给服务端的处理器（基于事件驱动）。
- 服务端处理器将传入的多个请求将它们分发到对应的处理线程上。
- Reactor 使用 IO 复用监听事件，收到连接事件、读事件、写事件后又分发给对应的线程，这就是网络高并发处理关键。

### 2.3、单 Reactor 单线程

<img src="https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20230710111001703.png" alt="image-20230710111001703" style="zoom:67%;" />

> 执行流程：

- Reactor 对象通过 Select 监控客户端请求事件，收到事件后就 通过 dispatch 分发到对应处理对象上。
- 如果是连接请求就通过 Acceptor 对象处理处理连接请求，然后创建一个 Handler 对象处理连接完成后的业务。
- 如果不是连接请求，直接通过 Reactor 分发调用连接对应的 Handler 来处理，然后 send 数据给客户端。

<font color=red>优点：</font>

- 模型简单，没有多线程、进程通信、竞争问题，全部在一个线程中完成。

<font color=red>缺点：</font>

- 只有一个线程，无法同时处理多个连接事件请求，无法发挥多核 CPU 性能，性能瓶颈差。
- 当线程意外终止，就会进入死循环，会导致整个系统服务不可用。

### 2.4、单 Reactor 多线程

<img src="https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20230710114508869.png" alt="image-20230710114508869" style="zoom:67%;" />

> 执行流程：

- Reactor 对象通过 Select 监听客户端事件，收到事件通过 Dispatch 分发到对应 Handler。
- 如果是连接请求，则通过 Acceptor 处理连接请求然后创建一个 Handler 对象处理完成连接后的各种事件。
- 如果不是连接请求，则由 Reactor 分发给对应的 handler 对象进行处理。
- handler 只负责响应事件，read 读取数据后，后分发给线程池中的线程进行业务处理。
- 线程池中线程处理完后，将结果返回给 handler，然后通过 send 将结果发送给客户端。

<font color=red>优点：</font>

- 可以充分利用 CPU 多核处理能力，提高高并发性能。

<font color=red>缺点：</font>

- 多线程处理业务逻辑对于数据共享和访问比较复杂。
- Reactor 线程处理所有事件监听和响应，在 Reactor 单线程运行下，高并发场景可能出现性能瓶颈。

### 2.5、主从 Reactor 多线程

<img src="https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20230710134034246.png" alt="image-20230710134034246" style="zoom:67%;" />

> 执行流程：

- Reactor 主线程 MainReactor 对象监听连接事件，然后通过 Acceptor 处理连接事件。
- 当 Acceptor 处理连接事件后，MainReactor 将连接分配给 SubReactor 多个子程序进行处理。
- subReactor 将连接加入到队列中进行监听，并创建 handler 进行处理各种事件。
- 客户端出现事件需要处理，handler 就 read 读取数据，然后分发给线程池中某个线程进行处理并返回结果。
- Reactor 主线程可以对应多个 subReactor 子线程，提高高并发性能。

<font color=red>优点：</font>

- 父线程与子线程之间数据交互职责明确，父线程只需要接受新连接，子线程会完成后续业务处理。
- 高并发性能高，吞吐量大，子线程可以有多个也不需要返回数据。

<font color=red>缺点：</font>

- 编程复杂度高。

> 小总结：单 Reactor 单线程类似于只有一个人，担任前台和服务员；单 Reactor 多线程类似于有一个前台和多个服务员；主从 Reactor 多线程类似于有多个前台多个服务员。

### 2.6、Netty 模型

> Netty 模型：主要基于主从 Reactor 多线程模型做了一点改进，其中主从 Reactor 多线程模型有多个 Reactor。

![image-20230710141334108](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20230710141334108.png)

- BossGroup 线程维护 Selector，只关注处理连接事件 Accept。
- 当接收到 Accept 事件，获取到对应的 SocketChannel，封装成 NIOSocketChannel 并注册到 Worker 线程中并维护。
- 当 Worker 线程监听到维护的管道中发生了事件后，就交给对应的 handler 进行处理，handler 然后在交给线程池中线程进行处理。

### 2.7、异步模型

> 异步：异步调用时是不能立刻获取到执行的结果，实际上在处理完这个调用组件完成之后，通过状态、通知和回调来通知调用者。

​		Netty 中的 IO 操作是异步的，包括Bind、Write、Connect等会返回一个 ChannelFuture，通过 Future-Listener 机制，用户可以方便主动获取或者通过通知获取 IO 执行的结果。

> Future-Listener 机制：

1. 当 Future 创建时，处于非完成状态，调用者可以通过返回的 ChannelFuture 来获取操作执行的状态，也可以注册监听函数来执行完成后的状态。

- isDone()：判断当前操作是否完成。
- isSuccess()：判断已完成的当前操作是否成功。
- getCause()：获取已完成的当前操作失败的原因。
- isCancelled()：判断已完成的当前操作是否被取消。
- addListener()：注册监听器，当前操作已完成就会通知指定的监听器去执行对应操作。

```java
// 绑定一个端口并且同步, 生成了一个 ChannelFuture 对象
// 启动服务器(并绑定端口)
ChannelFuture cf = bootstrap.bind(9000).sync();

// 给cf注册监听器，当bind异步执行完成后，通知指定监听器执行
cf.addListener(new ChannelFutureListener() {
    @Override
    public void operationComplete(ChannelFuture future) throws Exception {
        if (future.isSuccess()) {
            System.out.println("监听端口 9000 成功");
        } else {
            System.out.println("监听端口 9000 失败");
        }
    }
});
```

## 3、Netty 核心组件

### 3.1、Bootstrap、ServerBootStrap

> Bootstrap：引导，一个 Netty 应用通常由一个 Bootstrap 开始，主要是配置整个 netty 程序串联各个组件，Bootstrap 类是客户端程序的启动引导类，ServerBootstrap 是服务端的引导类。

~~~java
/**
 * group()：设置服务端或者客户端对应 EventLoop 组。
 * channel(Channel.class)：用于设置管道的实现类
 * option(ChannelOptionl常量, value)：用来给 Channel 添加配置
 * childOption(ChannelOptionl常量, value)：用来给接收到的通道添加配置
 * childHandler(ChannelHandler)：该方法用来设置业务处理类（自定义的handler）
 * ChannelFuture bind(int inetPort)：用于服务器绑定端口
 * ChannelFuture connect(String inetHost, int inetPort)：用于客户端连接服务器
*/
~~~

### 3.2、Future、ChannelFuture

> Netty 中所有 IO 操作都是异步的，不能立刻知道消息是否被正确执行，可以通过 Future、ChannelFuture 去获取消息执行情况，可以注册事件当操作执行成功或失败时自动触发注册的监听事件。

```java
/**
 * Channel channel()：返回当前正在进行 IO 的管道
 * ChannelFuture sync()：等待异步执行完成
 * void get()：阻塞等待执行完成
 * isDone()：判断当前操作是否完成。
 * isSuccess()：判断已完成的当前操作是否成功。
 * getCause()：获取已完成的当前操作失败的原因。
 * isCancelled()：判断已完成的当前操作是否被取消。
 * addListener()：注册监听器，当前操作已完成就会通知指定的监听器去执行对应操作。
 */
```

### 3.3、Channel、Selector

> Channel：netty 网络通信组件，能够执行网络 IO 操作，通过 Channel 可以获取网络连接通道状态、网络连接配置参数。它提供异步网络 IO 操作，调用立即返回 ChannelFuture 对象，通过注册监听器到 ChannelFuture 上，可以在 IO 操作成功、失败、取消时回调通知调用方。

- NioSocketChannel：异步的客户端 TCP Socket 连接。
- NioServerSocketChannel：异步服务器端 TCP Socket 连接。
- NioDatagramChannel：异步的 UDP 连接。
- NioSctpChannel：异步的客户端 Sctp 连接。
- NioSctpServerChannel：异步的 Sctp 服务端连接，这些通道覆盖了 UDP、TCP、网络IO、文件 IO。

> Selector：netty 基于 Selector 对象实现 IO 多路复用，通过 Selector 一个线程去监听多个连接 Channel 事件。

​		当向一个 Selector 中注册 Channel 后，Selector 内部机制就会自动不断轮询这些 Channel 是否已有 IO 事件(读、写、连接)，这样就使用一个线程高效管理了多个 Channel 管道。

### 3.4、ChannelHandler

> ChannelHandler：是一个顶级接口，处理 IO 事件或拦截 IO 操作，并将其转发给 ChannelPipeLine 的下一个处理器，它下面有对应的子接口和抽象类，可以实现或继承对应类来自定义处理器。

- ChannelInboundHandler：用于处理入站 IO 事件。
- ChannelOutboundHandler：用于处理出站 IO 事件。
- ChannelInboundHandlerAdapt：用于处理入站 IO 事件的适配器
- ChannelOutboundHandlerAdapt：用于处理出站 IO 事件的适配器

> 自定义处理器：通常需要继承 ChannelInboundHandlerAdapter，然后重写相应方法实现业务逻辑，

```java
public class DIYHandler extends ChannelInboundHandlerAdapter {
    // 通道就绪事件
    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        super.channelActive(ctx);
    }
    // 通道读取事件
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        super.channelRead(ctx, msg);
    }
    // 数据读取完成事件
    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        super.channelReadComplete(ctx);
    }
    // 出现异常事件
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        super.exceptionCaught(ctx, cause);
    }
}
```

### 3.5、Pipeline、ChannelPipeline

> ChannelPipeLine：是一个 handler 集合，它负责处理和拦截 inbound 和 outboud 的事件和操作，可以理解是保存 ChannelHandler 的双向链表，每一个 Channel 都有且仅有一个 ChannelPipeline 与之对应。

![image-20230710180223206](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20230710180223206.png)

​		一个 Channel 包含一个 ChannelPipeline，而 ChannelPipeline 中又维护了一个由 ChannelHandlerContext 组成的双向链表，每个ChannelHandlerContext  又关联一个 ChannelHandler。

```java
/**
 * addLast()：在处理链表的最后一个位置增加一个处理器
 * addFirst()：在处理链表的第一个位置增加一个处理器
 */
```









