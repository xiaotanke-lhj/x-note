# 1、编程规范

> 命名规范：

- 类名首字母大写、方法名首字母小写、属性名首字母小写、常量全部大写单词之间使用_分割、枚举全部大写。
- 方法名、参数名、成员变量、局部变量都统一使用 lowerCamelCase 风格，必须遵从驼峰形式。
- 类名、方法名、属性名不能使用汉语拼音，需要见名知意。
- 对于boolean类型属性不能以 is 开头。(在某些框架序列化时可能出现找不到属性)
- 包名小写，具有单数形式，对于复数形式可以使用类名复数。
- 入参以 DTO 结尾、出参以 VO 结尾、数据库实体类以 xxxDO，xxx为数据库表名。

> 编码规范：

- 避免通过一个类对象去使用静态方法或则静态属性，直接使用类名去调用。
- 使用 equals() 方法时需要确定前一个对象不能 NULL，正例：`"test".equals(object)`。
- 定义类属性时，针对基本数据类型时必须使用它的包装类，在局部变量中使用基本数据类型。(如果使用基本数据类型，数据库查询结果可能为null，会发生自动拆箱，可能出现NPE异常)
- 避免重复代码产生，对后期维护不方便，可能在后期修改代码忘记修改副本代码。

> 集合相关事项：

- 重写`equals()`方法后必须重写`hashCode()`方法。

==在某些集合框架中，元素的判断是否相同是根据元素的`equals()`和`hashCode()`。例如 Set 集合、Map 的 key 值判断。==

- ArrayList 的 subList() 结果不可强转成 ArrayList，因为这个方法返回的不是ArrayList，而是它的一个内部类 SubList，可以直接使用List去接收。

==使用了 subList() 方法后需要注意不能对源集合中元素个数进行修改，修改了源集合元素个数子集合的遍历、修改、增加均产生 ConcurrentModificationException 异常==

- 集合转数组，使用 list.toArray(T[] array)，不能使用它的无参方法去转换，只能是 Object[]类，若强转其它类型数组将出现 ClassCastException 错误。

~~~java
// 正确写法
List<Integer> list = Lists.newArrayList();
list.add(1);
list.add(2);
Integer[] array = new Integer[list.size()];
array = list.toArray(array);
~~~

- 数组转集合，使用 Arrays.asList() 将数组转换成集合，它返回的是 Arrays 类中的一个内部类，并没有实现对应的集合方法。
    - 不能对转换后的集合进行修改操作，增加、修改、删除都会报 UnsupportedOperationException 异常。
    - 如果对源数组修改元素，list 中的元素也会修改。

~~~bash
String[] array = new String[] {"a","b","c"};
List<String> list = Arrays.asList(array);
array[0] = "hello";
System.out.println(list.get(0)); // 输出hello
~~~

- 不要在增强 for 循环进行元素的增加、删除操作，因为在生成 Iterator 会维护一个 exceptedModCount 参数为修改元素次数，如果你在遍历过程中删除元素，List中 modCount 就会变化

~~~java
List<Integer> list = new ArrayList<>();
list.add(1);
list.add(2);

// 错误姿势：
for (Integer integer : list) {
    if (integer == 2){
        // 除了删除倒数第二个元素，其它删除都会报错
        list.remove(integer);
    }
}

// 正确姿势：
Iterator<Integer> it = list.iterator();
while(it.hasNext()){
    Integer temp = it.next();
    if(temp == 2){
        it.remove();
    }
}
~~~

- 遍历 Map 优雅方法：遍历 map 通常通过 key 去遍历，通过这个方法实际上是遍历了两次，一次 Set、一次 Map。

~~~bash
// 遍历map，只遍历了一次
Map<String, String> map = new HashMap<>();
map.put("a","1");
map.put("b","2");
for (Map.Entry<String, String> entry : map.entrySet()) {
    System.out.println("key=>"+entry.getKey());
    System.out.println("value=>"+entry.getValue());
}
~~~

![image-20230522173213082](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20230522173213082.png)

> 并发处理：

- 线程资源必须通过线程池提供，不允许在应用中自行显式创建线程。
- 线程池不允许使用 Executors 去创建，而是通过 ThreadPoolExecutor 的方式。
- 对多个资源、数据库表、对象同时加锁时，需要保持一致的加锁顺序，否则可能会造成死锁。

# 2、日志处理

> 异常处理：

- 不要捕获运行时异常，这些异常需要程序员预检查去避免。
- 使用 try-catch 捕获异常需要进行解决，不要不做任何处理。
- 在事务的代码块中存在 try-catch，出现了异常 catch 后如果需要回滚事务，请手动回滚。
- 不要再 finally 语句中使用 return 语句。
- 方法不能返回 NULL，避免调用者出现 NPE，如果需要返回 NULL 请在注释中写明。
- 在代码中是抛出异常还是封装错误返回信息。
    - 在自己系统中建议抛出异常。
    - 对于对外面暴露的 API 需要封装返回信息，应用之间的调用也需要封装放回信息。

> 日志处理：

- 应用中不可直接使用日志系统（Log4j、Logback）中的 API，而应依赖使用日志框架 SLF4J 中的 API，使用门面模式的日志框架。

~~~java
import org.slf4j.Logger; 
import org.slf4j.LoggerFactory;

private static final Logger logger = LoggerFactory.getLogger(Abc.class);
~~~

- 记录日志文件名称：appName_logType_logName.log
    - appName：应用名称
    - logType：日志类型
    - logName：日志名称
- 异常信息应该包括两类信息：案发现场信息和异常堆栈信息。如果不处理，那么往上抛。

~~~java
logger.error(各类参数或者对象 toString + "_" + e.getMessage(), e);
~~~

- 禁止输出大量无效日志，禁止保存 debug 日志，大量日志不能快速定位错误点。

# 3、 数据库约束

> 建表规范：

- 表名、字段名必须使用小写字母或数字；禁止出现数字开头，禁止两个下划线中间只出现数字。
- 禁用保留字，如 desc、range、match、delayed 等，请参考 MySQL 官方保留字。保留字在查询时可能出现问题。
- 唯一索引名为 uk_字段名；普通索引名则为 idx_字段名。
- 小数类型为 decimal，禁止使用 float 和 double。
- 表的命名最好是加上“业务名称_表的作用”。

> 索引约束：

- 业务上具有唯一特性的字段，即使是组合字段，也必须建成唯一索引。
- 超过三个表禁止 join。需要 join 的字段，数据类型保持绝对一致；多表关联查询时，保证被关联的字段需要有索引。
- 利用延迟关联或者子查询优化超多分页场景。

> SQL 语句约束：

- 禁止在循环中去查询数据库，这样会导致数据库连接过多，需要关联可以使用连接查询或则先查一张表，然后查另一张表在代码中进行关联。
- 不要使用 count(列名)或 count(常量)来替代 count(`*`，count(*)会统计值为 NULL 的行，而 count(列名)不会统计此列为 NULL 值的行
- 使用 ISNULL()来判断是否为 NULL 值。
- 不得使用外键与级联，一切外键概念必须在应用层解决。
- 禁止使用存储过程，存储过程难以调试和扩展，更没有移植性。
- 数据订正时，删除和修改记录时，要先 select，避免出现误删除，确认无误才能执行更新语句。
- 在表查询中，一律不要使用 * 作为查询的字段列表，需要哪些字段必须明确写明。
- xml 配置中参数注意使用：#{}，#param# 不要使用${} 此种方式容易出现 SQL 注入。
- 不允许直接拿 HashMap 与 Hashtable 作为查询结果集的输出。











