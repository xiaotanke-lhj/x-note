# kkfileview 安装文档

> kkfileview：kkFileView为文件文档在线预览解决方案，该项目使用流行的spring boot搭建，易上手和部署，基本支持主流办公文档的在线预览，如doc,docx,xls,xlsx,ppt,pptx,pdf,txt,zip,rar,图片,视频,音频等等。

## 1、下载 kkfileview 安装包

~~~bash
# 下载压缩包
wget https://kkfileview.keking.cn/kkFileView-4.0.0.tar.gz
# 解压
tar -zxvf kkFileView-4.0.0.tar.gz
~~~

## 2、下载 LibreOffice 插件

> 注意：kkfileview 会依赖 LibreOffice 插件，不然在启动时会报错。

~~~bash
# 下载安装包
wget https://kkfileview.keking.cn/LibreOffice_7.1.4_Linux_x86-64_rpm.tar.gz
# 解压安装包
tar -zxvf LibreOffice_7.1.4_Linux_x86-64_rpm.tar.gz
# 安装
yum install -y LibreOffice_7.1.4.2_Linux_x86-64_rpm/RPMS/*.rpm
# 检查是否安装成功，安装目录下使用下面命令会有版本号输出
./program/soffice --version
~~~

## 3、修改配置文件并启动

> application.properties：在 kkfileview  安装目录下的 config 目录下。

~~~properties
# 如果没有繁琐的要求只需要改一处地方，
#openoffice home路径，需要指定到插件的安装目录下
# 这只是我的安装目录
office.home = /op/LibreOffice_7.1 

# 默认端口8012
server.port = ${KK_SERVER_PORT:8012}
~~~

## 4、解决 LibreOffice  文件预览乱码问题

~~~bash
# 下载字体包
wget http://kkfileview.keking.cn/fonts.zip
# 解压至 /usr/share/fonts 目录下
uzip fonts.zip
# 依次执行 mkfontscale 、mkfontdir 、fc-cache 使字体生效
mkfontscale
mkfontdir
fc-cache
# 在fonts 目录下依次执行
chmod -R 755 *.TTF
chmod -R 755 *.ttf
chmod -R 755 *.ttc
source /etc/profile
~~~

## 5、启动 kkfileview 服务

~~~bash
# 在安装目录下的 bin 文件夹下执行
sh ./start.sh
# 查看启动日志
sh ./showlog.sh
~~~

![image-20230608105109391](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20230608105109391.png)