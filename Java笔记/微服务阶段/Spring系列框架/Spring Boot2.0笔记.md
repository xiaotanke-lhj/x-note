# Spring Boot2.X 笔记

> Spring Boot是整合Spring技术栈的一站式框架，简化Spring技术栈的快速开发脚手架。通过自动装配将其他技术栈整合到Spring容器中，不用编写大量的配置文件，创建出生产级别的Spring应用。

- 创建独立的Spring应用
- 内嵌了web服务器，例如Tomcat等
- 自动start依赖，简化构建配置，自动配置Spring以及第三方技术
- 提供生产级别的监控、健康检查及外部配置
- 无代码生成、无需编写XML文件

## 1、Hello Boot

> 创建一个普通的maven项目，导入spring-boot-web开发需要的依赖。

```xml
<dependencies>
    <!-- spring boot相关依赖-->
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-web</artifactId>
        <version>2.6.8</version>
    </dependency>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-devtools</artifactId>
        <version>2.6.8</version>
    </dependency>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-test</artifactId>
        <version>2.6.8</version>
    </dependency>

    <dependency>
        <groupId>mysql</groupId>
        <artifactId>mysql-connector-java</artifactId>
        <version>8.0.29</version>
    </dependency>
    <dependency>
        <groupId>com.alibaba</groupId>
        <artifactId>druid</artifactId>
        <version>1.2.11</version>
    </dependency>
    <dependency>
        <groupId>org.mybatis.spring.boot</groupId>
        <artifactId>mybatis-spring-boot-starter</artifactId>
        <version>2.2.2</version>
    </dependency>

    <!-- 日志依赖-->
    <dependency>
        <groupId>org.apache.logging.log4j</groupId>
        <artifactId>log4j-api</artifactId>
        <version>2.18.0</version>
    </dependency>
    <dependency>
        <groupId>org.apache.logging.log4j</groupId>
        <artifactId>log4j-core</artifactId>
        <version>2.18.0</version>
    </dependency>
    <dependency>
        <groupId>org.apache.logging.log4j</groupId>
        <artifactId>log4j-slf4j-impl</artifactId>
        <version>2.18.0</version>
    </dependency>
    <dependency>
        <groupId>org.slf4j</groupId>
        <artifactId>slf4j-api</artifactId>
        <version>1.7.36</version>
    </dependency>
</dependencies>
```

> resoureces目录下创建application.yaml文件作为配置文件：

![image-20220808091957151](https://s2.loli.net/2022/08/08/qx21aEfWyVAe9M7.png)

> 主启动类HelloApplication：

```java
@SpringBootApplication
public class HelloApplication {
    public static void main(String[] args) {
        SpringApplication.run(HelloApplication.class,args);
    }
}
```

> 启动项目：

![image-20220808092503708](https://s2.loli.net/2022/08/08/CTnuRaPX8fBtvDL.png)

> 编写一个请求：请求所在包要在主启动类的同级目录下。

```java
@RestController
public class HelloController {
    @RequestMapping("/hello")
    public String hello(){
        return "Hello Spring Boot !! ";
    }
}
```

![image-20220808094205013](https://s2.loli.net/2022/08/08/RYd7kS5eu4IO1JA.png)

## 2、自动装配原理

### 2.1、启动器starter

> 启动器：在spring-boot项目中，以spring-boot-starter-*开头的称为启动器。你需要开发的某个场景就可以引入对应场景的启动器。这样他就可以自动帮你引入这个场景下的所有依赖。以*-spring-boot-starter开头的是第三方的启动器，我们也可以自定义自己的启动器。

```xml
<!-- 引入web开发的启动器，自动引入web开发需要的依赖-->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-web</artifactId>
    <version>2.6.8</version>
</dependency>
```

>  自动配置：导入对应场景的依赖以后，spring boot会进行自动配置好环境。(以web开发为例)

- 配置Tomcat：引入了Tomcat相关依赖，并自动做好配置。
- 自动装配Spring MVC：引入Spring MVC的全套组件。
- 自动配置web常见功能：例如DispatcherServlet、视图解析器、字符拦截器、文件上传等功能。
- 默认包结构：默认主程序同级目录下的包及其子包都可以扫描，也可以修改默认扫描包。

```java
// scanBasePackages：修改默认扫描包
@SpringBootApplication(scanBasePackages = "com")
public class HelloApplication {
    public static void main(String[] args) {
        SpringApplication.run(HelloApplication.class,args);
    }
}
```

- 默认属性配置：加载到容器中的组件属性都有默认的配置，也可以通过application.yaml中自定义配置。每个配置对应容器中一个组件

- 按需加载组件：只有引入了对应场景的启动器，才会自动配置对应组件，对于没有引入的启动器就不加载自动配置(SpringBoot中虽然写了很多类，但是只有部分引入启动器的类才会生效)

### 2.2、@Configuration

> 被这个注解标识的类是Spring的配置类，相当于是一个配置文件，在这个类中可以向容器中注入组件。

```java
@Configuration
public class MyConfig {

    // 向容器中创建方法名作为组件名的对象实例，对象就是方法的返回值
    @Bean
    public DruidDataSource druidDataSource(){
        return new DruidDataSource();
    }
}
```

```java
@Test
public void test1(){
    AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(MyConfig.class);
    DruidDataSource dataSource1 = context.getBean("druidDataSource", DruidDataSource.class);
    DruidDataSource dataSource2 = context.getBean("druidDataSource", DruidDataSource.class);
    System.out.println(dataSource1 == dataSource2); // 从容器获取到同一个对象
}
```

> Full模式：

```java
/**
 * proxyBeanMethods：
 *      -true：开启Full模式(默认)，@Configuration也会将MyConfig注入到容器中，但是是注入的MyConfig的增强代理类
 *             如果通过MyConfig对象去调用druidDataSource()方法，它会判断容器中是否存在组件，如果存在就返回同一个对象
 */
@Configuration(proxyBeanMethods = true)
public class MyConfig {

    // 向容器中创建方法名作为组件名的对象实例，对象就是方法的返回值
    @Bean
    public DruidDataSource druidDataSource(){
        return new DruidDataSource();
    }
}
```

```java
@Test
public void test2(){
    AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(MyConfig.class);
    // 从容器中获取增强的配置类
    MyConfig config = context.getBean("myConfig", MyConfig.class);

    // 增强类调用方法获取对象
    DruidDataSource dataSource1 = config.druidDataSource();
    DruidDataSource dataSource2 = config.druidDataSource();

    // 在Full模式下，调用方法也是获取的同一个对象
    System.out.println(dataSource1 == dataSource2); // true
}
```

> Lite模式：

```java
/**
 * proxyBeanMethods：
 *      -false：开启Lite模式，@Configuration也会将MyConfig注入到容器中，但是是注入的MyConfig的普通对象
 *             如果通过MyConfig对象去调用druidDataSource()方法，它直接返回一个新的对象
 */
@Configuration(proxyBeanMethods = false)
public class MyConfig {

    // 向容器中创建方法名作为组件名的对象实例，对象就是方法的返回值
    @Bean
    public DruidDataSource druidDataSource(){
        return new DruidDataSource();
    }
}
```

```java
@Test
public void test2(){
    AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(MyConfig.class);
    // 从容器中获取增强的配置类
    MyConfig config = context.getBean("myConfig", MyConfig.class);

    // 增强类调用方法获取对象
    DruidDataSource dataSource1 = config.druidDataSource();
    DruidDataSource dataSource2 = config.druidDataSource();

    // 在Lite模式下，调用方法会重新获取一个对象
    System.out.println(dataSource1 == dataSource2); // false
}
```

> Full模式与Lite模式的选择：它们两个主要就是解决组件依赖问题，如果在配置类中创建的对象会被其他组件使用，那就使用Full模式。如果不会被其他主键使用，就使用Lite模式，这样创建对象就不会检查判断一边，直接返回一个新的对象，提高性能。

### 2.3、@Import

> 使用在任何一个组件类或者配置类上，通过这个注解向容器中导入多个组件。注解有一个value属性数组，是导入组件的class对象。

```java
// 给容器中自动创建一个名为User全限定类名的User组件和一个MyConfig全限定类型的MyConfig组件
// 组件名车默认是类的全限定类名
@Import({User.class,MyConfig.class})
@Configuration
public class ImportTest {

}
```

```java
@Test
public void test3(){
    AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ImportTest.class);
    User bean = context.getBean("com.xiaotanke.entity.User", User.class);
    System.out.println(bean);
}
```

### 2.4、@Conditional

> 条件装配，满足注解指定的条件以后才进行组件的注入，如果不满足条件就不进行注入。

<img src="https://s2.loli.net/2022/08/08/Uy3VQhtlS46je8q.png" alt="image-20220808162935754"  />

```java
// 可以作用在方法上，也可以作用在类上，类上表示类中方法组件注入都要满足条件才可以
@ConditionalOnBean(name = "user",value = {Admin.class})
@Configuration
public class ConditionalTest {
    @Bean("user1")
    // 当容器中存在user名称的组件，同时存在Admin类型的组件时才会进行组件注入
    @ConditionalOnBean(name = "user",value = {Admin.class})
    public User user1(){
        return new User();
    }
}
```

### 2.5、@ImportResource

> 在任何一个配置上，通过这个注解可以解析Spring的xml配置文件，将文件中注册的注册自动注入到容器中。

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd">
    <bean id="user2" class="com.xiaotanke.entity.User"/>
</beans>
```

```java
// 参数是配置文件的路径
@ImportResource("classpath:bean.xml")
@Configuration
public class ImportResourceTest {

}
```

### 2.6、@ConfigurationProperties

> 配置绑定，将yaml文件中的配置自动配置到类的属性上。

```yaml
user:
  name: 张三
  age: 18
```

```java
// 只有在容器中的组件才能进行这样的属性配置
// @ConfigurationProperties(value = "user")  两个注解作用相同
// prefix 对应的是配置文件下的所有属性自动配置到类的属性上
// 注意属性必须有set方法才能进行自动配置
@Component("user")
@ConfigurationProperties(prefix = "user")
@Data
public class User {
    private String name;
    private String age;
}
```

> 配置类开启配置绑定：如果需要配置的类不在容器中，我们可以通过配置类方式开启配置绑定。

```java
@Configuration
// 开启某个类的配置绑定，参数是类的class对象，这样在User类上也可以使用@ConfigurationProperties(prefix = "user")进行配置绑定，即使user不在容器中。这个注解是直接把user自动注入到容器中
@EnableConfigurationProperties({User.class})
public class MyConfig { }
```

### 2.7、自动装配原理

> @SpringBootApplcation：标识这个项目是SpringBoot项目，它是一个复合注解。它的主要有三个注解@SpringBootConfiguration、@EnableAutoConfiguration、@ComponentScan。

- @SpringBootConfiguration：它底层就是一个@Configuration注解，标识主程序也是一个配置类，是一个核心配置类。

- @ComponentScan：指定扫描包，默认扫描主程序下同级目录下的包的所有组件注解，也可以通过value属性修改扫描包。

- ==@EnableAutoConfiguration==：springboot自动装配的核心注解，也是一个复合注解，它完成了组件的自动装配。

  - @AutoConfigurationPackage：自动配置包，将主程序同级目录下的所有包扫描到的所有组件批量注册到容器中。

  ```java
  @Import(AutoConfigurationPackages.Registrar.class) // 导入了Registrar这个组件
  public @interface AutoConfigurationPackage {}
  ```

  ~~~java
  static class Registrar implements ImportBeanDefinitionRegistrar, DeterminableImports {
  
     @Override
     public void registerBeanDefinitions(AnnotationMetadata metadata, BeanDefinitionRegistry registry) {
        // 批量注册组件
        // new PackageImports(metadata).getPackageNames().toArray(new String[0])
        //  new PackageImports(metadata)：metadata注解标注的类的包，这个注解是@SpringBootApplication的子注解，所以解析得到的包就是主程序所在包，这样就解释了包名需要和主程序同级
        register(registry, new PackageImports(metadata).getPackageNames().toArray(new String[0]));
     }
  
     @Override
     public Set<Object> determineImports(AnnotationMetadata metadata) {
        return Collections.singleton(new PackageImports(metadata));
     }
  }
  ~~~

  - @Import(AutoConfigurationImportSelector.class)：自动配置选择器，引入AutoConfigurationImportSelector组件选择对应配置类

  ```java
  @Override
  public String[] selectImports(AnnotationMetadata annotationMetadata) {
     if (!isEnabled(annotationMetadata)) {
        return NO_IMPORTS;
     }
     // 获取所有配置类实体，返回一个字符串数组
     AutoConfigurationEntry autoConfigurationEntry = getAutoConfigurationEntry(annotationMetadata);
     return StringUtils.toStringArray(autoConfigurationEntry.getConfigurations());
  }
  ```

  ```java
  protected AutoConfigurationEntry getAutoConfigurationEntry(AnnotationMetadata annotationMetadata) {
     if (!isEnabled(annotationMetadata)) {
        return EMPTY_ENTRY;
     }
     AnnotationAttributes attributes = getAttributes(annotationMetadata);
     // 获取所有的候选自动配置
     List<String> configurations = getCandidateConfigurations(annotationMetadata, attributes);
     
     // 下面都是处理这些配置，去掉重复等
     configurations = removeDuplicates(configurations);
     Set<String> exclusions = getExclusions(annotationMetadata, attributes);
     checkExcludedClasses(configurations, exclusions);
     configurations.removeAll(exclusions);
     configurations = getConfigurationClassFilter().filter(configurations);
     fireAutoConfigurationImportEvents(configurations, exclusions);
     return new AutoConfigurationEntry(configurations, exclusions);
  }
  ```

  ```java
  // 调用的一个核心方法，这个方法得到自动装配的所有组件，在spring-boot-autoconfigure-2.6.8.jar中一共配置了127个自动装配的组件，一旦项目启动时，就会自动加载这127个组件，这在META-INF/spring.factories文件中是写死了的
  private static Map<String, List<String>> loadSpringFactories(ClassLoader classLoader) {
  		Map<String, List<String>> result = cache.get(classLoader);
  		if (result != null) {
  			return result;
  		}
  		result = new HashMap<>();
  		try {
              // 加载一个资源文件FACTORIES_RESOURCE_LOCATION："META-INF/spring.factories"
              // 它会加载项目中META-INF下的所有spring.factories
  			Enumeration<URL> urls = classLoader.getResources(FACTORIES_RESOURCE_LOCATION);
  			while (urls.hasMoreElements()) {
  				URL url = urls.nextElement();
  				UrlResource resource = new UrlResource(url);
  				Properties properties = PropertiesLoaderUtils.loadProperties(resource);
  				for (Map.Entry<?, ?> entry : properties.entrySet()) {
  					String factoryTypeName = ((String) entry.getKey()).trim();
  					String[] factoryImplementationNames =
  							StringUtils.commaDelimitedListToStringArray((String) entry.getValue());
  					for (String factoryImplementationName : factoryImplementationNames) {
  						result.computeIfAbsent(factoryTypeName, key -> new ArrayList<>())
  								.add(factoryImplementationName.trim());
  					}
  				}
  			}
  
  			// Replace all lists with unmodifiable lists containing unique elements
  			result.replaceAll((factoryType, implementations) -> implementations.stream().distinct()
  					.collect(Collectors.collectingAndThen(Collectors.toList(), Collections::unmodifiableList)));
  			cache.put(classLoader, result);
  		}
  		catch (IOException ex) {
  			throw new IllegalArgumentException("Unable to load factories from location [" +
  					FACTORIES_RESOURCE_LOCATION + "]", ex);
  		}
  		return result;
  	}
  ```

  ![image-20220809095359186](https://s2.loli.net/2022/08/09/Q43sgPXyxalMD9R.png)

> 按需开启自动配置项：虽然在项目启动时就会去加载spring.factories文件，然后进行默认自动装配组件，但是并不是所有的组件都会注册到容器中，只有满足条件的组件才会注册到容器中。(以kafka自动装为例)

![image-20220809100653429](https://s2.loli.net/2022/08/09/Mq6ViwtRbPxGz2c.png)

> 总结：

- SpringBoot会自动加载所有的配置类，XXXAutoConfiguration。
- 每个配置类都会按照条件装配判断是否生效，默认配置是从XXXProperties中拿(每一个配置类都对应一个XXXProperties)，他们都可以绑定到核心配置文件上application.yaml
- 生效的配置类会向容器中注册很多的组件，容器中有了这些组件后就拥有了对应的功能。
- 可以自定义配置
  - 可以通过@Bean注解向容器中注册自定义的组件，在自动配置时Boot会判断容器是否已存在组件，如果存在就不会注册。(用户自定义了组件就用用户的，用户没有自定义组件就用默认的组件)
  - 可以在核心配置文件上，修改配置的默认属性，自定义属性内容

## 3、 Web 开发

### 3.1、静态资源配置

​	在spring boot中有默认4种静态资源的存放路径(都是类路径下)。

- classpath:/META-INF/resources/
- classpath:/resources/
- classpath:/public/
- classpath:/static/

​	在boot中的所有动态请求、静态资源请求都是映射 /**，相当于所有的请求都会进行拦截。拦截的请求会交给controller处理，如果在controller处理不了就会去找静态资源存放路径下找静态资源，如果静态资源没有就会返回404错误。

~~~java
// SpringBoot设置的默认的资源访问路径
// this.mvcProperties.getStaticPathPattern() 这个方法获取静态资源的访问路径，这个就相当于是 /webjars/**，但是这个方法返回的是 /** 这个路径
addResourceHandler(registry, this.mvcProperties.getStaticPathPattern(), (registration) -> {
   // 通过这个方法this.resourceProperties.getStaticLocations()获取映射路径
   registration.addResourceLocations(this.resourceProperties.getStaticLocations());
   if (this.servletContext != null) {
      ServletContextResource resource = new ServletContextResource(this.servletContext, SERVLET_LOCATION);
      registration.addResourceLocations(resource);
   }
});
~~~

~~~java
private static final String[] CLASSPATH_RESOURCE_LOCATIONS = { "classpath:/META-INF/resources/",
                                                              "classpath:/resources/", "classpath:/static/", "classpath:/public/" };

private String[] staticLocations = CLASSPATH_RESOURCE_LOCATIONS;

// 这个方法返回一个字符串数组，就是SpringBoot默认的静态资源的映射路径，只要访问 /** 就可以映射到上面数组中对应的静态资源路径
public String[] getStaticLocations() {
    return this.staticLocations;
}
~~~

> 自定义静态资源访问路劲：

```yaml
spring:
  mvc:
    # 设置静态资源的访问开头路径
    static-path-pattern: /res/**
  web:
    resources:
      # 设置静态资源存放路径，静态资源只能存放在这个路径下，是一个数组，默认就是那4种存放路径
      # 如果设置了这个属性，静态资源存放在其他路径就访问不到
      static-locations: [classpath:/assert/]
```

> webjars：通过maven的方式将需要的静态资源导入，可以去webjars官网查询静态资源的maven坐标。

```java
@Override
public void addResourceHandlers(ResourceHandlerRegistry registry) {
    // 查看配置文件中是否禁用了静态资源的
   if (!this.resourceProperties.isAddMappings()) {
      logger.debug("Default resource handling disabled");
      return;
   }
    // webjars的静态资源映射
   addResourceHandler(registry, "/webjars/**", "classpath:/META-INF/resources/webjars/");
    
    // boot的静态资源映射，如果自定义了就使用自定义的，没有自定义就是用默认的惊天资源映射
   addResourceHandler(registry, this.mvcProperties.getStaticPathPattern(), (registration) -> {
      registration.addResourceLocations(this.resourceProperties.getStaticLocations());
      if (this.servletContext != null) {
         ServletContextResource resource = new ServletContextResource(this.servletContext, SERVLET_LOCATION);
         registration.addResourceLocations(resource);
      }
   });
}
```

`classpath:/META-INF/resources/webjars/`这个目录在：

![image-20220104121845645](https://s2.loli.net/2022/01/04/HNe9XfQ7wmTAlJg.png)

如果我需要访问 `jquery.js`，就直接通过 `http://localhost/webjars/jquery/3.6.0/dist/jquery.js`进行访问，因为 `/webjars/**` 就直接映射到了 `/META-INF/resources/webjars/`这个目录下，可以直接访问这个目录下的静态资源。

### 5.3、 thymeleaf 模板引擎

​	我们一般开发使用的是html页面，但是我们需要将他们转换成jsp页面，这样我们就可以将后台查询的数据在前端展示出来。jsp就是一个模板引擎，但是spring boot 推荐我们使用thymeleaf这个模板引擎。

![image-20211117104040602](https://i.loli.net/2021/11/17/Zny7vY1NLrG6AI9.png)

==在resources文件夹下的template文件夹就是放thymeleaf的模板引擎的，相当于是WEB-INF下的文件，不能直接访问，而是通过controller跳转进行访问。它给我们拼接了一个访问路劲：没有前缀，只有后缀 `.html` ，所以访问页面都是html页面，不能使用jsp页面。==

1. 导入依赖

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-thymeleaf</artifactId>
</dependency>
```

2. 阅读源码

thymeleaf也存在一个自动装配类和一个配置类：

```java
// 在这个配置类中，可以看出默认写了页面访问的前缀和后缀，前缀是 /templates/，后缀是.html
@ConfigurationProperties(prefix = "spring.thymeleaf")
public class ThymeleafProperties {

   private static final Charset DEFAULT_ENCODING = StandardCharsets.UTF_8;

   public static final String DEFAULT_PREFIX = "classpath:/templates/";

   public static final String DEFAULT_SUFFIX = ".html";
```

== `templates`文件下的文件只能通过controller跳转访问，相当于是WEB-INF目录，上面的配置类相当于是一个视图解析器，通过视图解析器，跳转到对应的页面中。控制跳转的页面必须放在`templates`目录下，并且必须是`html`页面。==

3. 在创建一个控制器跳转到模板引擎

```java
@RequestMapping("/")
public String test(Model model){
    model.addAttribute("msg","你好,Thymeleaf!");
    return "index";
}
```

3. 在template文件夹下创建一个index.html文件

```html
<!DOCTYPE html>
<html lang="zh_CN" xmlns:th="http://www.thymeleaf.org">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
    <h1 th:text=${msg}></h1>
</body>
</html>
```

==这样就可以通过controller跳转到首页，并且在首页显示后端传入的数据。==

## 4、Thymeleaf 基础语法

[Thymeleaf官网:https://www.thymeleaf.org/](https://www.thymeleaf.org/)

==HTML中的所有元素都可以使用thymeleaf模板引擎来代替。`th:元素名` ==

### 4.1、简单的表达式

- 简单表达式:
  - 变量表达式: `${...}`：   `<h1 th:text="${msg}"></h1>`
  - 选择表达式: `*{...}`
  - 国际消息表达式: `#{...}`
  - URL 表达式: `@{...}`： `<a th:href="@{http://localhost:8080/gtvg/order/details(orderId=${o.id})}">view</a>`
  - 片段表达式: `~{...}`
- 字面量
  - 文本字面量: `'one text'`, `'Another one!'`,… ：需要使用单引号
  - 数字字面量: `0`, `34`, `3.0`, `12.3`,…
  - 布尔字面量: `true`, `false`
  - 空值字面量: `null`
  - 标识符字面量: `one`, `sometext`, `main`,…
- 文本操作符:
  - 字符串拼接: `+`
  - 字面替换: `|The name is ${name}|`：两个竖线不能省略。
- 算数运算符:
  - 二元运算符: `+`, `-`, `*`, `/`, `%`
  - 一元运算符: `-`
- 布尔运算符:
  - 二元运算符: `and`, `or`
  - 一元运算符: `!`, `not`
- 关系运算符:
  - 比较: `>`, `<`, `>=`, `<=` (`gt`, `lt`, `ge`, `le`)
  - 相等和不等: `==`, `!=` (`eq`, `ne`)
- 条件操作符:
  - 如果那么: `(if) ? (then)`：
  - If-then-else: `(if) ? (then) : (else)` ：`<p th:text="${msg}==99 ? '这个数等于99' : '这个数不等于99'"></p>`这个表达式很像三元运算符。
  - 默认值: `(value) ?: (defaultvalue)`
- 无操作表示:
  - No-Operation: `_`

### 4.2、遍历

​	使用 `th:each`来进行遍历数据，对于java中的数组、顺序表、链表、Map等都可以遍历。

```html
<ul>
    <!-- users是后端的数据，user就是遍历出来的每一项-->
    <li th:each="user : ${users}" th:text="${user}"></li>
</ul>
```

```html
<tr th:each="prod : ${prods}">
    <td th:text="${prod.name}">Onions</td> 
    <td th:text="${prod.price}">2.41</td>
    <td th:text="${prod.inStock}? #{true} : #{false}">yes</td>
</tr>
```

~~~html
<tr th:each="prod : ${prods}">
    <td th:text="*{name}">Onions</td> 
    <td th:text="*{price}">2.41</td>
    <td th:text="*{inStock}? #{true} : #{false}">yes</td>
</tr>
~~~

这三种方式的遍历都可以。

<font color=red>迭代状态：
</font>thymeleaf模板引擎的遍历提供了踪迭代状态的有用机制：*状态变量*。

- 当前*迭代索引*，从 0 开始。这是`index`属性。
- 当前*迭代索引*，从 1 开始。这是`count`属性。
- 迭代变量中的元素总数。这是`size`属性。
- 每次迭代的*iter 变量*。这是`current`属性。
- 当前迭代是偶数还是奇数。这些是`even/odd`布尔属性。
- 当前迭代是否是第一个。这是`first`布尔属性。
- 当前迭代是否是最后一次。这是`last`布尔属性。

```html
<table>
    <tr th:each="user,itemStat : ${users}">
        <!-- 每一项的序号-->
        <td th:text="${itemStat.count}"></td>
        <!-- 每一项的值-->
        <td th:text="${user}"></td>
        <!-- 集合中的长度-->
        <td th:text="${itemStat.size}"></td>
    </tr>
</table>
```

### 4.3、条件判断语句

条件评估：

- 如果值不为空：（这些判断成立）
  - 如果 value 是一个布尔值并且是`true`.
  - 如果 value 是一个数字并且不为零
  - 如果 value 是一个字符且非零
  - 如果 value 是 String 并且不是“false”、“off”或“no”
  - 如果 value 不是布尔值、数字、字符或字符串。
- （如果值为 null，则 th:if 将评估为 false）。

<font color=red>th:if：</font>如果条件成立就显示这个标签，如果不成立就不显示该标签。

```html
<p th:if="${users}!=null">不为空</p>
```

<font color=red>th:unless：</font>如果条件不成立才显示对应的标签，与 `th:if` 相反。

```html
<p th:unless="${users}!=null">为空</p>
```

<font color=red>th:switch：</font>与java中的switch类似。

```html
<div th:switch="${user.role}">
  <p th:case="'admin'">User is an administrator</p>
  <p th:case="#{roles.manager}">User is a manager</p>
  <p th:case="*">User is some other thing</p>
</div>
```

==请注意，一旦一个`th:case`属性被评估为`true`，`th:case`同一切换上下文中的所有其他属性都被评估为`false`。默认选项指定为`th:case="*"`：==

### 4.4、thymeleaf 布局

​	`th:insert`、`th:replace`、`th:include`，三个表示插入、替换和包含，可以将公共的页面抽出出来，然后实现页面部分的复用。

<font color=red>用法：
</font>这三个标签和`th:fragment`联合使用，使用`th:fragment`定义一个id，然后使用上面的三个进行复用。

```html
<!-- commons.html页面-->

<!-- th:fragment抽取为一个公共部分-->
<!--导航栏部分-->
<div th:fragment="nav">
    这是导航栏
</div>
```

```html
<!-- 在其他页面中引入公共部分-->

<!--导航栏公共-->
<!-- 第一个参数是公共页面的名称，如果存在路径，需要带有路径信息-->
<!-- 第二个参数是公共页面中的th:fragment="nav"设置的值-->
<div th:replace="~{commons :: nav}"></div>
```

上面三个的使用方式是一样的，一个是插入，相当于把公共部分添加一个div然后进行显示，一个是替换直接替换当前的元素。

## 5、注解开发

### 5.1、@PathVariable

```java
/**
 *  - @PathVariable: 从请求路径上获取参数
 *          - 可以直接获取某个参数
 *          - 也可以将参数直接封装到一个map中，但是map的key和value都必须是String
 */
@GetMapping("/{id}/{name}")
public String test1(@PathVariable("id") String id, @PathVariable("name") String name,
                    @PathVariable Map<String,String> pv){
    // 每个参数都会自动封装到map中
    pv.forEach((key, value)->{
        System.out.println("key = "+key+",value = "+value );
    });
    return "id = "+id+",name = "+name;
}
```

### 5.2、@RequestHeader

```java
/**
 *  - @RequestHeader：获取请求头参数，可以获取某一个请求头的参数，也可以将所有的请求头封装到一个
 *                      map中，map的key和value都必须是String
 */
@GetMapping("/header")
public Map<String,Object> test2(@RequestHeader("user-agent") String userAgent,
                                @RequestHeader Map<String,String> headers){
    Map<String, Object> map = new HashMap<>();
    map.put("user-agent",userAgent);
    map.put("headers",headers);
    return map;
}
```

### 5.3、@RequestParam

```java
/**
 * - @RequestParam：获取请求指定参数名的参数值，也可以封装成一个map，key和value都为string
 */
@GetMapping("/params")
public Map<String,Object> test3(@RequestParam("name") String name, @RequestParam Map<String,String> params){
    Map<String, Object> map = new HashMap<>();
    map.put("name",name);
    map.put("params",params);
    return map;
}
```

### 5.4、@CookieValue

```java
/**
 *  - @CookieValue：获取某个cookie的值，也可以将cookie信息封装成一个cookie对象
 */
@GetMapping("/cookie")
public Map<String,Object> test4(@CookieValue("Idea-bae5efab") String c,
                                @CookieValue("Idea-bae5efab")Cookie cookie){
    Map<String, Object> map = new HashMap<>();
    map.put("c",c);
    map.put("cookie",cookie);
    return map;
}
```

### 5.5、@RequestBody

```java
/**
 * - @RequestBody：获取请求体中的内容
 */
@PostMapping("post")
public Map<String,Object> test5(@RequestBody String content){
    Map<String, Object> map = new HashMap<>();
    map.put("content",content);
    return map;
}
```

## 6、国际化

1. 编写国际化的配置文件

   在resources目录下创建一个i18n的文件夹，下面创建`index.properties`，然后创建一个`index_zh_CN.properties`，然后就会发现这两个文件合并成了一个文件，然后右键添加`index_en_US.properties`文件，对应是默认显示、英文显示、中文显示。

   ![image-20220106151341975](https://s2.loli.net/2022/01/06/n3acG4XdbDW6lxp.png)

   编写对应的文件，通过键值对的显示编写，也可以通过可视化的界面进行编写，可视化的编写需要下载一个插件`Resource Bundle Editor`，在文件的左下方有一个`Resource Bundle`，点击就会进入可视界面的配置。

   ![image-20220106152109042](https://s2.loli.net/2022/01/06/lG3WfajcR9x4Xwv.png)

2. 编写springboot的配置文件

在`application.yaml`中编写

```yaml
# message是国际化配置，basename是配置文件的位置和名称，i18n是文件夹名称，index是文件名称
spring:
  messages:
    basename: i18n.index
```

3. thymeleaf 获取国际化文本

在前端页面名，通过 `thymeleaf` 的 `#{  }` 来获取国际化的默认文本。

```html
<!-- login.title是在国际化配置文件中的键值对-->
<h2 class="text-center" th:text="#{login.title}">登录</h2>
```

4. 编写国际化解析器

```java
@Override
@Bean
@ConditionalOnMissingBean(name = DispatcherServlet.LOCALE_RESOLVER_BEAN_NAME)
public LocaleResolver localeResolver() {
    // 如果用户配置了国际化，就是使用用户配置的
   if (this.webProperties.getLocaleResolver() == WebProperties.LocaleResolver.FIXED) {
      return new FixedLocaleResolver(this.webProperties.getLocale());
   }
    // 如果没有配置，就是用默认的国际化配置
    // AcceptHeaderLocaleResolver这个类实现了LocaleResolver接口，这就实现了国际化
   AcceptHeaderLocaleResolver localeResolver = new AcceptHeaderLocaleResolver();
   localeResolver.setDefaultLocale(this.webProperties.getLocale());
   return localeResolver;
}
```

```java
public Locale resolveLocale(HttpServletRequest request) {
    // 获取默认的地区语言
    Locale defaultLocale = this.getDefaultLocale();
    // 解析请求，如果请求的语言为空，就是用默认的语言
    if (defaultLocale != null && request.getHeader("Accept-Language") == null) {
        return defaultLocale;
    } else {
        // 如果不为空就是进行以下的操作
        Locale requestLocale = request.getLocale();
        List<Locale> supportedLocales = this.getSupportedLocales();
        if (!supportedLocales.isEmpty() && !supportedLocales.contains(requestLocale)) {
            Locale supportedLocale = this.findSupportedLocale(request, supportedLocales);
            if (supportedLocale != null) {
                return supportedLocale;
            } else {
                return defaultLocale != null ? defaultLocale : requestLocale;
            }
        } else {
            return requestLocale;
        }
    }
}
```

==上面是SpringBoot为我们已经编写好的语言解析器，我们需要自己自定义一个语言解析器。这个解析器需要实现`LocaleResolver`，并重写其中的`resolveLocale`方法。==

```java
public class MyLocaleResolver implements LocaleResolver {
    // 重写的resolveLocale方法
    @Override
    public Locale resolveLocale(HttpServletRequest request) {
        // 获取请求参数
        String lang = request.getParameter("lang");
        Locale locale = Locale.getDefault();
        if (lang==null || "".equals(lang)){
            return locale;
        }
        String[] temp = lang.split("-");
        // 第一个参数是国家，第二个参数是地区
        return new Locale(temp[0],temp[1]);
    }

    @Override
    public void setLocale(HttpServletRequest request, HttpServletResponse response, Locale locale) {

    }
}
```

==将我们自定义的语言解析器注入到spring容器中，交给springboot管理。在MvcConfig的配置文件中，使用`@Bean`注解注入。==

```java
@Bean
public LocaleResolver localeResolver() {
    return new MyLocaleResolver();
}
```

==前端请求需要传入请求语言的参数，这样才能更改浏览器自带的语言请求参数。==

```html
<a th:href="@{/index.html(lang='zh-CN')}">中文</a>
<a th:href="@{/index.html(lang='en-US')}">English</a>
```

## 7、拦截器

> 编写拦截器，实现HandlerInterceptor接口，重写下面三个方法：

```java
public class InterceptorTest implements HandlerInterceptor {
    /**
     * 进入某个请求之前执行的方法，如果方法返回true就放行方法，返回false就拦截方法
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if ("123".equals("123")) return true;
        return false;
    }

    /**
     * 请求响应之后执行的方法，可以对返回的请求做一些处理（还未返回给用户）
     */
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        HandlerInterceptor.super.postHandle(request, response, handler, modelAndView);
    }

    /**
     * 请求响应成功，可以做一些后续操作，例如日志的编写，（已响应给用户）
     */
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        HandlerInterceptor.super.afterCompletion(request, response, handler, ex);
    }
}public class LoginInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        Admin admin = (Admin) request.getSession().getAttribute("admin");
        if (admin==null){
            request.setAttribute("msg","您还未登录,请先登录!!!");
            request.getRequestDispatcher("/login.html").forward(request,response);
            return false;
        }
        return true;
    }
}
```

> 编写拦截器配置：

```java
@Configuration
public class WebConfig implements WebMvcConfigurer {

    // 添加拦截器
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 注册拦截器，可以配置多个拦截器
        registry.addInterceptor(new InterceptorTest())
                // 拦截的请求路径，参数是可变字符串
                .addPathPatterns("/**")
                // 排除不拦截的请求路径，参数是可变字符串
                .excludePathPatterns("/assert/**");
    }
}
```

## 8、文件上传

> 文件controller：

```java
@PostMapping("/upload")
public Map<String,Object> upload(@RequestParam("name") String name,
                                 @RequestPart("image") MultipartFile image,
                                 @RequestPart("images") MultipartFile[] images) throws IOException {
    Map<String, Object> map = new HashMap<>();
    map.put("name",name);
    // 单文件文件上传
    if (!image.isEmpty()) { // 单文件判断是否为空文件
        image.transferTo(new File("E:\\file\\"+image.getOriginalFilename()));
    }

    // 多文件上传
    if (images.length>0) {
        for (MultipartFile file : images) {
            if (!file.isEmpty()) {
                file.transferTo(new File("E:\\file\\"+file.getOriginalFilename()));
            }
        }
    }
    map.put("msg","文件上传成功");
    return map;
}
```

> 文件上传配置：

```yaml
spring:
  servlet:
    multipart:
      max-file-size: 10MB # 单个文件最大大小，默认是1MB
      max-request-size: 100MB # 一次请求中文件上传最大大小，默认10MB
```

## 9、SpringBoot任务

### 9.1、异步任务

​	异步任务就是通过开启另一个线程去执行对应的任务，而主线程提前返回结果。

例如：下面这个程序会让用等待5秒后才会有返回结果，可以使用异步任务让这个任务在另一个线程继续执行，但是提前把结果返回给用户。

```java
public void test(){
    System.out.println("正在执行操作~~");
    // 休眠3秒
    try {
        Thread.sleep(5000);
    } catch (InterruptedException e) {
        e.printStackTrace();
    }
    System.out.println("操作执行完成~~");
}
```

<font color=red>使用步骤：
</font>

1. 在主方法上开启异步功能，使用`@EnableAsync`注解。

```java
@SpringBootApplication
@MapperScan("com.xiaotanke.mapper")
@EnableAsync
public class SpringBootPracticeApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootPracticeApplication.class, args);
    }

}
```

2. 在异步的方法是使用`@Async`告诉`spring`这是一个异步任务，它就会开启一个新的线程执行对应的方法。

```java
@Async // 告诉spring这是一个异步任务
public void test(){
    System.out.println("正在执行操作~~");
    // 休眠3秒
    try {
        Thread.sleep(5000);
    } catch (InterruptedException e) {
        e.printStackTrace();
    }
    System.out.println("操作执行完成~~");
}
```

### 9.2、邮件任务

1. 导入依赖

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-mail</artifactId>
</dependency>
```

2. 使用qq的邮件授权功能

![image-20220119140854631](https://s2.loli.net/2022/01/19/SwqtT3XKi5fkpRM.png)

3. 配置邮件发送

```java
// 发送邮件的核心实现类，由springboot已经写好
@Autowired
JavaMailSenderImpl javaMailSender;
```

```yaml
spring:
    mail:
        # 发送人
        username: 2576391408@qq.com
        # 授权码
        password: XXXXXXXXXXXX
        # 以smtp开头，如果是qq就是qq.com,如果是网易就是163.com结尾
        host: smtp.qq.com
        # qq邮件需要开启加密，其他邮箱不需要
        properties.mail.smtp.ssl.enable: true
```

4. 简单的邮件发送

```java
// 一个简单的邮件
@Test
public void simpleMail(){
    // 发送的内容配置
    SimpleMailMessage message = new SimpleMailMessage();
    // 邮件的主题
    message.setSubject("邮件测试主题");
    // 邮件的正文
    message.setText("我是nidie");
    // 发送给谁，可变参数，可以同时发送给多人
    message.setTo("1744850766@qq.com");
    // 谁发送的
    message.setFrom("LHJ160414@163.com");
    // 发送邮件
    javaMailSender.send(message);
}
```

5. 一个复杂的邮件，包括附件等等

```java
// 一个复杂的邮件
@Test
public void complexMail() throws MessagingException {
    // 创建一个复杂的邮件
    MimeMessage message = javaMailSender.createMimeMessage();
    // 配置邮件，参数需要一个MimeMessage对象，第二个参数是boolean表示是否支持多文件，第三个参数就是编码
    MimeMessageHelper helper = new MimeMessageHelper(message,true,"utf-8");
    // 设置主题
    helper.setSubject("复杂邮件测试");
    // 设置正文，第一个参数就是文本，第二个参数表示是否解析为html
    helper.setText("<h1 style='color:red'>我是复杂邮件</h1>",true);
    // 添加附件
    helper.addAttachment("测试.jpg",new File("src/main/resources/attachment1.jpg"));
    helper.setTo("2576391408@qq.com");
    helper.setFrom("LHJ160414@163.com");

    // 发送
    javaMailSender.send(message);
}
```

### 9.3、定时任务



















