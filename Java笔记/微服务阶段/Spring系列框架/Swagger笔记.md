# Swagger笔记

## 1、Swagger简介

​	`Swagger` 是一个规范且完整的框架，用于生成、描述、调用和可视化 `RESTful` 风格的 `Web` 服务。`Swagger` 的目标是对 `REST API` 定义一个标准且和语言无关的接口，可以让人和计算机拥有无须访问源码、文档或网络流量监测就可以发现和理解服务的能力。当通过 `Swagger` 进行正确定义，用户可以理解远程服务并使用最少实现逻辑与远程服务进行交互。与为底层编程所实现的接口类似，`Swagger` 消除了调用服务时可能会有的猜测。

- 支持在线同步`API`文档，不用手动编写`API`文档。
- 支持`Web`页面的在线测试`API`，`Swagger` 生成的文档还支持在线测试。参数和格式都定好了，直接在界面上输入参数对应的值即可在线测试接口。

[swagger官网：https://swagger.io/](https://swagger.io/)

## 2、Swagger使用步骤

1. 导入依赖

```xml
<dependency>
    <groupId>com.spring4all</groupId>
    <artifactId>swagger-spring-boot-starter</artifactId>
    <version>1.7.1.RELEASE</version>
</dependency>
```

2. 配置`swagger`

```java
@Configuration
@EnableSwagger2  // 开启swagger
public class SwaggerConfig {
    
    // 这样就可以使用swagger的默认配置 
}
```

3. 测试：访问`localhost:8080/swagger-ui.html`就可以访问默认的页面

![image-20220117134217497](https://s2.loli.net/2022/01/17/boTRXPfQ9qgZ48i.png)

## 3、Swagger配置信息

```java
// 配置Docket的相关信息
@Bean
public Docket getDocket(){


    // 文档类型设置为DocumentationType.SWAGGER_2
    return new Docket(DocumentationType.SWAGGER_2)
            // 设置对应的swagger的api信息，参数是一个Contact对象
            // 默认是ApiInfo实体类
            .apiInfo(this.getApiInfo());
}
/*
 默认的信息
 static {
    DEFAULT = new ApiInfo("Api Documentation",
    "Api Documentation", "1.0", "urn:tos",
    DEFAULT_CONTACT, "Apache 2.0",
    "http://www.apache.org/licenses/LICENSE-2.0", new ArrayList());
 }
 */
private ApiInfo getApiInfo(){
    // 文档的作者信息
    Contact contact = new Contact("xiaotanke", "http://www.yujiangg.com", "LHJ160414@163.com");
    return new ApiInfo(
            // 题目
            "测试的Swagger文档",
            // 描述
            "这是一个测试api文档",
            // 版本
            "v-1.0",
            // 作者网站
            "http://www.yujiangg.com",
            // 作者的个人信息
            contact,
            // 开源信息
            "Apache 2.0",
            "http://www.apache.org/licenses/LICENSE-2.0",
            new ArrayList()
    );
}
```

## 4、Swagger配置扫描

​	`swagger`默认是扫描项目的全部接口，即扫描项目中的所有`controller`中的接口，但是我们可以自定义扫描接口。

```java
// 配置Docket的相关信息
    @Bean
    public Docket getDocket(){


        // 文档类型设置为DocumentationType.SWAGGER_2
        return new Docket(DocumentationType.SWAGGER_2)
                // 设置对应的swagger的api信息，参数是一个Contact对象
                // 默认是ApiInfo实体类
                .apiInfo(this.getApiInfo())
                // 自定义扫描接口，select()和build()是联合使用的，只能在之间配置
                // 两种方式，选择一种
                .select()
                // apis配置扫描接口范围
                /*
                    basePackage(): 扫描指定包下的接口
                    any(): 全部扫描，默认全部扫描
                    none(): 全都不扫描
                    withMethodAnnotation(): 扫描方法上有指定注解的接口，参数是指定注解的class对象
                    RequestHandlerSelectors(): 扫描类上有指定注解的接口，参数是指定注解的class对象
                 */
//                .apis(RequestHandlerSelectors.basePackage("com.xiaotanke.controller"))
                .apis(RequestHandlerSelectors.any())
//                .apis(RequestHandlerSelectors.none())
//                .apis(RequestHandlerSelectors.withMethodAnnotation(RequestMapping.class))
//                .apis(RequestHandlerSelectors.withClassAnnotation(Controller.class))
                // 扫描过滤请求下的接口
                /*
                    ant(): 添加一个过滤请求，扫描这下面的接口
                    any(): 全部扫描
                    none(): 全都不扫描
                    regex(): 扫描满足指定正则的接口
                 */
//                .paths(PathSelectors.ant("/admin/**"))
//                .paths(PathSelectors.any())
//                .paths(PathSelectors.none())
                .paths(PathSelectors.regex("正则字符串"))
                .build();
    }
```

## 5、Swagger其他配置

1. 配置swagger启动

```java
// 配置是否启动，默认是true启动，false不启用
.enable(false)
```

配置了这个就会出现下面的情况：

![image-20220117170647338](https://s2.loli.net/2022/01/17/ORZXirVfHqBn4ka.png)

应用：将`swagger`配置成在生产环境中使用，但是上线环境不启动。

1. 使用多环境配置，配置`dev(开发)`、`test(测试)`、`pro(线上)`三种环境。
2. 获取项目环境，根据环境判断是否开启swagger。

```java
// 配置Docket的相关信息
    @Bean
    public Docket getDocket(Environment environment){
        // 环境
        Profiles profiles = Profiles.of("dev","test");
        // acceptsProfiles() 监听项目的环境，参数是Profiles对象，如果存在上面指定的环境名称，就返回true，反之false
        boolean flag = environment.acceptsProfiles(profiles);

        // 文档类型设置为DocumentationType.SWAGGER_2
        return new Docket(DocumentationType.SWAGGER_2)
                // 设置对应的swagger的api信息，参数是一个Contact对象
                // 默认是ApiInfo实体类
                .apiInfo(this.getApiInfo())
                // 配置是否启动，默认是true启动，false不启用
                // 将flag的值作为参数传入，如果是dev或test环境就开启swagger
                .enable(flag)
                // 自定义扫描接口，select()和build()是联合使用的，只能在之间配置
                // 两种方式，选择一种
                .select()
                // apis配置扫描接口范围
                /*
                    basePackage(): 扫描指定包下的接口
                    any(): 全部扫描，默认全部扫描
                    none(): 全都不扫描
                    withMethodAnnotation(): 扫描方法上有指定注解的接口，参数是指定注解的class对象
                    RequestHandlerSelectors(): 扫描类上有指定注解的接口，参数是指定注解的class对象
                 */
//                .apis(RequestHandlerSelectors.basePackage("com.xiaotanke.controller"))
                .apis(RequestHandlerSelectors.any())
//                .apis(RequestHandlerSelectors.none())
//                .apis(RequestHandlerSelectors.withMethodAnnotation(RequestMapping.class))
//                .apis(RequestHandlerSelectors.withClassAnnotation(Controller.class))
                // 扫描过滤请求下的接口
                /*
                    ant(): 添加一个过滤请求，扫描这下面的接口
                    any(): 全部扫描
                    none(): 全都不扫描
                    regex(): 扫描满足指定正则的接口
                 */
//                .paths(PathSelectors.ant("/admin/**"))
//                .paths(PathSelectors.any())
//                .paths(PathSelectors.none())
//                .paths(PathSelectors.regex("正则字符串"))
                .build();
    }
```

## 6、Swagger分组配置

​	根据`API`文档生成多个分组，每个组对应自己的文档。多个组实际就是多个`Docket`对象，每一个`Docket`对象就是一个组。

```java
// 组名为B
@Bean
public Docket getBDocket(){
    return new Docket(DocumentationType.SWAGGER_2)
            // 组名
            .groupName("B");
}
// 组名为C
@Bean
public Docket getCDocket(){
    return new Docket(DocumentationType.SWAGGER_2)
            .groupName("C");
}
// 组名为D
@Bean
public Docket getDDocket(){
    return new Docket(DocumentationType.SWAGGER_2)
            .groupName("D");
}
```

![image-20220117221427689](https://s2.loli.net/2022/01/17/LBwKqai5ZsnC9br.png)

## 7、Swagger注解配置

1. 实体类注解

 ==当扫描的接口其中一个返回了一个实体对象，那么在`swagger`的`model`中就会有对应实体的内容。==

```java
// lombok注解
@Data
@AllArgsConstructor
@NoArgsConstructor
// swagger实体的注释，会在swagger中显示
@ApiModel("用户实体类")
public class User {
    // 实体类的字段注释
    @ApiModelProperty("自然主键")
    private Integer id;
    @ApiModelProperty("用户名")
    private String userName;
    @ApiModelProperty("用户密码")
    private String password;
}
```

![image-20220117224429331](https://s2.loli.net/2022/01/17/uRTlLof8hgZxvKV.png)

2. 接口注释

```java
@Controller
public class LoginController {


    // @ApiOperation 作用于方法上，注释某一个接口
    // @ApiParam 作用于参数，用作参数的注释
    @ApiOperation("测试接口")
    @GetMapping("/test1")
    public String test1(@ApiParam("用户名参数") String username){
        return username;
    }
}
```

![image-20220117225326670](https://s2.loli.net/2022/01/17/nyPGEzU9N4Ct3DO.png)

3. 接口测试

==注释注解在作用上就是一个文档的说明，没有实际 上的作用。增强文档的可读性，也便于测试接口。==

![image-20220117230923593](https://s2.loli.net/2022/01/17/idl2xR7AGY9pJV6.png)

![image-20220117231013595](https://s2.loli.net/2022/01/17/FBEl8VNnevQGY9K.png)

![image-20220117231052002](https://s2.loli.net/2022/01/17/O8YQHzwn7KmbAMr.png)

最近发现除了腾讯云和阿里云之外的一种好用的云服务器，那就是三丰云云服务器，它拥有众多的功能，其中一个就是可以免费试用一款云服务器，下面介绍它的使用方式。

[官方地址:https://www.sanfengyun.com/](https://www.sanfengyun.com/)

![image-20230307102210797](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20230307102210797-16781568307221-16824734711721.png)

然后进行一个实名认证和微信的绑定就可以申请一个 1c1g的免费服务器。

![image-20230307102330457](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20230307102330457-16781568307223-16824734711723.png)



三丰云是北京太极三丰云计算有限公司旗下网络服务品牌，十八年IDC老兵团队蛰伏三年后投资千万于2018年10月1日创建。公司致力于为大众提供优质的互联网基础服务和物联网服务，包括：域名注册、虚拟主机、云服务器、主机托管租用、CDN网站加速、物联网应用等服务。以帮助客户轻松、 高速、高效的应用互联网/物联网，提高企业竞争能力。，它拥有众多的功能，其中一个就是可以免费试用一款云服务器，下面介绍它的使用方式。

[官方地址:https://www.sanfengyun.com/](https://www.sanfengyun.com/)



