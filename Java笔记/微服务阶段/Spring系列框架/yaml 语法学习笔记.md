# yaml 语法学习笔记

​	**YAML**（/ˈjæməl/，尾音类似*camel*骆驼）是一个可读性高，用来表达数据[序列化](https://baike.baidu.com/item/序列化)的格式。YAML参考了其他多种语言，包括：[C语言](https://baike.baidu.com/item/C语言)、[Python](https://baike.baidu.com/item/Python)、[Perl](https://baike.baidu.com/item/Perl)，并从[XML](https://baike.baidu.com/item/XML)、电子邮件的数据格式（RFC 2822）中获得灵感。Clark Evans在2001年首次发表了这种语言，另外Ingy döt Net与Oren Ben-Kiki也是这语言的共同设计者。当前已经有数种编程语言或脚本语言支持（或者说解析）这种语言。

​	以前的配置文件通常使用xml文件，yaml对于xml配置文件更加简洁清楚，spring-boot官方就推荐使用yaml来作为配置文件。

- 大小写敏感
- 使用缩进表示层级关系
- 缩进的空格数量不重要，重要的是必须对齐

## 1、普通数据

语法：

~~~yaml
key: value  # value和冒号之间存在有一个空格
~~~

## 2、对象数据

- 单行结构：

语法：

~~~yaml
user: {name: 张三,age: 12} # 每一个冒号后面都有一个冒号，对象的每一个属性之间使用逗号隔开。
~~~

- 多行结构：

```yaml
user:
 name: 张三
 age: 12
# 第一个冒号没有空格，但是后面的冒号有一个空格，但是注意缩进。
```

## 3、数组数据

- 单行结构

~~~yaml
names: ["张三","李四","王五"]  # 冒号后面存在个空格，数据与数据之间使用逗号隔开
~~~

- 多行结构

~~~yaml
names:
 - "张三"
 - "李四"
 - "王五"
# 注意缩进，-与值之间存在一个空格
~~~

## 4、注释

~~~yaml
# 注释的内容
~~~

==yaml语法对缩进的要求很高==

## 5、$ 占位符

​	我们可以使用 $ 进行值的引用，如果在yaml中存在这个值，就是用这个对应的值。

```yaml
name: 张三

user:
 name: ${name} # 引用上面name这个普通数据的值
 age: 12
```

还可以指定一个数据的默认值，如果不存在这个值就使用默认值。

```yaml
name: 张三

user:
 name: ${name:默认值} # 如果不存在name这个值，就使用冒号后面的默认值
 age: 12
```









