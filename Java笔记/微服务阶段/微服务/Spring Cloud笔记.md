# Spring Cloud 笔记

> 微服务：是一种架构模式，它将独立单一应用分成一组小的服务，服务于服务之间相互协调最终给用户提供价值。服务运行在其独立的进程中，服务于服务之间采取轻量级的通信机制相互协作。每个服务都可以围绕着本业务进行构建，能够独立部署到对应环境，避免了统一、集中式的服务管理机制。

> Spring Cloud是一些列框架的有序集合，提供了一系列技术去协调服务与服务，例如配置管理、服务注册、服务发现、负载均衡、服务熔断、服务网关等等问题。是服务在分布式环境下很好的工作。

[Spring Cloud官网:https://spring.io/projects/spring-cloud](https://spring.io/projects/spring-cloud)

![image-20220803145843964](https://gitee.com/xiaotanke-lhj/image/raw/master/csdn/Juq46RGchIP8Btf.png)

[Cloud和Boot版本选择网址：https://start.spring.io/actuator/info](https://start.spring.io/actuator/info)，这个网址返回的是一个json字符串，将这个字符串使用在线json工具格式化后就可以查询每个版本对应需要的Boot版本。

![image-20220803150516063](https://gitee.com/xiaotanke-lhj/image/raw/master/csdn/NBzKUZY7juxX3lv.png)

==Spring Cloud 2021.0.3版本，Spring Boot 2.6.8版本==

> Spring Cloud技术学习及技术更新

![Cloud](https://gitee.com/xiaotanke-lhj/image/raw/master/csdn/RHYWNupDljZnBq1.png)

## 1、环境搭建

> 依赖导入：

```xml
<!-- 版本控制-->
<properties>
    <project.build.sourcesEncoding>UTF-8</project.build.sourcesEncoding>
    <maven.compiler.source>8</maven.compiler.source>
    <maven.compiler.target>8</maven.compiler.target>
    <junit.version>4.13.2</junit.version>
    <lombok.version>1.18.24</lombok.version>
    <mysql.version>8.0.29</mysql.version>
    <druid.version>1.2.11</druid.version>
    <mybatis.version>2.2.2</mybatis.version>
    <log4j2.version>2.18.0</log4j2.version>
    <slf4j.version>1.7.36</slf4j.version>
    <spring-boot.version>2.6.8</spring-boot.version>
    <spring-cloud.version>2021.0.3</spring-cloud.version>
</properties>

<dependencyManagement>
    <dependencies>
        <!-- SpringBoot 依赖配置 -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-dependencies</artifactId>
            <version>${spring-boot.version}</version>
            <type>pom</type>
            <scope>import</scope>
        </dependency>

        <!-- 微服务系列-->
        <!-- SpringCloud微服务 -->
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-dependencies</artifactId>
            <version>${spring-cloud.version}</version>
            <type>pom</type>
            <scope>import</scope>
        </dependency>

        <!-- 常用工具-->
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>${junit.version}</version>
        </dependency>
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>${lombok.version}</version>
            <scope>provided</scope>
        </dependency>

        <!-- 数据库连接及操作-->
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>${mysql.version}</version>
        </dependency>
        <dependency>
            <groupId>com.alibaba</groupId>
            <artifactId>druid</artifactId>
            <version>${druid.version}</version>
        </dependency>
        <dependency>
            <groupId>org.mybatis.spring.boot</groupId>
            <artifactId>mybatis-spring-boot-starter</artifactId>
            <version>${mybatis.version}</version>
        </dependency>

        <!-- 日志依赖-->
        <dependency>
            <groupId>org.apache.logging.log4j</groupId>
            <artifactId>log4j-api</artifactId>
            <version>2.18.0</version>
        </dependency>
        <dependency>
            <groupId>org.apache.logging.log4j</groupId>
            <artifactId>log4j-core</artifactId>
            <version>2.18.0</version>
        </dependency>
        <dependency>
            <groupId>org.apache.logging.log4j</groupId>
            <artifactId>log4j-slf4j-impl</artifactId>
            <version>2.18.0</version>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <version>${slf4j.version}</version>
        </dependency>
    </dependencies>
</dependencyManagement>
```

> dependencyManagement与dependencies的区别：

- dependencyManagement：

管理依赖版本号，在聚合类项目中子模块继承以后依赖可以不用写版本号，而是直接继承dependencyManagement配置的版本号，如果在子项目中指定了版本号，就会使用子项目的版本号。这个一般出现在父模块中。多个子项目依赖可以使用共同的版本号，有利于版本修改。==dependencyManagement只是声明了依赖，并没有引入依赖，在子模块中需要自己按需求引入依赖==

- dependencies：实现引入依赖

## 2、服务注册中心之Eureka

![image-20220805161017411](https://gitee.com/xiaotanke-lhj/image/raw/master/csdn/PcMl1EYHhSDwTod.png)

> Eureka包含两个组件：Eureka Server 和 Eureka Client
>
> Eureka Server：提供服务注册，各个微服务节点通过配置启动后，在Eureka Server中心进行注册，会服务注册表中存储服务的相关信息。
>
> Eureka Client：通过注册中心进行访问，一个Java客户端。客户端内置一个使用轮询负载算法的负载均衡器。在服务启动后会向Eureka Server按照一定周期发送心跳，如果在多个心跳周期Eureka Server都没有收到服务发送的心跳，注册中心会从服务注册表上将对应服务移除。

### 2.1、Eureka单机部署

```xml
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
    <version>3.1.3</version>
</dependency>
```

> 配置yml：

```yaml
server:
  port: 7001
eureka:
  instance:
    hostname: 127.0.0.1 # 服务中心注册地址
  client:
    register-with-eureka: false # 不将自己注册到服务中
    fetch-registry: false # 表示自己是服务注册中心，不需要检索服务
    service-url:
      # eureka server访问地址
      defaultZone: http://${eureka.instance.hostname}:${server.port}/eureka/
```

> 配置启动类：

```java
@SpringBootApplication
@EnableEurekaServer  // 标识是一个eureka server 服务注册中心
public class EurekaApplication {
    public static void main(String[] args) {
        SpringApplication.run(EurekaApplication.class,args);
    }
}
```

> 访问服务注册中心：http://localhost:7001/

![image-20220805165131989](https://gitee.com/xiaotanke-lhj/image/raw/master/csdn/C8rh3FV9esLO6RT.png)

> 服务进行注册：

```xml
<!-- eureka-client-->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
    <version>3.1.3</version>
</dependency>
```

```yaml
eureka:
  client:
    register-with-eureka: true  # 将自己注册进Eureka Server中心
    # 是否从Eureka Server获取自己的注册信息，集群必须设置为true
    fetch-registry: true
    service-url:
      # Eureka Server 地址
      defaultZone: http://127.0.0.1:7001/eureka
```

> 先启动Eureka Server，然后启动Eureka Client，在访问 http://localhost:7001/

![image-20220806103814676](https://gitee.com/xiaotanke-lhj/image/raw/master/csdn/bxJDmnCKYGI5wpX.png)

### 2.2、Eureka集群部署

> Eureka原理：

![未命名白板](https://gitee.com/xiaotanke-lhj/image/raw/master/csdn/mJpH341EAlPszZX.png)

​	在单机部署中存在单点故障的问题，所以需要集群来解决单点故障，保证服务的高可用性。如果只有一个注册中心，如果它挂了，就会导致整个服务环境就不可使用，所以要搭建Eureka集群实现负载均衡、故障容错。

> 集群环境搭建(以3台服务器为集群)，修改host配置文件，模拟三台服务器(`C:\Windows\System32\drivers\etc\hosts`)

~~~txt
127.0.0.1 eureka7001.com # 使用本地模拟三台eureka服务器，使用不同的端口
127.0.0.1 eureka7002.com
127.0.0.1 eureka7003.com
~~~

> 修改配置`yml`配置文件，让Eureka Server之间相互注册：

```yaml
eureka:
  instance:
    hostname: eureka7001.com # 服务中心注册地址
  client:
    register-with-eureka: false # 不将自己注册到服务中
    fetch-registry: false # 表示自己是服务注册中心，不需要检索服务
    service-url:
      # 向其它eureka server 注册中心注册自己，多个使用逗号隔开
      defaultZone: http://eureka7002.com:7002/eureka/,http://eureka7003.com:7003/eureka/,
# 将其他两个注册中心也像这样相互注册
```

> 启动三个注册中心：

![image-20220806200611855](https://gitee.com/xiaotanke-lhj/image/raw/master/csdn/Kjsk9SD1BUry43Y.png)

> 将服务注册到Eureka Server中心：

```yaml
eureka:
  client:
    register-with-eureka: true  # 将自己注册进Eureka Server中心
    # 是否从Eureka Server获取自己的注册信息，集群必须设置为true
    fetch-registry: true
    service-url:
      # Eureka Server 地址，多个使用逗号隔开，服务需要注册到集群中的所有server中
      defaultZone: http://eureka7001.com:7001/eureka,http://eureka7002.com:7002/eureka,http://eureka7003.com:7003/eureka
```

![image-20220806204955898](https://gitee.com/xiaotanke-lhj/image/raw/master/csdn/mhWAzRHlpcVErIQ.png)

> 服务集群的部署：将服务部署到多个服务器中，然后以相同服务名称注册到eureka server中心

![image-20220806205532471](https://gitee.com/xiaotanke-lhj/image/raw/master/csdn/SGqTYj61PJCwp7n.png)

> 服务消费，调用注册到服务中心的服务：以服务名称去调用，而不是用服务地址去调用

```java
@Configuration
public class ApplicationContextConfig {

    @Bean
    @LoadBalanced // 开启eureka默认的负载均衡
    public RestTemplate getRestTemplate(){
        return new RestTemplate();
    }
}
```

```java
@RestController
@RequestMapping("/order")
public class OrderController {

    // 使用注册到eureka server中心的服务的名称
    private static final String PAY_URL = "http://PAYAPPLICATION";
    private static final Logger LOGGER = LoggerFactory.getLogger(OrderController.class);
    @Autowired
    private RestTemplate restTemplate;

    // restTemplate调用远程服务
    @GetMapping("/{id}")
    public Result getById(@PathVariable Long id){
        return restTemplate.getForObject(PAY_URL+"/cloud/pay/"+id,Result.class);
    }
}
```

==服务集群中，服务以相同的服务名称注册到eureka server中，然后在服务调用的时候会根据eureka默认的负载均衡去调用服务名称下的服务集群。==

![image-20220806211254789](https://gitee.com/xiaotanke-lhj/image/raw/master/csdn/qDmFr2NGkncCJRZ.png)

> 修改服务主机名称和显示ip地址：

```yaml
eureka:
  client:
    register-with-eureka: true  # 将自己注册进Eureka Server中心
    # 是否从Eureka Server获取自己的注册信息，集群必须设置为true
    fetch-registry: true
    service-url:
      # Eureka Server 地址，多个使用逗号隔开，服务需要注册到集群中的所有server中
      defaultZone: http://eureka7001.com:7001/eureka,http://eureka7002.com:7002/eureka,http://eureka7003.com:7003/eureka
  instance:
    instance-id: PayApplication8003 # 修改服务的主机名称，而不是显示本机的主机名称
    prefer-ip-address: true # 显示ip地址
```

![image-20220807001809629](https://gitee.com/xiaotanke-lhj/image/raw/master/csdn/VM3Dk5HBUaOv9bC.png)

### 2.3、服务发现Discovery

​	对于注册到eureka server中心的服务，可以通过服务发现来获取服务信息。

```java
@SpringBootApplication
@EnableEurekaClient
@EnableDiscoveryClient // 开启服务发现，可以获取到eureka server中的注册好的服务
public class PayApplication {
    public static void main(String[] args) {
        SpringApplication.run(PayApplication.class,args);
    }
}
```

```java
/**
 * 服务发现
 */
@Autowired
private DiscoveryClient discoveryClient;

@GetMapping("/discovery")
public Object getService(){
    // 注册到eureka server中心的服务，以服务名称展示
    List<String> services = discoveryClient.getServices();
    for (String service : services) {
        System.out.println("发现一个服务："+service);
    }

    // 通过服务名称发现这个名称下的服务集群
    List<ServiceInstance> payApplication = discoveryClient.getInstances("PayApplication");

    System.out.println("PayApplication下的服务: ");
    for (ServiceInstance instance : payApplication) {
        // 服务名称 + 主机ip地址 + 端口号 + url地址
        System.out.print(instance.getServiceId()+"\t"+instance.getHost()+"\t"+instance.getPort()+"\t"+instance.getUri());
        System.out.println();
    }
    return discoveryClient;
}
```

![image-20220807102508810](https://gitee.com/xiaotanke-lhj/image/raw/master/csdn/V9liakNgDw1FBeO.png)

![image-20220807102556630](https://s2.loli.net/2022/08/07/wRAWQLxeYVzlcJv.png)

### 2.4、Eureka自我保护

![image-20220807102813019](https://gitee.com/xiaotanke-lhj/image/raw/master/csdn/76LOP3U8MvrNdA4.png)

> eureka中出现红色文字就出现了自我保护模式，主要是因为一组客户端与eureka server之间存在网络分区场景下的保护。一旦进入保护模式，eureka server尝试保护注册的服务，不再删除服务注册表中的数据，也就是不会再注销任何微服务。可能是因为网络延迟、卡顿等原因，导致client没有按时给server发送心跳，但是服务还是健康的，所以自我保护模式就是一种网络异常的安全措施，不会盲目注销健康的服务。

> 关闭保护模式：

```yaml
eureka:
  instance:
    hostname: eureka7001.com # 服务中心注册地址
  client:
    register-with-eureka: false # 不将自己注册到服务中
    fetch-registry: false # 表示自己是服务注册中心，不需要检索服务
    service-url:
      # 向其它eureka server 注册中心注册自己，多个使用逗号隔开
      defaultZone: http://eureka7002.com:7002/eureka/,http://eureka7003.com:7003/eureka/
  server:
    # 关闭保护机制，默认是true开启，false是关闭保护机制
    enable-self-preservation: false
    # 服务发送心跳的时间
    eviction-interval-timer-in-ms: 2000
```

![image-20220807161821323](https://gitee.com/xiaotanke-lhj/image/raw/master/csdn/Z9yDhEBcvf371xp.png)

> 服务设置：

```yaml
eureka:
  client:
    register-with-eureka: true  # 将自己注册进Eureka Server中心
    # 是否从Eureka Server获取自己的注册信息，集群必须设置为true
    fetch-registry: true
    service-url:
      # Eureka Server 地址，多个使用逗号隔开，服务需要注册到集群中的所有server中
      defaultZone: http://eureka7001.com:7001/eureka,http://eureka7002.com:7002/eureka,http://eureka7003.com:7003/eureka
  instance:
    instance-id: PayApplication8001
    prefer-ip-address: true
    # client向server发送心跳的间隔时间，默认是30s
    lease-renewal-interval-in-seconds: 10
    # server收到最后一次心跳后等待的时间，默认是90s，超过这个时间就会删除服务
    lease-expiration-duration-in-seconds: 30
```

## 3、`OpenFeign` 远程服务调用

> `OpenFeign`：是Spring Cloud在Feign的基础上支持了`Spring MVC`注解，可以解析`Spring MVC`的`@RequestMapping`注解下的接口，并通过代理方法产生实现类，实现类中做了负载均衡实现并调用其它服务。



















