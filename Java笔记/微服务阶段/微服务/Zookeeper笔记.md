# Zookeeper笔记

> Zookeeper：一个开源的，为分布式提供服务协调项目。它负责存储和管理服务数据，一旦这些数据发生变化，Zookeeper就负责通知Zookeeper上的观察者并做出相应的反应。

![image-20220829164633457](https://gitee.com/xiaotanke-lhj/image/raw/master/csdn/image-20220829164633457.png)

> Zookeeper特点：

- Zookeeper集群中有一个领导者，多个跟随者组成。
- 集群中如果服务器只要半数存活就可以提供服务(不包含半数)。
- 集群中的全局数据一致，每个server都是保存一份相同的数据副本，客户端无论连接到哪个server都是一样的。
- 请求顺序执行，针对来自同一个客户端的请求这些请求会按照顺序去执行。
- 数据更新原子性，每次数据更新要么都成功，要么都失败。
- 实时性，在一定时间范围内客户端都能读取到最新的数据。

> 数据结构：

​	Zookeeper的数据模型和Unix文件系统类似，整体上可以看成一颗树。每一个节点看成一个ZNode，每个ZNode默认存储1MB的数据并且通过路径进行唯一标识。

![image-20220829201847719](https://gitee.com/xiaotanke-lhj/image/raw/master/csdn/image-20220829201847719.png)

## 1、环境搭建

[下载地址：https://downloads.apache.org/zookeeper/](https://downloads.apache.org/zookeeper/)。上传服务器，服务器上必须保证有Java环境。

~~~bash
 # 解压
tar -zxvf xxxx.tar.gz
~~~

> 修改配置文件：在Zookeeper安装目录下的conf下，将`zoo_sample.cfg`修改为`zoo.cfg`

~~~bash
cp zoo_sample.cfg zoo.cfg
# 创建文件夹data来存放数据
mkdir data
# 修改配置文件
dataDir = 创建的data文件路劲
~~~

> 启动zookeeper：在安装目录下的bin文件夹下，使用zkServer.sh来启动。

~~~bash
# 启动/停止
bin/zkServer.sh start/stop
# 日志在控制台输出启动
bin/zkServer.sh start-foreground
# 查看zookeeper状态
bin/zkServer.sh status
~~~

![image-20220829214309243](https://gitee.com/xiaotanke-lhj/image/raw/master/csdn/image-20220829214309243.png)

![image-20220829215500284](https://gitee.com/xiaotanke-lhj/image/raw/master/csdn/image-20220829215500284.png)

> 启动客户端：使用zkCli.sh来启动客户端。

~~~bash
# 启动客户端连接zookeeper服务器
bin/zkCli.sh
# zookeeper服务的文件系统和unix系统相同，大部分的Linux也可以使用
# 退出客户端
quit
~~~

![image-20220829214843048](https://gitee.com/xiaotanke-lhj/image/raw/master/csdn/image-20220829214843048.png)

> 配置文件参数：

~~~bash
# 心跳时间，集群中服务器之间心跳发送时间，默认2000毫秒
tickTime=2000
# 初始通信时限，服务启动时延时时间为：10*tickTime，默认20s
initLimit=10
# 同步通信时限，服务正常运行时延时时间：5*2000，默认10s
syncLimit=5
# 服务端连接口号
clientPort=2181
~~~

## 2、Zookeeper内部原理

### 2.1、选举机制

<font color=red>半数机制：</font>集群服务在半数也上集群就可用，所以一般Zookeeper服务器是季数个。

> 选举机制：Zookeeper没有在配置文件中配置Leader和Follower，但是在Zookeeper工作是会有一个Leader和多个Follower的，这个Leader是通过内部选举出来的，其它服务就是Follower。

<font color=red>选举过程： (以5台服务为例，id分别是1、2、3、4、5)</font>

- 每一台服务器已连接都会选举自己，然后查看自己选举的票数是否超过一半3(所有服务器)，没有就会投票id最大的server。
- 第一台服务启动，选举自己没有超过半数5/2，它的选举状态就一直是LOOKING。
- 第二台服务启动，先选举自己，没有超过半数5/2，第一台和第二台服务就选举id最大服务2。
- 第三台服务启动，先选举自己，没有超过半数5/2，前两台及自己都会投自己，那自己就有3票，这时第三台服务就满足就成为Leader，其它就成为Follower。
- 第四台、第五台服务启动，由于已经有Leader，就只能成为Follower了。

### 2.2、节点类型

> 持久型节点：客户端与服务端断开连接，创建的节点不删除。

​		客户端与Zookeeper断开连接后，节点依然存在只是对该节点名称(在创建ZNode节点时，节点名称会附加一个值，就是顺序号，顺序号是一个单调递增的计数器，由父节点维护)进行了顺序编号。

> 短暂型节点：客户端与服务端断开连接，创建的节点自己删除。

> 持久性顺序节点：

> 短暂性顺序节点：

## 3、集群搭建

(以三台服务器为例，id分别是1、2、3)

> 修改配置文件：

~~~bash
# 添加集群服务器内容
# server.id=xx.xx.xx:2888:3888
# id 服务端编号
# xx.xx.xx.xx	ip地址
# 2888	服务端之间数据同步的端口号
# 3888	服务端选举Leader端口号，当Leader宕机后会通过这个端口重新选举
server.1=xx.xx.xx.xx:2888:3888
server.2=xx.xx.xx.xx:2888:3888
server.3=xx.xx.xx.xx:2888:3888

# 注意：如果是云服务器一定要把对应端口的防火强打开，并且添加下面配置
quorumListenOnAllIPs=true # 云服务器不配置这句话会连接不上，害我找好久的解决方案
~~~

> 创建myid文件：在dataDir目录下创建文件并设置自己的id，要配置文件中给自己设置的id相同。

~~~bash
# 创建文件
touch myid
# 输入id
echo id号 > myid
~~~

> 依次启动各个服务：

~~~bash
# 启动/停止
bin/zkServer.sh start/stop
# 日志在控制台输出启动
bin/zkServer.sh start-foreground
# 查看zookeeper状态
bin/zkServer.sh status
~~~

![image-20220830145659636](https://gitee.com/xiaotanke-lhj/image/raw/master/csdn/image-20220830145659636.png)

> 客户端命令：

~~~bash
# 查看所有命令
help
ls / # 查看当前下的子节点
create /node1 # 创建一个普通节点
create /node2 "数据" # 创建一个带有数据的节点
get /node1 # 获取这个节点下的数据




~~~

![image-20220830160526975](https://gitee.com/xiaotanke-lhj/image/raw/master/csdn/image-20220830160526975.png)























































