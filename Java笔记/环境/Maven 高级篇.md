`😀😀😀创作不易，各位看官点赞收藏.`

# Maven 高级篇

[TOC]

> Maven：是Apache组织维护的一款专门为Java项目提供构建和依赖管理的工具。

1. 构建过程：

- 清理（clean）：清理上一次构建信息，删除target文件夹。
- 编译（compile）：将Java源程序编译成class文件。
- 测试（test）：运行提前准备好的测试程序。
- 打包（package）：将项目打包成一个jar或war包。
- 安装（install）：把一个使用Maven打包成的jar包或war安装到Maven仓库。
- 部署（deploy）：将准备好的jar或war部署到服务器上运行。

![image-20221118200624966](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20221118200624966.png)

2. 依赖管理：管理项目中所需要的依赖jar包，从远程仓库去下载jar包，然后引入项目。也存在模块A需要使用模块B中的类，则通过Maven依赖管理引入模块B的jar包。

- jar 包下载：使用坐标，从中央远程仓库中下载需要的jar 包。
- jar 包依赖：模块与模块之间，使用Maven完成依赖的传递。
- jar 包冲突：通过对依赖管理配置，让某些jar 包不被导入。

3. Maven工作机制：

![image-20221118201329160](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20221118201329160.png)

## 1、Maven 安装

[下载地址:https://maven.apache.org/download.cgi](https://maven.apache.org/download.cgi)

![image-20221118201716404](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20221118201716404.png)

> 解压文件并配置Maven：

1. 解压：将压缩包解压到一个没有中文的路劲下，解压完成后如下图。

![image-20221118202220774](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20221118202220774.png)

2. 指定本地仓库位置：在 conf/setting.xml 中，修改默认的仓库位置。保存路劲需要是非中文路径。

![img](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/3445ebaa1dde0a3f3688aed48ae452d1.png)

3. 配置阿里云镜像：在 conf/setting.xml 中，修改默认镜像，提高jar 包的下载速度。

![img](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/bd0c3a4a9c13be395a91820d79e1a931.png)

~~~xml
<mirror>
	<id>alimaven</id>
	<name>aliyun maven</name>
	<url>http://maven.aliyun.com/nexus/content/groups/public/</url>
	<mirrorOf>central</mirrorOf>
</mirror>
~~~

4. 配置Maven 工程的JDK版本：在 conf/setting.xml 中，修改 profiles 标签，默认是jdk-1.5，修改成jdk-1.8。

~~~xml
<profile>
    <id>jdk-1.8</id>
    <activation>
        <activeByDefault>true</activeByDefault>
        <jdk>1.8</jdk>
    </activation>

    <properties>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>
        <maven.compiler.compilerVersion>1.8</maven.compiler.compilerVersion>
    </properties>
</profile>
~~~

> 配置系统Maven 环境变量：

1. 检查系统JAVA_HOME环境是否正确，因为Maven 需要Java环境，使用`java -version`命令

![image-20221119104416692](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20221119104416692.png)

2. 配置MAVEN_HOME环境，在系统变量中在Path中添加。

![image-20221119104753058](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20221119104753058.png)

3. 在Path下添加以下内容：

![image-20221119104852603](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20221119104852603.png)

4. 检查是否配置成功，使用 `mvn -v `命令

![image-20221119104935997](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20221119104935997.png)

## 2、Maven 核心概念

### 2.1、坐标

> 坐标：由三部分组成groupId、artifactId、version，它用于去定位中央仓库中jar 包的位置。

- groupId：公司的域名的逆序，例如`org.spring`。
- artifactId：在一个项目中对应的某个模块名称。
- version：模块名称的版本号。

```xml
<!-- 每个坐标使用dependency标签包裹起，这样就定位到一个jar 包，在projectlombok.org下有一个lombok模块，版本是1.18.24。maven就会去中央仓库去找，然后下载。-->
<dependency>
    <groupId>org.projectlombok</groupId>
    <artifactId>lombok</artifactId>
    <version>1.18.24</version>
</dependency>
```

> 坐标与Jar 存放本地仓库路径映射：从坐标能知道jar 包存放在本地仓库的位置。

例如：上面坐标映射为：`Maven仓库位置/org/projectlombok/lombok/1.18.24/lombok.jar`

### 2.2、基础操作

> 初始化maven项目：`mvn archetype:generate`

![image-20221119115710973](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20221119115710973.png)

![image-20221119115801878](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20221119115801878.png)

直接回车，然后设置对应项目的groupId、artifactId、version。

![image-20221119120044062](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20221119120044062.png)

![image-20221119121814494](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20221119121814494.png)

> 解读 pom.xml 文件：pom.xml是maven工程的核心配置文件，`Project Object Module`项目模型。

```xml
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <!-- maven2之后，使用4.0.0，默认-->
  <modelVersion>4.0.0</modelVersion>

  <!-- 项目的坐标信息-->
  <groupId>com.jx</groupId>
  <artifactId>maven-test</artifactId>
  <version>1.0-SNAPSHOT</version>

  <!-- 项目的打包方式：
        - jar：Java项目打成jar包
        - war：web项目，打成war在tomcat运行
        - pom：管理其它maven项目的pom文件
   -->
  <packaging>jar</packaging>

  <!-- 定义属性: 可以自定义属性，然后在其它地方通过${属性名使用}，也有maven自定义的属性-->
  <properties>
    <!-- 指定读取源码时的编码方式-->
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
  </properties>

  <!-- 引入的三方依赖jar包，根据gav坐标引入-->
  <dependencies>
    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>3.8.1</version>
      <scope>test</scope>
    </dependency>
  </dependencies>
</project>
```

> Maven 项目目录规则：这是Maven 约定的目录规则，不能改变(约定大于配置)。使用约定的目录结构，这样可以实现项目的自动化，自动构建、编译、打包等操作，因为知道每个目录下存放的是什么文件。

![image-20221119130126172](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20221119130126172.png)

==Maven 都会去依赖一个超级POM，它里面定义了一个项目结构，这样其它的POM也需要按照这个结构去定义。另外在构建时会生成一个target文件，去存放构建生成的文件。==

> 常用命令：

==运行 Maven 中构建相关命令时，必须进入到 pom.xm 所在目录，不然后会报错。==

1. `mvn clean`：删除 target 目录。
2. `mvn compile`：编译主程序，把编译结果输出到 target/classes 下。
3. `mvn test-compile`：编译测试程序，把编译结果输出到 target/classes 下。
4. `mvn test`：执行测试程序并将测试报告存放到 target/surefire-reports 下。
4. `mvn package`：打包，将程序打包成指定程序，jar 或 war 包，存放在target 下，打包成的名称是 artifactId-version.jar 。
4. `mvn install`：安装，将打包好的jar 包存放到本地仓库，存放路劲就是按在gav 规定的路径。
4. `mvn clean install -Dmaven.test.skip=true`：上面的命令可以一次执行多个，常见打包命令会去跳过 test 阶段。

### 2.3、依赖

> 依赖方式：

1. 第三方依赖：使用gav 坐标从中央仓库下载jar 包到本地，在项目中就可以直接使用。
2. 模块之间依赖：模块 A 可以依赖模块 B 中编写的 Java 程序，通过引入模块 B 的gac 坐标。

![image-20221119165356038](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20221119165356038.png)

> 依赖范围：在使用 gav 坐标时，可以指定一个`<scope>`标签去指定 jar 包的使用范围以及使用时间。

```xml
<dependency>
  <groupId>junit</groupId>
  <artifactId>junit</artifactId>
  <version>4.13.2</version>
  <!-- 指定 jar 的使用范围和时间-->
  <scope>test</scope>
</dependency>
```

| 范围            | main目录 | test目录 | 开发过程 | 部署服务器 |
| --------------- | -------- | -------- | -------- | ---------- |
| compile(默认值) | 有效     | 有效     | 有效     | 有效       |
| test            | 无效     | 有效     | 有效     | 无效       |
| provided        | 有效     | 有效     | 有效     | 无效       |

- compile：默认值，任何目录、时间都有效。
- test：只在test目录下有效，打包时不会将这个 jar 包打包。
- provided：以提供的，表示在部署的服务器上已经提供了这个 jar 包，可以不用打包，如果打包可能会出现冲突。

> 依赖传递：在一个项目中 A 依赖 B，然后 B 依赖 C，那么 A 可以直接使用 C，相当于是C传递给B，然后B传递给A。

`mvn dependency:tree`用于查看依赖的树形结构。如果在没有使用继承管理模式，从一个模块去依赖另一个模块需要将另一个模块进行 install 安装到本地仓库才能使用。

![image-20221126130052883](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20221126130052883.png)

==在 jar 包的使用范围是 test、provided 时，该依赖不能进行传递，因为 test 只在测试范围有效，test、provided 都不会打包。==

> 依赖排除：当引入的两个依赖中，存在相同依赖并且相同依赖的版本号不同就会出现依赖冲突，这就需要排除冲突的依赖。

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-web</artifactId>
    <version>2.6.8</version>
    <!-- 当前依赖下排除某些依赖-->
    <exclusions>
        <!-- 指定排除依赖的groupId和artifactId，不需要指定版本号-->
        <!-- 可以配置多个，当引入当前依赖时，会排除当前依赖下的排除的依赖-->
        <exclusion>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter</artifactId>
        </exclusion>
    </exclusions>
</dependency>
```

### 2.4、继承

> Maven 继承：模块 B 继承 模块 A ，A 作为父工程，B作为子工程，本质上是B继承了A的 pom.xml 配置。作用：就是在父工程中实现项目的统一依赖管理，具体就是管理依赖的版本信息。

- 创建父工程：使用创建命令创建父工程，父工程用来管理子工程的，所以在父工程中不编写代码且打包方式必须是 `pom` 方式。

```xml
<groupId>com.jx</groupId>
<artifactId>maven-parent</artifactId>
<packaging>pom</packaging>
<version>1.0-SNAPSHOT</version>
```

- 创建子工程：在父工程的文件目录下去创建Maven工程。

```xml
<!-- 创建的子工程会自动去指定自己的父工程，下面是父工程的gav坐标-->
<parent>
    <artifactId>maven-parent</artifactId>
    <groupId>com.jx</groupId>
    <version>1.0-SNAPSHOT</version>
</parent>
<modelVersion>4.0.0</modelVersion>

<!-- 当子工程的groupId和version与父工程相同时，可以在pom中省略，直接使用父工程的，当然也可以指定自己的-->
<artifactId>module-01</artifactId>
```

当创建子工程后，会在父工程的 pom 文件下发现 `modules` 标签。

```xml
<!-- 父工程下管理的子工程-->
<modules>
    <!-- 可以有多个子工程-->
    <module>module-01</module>
</modules>
```

> 父工程管理依赖版本：将项目中所用到的依赖在父工程中进行版本管理，然后所有子工程继承的父工程的依赖版本全都是一致的。

```xml
<!-- 父工程统一管理依赖，管理其版本号 -->
<!-- 在父工程中并不是实际引入了这些依赖，而是进行了管理，子工程需要什么依赖还是需要自己引入，只是不用写版本号了-->
<!-- 当父工程的版本发生变化，所有子工程引入了的依赖就对应变化-->
<dependencyManagement>
    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
            <version>2.6.8</version>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-redis</artifactId>
            <version>1.3.6.RELEASE</version>
        </dependency>
    </dependencies>
</dependencyManagement>
```

> 子工程引入父工程依赖：

```xml
<dependencies>
    <!-- 子工程引入符工程中管理的依赖，可以不用写版本号使用父工程版本，实现统一管理-->
    <!-- 也可以使用自己的版本号，添加自己版本就会覆盖父工程的版本，使用自己指定版本-->
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-web</artifactId>
    </dependency>
</dependencies>
```

> 父工程自定义属性变量：

```xml
<properties>
    <maven.compiler.source>8</maven.compiler.source>
    <maven.compiler.target>8</maven.compiler.target>
    <!-- 自定义属性变量，变量名就是标签名，值就是标签值-->
    <!-- 这样就可以实现版本的统一管理-->
    <spring.boot.version>2.5.6</spring.boot.version>
</properties>
<dependencyManagement>
    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
            <!-- 使用方式就是 ${变量名}-->
            <version>${spring.boot.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-redis</artifactId>
            <version>1.3.6.RELEASE</version>
        </dependency>
    </dependencies>
</dependencyManagement>
```

> IDEA配置Maven：需要在后面的 Override 打钩才能进行配置。

![image-20221126151250624](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20221126151250624.png)

### 2.5、生命周期

​	在 Maven 中为了自动化构建项目，设定了三个生命周期，每个生命周期中的每个环节都是构建过程中的一个操作。==每个生命周期他有好多个环节，不管从生命周期的哪个环节下命令，他都是从生命周期的最开始环节开始执行，直到执行到下命令的环节。==Maven中有三个主要的生命周期Clean、Site、Default。

![image-20221126162528353](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20221126162528353.png)

![image-20221126162604997](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20221126162604997.png)

## 3、Maven 深入

### 3.1、Spring Boot  打包

> spring-boot-maven-plugin打包插件：在没有使用这个 Maven 打包插件时，会打成一个普通的 jar 包，没有打包依赖也没有指定main-class，所以无法直接运行。使用 Maven 打包插件可以将 Spring Boot 项目打包成一个可以运行的 jar 包，==它可以自动打包所依赖的第三方 jar 包和内置的 Tomcat 中的 jar 包。==

~~~xml
<!-- 项目构建时环境-->
<build>
    <!-- 打包后的文件名，一般是模块名称-->
    <finalName>${project.artifactId}</finalName>
    <!-- 打包插件，可以有多个-->
    <plugins>
        <!-- 具体的某个插件-->
        <plugin>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-maven-plugin</artifactId>
            <version>2.0.1.RELEASE</version>
        </plugin>
    </plugins>
</build>
~~~

<font color=red>注意：</font>`mvn clean package spring-boot:repackage`打包命令，`spring-boot:repackage`命令必须在前两个命令前面，先要打成一个普通的 jar 包，不然后报错`Source file must not be null`。

> 设置默认执行的goal：每次执行都要去执行三个命令，可以在 execution 标签内指定默认执行的 goal。

```xml
<!-- 项目构建时环境-->
<build>
    <!-- 打包后的文件名，一般是模块名称-->
    <finalName>${project.artifactId}</finalName>
    <!-- 打包插件，可以有多个-->
    <plugins>
        <!-- 具体的某个插件-->
        <plugin>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-maven-plugin</artifactId>
            <version>2.6.8</version>
            <!-- 指定默认的执行目标-->
            <executions>
                <execution>
                    <goals>
                        <goal>repackage</goal>
                    </goals>
                </execution>
            </executions>
        </plugin>
    </plugins>
</build>
```

<font color=red>注意：</font>采用如上配置时，只需要运行mvn的package阶段即可，由于repackage目标默认绑定在default生命周期的package阶段，所以运行mvn的package时，便会按照如上配置执行repackage目标。

### 3.2、超级 POM

> 超级POM：如果pom.xml没有指定父工程，它会默认去继承一个超级POM文件，类似于Java中的Object类。在超级POM定义了一些默认的配置，文件存放位置、打包编译输出位置以及一些基本信息。

![image-20221127135135785](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20221127135135785.png)

> 有效POM：在Maven 实际的构建过程中，实际是通过有效POM去完成构建的，而有效POM是当前pom.xml文件、父pom.xml、超级pom.xml文件组合起来的，而子pom.xml会覆盖父pom.xml文件的内容，相当于离当前pom.xml越近，优先级越高，没有设置就是用默认的。

~~~xml
<!-- 超级pom文件-->
<project>
    <modelVersion>4.0.0</modelVersion>
    
    <!-- 镜像仓库-->
    <repositories>
        <repository>
            <id>central</id>
            <name>Central Repository</name>
            <url>https://repo.maven.apache.org/maven2</url>
            <layout>default</layout>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
        </repository>
    </repositories>

    <!-- 插件仓库-->
    <pluginRepositories>
        <pluginRepository>
            <id>central</id>
            <name>Central Repository</name>
            <url>https://repo.maven.apache.org/maven2</url>
            <layout>default</layout>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
            <releases>
                <updatePolicy>never</updatePolicy>
            </releases>
        </pluginRepository>
    </pluginRepositories>

    <!-- 构建环境-->
    <build>
        <!-- 构建输出目录-->
        <directory>${project.basedir}/target</directory>
        <!-- 构建源码输出目录-->
        <outputDirectory>${project.build.directory}/classes</outputDirectory>
        <!-- 打包的名称-->
        <finalName>${project.artifactId}-${project.version}</finalName>
        <!-- 测试输出目录-->
        <testOutputDirectory>${project.build.directory}/test-classes</testOutputDirectory>
        <!-- 源码存放位置-->
        <sourceDirectory>${project.basedir}/src/main/java</sourceDirectory>
        <scriptSourceDirectory>${project.basedir}/src/main/scripts</scriptSourceDirectory>
        <!-- 测试源码存放位置-->
        <testSourceDirectory>${project.basedir}/src/test/java</testSourceDirectory>
        <!-- 资源目录-->
        <resources>
            <resource>
                <directory>${project.basedir}/src/main/resources</directory>
            </resource>
        </resources>
        <!-- 测试资源目录-->
        <testResources>
            <testResource>
                <directory>${project.basedir}/src/test/resources</directory>
            </testResource>
        </testResources>
        <!-- 插件管理-->
        <pluginManagement>
            <!-- NOTE: These plugins will be removed from future versions of the super POM -->
            <!-- They are kept for the moment as they are very unlikely to conflict with lifecycle mappings (MNG-4453) -->
            <plugins>
                <plugin>
                    <artifactId>maven-antrun-plugin</artifactId>
                    <version>1.3</version>
                </plugin>
                <plugin>
                    <artifactId>maven-assembly-plugin</artifactId>
                    <version>2.2-beta-5</version>
                </plugin>
                <plugin>
                    <artifactId>maven-dependency-plugin</artifactId>
                    <version>2.8</version>
                </plugin>
                <plugin>
                    <artifactId>maven-release-plugin</artifactId>
                    <version>2.5.3</version>
                </plugin>
            </plugins>
        </pluginManagement>
    </build>

    <reporting>
        <outputDirectory>${project.build.directory}/site</outputDirectory>
    </reporting>

    <profiles>
        <!-- NOTE: The release profile will be removed from future versions of the super POM -->
        <profile>
            <id>release-profile</id>

            <activation>
                <property>
                    <name>performRelease</name>
                    <value>true</value>
                </property>
            </activation>

            <build>
                <plugins>
                    <plugin>
                        <inherited>true</inherited>
                        <artifactId>maven-source-plugin</artifactId>
                        <executions>
                            <execution>
                                <id>attach-sources</id>
                                <goals>
                                    <goal>jar-no-fork</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                    <plugin>
                        <inherited>true</inherited>
                        <artifactId>maven-javadoc-plugin</artifactId>
                        <executions>
                            <execution>
                                <id>attach-javadocs</id>
                                <goals>
                                    <goal>jar</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                    <plugin>
                        <inherited>true</inherited>
                        <artifactId>maven-deploy-plugin</artifactId>
                        <configuration>
                            <updateReleaseInfo>true</updateReleaseInfo>
                        </configuration>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>
</project>
~~~

### 3.3、build 标签

> build 标签：一般用于配置构建项目时的一些参数配置和一些打包插件，如果没有配置一般使用超级POM的默认配置。

<font color=red>常用配置：
</font>

~~~xml
<build>
    	<!-- 在属性引用中，以project表示当前目录，后面继续点来表示当前目录下的标签值-->
    	<!-- 如果是数组，可以使用下标-->
        <!-- 构建输出目录-->
        <directory>${project.basedir}/target</directory>
        <!-- 构建源码输出目录-->
        <outputDirectory>${project.build.directory}/classes</outputDirectory>
        <!-- 打包的名称-->
        <finalName>${project.artifactId}-${project.version}</finalName>
        <!-- 测试输出目录-->
        <testOutputDirectory>${project.build.directory}/test-classes</testOutputDirectory>
        <!-- 源码存放位置-->
        <sourceDirectory>${project.basedir}/src/main/java</sourceDirectory>
        <scriptSourceDirectory>${project.basedir}/src/main/scripts</scriptSourceDirectory>
        <!-- 测试源码存放位置-->
        <testSourceDirectory>${project.basedir}/src/test/java</testSourceDirectory>
        <!-- 资源目录-->
        <resources>
            <resource>
                <directory>${project.basedir}/src/main/resources</directory>
            </resource>
        </resources>
        <!-- 测试资源目录-->
        <testResources>
            <testResource>
                <directory>${project.basedir}/src/test/resources</directory>
            </testResource>
        </testResources>
</build>
~~~

> plugins 插件标签：用来指定项目构建时使用的插件。

```xml
<build>
    <!-- 打包后的文件名，一般是模块名称-->
    <!-- 在属性引用中，以project表示当前目录，后面继续点来表示当前目录下的标签值-->
    <!-- 如果是数组，可以使用下标-->
    <finalName>${project.artifactId}</finalName>
    <!-- 打包插件，可以有多个-->
    <plugins>
        <!-- 具体的某个插件-->
        <plugin>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-maven-plugin</artifactId>
            <version>2.6.8</version>
            <!-- 指定默认的执行目标-->
            <executions>
                <execution>
                    <!-- 唯一标识-->
                    <id>boot-maven-plugin</id>
                    <!-- 关联的Maven生命周期的一个环节-->
                    <phase>package</phase>
                    <!-- 当执行到maven对应的生命周期时，插件执行的目标，可以配置多个-->
                    <goals>
                        <goal>repackage</goal>
                    </goals>
                </execution>
            </executions>
        </plugin>
    </plugins>
</build>
```

<font color=red>插件执行过程：</font>可以在 `configuration`标签中去自定义自己插件的执行过程，不同的插件的执行过程不一致，因插件不同有不同的配置。

> 指定 JDK 版本：我们在 `setting.xml`指定了 JDK 的版本，但是也可以在插件中去配置，这样无论项目在那个Maven环境下都可以使用指定 JDK 版本。

```xml
<!-- 这个插件是超级POM中管理的一个插件-->
<plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-compiler-plugin</artifactId>
    <!-- 指定 JDK 版本，只有这个插件可以配置-->
    <configuration>
        <!-- 调用Java编译器传入的参数，要求编译器使用指定 JDK 版本来兼容我们的源码-->
        <source>1.8</source>
        <!-- 也是编译器的一个参数，生成特定版本的类文件-->
        <target>1.8</target>
        <encoding>UTF-8</encoding>
    </configuration>
</plugin>
```

<font color=red>注意：</font>也可以通过在属性定义中去指定 JDK 编译的版本。

```xml
<properties>
    <maven.compiler.source>8</maven.compiler.source>
    <maven.compiler.target>8</maven.compiler.target>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
</properties>
```

### 3.4、深入依赖

> import：在Maven 中只能继承一个父pom，如果需要继承多个管理依赖的pom，可以使用import 来引入。

```xml
<!-- 这样也相当于是继承了一个父依赖-->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-dependencies</artifactId>
    <type>pom</type>
    <scope>import</scope>
    <version>2.6.8</version>
</dependency>
```

<font color=red>注意：
</font>

- 引入的依赖必须是pom打包类型的依赖，就是一个管理依赖的依赖。
- 这个引入的位置必须是`dependencyManagement`标签中。

> System：本地依赖，如果需要引入本地磁盘上的依赖，可以使用这个类型，一般不用这种方式。

```xml
<dependency>
    <groupId>xxx</groupId>
    <artifactId>xxx</artifactId>
    <version>xxxx</version>
    <!-- jar在磁盘的位置-->
    <systemPath>例如d:/xxxx.jar</systemPath>
    <scope>system</scope>
</dependency>
```

> runtime：运行时依赖，表示这个依赖在运行时才起作用，在编译时没有作用。

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-devtools</artifactId>
    <version>${spring.boot.version}</version>
    <!-- 只有在项目跑起来，这个依赖才会被用到-->
    <scope>runtime</scope>
</dependency>
```

> 依赖版本仲裁：当在依赖中存在依赖同一个jar，但是依赖对应jar 包的版本不同，这时在Maven 中使用了依赖版本仲裁。

- 最短路劲：在依赖的路径中，会使用离当前项目最近的依赖，下面pro25 会使用log4j 1.2.12 这个版本。

![image-20221127160057349](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20221127160057349.png)

- 最先声明：当依赖的路径中距离相同时，谁先声明依赖就是用哪个依赖。

![image-20221127160246402](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20221127160246402.png)

### 3.5、profile 标签

> profile：可以设置成在不同的环境下去激活不同的配置。例如生产环境、测试环境、开发环境等，通过配置不同profile 去启用不同环境配置。其实pom就是一个profile，在project 标签下除了`modelVersion`标签不能写，其它标签都是可以写到profile 中的，一般吧profile 标签写到pom 文件的最后，以便不同环境去覆盖之前配置好的环境。

- setting.xml 配置：可以在Maven 的配置文件中去配置对应profile 标签，这样会全局生效。
- pom.xml 文件：

```xml
<!-- 配置多个环境-->
<profiles>
    <!-- 某个环境-->
    <profile>
        <!-- 唯一标识，在使用命令行打包时可以通过-D[id名]来激活某个环境-->
        <id>dev</id>
        <!-- 激活方式-->
        <activation>
            <!-- 默认被激活-->
            <activeByDefault>true</activeByDefault>
            <!-- 当环境时1.8时，改环境被激活-->
            <jdk>1.8</jdk>
        </activation>

        <!-- 下面就是改环境下对应的配置，会覆盖profile之外的配置，这个-->
        <properties>
            <profiles.active>dev</profiles.active>
            <nacos.namespace>test</nacos.namespace>
            <nacos.server-addr>127.0.0.0:8848</nacos.server-addr>
        </properties>
    </profile>
</profiles>
```

> 基于环境激活profile：Maven 可以是基于运行环境去激活对应的配置环境，一个profile 被激活那么profile 定义的配置会覆盖原来的配置。

```xml

<profile>
    <id>test</id>
    <!-- 激活条件-->
    <activation>
        <!-- 关闭默认激活-->
        <activeByDefault>false</activeByDefault>
        <jdk>1.8</jdk>
        <os>
            <name>Window XP</name>
        </os>
        <property>
            <name>mavenVersion</name>
            <value>3.8.6</value>
        </property>
    </activation>
</profile>
```

- Maven 3.2.2 之前：如果有一个条件满足就会被激活。
- Maven 3.2.3 开始：要配置的所有条件全部满足才会被激活。

> 资源过滤：在配置文件中可以去使用pom.xml 文件声明的变量，properties 文件使用`${xxxx}`，yaml 文件使用`@xxxx@`引用。

- 配置资源过滤：

~~~xml
<build>
    <resources>
        <resource>
            <directory>${project.basedir}/src/main/resources</directory>
            <!-- 开启资源过滤-->
            <filtering>true</filtering>
        </resource>
    </resources>
</build>
~~~

```yaml
spring:
  profiles:
    # 使用pom文件声明的属性
    active: @profiles.active@
  application:
    name: module-01
  cloud:
    nacos:
      discovery:
        server-addr: @nacos.server-addr@
```

![image-20221127203027447](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20221127203027447.png)

## 4、搭建Nexus 私服

> Nexus私服：一个特殊的远程仓库，它是架设在局域网内的仓库服务，供局域网内的开发人员使用。 当Maven 需要下载构建的使用， 它先从私服请求，如果私服上没有的话，则从外部的远程仓库下载，然后缓存在私服上 ，再为Maven的下载请求提供服务。

### 4.1、安装与启动

[下载地址](https://blog.csdn.net/u010741112/article/details/103886347?ops_request_misc=&request_id=&biz_id=102&utm_term=nexus%E4%B8%8B%E8%BD%BD&utm_medium=distribute.pc_search_result.none-task-blog-2~all~sobaiduweb~default-0-103886347.142^v66^control,201^v3^add_ask,213^v2^t3_control1&spm=1018.2226.3001.4187)，下载后直接服务器然后解压。

![image-20221128132045743](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20221128132045743.png)

1. 修改默认端口：在`etc/nexus-default.properties`下修改。

![image-20221128132247698](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20221128132247698.png)

2. 启动nexus 服务：在bin 目录下执行`./nexus start`，检查启动状态：`./nexus status`

![image-20221128164207683](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20221128164207683.png)

3. 通过浏览器访问，`ip:端口号`

![image-20221128164531779](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20221128164531779.png)

4. 登录

![image-20221128164522214](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20221128164522214.png)

5. 配置

![image-20221128164932057](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20221128164932057.png)

### 4.2、仓库

> 仓库类型：nexus 一共有三种仓库类型proxy、group、hosted。

| 类型   | 作用                               |
| ------ | ---------------------------------- |
| proxy  | 某个远程仓库的代理，一般是中央仓库 |
| group  | 存放：第三方jar 包的仓库           |
| hosted | 存放：团队自己开发的仓库           |

![image-20221128172913298](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20221128172913298.png)

> 修改代理仓库镜像为阿里云镜像：

![image-20221128182629526](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20221128182629526.png)

![image-20221128182749793](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20221128182749793.png)

~~~xml
http://maven.aliyun.com/nexus/content/groups/public/
~~~

### 4.3、使用

<font color=red>从私服仓库下载jar 包：</font>

- 修改Maven 的镜像为私服镜像：

![image-20221128183421079](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20221128183421079.png)

~~~xml
<mirror>
    <id>nexusmaven</id>
    <name>nexus maven</name>
    <url>http://192.168.32.128:11234/repository/maven-central/</url>
    <mirrorOf>central</mirrorOf>
</mirror>
~~~

- 私服访问密码：如果禁止了匿名访问，必须在setting.xml 文件中指定访问密码。

~~~xml
<servers>
    <server>
        <!-- 和配置镜像id相同-->
        <id>nexusmaven</id>
        <username>admin</username>
        <password>liujixing</password>
    </server>
</servers>
~~~

- 测试下载jar 包

![image-20221128221402725](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20221128221402725.png)

![image-20221128221614499](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20221128221614499.png)

<font color=red>上传jar 包：</font>将自己打包好的jar 包上传到私服中，提供给其他人下载。

- 在pom.xml 添加私服的地址

```xml
<distributionManagement>
    <snapshotRepository>
        <!-- id和name需要和setting.xml 配置的镜像一致-->
        <id>nexusmaven</id>
        <name>nexus maven</name>
        <!-- 私服中存放自己jar 的仓库地址-->
        <url>http://192.168.32.128:11234/repository/maven-snapshots/</url>
    </snapshotRepository>
</distributionManagement>
```

- 使用 `mvn deploy`将jar 包部署到私服上

​	![image-20221129202019408](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20221129202019408.png)

<font color=red>依赖私服上的jar 包：</font>在Maven 中默认访问的仓库是setting.xml 部署的public 仓库，如果想使用别人私有的jar 包需要在仓库中配置一个`repository`。

```xml
<repositories>
    <repository>
        <!-- 和setting.xml 配置相同-->
        <id>nexusmaven</id>
        <name>nexus maven</name>
        <!-- 别人存放jar 的仓库-->
        <url>http://192.168.32.128:11234/repository/maven-releases/</url>
        <snapshots>
            <enabled>true</enabled>
        </snapshots>
        <releases>
            <enabled>true</enabled>
        </releases>
    </repository>
</repositories>
```

最近发现除了腾讯云和阿里云之外的一种好用的云服务器，那就是三丰云云服务器，它拥有众多的功能，其中一个就是可以免费试用一款云服务器，下面介绍它的使用方式。

[官方地址:https://www.sanfengyun.com/](https://www.sanfengyun.com/)

![image-20230307102210797](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20230307102210797-16781568307221-16812649059123.png)

然后进行一个实名认证和微信的绑定就可以申请一个 1c1g的免费服务器。

![image-20230307102330457](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20230307102330457-16781568307223-16812649059111.png)



三丰云是北京太极三丰云计算有限公司旗下网络服务品牌，十八年IDC老兵团队蛰伏三年后投资千万于2018年10月1日创建。公司致力于为大众提供优质的互联网基础服务和物联网服务，包括：域名注册、虚拟主机、云服务器、主机托管租用、CDN网站加速、物联网应用等服务。以帮助客户轻松、 高速、高效的应用互联网/物联网，提高企业竞争能力。，它拥有众多的功能，其中一个就是可以免费试用一款云服务器，下面介绍它的使用方式。

[官方地址:https://www.sanfengyun.com/](https://www.sanfengyun.com/)
