# Ajax学习笔记

AJAX 是开发者的梦想，因为您能够：

- 不刷新页面更新网页
- 在页面加载后从服务器请求数据
- 在页面加载后从服务器接收数据
- 在后台向服务器发送数据

## 1、原生`js`实现`ajax`

```js
<script>
    var box = $(".box");
    $("button").click(function () {
        // 创建ajax对象
        var xhr = new XMLHttpRequest();
        // 设置响应体的类型，以json字符串的结果会自动转换成js对象
        xhr.responseType = "json";
        /*
           这个方法第一个参数是访问的类型，第二个参数是请求的url
           初始化，可以在url后面跟加参数
         */
        xhr.open("Get","http://localhost/ssm/test");
        // 设置请求头
        xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
        // 发送请求，如果请求方式时post请求，在send()方法中设置请求参数
        // xhr.send("a=100&b=200");
        xhr.send();

        // 设置请求超时的时间，2s
        xhr.timeout = 2000;
        // 当请求超时时的回调函数
        xhr.ontimeout = function () {
            alert("请求超时!");
        };
        // 网络异常时的回调函数
        xhr.onerror = function () {
          alert("您的网络异常!");
        };
        // 设置取消请求
        // xhr.abort();

        // 获取响应，当xhr.readyState为4时，服务器就全部响应完毕
        xhr.onreadystatechange = function () {
            if(xhr.readyState === 4){
                // 状态码以2开头的就是成功响应
                if(xhr.status >= 200 && xhr.status < 300){
                    box.html(xhr.response.userName);
                }
            }
        };
    });
</script>
```



## 2、`jQuery`实现`ajax`

<font color=red>get()方法：
</font>通过get请求的方式请求。

```js
$(function () {
    // 第一个方法$.get()，通过方法来请求
    var box = $(".box");
    $("button").click(function () {
        $.get("http://localhost/ssm/test",{
            a : 100,
            b : 200
        },function (data) {
            // 当请求成功时执行的回调函数
            console.log(data);
            box.html(data.userName);
        },"json");
    });
});
```

四个参数：

- `url`：请求的地址。
- data：参数，以 `{key:value}`的形式传入，多个值之间使用逗号隔开。
- function(data){}：请求成功时的回调函数，返回的数据封装到data中，以参数形式传入。
- 返回的数据类型：设置返回的数据类型，以字符串的形式传入，如果是`json`则`json`字符串的数据会自动转换成`js`对象。

==该方法的第一个参数是必须的，后面的三个参数是可选值。该方法请求成功时可调用回调函数。如果需要在出错时执行函数，请使用 `$.ajax`，没有出错的回调函数。==

<font color=red>getJSON()：
</font>

```js
// 第二个方法$.getJSON()
$("button").click(function () {
    $.getJSON("http://localhost/ssm/test",
        {
            a : 100,
            b : 200
        },
        function (data) {
            console.log(data);
            box.html(data.userName);
        }
    );
});
```

==这个方法和get()方法类似，只是不用指定返回的数据类型，直接将返回的`json`字符串自动转换成`js`对象。通过这种方式同时解决了跨域的问题。==

<font color=red>post()：
</font>通过post请求的方式请求。

```js
// 第三个方法$.post()
$("button").click(function () {
    $.post("http://localhost:80/ssm/test1",
        {
            a : 100,
            b : 200
        },
        function (data) {
            console.log(data);
            box.html(data.userName);
        },"json");
});
```

==参数与get()方法的请求类似。==

<font color=red>ajax()：</font>

```js
$("button").click(function () {
    $.ajax(
        {
            type: "get",   // 请求的类型
            url: "http://localhost/ssm/test",  // 请求的地址
            data: "a=100&b=200",  // 请求的参数
            dataType: "json",  // 返回数据的类型
            timeout: 2000,  // 超时时间，2s
            success: function (data) {  // 请求成功时的回调函数
                console.log(data);
                box.html(data.userName);
            },
            error: function () {  // 请求失败的回调函数
                console.log("您的请求不存在!");
            }
        }
    );
});
```

==通常使用这种方式将参数作为一个对象传入，`type`表示访问的类型，`url`表示请求的路径，`data`表示参数，`success`表示请求成功时的回调函数，后端返回的数据会封装到data中，`dataType`表示设置返回数据类型，`json`是将返回的`json`字符串转换成`js`对象。==

<font color=red>load()：
</font>载入远程 HTML 文件代码并插入至 DOM 中。

```js
$("button").click(function () {
    // 将远程请求的数据加载到指定的元素中，可以是数据中的某一个元素
    // 通常用于刷新页面的中的某个部分，在请求地址后面给一个页面的中元素的选择器
    box.load("http://localhost/ssm/index.html #content");
});
```



## 3、跨域问题

==一般跨域问题是由后端程序员解决的问题，前端也可以解决，但是不方便，下面介绍几种跨域问题的解决方案。==

1. 使用`jsonp`的方式方式请求，这是在前端解决跨域请求，但是这种方式只支持get方式的请求，不推荐使用。
2. 设置响应头，在后端设置一个响应头让它支持跨域请求，固定格式。

```java
response.setHeader("Access-Control-Allow-Origin","*");
```

3. `spring-mvc`中解决跨域问题，可以在controller的请求上面添加一个注解`@CrossOrigin`，这样也可以解决跨域问题。

```java
@RequestMapping(value = "/test",method = RequestMethod.GET)
@ResponseBody
@CrossOrigin
public String test(HttpServletResponse response){
    // 设置响应头，解决跨域问题
    response.setHeader("Access-Control-Allow-Origin","*");
    User user = new User(1, "张三", "123");
    return JSON.toJSONString(user);
}
```

4. 设置过滤器，将需要跨域的请求进行拦截筛选。

~~~java

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


@Component
public class CrossFilter implements Filter {
    private static Logger logger = LoggerFactory.getLogger(CrossFilter.class);
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }
    @Override
    public void destroy() {
    }
    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {
      	// 设置请求跨域
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        httpServletRequest.getSession();
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        httpResponse.setHeader("Access-Control-Allow-Origin", "*");
        httpResponse.setHeader("Access-Control-Allow-Methods", "*");
        httpResponse.setHeader("Access-Control-Max-Age", "3600");
        httpResponse.setHeader("Access-Control-Allow-Headers",
                "Origin, X-Requested-With, Content-Type, Accept, Connection, User-Agent, Cookie");
        httpResponse.setHeader("Access-Control-Allow-Credentials", "true");
        httpResponse.setHeader("Content-type", "application/json");
        httpResponse.setHeader("Cache-Control", "no-cache, must-revalidate");
        chain.doFilter(request, httpResponse);
    }
}
~~~



最近发现除了腾讯云和阿里云之外的一种好用的云服务器，那就是三丰云云服务器，它拥有众多的功能，其中一个就是可以免费试用一款云服务器，下面介绍它的使用方式。

[官方地址:https://www.sanfengyun.com/](https://www.sanfengyun.com/)

![image-20230307102210797](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20230307102210797-16781568307221-16845143036393.png)

然后进行一个实名认证和微信的绑定就可以申请一个 1c1g的免费服务器。

![image-20230307102330457](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20230307102330457-16781568307223-16845143036391.png)



三丰云是北京太极三丰云计算有限公司旗下网络服务品牌，十八年IDC老兵团队蛰伏三年后投资千万于2018年10月1日创建。公司致力于为大众提供优质的互联网基础服务和物联网服务，包括：域名注册、虚拟主机、云服务器、主机托管租用、CDN网站加速、物联网应用等服务。以帮助客户轻松、 高速、高效的应用互联网/物联网，提高企业竞争能力。，它拥有众多的功能，其中一个就是可以免费试用一款云服务器，下面介绍它的使用方式。

[官方地址:https://www.sanfengyun.com/](https://www.sanfengyun.com/)



