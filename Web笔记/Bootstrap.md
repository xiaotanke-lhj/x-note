# boostrap框架学习笔记

- 它是目前世界最受欢迎的响应式、移动设备优先的门户和应用前端框架
- 提供了高质量的HTML、CSS和JS，让前端开发更加方便
- 中文文档地址：[https://v4.bootcss.com/docs/getting-started/introduction/](https://v4.bootcss.com/docs/getting-started/introduction/)



**浏览器与设备的兼容**

- 支持几乎所有的浏览器

- 支持浏览器列表

![image-20210802223059883](C:\Users\LHJ16\AppData\Roaming\Typora\typora-user-images\image-20210802223059883.png)

- 支持的移动设备（Android、IOS等）

- 支持桌面的浏览器（多数）

## 1、布局容器

​	Bootstrap 需要为页面内容和栅格系统包裹一个 `.container` 容器。我们提供了两个作此用处的类。注意，由于 `padding` 等属性的原因，这两种 容器类不能互相嵌套。

- `.container` ：类用于固定宽度并支持响应式布局的容器，width为100%。也称固定容器

- `.container-fluid` ：类用于 100% 宽度，占据全部视口（viewport）的容器，width为auto。也称流体容器

<font color=red>固定容器：
</font>

==固定容器的大小是根据设备的大小设置的，它有一个阈值，当阈值为一个值时，容器的大小为一个值。==

| 阈值（设备大小）          | 容器大小                 |
| ------------------------- | ------------------------ |
| 大于1200px                | 1170px                   |
| 大于等于992px，小于1200px | 970px                    |
| 大于等于768px，小于992px  | 750px                    |
| 小于768px                 | auto，相当于一个流体容器 |

<font color=red>栅格系统：
</font>

这是栅格系统的参数：

![image-20211104130915714](https://i.loli.net/2021/11/04/uVoa82wMnO5Utxb.png)

==相当于是每一行中，将width平均分为12列，当设备的大小改变时，会根据每一列占的比例进行变化。==

```html
<div class="container">
    <!--这是每一行，无论设备的大小，都是按照比例进行分配的-->
    <div class="row">
        <!--这个div在这一行中占用了8列-->
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8" style="background: pink">我站了8列</div>
        <!--这个div在这一行中占用了4列-->
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="background: green">我站了4列</div>
    </div>
</div>
```











