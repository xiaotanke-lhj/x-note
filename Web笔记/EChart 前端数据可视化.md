# `EChart` 前端数据可视化

​	一个`JS`插件库，性能好，可兼容PC和移动端，并且兼容各种浏览器，提供多种图表，且可以自己定制图标。

[官网:https://echarts.apache.org/zh/index.html](https://echarts.apache.org/zh/index.html)

## 1、使用方式

- 下载并引入`echarts`插件，可以在官网上下载，也可以使用在线的`CDN`
- 准备一个具有宽度和高度的DOM容器
- 初始化`echarts`实例对象
- 指定配置项和数据
- 将配置项和数据指定给`echarts`实例对象

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
<!--    引入echarts-->
    <script src="https://cdn.bootcdn.net/ajax/libs/echarts/5.1.2/echarts.common.min.js"></script>
    <script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <style>
        div{
            width: 500px;
            height: 400px;
        }
    </style>
</head>
<body>
<!--    创建一个具有宽高的DOM元素-->
    <div>

    </div>
</body>
<script>
    // 实例一个echarts对象，这里一定要是DOM对象，不能是jquery获取的jquery的对象
    var myEcharts = echarts.init($("div")[0]);
    // 指定数据项和数据
    var option = {
        title: {
            text: 'ECharts 入门示例'
        },
        tooltip: {},
        legend: {
            data: ['销量']
        },
        xAxis: {
            data: ['衬衫', '羊毛衫', '雪纺衫', '裤子', '高跟鞋', '袜子']
        },
        yAxis: {},
        series: [
            {
                name: '销量',
                type: 'bar',
                data: [5, 1000, 36, 10, 10, 20]
            }
        ]
    };
    // 将配置项指定给echarts对象
    myEcharts.setOption(option);
</script>

</html>
```

## 2、`Echarts`的基础配置

### 2.1、title配置

```js
title: {
    // 标题的内容，支持\n换行
    text: 'ECharts 入门示例',
    // 是否显示标题，默认是true
    show: true,
    // 设置点击标题的跳转链接
    link: "https://www.baidu.com",
    // 设置新链接的打开方式，self当前窗口打开，blank新窗口打开（默认）
    target: "blank",
    // 设置标题的样式，值是一个对象，设置对象的样式，属性名是驼峰
    textStyle: {
        color: "red",
        fontSize: "18px",
    },
    // 设置图标的二级标题，支持\n换行
    subtext: "二级标题",
    // 设置二级标题的跳转链接，与一级标题的链接一样
    sublink: "http://www.yujiangg.com",
    subtarget: "blank",
    subtextStyle: {},
    // 整体（包括 text 和 subtext）的水平对齐
    // 可选值：'auto'、'left'、'right'、'center'。一般是auto
    textAlign: "auto",
    // 整体（包括 text 和 subtext）的垂直对齐。
    // 可选值：'auto'、'top'、'bottom'、'middle'。一般是auto
    textVerticalAlign: "auto",
    // 标题内边距，单位px，默认各方向内边距为5，接受数组分别设定上右下左边距。
    // padding: [
    //   0,1,2,3
    // ],
    // 主副标题之间的间距。
    // itemGap: 10,
    // title 组件离容器左侧的距离。
    //left 的值可以是像 20 这样的具体像素值，可以是像 '20%' 这样相对于容器高宽的百分比，也可以是 'left', 'center', 'right'。
    //如果 left 的值为'left', 'center', 'right'，组件会根据相应的位置自动对齐。
    left: "center",
    // 还有top、right、bottom和上面这个类似
},
```







