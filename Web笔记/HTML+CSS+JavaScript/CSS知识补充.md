[TOC]

## 1、颜色渐变

​	通过渐变可以设置一些复杂的背景颜色，可以实现从一个颜色向其它颜色过渡的效果。

<font color=red>线性渐变：</font>颜色沿着一条直线发生变化，渐变是背景图片的渐变，所以也是通过background-image属性设置的。

格式：`background-image: linear-gradient(red,yellow);`

- 值linear-gradient设置颜色渐变，上面的示例就是红色在开头，

  黄色在结尾，中间是过渡区域，我们可以指定一个渐变的方向，默认是向下渐变。

- to left（向左渐变），to right（向右渐变），to bottom（向下渐变）。

```css
background-image: linear-gradient(to left,red,yellow);
```

- 也可以指定角度，以上为0度来计算。

```css
background-image: linear-gradient(45deg,red,yellow);
```

上面是向45度方向渐变。

- 渐变可以指定多个颜色，多个颜色下默认情况下是平均分配的，也可以指定每个渐变的分布。

```css
background-image: linear-gradient(red 20px,yellow 180px);
```

颜色后面指定的是颜色渐变开始的位置。

<font color=red>径向渐变：</font>沿着半径进行颜色的渐变。

```css
background-image: radial-gradient(red,yellow);
```

1. 默认情况下，径向渐变的形状根据元素的形状来计算。

正方形--》圆形渐变，长方形--》椭圆形渐变

<img src="https://i.loli.net/2021/08/21/Y4pzPCXEScfHner.png" alt="image-20210821162301529" style="zoom:80%;" />

2. 也可以指定渐变的大小：

- circle：渐变的形状是圆形。
- ellipse：渐变的形状是椭圆形。
- 可以是自己指定的值。

```css
background-image: radial-gradient(circle,red,yellow);
background-image: radial-gradient(100px 100px,red,yellow);
```

3. 也可以指定见渐变的位置：就是指定渐变的原点位置，也可以指定方位词。

```css
background-image: radial-gradient(100px 100px at 0 0,red,yellow);
background-image: radial-gradient(100px 100px at center,red,yellow);
```



## 2、设置网页标题的图标

```html
<head>
   <meta charset="UTF-8">
   <title>设置网页标题的图标</title>
   <link rel="icon" href="favicon.ico">
</head>
```

​	需要一张`ico`图标，可以去制作`ico`图标的网站制作，一般把名称命名为`favicon.ico`，然后放在项目的根目录下。最后在head标签中使用link标签引入，rel必须是icon类型，`href`指定图标的路径。

![image-20210824214142163](https://i.loli.net/2021/08/24/nlr872tBEG1XLHp.png)



## 3、动画（animation）

### 1、过渡

1. transition-property属性：指定要执行过渡的属性。

- 可以指定多个属性，使用逗号隔开。
- 如果所有属性都需要过渡，则使用all关键字。
- 大部分属性都是支持过渡效果的，过渡必须是从一个有效值向另一个有效值进行过渡。

```css
transition-property: margin-left,width,height;
```

==注意：它必须和transition-duration联合使用才有效果。==

2. transition-duration属性：指定过渡效果的持续时间。

时间单位：`1s==1000ms。`

```css
transition-duration: 3s;
```

3. transition-timing-function属性：指定过渡的时序函数。

可选值：

- ease：默认值，慢速开始，先加速，再减速。
- linear：匀速运动。
- ease-in：加速运动。
- ease-out：减速运动。
- ease-in-out：先加速，后减速。

<font color=red>拓展贝塞尔曲线：
</font>

我们也可以使用贝塞尔曲线来指定过渡的时序函数，`cubic-bezier()`。

```css
transition-timing-function: cubic-bezier(0,1.53,.97,-0.85);
```

制作贝塞尔曲线的官网：[https://cubic-bezier.com/](https://cubic-bezier.com/)

![image-20210825132826463](https://i.loli.net/2021/08/28/PG3kToyLOXf5vWc.png)

4. transition-delay属性：过渡效果的延迟，等待一段时间后再进行过渡。

```css
transition-delay: 2s;
```

相当于等`2s`后再进行过渡。

5. transition属性：用于简写过渡的所有属性，没有顺序要求，只有一个要求，延迟再持续时间的前面。

```css
transition: margin-left 2s 2s cubic-bezier(.2,.84,.83,.28);
```

### 2、动画（animation）

​	动画与过渡类似，都是可以实现一些动态的效果，不同的是过渡是在某个属性发生变化是才会触发。

<font color=red>步骤：
</font>

1. 必须首先设置一个关键帧，关键帧设置了动画执行的每一个步骤。

```css
@keyframes test {
 /* 动画开始的状态*/
 from{
  margin-left: 0;
 }
 /* 动画结束的状态*/
 to{
  margin-left: 700px;
 }
}
```

相当于margin-left由0到700过渡。

==注意：除了使用from和to，还可以使用百分比，from相当于0%，to相当于100%。

```css
@keyframes test {
 /* 动画开始的状态*/
 0%{
  margin-left: 0;
 }
 /* 动画结束的状态*/
 100%{
  margin-left: 700px;
 }
}
```

2. 设置动画。

- animation-name属性：指定对当前元素生效的关键帧的名字。

```css
animation-name: test;
```

也需要指定一个动画执行的时间。

- animation-duration属性：指定动画执行的时间。

```css
animation-duration: 2s;
```

- animation-delay属性：设置动画的延时。

```css
animation-delay: 1s;
```

- animation-timing-function属性：用来指定动画的时序函数。

```css
animation-timing-function: initial;
```

可选值与过渡的可选值一样。

- animation-iteration-count属性：动画执行的次数。

```css
animation-iteration-count: 2;
```

可选值：

整数和小数：指定一个动画执行的次数。

infinite：指定动画执行无限次。

- animation-direction属性：指定动画运行的方向。

可选值：

normal：默认值，从from到to，每次都一样。

reverse：从to到from，每次都一样。

alternate：从from到to，重复执行时，动画反方向执行。

alternate-reverse：从to到from，重复执行时，动画反方向执行。

```css
animation-direction: alternate;
```

- animation-play-state属性：设置动画的状态，播放或者暂停。

可选值：

paused：设置动画的暂停。

running：设置动画的播放。

```css
animation-play-state: paused;
```

- animation-fill-mode属性：指定动画的填充样式。

可选值：

none：默认值，动画执行完后回到原来的位置。

forwards：动画执行完后，会停止到动画结束的位置。

backwards：动画延时等待时，元素就会处于开始的位置（from位置）。

both：结合了forwards和backwards两者的。

```css
animation-fill-mode: forwards;
```

- animation属性：简写动画的所有属性，没有顺序要求，但是延时必须在持续时间的后面。

### 3、变形（transform）

​	指通过`css`来改变元素的形状或位置，==变形不会影响到页面的布局，不会脱离文档流。==

<font color=red>transform属性：</font>用来设置元素的变形效果。

1. 平移：

- `translateX()`：沿着x轴方向平移。
- `translateY()`：沿着y轴方向平移。
- `translateZ()`：沿着Z轴方向平移，z轴方向就是对着自己的方向。

```css
transform: translateX(200px);
```

就是往x轴移动`200px`，也可以指定一个百分比，这个百分比是相对于自生计算的。

可以指定多个值，之间使用空格隔开。

```css
transform: translateX(200px) translateY(100px);
```

z轴平移：调整元素在z轴的位置，距离越大，元素离人越近。z轴的平移属于立体效果（近大远小），默认的情况下网页不支持透视的。

==注意：如果需要网页支持立体效果，需要给网页设置视距（人眼距网页的距离），一般给`html`标签设置，设置为800-1200之间。=

```css
html{
   perspective: 800px;
}
```

2. 旋转：通过旋转，可以使元素沿着x、y、z轴旋转指定的角度。

- `rotateX`：沿着x轴旋转。
- `rotateY`：沿着y轴旋转。
- `rotateZ`：沿着z轴旋转

```css
transform: rotateX(45deg);
```

`backface-visibility`属性：可以设置是否显示元素的背面。

可选值：

visibility：默认值，可以显示。

hidden：不显示背面。

3. 缩放：通过缩放，来将元素进行放大或缩小。

- `scaleX()`：水平方向缩放。
- `scaleY()`：垂直方向的缩放。
- `scale()`：两个方向的缩放。

```css
transform: scale(2);
```

括号中可以指定一个整数或小数，指定缩放的倍数。

<font color=red>transform-origin属性：
</font>用来设置变形的原点位置。

可选值：

- center：默认值，以元素的中间作为变形原点。
- 指定值：以指定值作为原点。

```css
transform-origin: 10px 10px;
```

​	

## 4、弹性盒子

测试用例：

```html
<ul>
   <li>1</li>
   <li>2</li>
   <li>3</li>
</ul>
```

```css
*{
   list-style: none;
   margin: 0;
   padding: 0;
}
ul{
   width: 800px;
   border: 10px solid red;
   margin: 100px auto;
}
li{
   width: 100px;
   height: 100px;
   background-color: #bfa;
   font-size: 50px;
   text-align: center;
   line-height: 100px;
}
li:nth-child(2){
   background-color: orange;
}
li:nth-child(3){
   background-color: #1e7e34;
}
```

![image-20210827165526860](https://i.loli.net/2021/08/27/zcQajtJLh7dTVHX.png)



弹性盒子（flex）：

​	是一种`css`中的一种布局手段，它主要用来代替浮动来完成对应的布局。可以让元素可以跟顺页面的大小的改变进行改变。

### 1、弹性容器

​	要使用弹性盒子，必须先将一个元素设置为弹性容器。通过display设置为弹性容器。

可选值：

- flex：设置为快元素的弹性容器。
- `inline-flex`：设置为行内的弹性容器。

```css
ul{
   width: 800px;
   border: 10px solid red;
   margin: 100px auto;
   display: flex;
}
```

我们就给`ul`设置为了弹性容器。

![image-20210827165544526](https://i.loli.net/2021/08/27/63EYKGFoRn4dsCL.png)

### 2、弹性元素

弹性容器的直接子元素就是弹性元素（弹性项）。

一个元素可以同时是弹性容器和弹性元素。

<font color=red>主轴和侧轴：</font>

主轴：弹性元素的排列方向。

侧轴：与主轴垂直的方向。

### 3、弹性容器的样式

- flex-direction属性：指定容器中弹性元素的排列方式。

可选值：

row：默认值，弹性元素在容器中水平排列（左向右）。

row-reverse：弹性元素在容器中反向水平排列（右向左）。

column：弹性元素在容器中纵向排列（上到下）。

column-reverse：弹性元素在容器中由下到上排列。

```css
ul{
   width: 800px;
   border: 10px solid red;
   margin: 100px auto;
   display: flex;
   flex-direction: row-reverse;
}
```

![image-20210827170300233](https://i.loli.net/2021/08/28/A7iSIgd3tbmOXVn.png)

- flex-wrap属性：设置弹性元素是否在弹性容器中换行。

可选值：

`nowrap`：默认值，元素不会换行。

wrap：元素沿着侧轴自动换行换行。

wrap-reverse：元素沿着侧轴反方向换行。

- flex-flow属性：是前面两个属性的简写属性，没有顺序要求。

```css
flex-flow: row wrap;
```

上面表示，水平排列，自动换行。

- justify-content属性：指定如何分配主轴上的空白空间。

可选值：

flex-start：元素沿着主轴的起边排列，空白全在排列的另一边。

![image-20210828131729770](https://i.loli.net/2021/08/28/r2FQ93DTqOywId7.png)

flex-end：元素沿着主轴的终边排列，空白也是在另一边。

![image-20210828131921570](https://i.loli.net/2021/08/28/Hk38D5SmjIdswrb.png)

center：元素居中排列，空白在两侧。

![image-20210828132016734](https://i.loli.net/2021/08/28/bQevMAXoOI5ZTdB.png)

space-around：空白分布在元素的两侧。

![image-20210828132145029](https://i.loli.net/2021/08/28/Pr3hGHqgYy6VckC.png)

space-between：空白均匀分不到元素与元素之间。

![image-20210828132253038](https://i.loli.net/2021/08/28/ue5DFnGJoVwx7B2.png)

space-evenly：空白分布到元素的单侧。

![image-20210828132338598](https://i.loli.net/2021/08/28/w8ZeyfS42RvcpNm.png)

- align-items属性：元素在侧轴上如何对齐。

可选值：

stretch：默认值，将一行的元素设置为相同的值。

![image-20210828142723898](https://i.loli.net/2021/08/28/jvMDlXQ9HPqkRfF.png)

flex-start：元素不会拉伸，沿着侧轴起边对齐。

<img src="https://i.loli.net/2021/08/28/qekDAjLCo1m6nIT.png" alt="image-20210828142927780" style="zoom:80%;" />

flex-end：沿着侧轴的终边对齐。

<img src="https://i.loli.net/2021/08/28/YV7Z5yhBCeTczEX.png" alt="image-20210828151336264" style="zoom:80%;" />

center：居中对齐。

<img src="https://i.loli.net/2021/08/28/c6A8BHyFgEa7KVi.png" alt="image-20210828151414321" style="zoom:80%;" />

baseline：基线对齐的方式。

- align-content属性：指定侧轴的空白空间分布。

可选值和justify-content属性的可选值一样，效果也类似，但是是侧轴上的空白空间。

### 4、弹性元素的样式

- flex-grow属性：指定弹性元素的伸展的系数，当父元素存在多余空间时，会按照指定的比例伸展。

==注意：默认值是0，给每个弹性元素指定该属性，会按照对应的比例分配。值是整数也可以是小数，相当于把父元素多余的空间按比例分配给弹性元素==

```css
li{
   width: 100px;
   height: 100px;
   background-color: #bfa;
   font-size: 50px;
   text-align: center;
   line-height: 100px;
   flex-grow: 1;
}
```

![image-20210827171019356](https://i.loli.net/2021/08/28/qTugcAR7QZlLphN.png)

- flex-shrink属性：指定弹性元素的收缩比例。

当父元素的空间不足以容纳所有的子元素时，就会对弹性元素进行收缩。

```css
li{
   width: 100px;
   height: 100px;
   background-color: #bfa;
   font-size: 50px;
   text-align: center;
   line-height: 100px;
   flex-shrink: 1;
}
```

默认值是1，也是可以自指定小数的。当弹性空间不足的时候，弹性元素就会按照指定的比例收缩。

- flex-basis属性：指定的是元素在主轴上的基础长度。

如果主轴是横向，则该值指定的就是元素的宽度，相当于width属性，之前设置的width无效。

如果主轴是纵向，则该值指定的就是元素的高度，相当于height属性，之前设置的height无效。

默认值是auto，元素还是以width和height来设置宽高。

- flex属性：上面三个属性的简写属性，有顺序要求，伸张系数、缩展系数、基础长度。

```css
flex: 1 1 200px;
```

  也有可选值：

initial：对应0 1 auto。

auto：对应1 1 auto。

none：对应0 0 auto。

- order属性：决定弹性元素在弹性容器中的排列顺序。

```css
li:nth-child(1){
   order: 3;
}
li:nth-child(2){
   background-color: orange;
   order: 2;
}
li:nth-child(3){
   background-color: darkgreen;
   order: 1;
}
```

我们把123，倒过来排列。

![image-20210828155136268](https://i.loli.net/2021/08/28/mvTRGSpsjN16alo.png)









