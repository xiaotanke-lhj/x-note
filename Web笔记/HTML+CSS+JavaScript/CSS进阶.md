# `CSS` 进阶

[TOC]



## 1、文档流

文档流：网页是一个多层结构，一层接着一层，我们把最底下的一层称为文档流，是网页的基础。我们通过css来设置每一层的样式。

<img src="https://i.loli.net/2021/08/10/BjlxZKDTC6P2G7p.png" alt="image-20210810201327041" style="zoom:80%;" />

<font color=red>注意：</font>

1. 从侧面看是有很多层，用户只能看见最上面的一层。
2. 对于元素来说，一共有两种状态。
   - 在文档流中。
   - 不在文档流中（脱离文档流）。

<font color=red>元素在文档流中的特点：</font>

1. 块元素：
   - 如果在文档流中，会在页面中独占一行，无论你的宽高。
   - 默认宽度是父元素的全部宽度。
   - 默认高度是内容撑开。

2. 行类元素：
   - 在文档流中不会独占一行，只占自身的大小，至左向右水平排列。
   - 如果一行中不能容下所有的行类元素，它会自动换到下一行。
   - 默认宽度和高度都是内容撑开。

==注意：当快元素和行类元素脱离文档流后，就不具有这些 属性了。以后在学==



## 2、盒子模型

- `CSS`将页面中的所有元素都设置为了一个矩形的盒子。
- 将元素设置为矩形后，对页面的布局就变成将不同的盒子放在不同的位置上。
- 每一个盒子都有以下组成，Content（内容区）、Padding（内边距）、Border（边框）、Margin（外边距）。

![image-20210810214851988](https://i.loli.net/2021/08/10/OnyNaIi7xAqglhd.png)

1. 内容区：元素中的所有子元素和文本内容都在内容区中排列

==注意：内容区的大小由width和height属性来设置的。width的默认值是auto==

2. 边框：边框属于盒子边缘，边框里面属于盒子内部，边框外边属于盒子的外部。

设置边框，至少设置三个属性：

border-width：设置四个方向边框的宽度，默认值是`3px`，可能不同浏览器各有不同。

```css
border-width: 10px,20px,30px,40px;
```

四个值：对应上、右、下、左。

三个值：对应上、左右、下。

两个值：对应上下、左右。

一个值：对应四个方向。

除了以下的设置方式，用来单独指定各个边的样式，还可以采用下面的方式：

```css
/*	上边*/
border-top-width: 2px;
/*	右边边*/
border-right-width: 2px;
/*	下边*/
border-bottom-width: 2px;
/*	左边边*/
border-left-width: 2px;
```

border-color：设置四个边框的颜色，这个和边框的宽度类似，都有指定各个方向的颜色。

border-style：边框的样式。（有三个值，dotted：点线边框，dashed：虚线边框，solid：实线边框，double：双线边框，none：不存在边框），这个和上面一样都可以单独指定方向的样式。

![image-20210810222544430](https://i.loli.net/2021/08/11/dT4IjY57DtsS6Aa.png)

```css
.box{
    width: 200px;
    height: 200px;
    /* 设置边框的宽度*/
    border-width: 2px;
    /* 设置边框的颜色*/
    border-color: red;
    /*	设置边框的样式*/
    border-style: solid;
}
```

可以通过一个border属性来简写设置，没有顺序要求，注意使用空格隔开，这个也是可以单独指定某一边的样式。

```css
.box{
    width: 200px;
    height: 200px;
    border: 2px red solid;
}
```

3. 内边距：内容区和边框之间的距离，一共有四个方向的内边距（padding-top、padding-right、padding-bottom、padding-left）。

```css
.box2{
    width: 500px;
    height: 500px;
    background-color: #bfa;
    padding-top: 20px;
    padding-right: 20px;
    padding-bottom: 20px;
    padding-left: 20px;
}
```

<font color=red>注意：</font>

- 内边距的设置会影响到盒子的大小。
- 背景颜色也会延伸到内边距。

```html
<div class="box2">我是盒子2</div>
```

```css
.box2{
    width: 500px;
    height: 500px;
    background-color: #bfa;
    padding-top: 100px;
    padding-right: 100px;
    padding-bottom: 100px;
    padding-left: 100px;
}
```

![image-20210811130639575](https://i.loli.net/2021/08/11/EBsAGnLatbIp7OV.png)

==注意：一个盒子的可见区域是内容区+内边距+边框。==

也可以使用padding来简写四个方向的内边距，规则和边框一样。

```css
padding: 10px 20px 30px 40px;
```

4. 外边距：盒子可见区以外的区域，盒子的实际占的空间。

<font color=red>注意：</font>

- 外边距不会影响盒子可见区域的大小，是与其它元素之间的距离，但是会影响盒子的位置。
- 一共有四个方向的外边距（margin-top、margin-right、margin-bottom、margin-left）。

```css
.box3{
    width: 200px;
    height: 200px;
    background-color: #bfa;
    margin-top: 100px;
    margin-right: 100px;
    margin-bottom: 100px;
    margin-left: 100px;
}
```

![image-20210811150148684](https://i.loli.net/2021/08/11/eFW7Lk8KvMS1bnT.png)

==注意：margin等属性，可以指定负值，它就会向反方向移动。margin也是可以简写，和上面的规则一样。==



## 3、盒子模型的水平布局

测试事例：

```html
<div class="outer">
    <div class="inner"></div>
</div>
```

```css
.outer{
    width: 800px;
    height: 200px;
    border: 2px solid red;
}
.inner{
    width: 200px;
    height: 200px;
    background-color: #bfa;
}
```

![image-20210811154005288](https://i.loli.net/2021/08/11/71w4vIl5OVNEG68.png)

<font color=red>元素水平方向的布局：</font>元素在其父元素中水平方向的位置由以下几个属性来决定。

margin-left、border-left、padding-left、width、padding-right、border-right、margin-right。

一个元素在其父元素中，水平布局必须满足下面这个等式：

<font color=red>margin-left+border-left+padding-left+width+padding-right+border-right+margin-right = 其父元素内容区的width</font>

上面的例子就是：0+0+0+200+0+0+0 = 800

显然上面的等式不成立，这就称为过渡约束，则浏览器会自动调整。

<font color=red>调整方式：</font>

- 如果这7个值中没有设置auto的情况，则浏览器自动调整margin-right的值使等式满足。

上面的例子就为 0+0+0+200+0+0+600 = 800

![image-20210811154940088](https://i.loli.net/2021/08/17/aO7u9BSJkGbrPjl.png)

在例如把margin-left设置为`100px`，则等式为：100+0+0+200+0+0+500 = 800

![image-20210811155210096](https://i.loli.net/2021/08/17/ohKPXOHiUJMvg3S.png)

- 这7个值中有三个值可以设置为auto，分别是width、margin-left、margin-right。

如果某个值为auto，则浏览器会调整auto那个属性来使等式来成立。

例如：

```css
.inner{
    width: auto;
    height: 200px;
    background-color: #bfa;
}
```

则等式就为：0+0+0+auto+0+0+0 == 800    auto = 800，即width = `800px`

![image-20210811155850893](https://i.loli.net/2021/08/11/SyQETIGgi8aFYUq.png)

在例如，我再把margin-left设置为`100px`

则等式就为：100+0+0+auto+0+0+0 = 800	auto = 700

![image-20210811160121416](https://i.loli.net/2021/08/17/PF7oKgc5Zd8ahDY.png)

==注意：如果不给width设置值，默认是等于auto的。==

- 如果将宽度和一个外边距设置为auto，则宽度会调整到最大，设置auto的外边距则就自动为0。
- 如果将两个外边距设置为auto，宽度为固定值，两个外边距就自动设置为相同满足等式。

通常使用这个来使子元素在父元素中居中，==注意必须给子元素指定固定的width==。

示例：

```css
.inner{
    width: 200px;
    height: 200px;
    background-color: #bfa;
    margin-left: auto;
    margin-right: auto;
}
```

![image-20210811161805765](https://i.loli.net/2021/08/11/PI9ic4d3RZsqo8B.png)

简写为：`margin: 0 auto;`，0是上下边距，auto是左右边距。

- 如果三个值都设置为auto，则宽度最大，两个外边距都自动为0。

- 如果等式像以下这样：

0+0+0+1000+0+0+0 = 800，这样盒子就大于父元素的内容宽度，则浏览器自动调整，使调整的属性为负值。

调整后：0+0+0+1000+0+0+-200 = 800。

==注意无论增莫样，等式是必须满足的。==



## 4、盒子模型的垂直布局

测试示例：

```html
<div class="outer">
    <div class="inner"></div>
</div>
```

```css
.outer{
    background-color: #bfa;
}
.inner{
    width: 100px;
    height: 100px;
    background-color: red;
}
```

![image-20210811172019485](https://i.loli.net/2021/08/11/3jUi4VzuNlT7mvY.png)

<font color=red>注意事项：</font>

- 默认情况下父元素会被子元素撑开，如果父元素不设置宽和高，会被子元素的大小撑开。

<font color=red>处理子元素溢出：</font>

​	如果子元素的大小比父元素的大小还大，会存在溢出效果。

```css
.outer{
    width: 200px;
    height: 100px;
    background-color: #bfa;
}
.inner{
    width: 100px;
    height: 200px;
    background-color: red;
}
```

这样父元素比子元素的高度还低：

<img src="https://i.loli.net/2021/08/11/IyHiEdbYnAUvkZW.png" alt="image-20210811172339466" style="zoom:80%;" />

<font color=red>处理方法：</font>

使用`overflow`属性来解决，是给父元素设置这个属性，下面是几个可选值。

- visible：默认值，子元素会从父元素中溢出，在父元素的外部显示溢出的内容。
- hidden：溢出的内容会被裁剪，不会显示。

![image-20210811172740991](https://i.loli.net/2021/08/11/jdzN7y1kRopBZOe.png)

- scroll：会在父元素上生成两个滚动条，通过滚动条查看溢出的内容。

![image-20210811172913553](https://i.loli.net/2021/08/11/YH4W2Vv1DP9udIw.png)

- auto：`scroll` 会生成两个滚筒条，但是可能有一个不会使用，auto会自动生成有用的滚动条，不使用的就不会生成。

<font color=red>拓展：</font>

还有两个属性也可以处理溢出，分别是overflow-x、overflow-y，这两个就单独处理某个方向的溢出，可选值都是一样的，x是水平方向，y是垂直方向。



## 5、盒子模型的外边距折叠

测试用例：

```html
<div class="box3">1</div>
<div class="box4">2</div>
```

```css
.box3,.box4{
    width: 200px;
    height: 200px;
    font-size: 30px;
}
.box3{
    background-color: #bfa;
}
.box4{
    background-color: red;
}
```

<img src="https://i.loli.net/2021/08/11/WI8AyRfDoTiaQcw.png" alt="image-20210811180850338" style="zoom:80%;" />

如果给1设置下外边距`100px`，给2设置一个上外边距`100px`。按理来说，两个盒子之间的距离就为`200px`，但是实际它们之间只有`100px`。

以上情况就发生了外边距的重叠（折叠），分成两种情况，兄弟元素的重叠，父子元素的重叠。

<font color=red>兄弟元素外边距的重叠：</font>

兄弟元素的相邻垂直外边距会取两者之间的最大值。

```css
.box3{
    background-color: #bfa;
    margin-bottom: 50px;
}
.box4{
    background-color: red;
    margin-top: 100px;
}
```

<img src="https://i.loli.net/2021/08/11/F374pm1MaNLjsId.png" alt="image-20210811181548659" style="zoom:80%;" />

<font color=red>兄弟元素重叠的特殊情况：</font>（一般不会遇到）

- 如果两个设置的值是一正一负，则取两者的和。
- 如果两个的设置的值都是负值，则取两个绝对值中较大的值。

<font color=red>父子元素的外边距重叠：</font>

示例：

```html
<div class="box1">
    <div class="box2"></div>
</div>
```

```css
.box1{
    width: 300px;
    height: 300px;
    background-color: #bfa;
}
.box2{
    width: 100px;
    height: 100px;
    background-color: red;
}
```

<img src="https://i.loli.net/2021/08/11/4SqWTVp5QCynIPi.png" alt="image-20210811221824107" style="zoom:80%;" />

现在需要把`box2`向下移`100px`，而`box1`不动。我们给`box2`设置一个margin-top会导致两个盒子一起向下移动，达不到效果。

<img src="https://i.loli.net/2021/08/11/WM1RLm8BwcD6b7x.png" alt="image-20210811222043397" style="zoom:80%;" />

<font color=red>产生的原因：</font>

<img src="https://i.loli.net/2021/08/11/pftjKVOwsJNgxWk.png" alt="image-20210811223249741" style="zoom:80%;" />



父子元素相邻的外边距，子元素会传递给父元素（上外边距），相当于给父元素也设置了上外边距。

<font color=red>解决办法：</font>

方式一：设置`box1`的内边距，然后高度减去相应的长度。

```css
.box1{
    width: 300px;
    height: 200px;
    background-color: #bfa;
    padding-top: 100px;
}
.box2{
    width: 100px;
    height: 100px;
    background-color: red;
}
```

方式二：给`box1`设置一个边框，边框颜色改为对应的颜色，然后设置`box2`的margin-top。

```css
.box1{
    width: 300px;
    height: 300px;
    background-color: #bfa;
    border: 1px solid #bfa;
}
.box2{
    width: 100px;
    height: 100px;
    background-color: red;
    margin-top: 100px;
}
```

效果：

<img src="https://i.loli.net/2021/08/11/EwQkVU2t7ofYNa1.png" alt="image-20210811222845735" style="zoom:80%;" />



## 6、行类元素的盒模型

<font color=red>注意事项：</font>

- 行类元素不会独占一行。

- 行类元素不支持设置宽高。
- 行类元素可以设置padding、border、margin，但是垂直方向上不影响页面的布局。
- `rmargin`在水平方向上不会像快元素那样选择较大值，它是直接相加。

<img src="https://i.loli.net/2021/08/12/d1XF8vmLtN3QTOG.png" alt="image-20210812114750991" style="zoom:80%;" />

<font color=red>display属性：</font>

​	用于设置元素的显示类型，可以通过display将快元素转换成行类元素，将行类元素转换成快元素。

可选值：

- `inline`，将元素设置行类元素。
- `block`，将元素设置为快元素。
- `inline-block`，将元素设置为行类快元素。
- `none`，将元素隐藏，不会在页面中显示，而且在页面也不会站占用位置。
- `table`，将元素设置为表格。

<font color=red>visibility属性：</font>

​	用来设置元素的显示状态。

可选值：

- `visible`：默认值，在页面中正常显示。
- `hidden`：将元素在页面中隐藏，不显示，但是在页面中会占用位置，这就是和`displlay`中`none`的区别。



## 7、浏览器的默认样式

​	通常情况，浏览器会为元素设置一些默认样式，默认样式会影响到页面的布局。通常情况下在编写网页时必须去除浏览器的默认样式，因为不同浏览器的默认样式不同。常见的默认样式就是margin、padding。

方式一：使用通配符选择器去掉所有的默认样式

```css
*{
    margin: 0;
    padding: 0;
}
```

方式二：在一些简单的练习中我们可以采用通配符去除默认样式，由于通配符也可能会存在不能全部默认样式的去除，我们在开发中通常使用重置样式表来去除所有的默认样式，你只需把重置样式表引入到你的网页即可。

重置样式表：

```css
/* http://meyerweb.com/eric/tools/css/reset/ 
   v2.0 | 20110126
   License: none (public domain)
*/

html, body, div, span, applet, object, iframe,
h1, h2, h3, h4, h5, h6, p, blockquote, pre,
a, abbr, acronym, address, big, cite, code,
del, dfn, em, img, ins, kbd, q, s, samp,
small, strike, strong, sub, sup, tt, var,
b, u, i, center,
dl, dt, dd, ol, ul, li,
fieldset, form, label, legend,
table, caption, tbody, tfoot, thead, tr, th, td,
article, aside, canvas, details, embed, 
figure, figcaption, footer, header, hgroup, 
menu, nav, output, ruby, section, summary,
time, mark, audio, video {
	margin: 0;
	padding: 0;
	border: 0;
	font-size: 100%;
	font: inherit;
	vertical-align: baseline;
}
/* HTML5 display-role reset for older browsers */
article, aside, details, figcaption, figure, 
footer, header, hgroup, menu, nav, section {
	display: block;
}
body {
	line-height: 1;
}
ol, ul {
	list-style: none;
}
blockquote, q {
	quotes: none;
}
blockquote:before, blockquote:after,
q:before, q:after {
	content: '';
	content: none;
}
table {
	border-collapse: collapse;
	border-spacing: 0;
}
```

==注意：重置样式表必须引入必须在其它引入样式文件的上面，避免会把之前的样式覆盖掉。==



## 8、盒子的大小

​	默认情况下，盒子的可见大小由内容区、内边距、边框决定。

示例：

```html
<div class="box1"></div>
```

```css
.box1{
    width: 100px;
    height: 100px;
    background-color: #bfa;
    padding: 10px;
    border: 10px solid red;
}
```

上面这个盒子的大小就是`140px`。

<font color=red>box-sizing属性：</font>用来设置盒子大小的计算方式。

可选值：

- content-box：设置的宽度和高度用来设置整个盒子内容区域的大小，padding和border需另外设置。
- border-box：设置的宽度和高度用来设置整个盒子可见大小，width就包括内容区、padding、border。



## 9、盒子的轮廓和圆角

<font color=red>轮廓：</font>

​	使用outline属性来设置轮廓，用法和border的用法一样，但是与border不同的是，==轮廓不会影响到盒子的可见框的大小。==

```html
<div class="box1"></div><span>我是span</span>
```

```css
.box1{
    width: 200px;
    height: 200px;
    background-color: #bfa;
    outline: 10px solid red;
}
```

![image-20210812173457391](https://i.loli.net/2021/08/12/6zmI4qev9uNnZVc.png)

<font color=red>阴影：</font>

​	使用box-shadow属性来设置阴影，阴影和轮廓一样也是不会影响盒子的可见大小。默认阴影在元素的正下方，想要看到阴影，需要给阴影指定偏移量。

```css
.box1{
    width: 200px;
    height: 200px;
    background-color: #bfa;
    box-shadow: 10px 10px 30px rgba(0,0,0,0.6);
}
```

box-shadow值得详解：

1. 第一个值：水平位移，设置阴影的水平位置，正值右移，负值左移。
2. 第二个值：垂直位移，设置阴影的垂直位置，正值下移，负值上移。
3. 第三个值：阴影的模糊半径。
4. 第四个值：阴影的颜色，一般使用`rgba`来设置，可以设置颜色的透明度。

效果：

<img src="https://i.loli.net/2021/08/12/38aAl9sWpbLUBrF.png" alt="image-20210812191028120" style="zoom:80%;" />

<font color=red>圆角：</font>

​	用来设置盒子的圆角效果，通常使用border-radius属性来设置，设置的是圆的半径大小。

```css
.box1{
    width: 200px;
    height: 200px;
    background-color: #bfa;
    box-shadow: 10px 10px 30px rgba(0,0,0,0.6);
    border-radius: 30px;
}
```

效果：

<img src="https://i.loli.net/2021/08/12/E4FnJtXHZxpdfWj.png" alt="image-20210812191619852" style="zoom:80%;" />

拓展：

1. 我们可以单独设置每一个角的圆角效果。
   - border-top-left-radius，设置左上角。
   - border-top-right-radius，设置右上角。
   - border-bottom-left-radius，设置左下角。 
   - border-bottom-right-radius，设置右下角。

==注意：上面的四个属性可以设置两个值，这就是设置椭圆效果，以这样两个半径来画椭圆。==

例如：`border-top-left-radius: 30px 20px;`，第一个值是水平方向的半径，第二个值是垂直方向的半径。

2. 可以一次设置四个角的圆角效果，border-radius直接设置。

指定四个值：分别对应，左上、右上、左下、右下

指定三个值：分别对应，左上、右上/左下、右下

指定两个值：分别对应，左上/右下、右上/左下

指定一个值：四个角都是这个值。

3. border-radius也可以设置为`border-radius: 20px / 30px;`，这个就是画椭圆。
4. 画圆型：`border-radius: 50%;`

<img src="https://i.loli.net/2021/08/12/itkUCJGgds8W5YP.png" alt="image-20210812193657810" style="zoom:80%;" />



## 10、浮动

测试用例：

```html
<div class="box1"></div>
<div class="box2"></div>
```

```css
.box1{
    width: 100px;
    height: 100px;
    background-color: #bfa;
    float: left;
}
.box2{
    width: 200px;
    height: 200px;
    background-color: #1e7e34;
}
```

​	通过浮动可以使一个元素向其父元素的左侧或右侧移动。

<font color=red>float属性：</font>用于设置元素的浮动。

可选值：

- none：默认值，元素不浮动。
- left：元素向左浮动。
- right：元素向右浮动。
- inherit：元素继承其父元素的float属性。

<font color=red>注意事项：</font>

1. 元素设置了浮动（设置的值不是none），水平布局的等式不需要强制满足。
2. 元素设置浮动后，会完全从文档流中脱离，不会占用文档流的位置，下边的元素会向上移动。



<img src="https://i.loli.net/2021/08/13/pucLXsR9mzrv3an.png" alt="image-20210813173609568" style="zoom:80%;" />

<font color=red>浮动的特点：</font>

1. 浮动元素会完全脱离文档流，不再占据文档流中的位置。
2. 设置浮动以后元素向父元素的左侧或右侧移动。
3. 浮动元素默认不会从父元素中移出。
4. 浮动元素向左或向右移动时，不会超过它前边的其它浮动元素。
5. 浮动元素不会盖住文字，文字会自动环绕在浮动元素的周围可以利用浮动来设置文字环绕图片的效果。

```html
<div class="box1"></div>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus commodi iusto laudantium odit voluptatem.
    Accusamus, et exercitationem expedita ipsum perspiciatis porro praesentium quas voluptate. Assumenda explicabo
    nobis odit quis repellat.
</p>
```

```css
.box1{
    width: 100px;
    height: 100px;
    background-color: #bfa;
    float: left;
}
```

![image-20210813192233143](https://i.loli.net/2021/08/13/32rTwetyJQjSdgO.png)

​	==浮动目前的主要作用，就是让页面中的元素可以水平排列，通过浮动可以制作一些水平方向的布局。==

<font color=red>脱离文档流的特点：</font>

快元素：

1. 块元素不独占页面的一行了。
2. 脱离文档流后，快元素的宽高默认被内容撑开，但是也是可以指定宽高。

行类元素：

​	行类元素脱离文档流后会变成和快元素脱离文档流一样的效果。

==总之，元素脱离文档流以后就不在区分快元素和行类元素了。==



## 11、高度塌陷和`BFC`

​	在实际开发中，由于不能确定内容的多少，我们一般不会将高度写一个确定的高度，而是默认被子元素撑开，子元素有多少，父元素就会自动变多高。

```html
<div class="outer">
    <div class="inner"></div>
</div>
```

```css
.outer{
    border: 10px solid red;
}
.inner{
    width: 100px;
    height: 100px;
    background-color: #bfa;
}
```

![image-20210814120201175](https://i.loli.net/2021/08/14/lLRViOpJPGI7oWj.png)

==在浮动布局的时候，父元素是默认被子元素撑开的。当子元素浮动后，其完全脱离文档流，子元素会从文档流中脱离出来，将无法撑起父元素的高度，导致父元素的高度丢失。==

给子元素设置浮动后：

![image-20210814120517674](https://i.loli.net/2021/08/14/oc3BhQUf5DHnauY.png)

==父元素高度丢失后，其它元素就会自动向上移动，就会导致页面的布局混乱。<font color=red>这个就是高度塌陷</font>。==

<font color=red>解决办法：
</font>

1. `BFC`（Block Formatting Context）：块级格式化环境，是`css`的一个隐藏属性，可以为一个元素开启`BFC`，开启`BFC`后该元素会变成一个独立的布局区域。使用`BFC`来解决高度塌陷还是存在一些副作用。

开启`BFC`元素的特点：

- 开启`BFC`的元素不会被浮动的元素所覆盖。
- 开启`BFC`的元素子元素和父元素外边距不会重叠。
- 开启`BFC`的元素可以包含浮动的子元素。

如何开启`BFC`：

- 将元素也设置浮动。（不推荐）
- 将元素设置为行类快元素。（不推荐使用）
- 将元素的overflow设置一个非visible的值，常把值设置为hidden，也可以设置为其它值。

```css
.outer{
    border: 10px solid red;
    overflow: hidden;
}
.inner{
    width: 100px;
    height: 100px;
    background-color: #bfa;
    float: left;
}
```

![image-20210814125608038](https://i.loli.net/2021/08/14/L6zboSG35QlVidC.png)

2. clear属性：清除浮动元素对当前元素的影响。

   如果我们不希望某个元素因为其它元素的浮动的影响而改变位置，我们可以通过clear属性来清除浮动元素所产生的影响。

可选值：

- left：清除左侧浮动元素对当前元素的影响。
- right：清除右侧浮动元素对当前元素的影响。

==注意：左侧和右侧是对于设置浮动元素是的左侧和右侧。==

原理：

​	设置清除浮动后，浏览器会自动为元素添加一个上外边距，以使其位置不受其它元素的影响。

使用clear属性解决高度塌陷问题：

​	我们可以使用clear属性结合伪元素来解决高度塌陷问题，子元素设置了浮动后，父元素的高度无法撑开，所以我们给父元素添加一个伪元素来撑开高度。

```html
<div class="outer">
    <div class="inner"></div>
</div>
```

```css
.outer{
    border: 10px solid red;
}
.inner{
    width: 100px;
    height: 100px;
    background-color: #bfa;
    float: left;
}
.outer::after{
    content: '';
    display: block;
    clear: both;
}
```

<img src="https://i.loli.net/2021/08/14/REUDkQ5uCaMZyiY.png" alt="image-20210814134746201" style="zoom:150%;" />

==注意：伪元素默认是行类元素，它不会独占一行，所以必须把它重新设置为快元素。==



## 12、`clearfix`类

==它是用于最终解决高度塌陷和外边距重合的问题。==

```css
.clearfix::before,
.clearfix::after{
    content: '';
    display: table;
    clear: both;
}
```

当遇到高度塌陷和外边距重合的问题时，就直接使用该类。

外边距重叠原理：是在父元素的最前面位置添加一个伪元素来将子元素与父元素隔开，但是display必须是table值。

高度塌陷：是在父元素最后面的位置添加一个伪类，然后清除浮动，display的值可以为block和table。

==总之，当遇到上面的问题，就直接使用上面的类就可以了。==



## 13、定位

​	是一种更加高级的布局手段，通过定位可以将元素摆放到页面的任意位置，通过position属性来开启定位。开启定位后，元素的层级变高，比浮动还高。

<font color=red>position属性：</font>

可选值：

- static：默认值，元素是静止的，没有开启定位。
- relative：开启元素的相对定位。
- absolute：开启元素的绝对定位。
- fixed：开启元素的固定定位。
- sticky：开启元素的粘滞定位。



## 14、相对定位

​	当元素的position属性值设置为relative时，则开启了元素的相对定位。

<font color=red>测试用例：
</font>

```html
<div class="box1"></div>
<div class="box2"></div>
<div class="box3"></div>
```

```css
.box1{
    width: 200px;
    height: 200px;
    background-color: #bfa;
}
.box2{
    width: 200px;
    height: 200px;
    background-color: #ba8b00;

}
.box3{
    width: 200px;
    height: 200px;
    background-color: #0f6674;
}
```



<font color=red>偏移量：</font>

​	当元素开启了定位后，可以通过偏移量来设置元素的位置，偏移量只会影响自己的位置，不会影响其它元素的位置。

top：定位元素和定位位置上边的距离。

bottom：定位元素和定位位置下边的距离。

left：定位元素和定位位置左边的距离。

right：定位元素和定位位置右边的距离。

==top越大，元素越往下移动；bottom越大，元素越往上移动；left越大，元素越靠右；right越大，元素越靠左。==

==注意：我们一个方向，只使用一个方向的偏移量，我们常用left控制水平方向，top来控制垂直方向。==

我们给`box2`开启定位并设置偏移量：

```css
.box2{
    width: 200px;
    height: 200px;
    background-color: #ba8b00;
    position: relative;
    top: 200px;
    left: 200px;

}
```

<img src="https://i.loli.net/2021/08/14/fTYSVvyxUZBFDh1.png" alt="image-20210814160202591" style="zoom:80%;" />

<font color=red>相对位置：</font>

​	我们通常以元素之前的位置（元素在文档流中的位置）作为参考位置，称为相对位置。

<img src="https://i.loli.net/2021/08/14/h58AIXigrNbvxFd.png" alt="image-20210814160613293" style="zoom:80%;" />

<font color=red>相对定位的特点：</font>

1. 元素开启相对定位后，如果不设置偏移量元素是不会发生任何变化的。
2. 相对定位是参照元素在文档流中的位置进行定位的。
3. 相对定位会提升元素的层级，层级比浮动的层级还高。
4. 相对定位没有脱离文档流，也不会改变元素的性质，快还是快，行类元素还是行类元素。

​	<img src="https://i.loli.net/2021/08/14/H6Ldcp5rWZsnXze.png" alt="image-20210814161245957" style="zoom:80%;" />



## 15、绝对定位

​	当于元素的position属性值设置为absolute时，开启元素的绝对定位。

<font color=red>测试用例：
</font>

```html
<div class="box3">3
	<div class="box2">2
		<div class="box1">1</div>
	</div>
</div>
```

```css
body{
    font-size: 30px;
}
.box1{
    width: 100px;
    height: 100px;
    background-color: #bfa;
}
.box2{
    width: 200px;
    height: 200px;
    background-color: #ba8b00;
}
.box3{
    width: 300px;
    height: 300px;
    background-color: #0f6674;
}
```

<font color=red>绝对定位的特点：</font>

1. 开启绝对定位后，如果不设置偏移量，元素的位置就不会改变。
2. 开启绝对定位后，元素后脱离文档流，元素的性质就会发生改变，行类变成快，快的高度被内容撑开。
3. 开启绝对定位后，元素的层级就会提高，比浮动高。
4. 开启绝对定位，元素是相当于其包含快进行定位的。

<font color=red>包含快：（containing block）</font>

- 正常元素：

该元素的包含快就是离当前元素最近的祖先==快元素==。

```html
<div>3<div>2<div>1</div></div></div>
```

1的包含快就是2，2的包含快就是3。

```html
<div>3<span>2<span>1</span></span></div>
```

1的包含快是3，2的包含快是3。

- 绝对定位的包含快：

是离他最近的开启了定位的祖先元素，如果所有祖先元素都没有开启定位，则根元素（`html`：根元素，初始包含快）就是它的包含快。

我们给`box1`设置绝对定位，并设置偏移量：

```css
.box1{
    width: 100px;
    height: 100px;
    background-color: #bfa;
    position: absolute;
    top: 0;
    left: 0;
}
```

由于`box2`、`box3`没有开启定位，则`box1`以根元素作为定位。

![image-20210814170742498](https://i.loli.net/2021/08/17/vlVNT5sQaEY1xA6.png)

如果我们给`box3`开启定位：

<img src="https://i.loli.net/2021/08/14/MgaxSZdoW51wEbv.png" alt="image-20210814170918048" style="zoom:80%;" />

如果我们给`box2`开启定位：

```css
.box2{
    width: 200px;
    height: 200px;
    background-color: #ba8b00;
    position: relative;
}
```

<img src="https://i.loli.net/2021/08/14/GPsYnRABLX12ka3.png" alt="image-20210814171041912" style="zoom:80%;" />



## 16、固定定位

​	将元素的position属性设置为fixed值，则开启了元素的固定定位。固定定位是一种特殊绝对定位，所以固定的定位的特点和绝对定位的特性类似，但是也有不同，一般用于制作广告等。

<font color=red>唯一不同点：</font>

==固定定位始终是浏览器的视口进行定位的，不会随着网页的滚动而滚动。==

<font color=red>浏览器视口：</font>

​	浏览器显示页面内容的屏幕区域，视口可以分为布局视口，视觉视口和理想视口。



## 17、粘滞定位

​	当元素的position属性设置为sticky值时，则开启了元素的粘滞定位，它的特点和相对定位的特点相似。

<font color=red>唯一不同点：
</font>

​	粘滞定位可以在元素达到某个位置时就将其固定，一般用于广告等。



## 18、绝对定位元素的位置

<font color=red>水平布局：</font>在盒子水平方向等式的基础之上增加了两个值，left和right。

left + margin-left + border-left + padding-left + width + padding-right + border-right + margin-right + right == 包含快的width

当发生过度约束时：

1. 如果9个值中没有auto，则自动调整right的值来使等式满足。

==注意：可以设置auto的值为，margin、width、left、right。因为left和right的值是默认的，如果不设置偏移量，则会自动调整这两个值。==

2. 当9个值中存在auto，则就调整auto的值。

<font color=red>垂直方向的布局：
</font>在盒子垂直方向等式的基础上增加两个值，top和bottom。

top + margin-top + border-top + padding-top + height+ padding-bottom + border-bottom + margin-bottom + bottom == 包含快的height

发生过度约束的时，规则和水平方向类似。

<font color=red>应用：</font>在绝对定位的元素中，将子元素移动到父元素的最中心

```html
<div class="outer">
    <div class="inner"></div>
</div>
```

```css
.outer{
    width: 500px;
    height: 500px;
    background-color: #bfa;
    margin: 50px auto;
    position: relative;
}
.inner{
    width: 200px;
    height: 200px;
    background-color: #1e7e34;
    position: absolute;
    margin: auto;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
}
```



## 19、元素的层级

​	对于开启了定位的元素，可以通过z-index属性来指定元素的层级。

<font color=red>z-index属性：
</font>

​	z-index需要一个整数作为参数，值越大的元素，层级越高，层级越高的元素越先显示。

<font color=red>测试用例：</font>

```html
<div class="box1">1</div>
<div class="box2">2</div>
<div class="box3">3</div>
```

```css
body{
    font-size: 50px;
}
.box1{
    width: 200px;
    height: 200px;
    background-color: #bfa;
    position: absolute;
    top: 0;
    left: 0;
}
.box2{
    width: 200px;
    height: 200px;
    background-color: rgba(255,0,0,0.3);
    position: absolute;
    top: 50px;
    left: 50px;
}
.box3{
    width: 200px;
    height: 200px;
    background-color: #0f6674;
    position: absolute;
    top: 100px;
    left: 100px;
}
```

<img src="https://i.loli.net/2021/08/14/rgOuJvyw3hfSY7G.png" alt="image-20210814214631372" style="zoom:80%;" />

如果我们给每个元素指定一个层级，box1为3，box2为2，box3为1，则box1就会在最上层显示。

<img src="https://i.loli.net/2021/08/14/X6QyJTUxFML2O4G.png" alt="image-20210814214817321" style="zoom:80%;" />

<font color=red>拓展：</font>

- 如果元素的层级是一样的，则结构靠下的先显示。
- 祖先元素的层级再高也不能超过后代元素。



## 20、字体族

​	用来设置字体的相关样式。

1. color属性：用于设置字体的颜色。
2. font-size属性：用于设置字体的大小。

单位：

- `em`：相当于当前元素的一个font-size。
- `rem`：相当于根元素的一个font-size。
- `px`：像素来表示大小。

3. font-family属性：用于设置字体的格式。

可选值：

- `serif`：衬线字体。
- `sans-serif`：非衬线字体。
- `monospace`：等宽字体。

==注意：font-family属性可以指定多个值，使用逗号隔开，字体优先使用第一个，如果第一个无法使用则使用第二个，依次类推。==

```css
font-family: "Apple Color Emoji","微软雅黑";
```

4. @font-face声明快：可以给用户提供服务器的字体，提供给用户下载。

```css
@font-face {
    font-family: "myfont";
    src: url("");
}
```

- font-family：指定一个名称，给下面设置使用

```css
font-family: myfont;
```

- `src`：指定字体安装包的路径。



## 21、图标字体

​	在网页中经常需要使用一些图标，可以通过图片来引入，但是图片大小本身比较大并且不灵活。所以我们在使用图标时，我们通常把图标设置为字体，然后通过font-face的形式来对字体进行引入。

<font color=red>Fontawesome的使用：
</font>

1. 下载：[地址：https://fa5.dashgame.com/](https://fa5.dashgame.com/)
2. 解压。
3. 将`css`文件和`webfonts`文件引入到项目中。
4. 将`all.css`引入到网页中。
5. 通过类名来使用图标。

<font color=red>使用格式：
</font>

```html
<i class="fa fa-address-book"></i>
```

第一个类fa是固定的，第二个类是以`fa-`开头，然后后面接图标的名称，名称在`fontawesome`的图库中查找。

<font color=red>拓展：</font>

​	使用伪元素结合图标字体的使用，我们给每一个`li`的前边都添加一个图标。

```html
<div class="box">
    <ul>
        <li>第一个</li>
        <li>第二个</li>
        <li>第三个</li>
        <li>第四个</li>
    </ul>
</div>
```

```css
.box li{
    list-style: none;
}
.box li::before{
    content: '\f2b9';
    font-family: 'Font Awesome 5 Free';
    font-weight: 900;
}
```

==content属性是需要图标的编码，可以在`css`样式中查询，font-family、font-weight这两个属性是必须的，也可以设置其它的样式。==



## 22、阿里的图标库

​	一个阿里巴巴开发的一款图标库，用户可以下载别人的图标，也可以上传自己的图标。图标种类丰富，有上万种图标。

[官网：https://www.iconfont.cn/](https://www.iconfont.cn/)

<font color=red>使用方式：</font>

1. 方式一：

<img src="https://i.loli.net/2021/08/16/aqrnAgwe7fEtYJ1.png" alt="image-20210816132751968" style="zoom:67%;" />

2. 方式二：

步骤：

- 加入购物车

![image-20210816133700354](https://i.loli.net/2021/08/17/4mMcdY78zqFHKBR.png)

- 添加到项目中

<img src="https://i.loli.net/2021/08/16/HCxUMcNpF2wIkXQ.png" alt="image-20210816133826363" style="zoom:80%;" />

- 引入网页

![image-20210816134502939](https://i.loli.net/2021/08/16/A5OYlpP91ugFGE7.png)

```html
<!DOCTYPE html>
<html lang="zh">
<head>
	<meta charset="UTF-8">
	<title>阿里图标库</title>
	<style>
        @font-face {
            font-family: 'iconfont';  /* Project id 2745922 */
            src: url('//at.alicdn.com/t/font_2745922_szujzs2sox9.woff2?t=1629091756041') format('woff2'),
            url('//at.alicdn.com/t/font_2745922_szujzs2sox9.woff?t=1629091756041') format('woff'),
            url('//at.alicdn.com/t/font_2745922_szujzs2sox9.ttf?t=1629091756041') format('truetype');
        }
        body{
	        font-family: iconfont;
        }
	</style>
</head>
<body>
	<i class="iconfont">&#xe98b;</i>
</body>
</html>
```

==注意：@font-face引入样式后，需要将引入图标的位置的font-family设置为`iconfont`。然后通过i标签中的格式引入。==

3. 方式三：

![image-20210816134838402](https://i.loli.net/2021/08/16/IlYpzJjX6OKt8Fc.png)

```html
<!DOCTYPE html>
<html lang="zh">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="//at.alicdn.com/t/font_2745922_szujzs2sox9.css">
	<title>阿里图标库</title>
</head>
<body>
	<i class="iconfont icon-banquan"></i>
</body>
</html>
```

==注意：使用是通过类名来访问的，`iconfont`是必须的，后面的是图标的名称。==

4. 方式四：

<img src="https://i.loli.net/2021/08/16/GmpRLB81wZa2Erf.png" alt="image-20210816135146327" style="zoom:80%;" />

==特别注意：如果图标用于商业，需要联系作者，说明版权问题。==



## 23、行高

​	指的是文字占用的实际高度。

<font color=red>line-height属性：
</font>用于设置行高

​	行高可以指定一个指定的值（px，em），也可以指定一个整数，如果是整数，行高就是字体的倍数。行间距 = 行高 - 字体大小。

<font color=red>应用：
</font>

将字体居中显示，将行高设置为高度一样的值，使单行文字在元素中居中显示。



## 24、font属性

​	用于简写字体的样式。

格式：

```css
font: 其它样式 字体大小/行高 字体族;
```

==注意：其它样式的顺序可以改变，但是后面两个的样式的顺序不能改变。行高可以省略，但是默认是normal。==



## 25、文本样式

1. text-align属性：设置文本水平对齐方式。

可选值：

- left：默认值，左侧对齐。
- right：右侧对齐。
- center：居中对齐。
- justify：两端对齐。

2. vertical-align属性：设置文本垂直对齐方式。

可选值：

- baseline：默认值，基线对齐。
- top：顶部对齐。
- bottom：底部对齐。
- middle：居中对齐。

==注意：是相对于父元素来对齐的。==

3. text-decoration属性：用于设置文本修饰。

可选值：

- none：什么修饰都没有。
- underline：下划线。
- line-through：删除线。
- overline：上划线。

==注意：可以指定下划线的颜色即样式，但是在ie中不支持。==

```css
text-decoration: underline red dashed;
```

4. white-space属性：设置文本如何处理空白。

可选值：

- normal：正常显示。
- nowrap：不换行显示。
- pre：保留空白，按照书写的格式显示，不会去除空格，都用于显示代码。

5. text-overflow属性：设置文本溢出的处理。

可选值：

- clip：修剪文本。
- elipsis：显示省略号来代替文本。
- string：使用给定的字符串代替被裁剪的文本。

<font color=red>应用：</font>将多余的文本使用省略号显示。

```html
<div>
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum nulla quisquam quod voluptas! Animi 
    corporis cupiditate debitis et facere incidunt ipsa iste laboriosam ratione suscipit. A ad inventore quo totam.
</div>
```

```css
div{
    width: 200px;
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
}
```

==注意：必须指定一个宽度，white-space必须指定不换行，text-overflow指定使用省略号显示多余的文本。==



## 26、背景

1. background-color属性：设置背景颜色。

2. background-image属性：设置背景图片。

格式：`background-image: url("images/lab.png");`

拓展：

- 可以同时设置背景颜色和背景图片，这样背景颜色就会成为图片的背景颜色。
- 如果图片小于元素大小，则背景图片在元素中平铺重复出现。
- 如果图片大于元素大小，则有一部分会无法显示。

3. background-repeat属性：设置背景的重复方式。

可选值：

- repeat：默认值，背景会沿x、y轴双方向重复。
- repeat-x：沿着x轴方向重复。
- repeat-y：沿着y轴方向重复。
- no-repeat：背景图片不会重复。

4. background-position属性：用于设置背景图片的位置。

设置方式：

方式1：通过top、left、right、bottom、center几个表示背景图片的位置。

<img src="https://i.loli.net/2021/08/17/wEvk2VgtNYaUlqQ.png" alt="image-20210817160326448" style="zoom:80%;" />

```css
background-position: center left;
```

==注意：使用该属性必须指定两个值，如果只写一个值，另一个默认值center。==

方式2：可以直接指定偏移量来设置背景图片的位置。

```css
background-position: 100px 100px;
```

5. background-clip属性：用于设置背景的覆盖区域。

可选值：

- border-box：默认值，背景会出现在边框的下边。
- padding-box：背景不会出现在边框，只出现在内容区和内边距区。
- content-box：背景只会出现在内容区。

6. background-origin属性：设置图片的偏移量计算的原点。

- padding-box：默认值，背景图片的偏移量从内边距为原点计算。
- content-box：背景图片的偏移量从内容区为原点开始计算。
- border-box：背景图片的偏移量从边框为原点开始计算，一般不使用。

==注意：这个属性一般和background-position属性结合使用。==

7. background-size属性：设置背景图片的大小。

```css
background-size: 100px 100px;
```

第一个值是宽度，第二个值是高度，如果只写一个，第二个值默认是auto。

该属性也有其它的可选值：

- cover：图片的比例不变，将元素铺满。
- contain：图片的比例不变，将图片在元素中完整显示。

8. background-attachment属性：设置背景图片是否跟随元素移动。

可选值：

- scroll：默认值，背景图片会跟随元素移动。
- fixed：背景图片固定在页面，不会跟元素移动。

9. background属性：用于简写背景的各个样式。

   背景的所有样式都是可以使用background属性来设置，并且没有顺序要求，也没有哪个属性是必须写的。

<font color=red>注意点：</font>

- background-size必须写在background-position的后边，并且使用 / 隔开。
- background-origin必须写在background-clip的前边。

```css
background: #bfa url("images/1.图片1.png") no-repeat 500px 500px/100px 100px  padding-box content-box scroll;
```

![image-20210817171518868](https://i.loli.net/2021/08/17/hxY4X1rnSJc69Qa.png)



































​	









 













