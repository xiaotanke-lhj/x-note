# HTML



## 1、简介

**HTML 是用来描述网页的一种语言。**

- HTML 指的是超文本标记语言 (**H**yper **T**ext **M**arkup **L**anguage)
- HTML 不是一种编程语言，而是一种*标记语言* (markup language)
- 标记语言是一套*标记标签* (markup tag)
- HTML 使用*标记标签*来描述网页



## 2、我的第一个网页

```html
<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <title>我的第一个网页</title>
</head>
<body>
    <h1>我的第一个网页</h1>
</body>
</html>
```

一个基本的网页包括html、head、body、title等标签。

html：是网页的根标签

head：表示网页中的元数据，在网页中看不见，这个是给浏览器解析的

body：是网页的主体，用户能看见的都写在body中

title：是网页的标题，搜索引擎就会通过title来判断网页的主要内容。

title中的内容会被搜索结果显示的超链接。



## 3、自结束标签和注释

**自结束标签：**

​	在html中除了成对出现的标签（有开始有结束），还有自结束标签（只有开始）。

```xml
<img src="。。。" alt="。。">
```

**成对出现的标签：**

```html
<h1>我的第一个网页</h1>
```

**注释：**

​	不想在页面中显示出来，但是在源码中能看见。只是用来解释代码，给程序员看的。

```xml
<!--注释的内容-->
```

==注意：注释不能嵌套。==



## 4、标签中的属性

​	在开始标签或自结束标签中设置属性，但是不能在结束标签中设置属性。

属性是通过键值对设置的，公式 - 》属性名="属性值"。

```xml
 <h1>我的<font color="red">第二个</font>网页</h1>
```

color就是属性名，red就是属性值。

属性和标签名或其它属性使用空格隔开。属性是通过官方文档规定书写的。有些属性有属性值，但是有些没有属性值，直接使用属性名。



## 5、文档声明

​	用来告诉浏览器我们使用的版本，是html4、还是html5。

```html
<!doctype html> 方式1
<!Doctype Html> 方式2
```

使用上面的方式来声明文档，这个必须写在网页的第一行。



## 6、关于进制

**进制：**

- 二进制：满二进一，单位进制：0，1
- 八进制：满八进一，单位进制：0，1，2，3，4，5，6，7
- 十进制：满十进一，单位数字：0，1，2，3，4，5，6，7，8，9
- 十六进制：满十六进一，单位数字：0，1，2，3，4，5，6，7，8，9，A(a),B(b),C(c),D(d),E(e),F(f)



## 7、字符编码

​	所有的数据在计算机中都是以二进制存储的，所以一段数据在存储到内存中时都需要转换成二进制编码。当我们读取数据时，计算机就会将编码转换成字符。这个过程分为编码和解码。

**编码：**

​	将字符串转换成二进制的过程。

**解码：**

​	将二进制码转换成字符串。

**字符集：**

​	编码和解码所采用的规则。

==注意：如果编码和解码采用的字符集不一样就会导致字符乱码的问题==

**常见字符集：**

​	ASCII（美国使用的128个字符）、ISO8859-1（欧洲的256个字符）、GB2312（中国）、GBK（中国GB2312的扩展）、UTF-8（万国码，几乎包含了世界上所有国家的语言）

所以我们一般开发都是用UTF--8的字符集，在html页面中，需要在head标签中写一个meta标签来指定编码。

```html
<head>
    <meta charset="UTF-8">
    <title>标签的属性</title>
</head>
```



## 8、实体

​	在网页中编写多个空格，浏览器自动解析为一个空格。

所以在html中我们会书写一些特殊的字符，比如空格、大于、小于等。

如果我们需要在网页中添加一些特殊的字符，我们就需要书写实体（转义字符）。

**规则 ：**   &实体名;

**常见实体名：**

<img src="https://i.loli.net/2021/08/07/23ZLWEMzjxUNdIs.png" alt="image-20210807143834157" style="zoom:80%;" />



## 9、meta标签

​	meta写在head标签中，是给浏览器看的，是页面的元数据。必须有开始标签，不能有结束标签。

**meta的常见属性：**

- charset属性：设置页面的编码字符集

  - 值为各种字符集编码

- name属性：指定数据的名称

  - 值：

    author（作者）、description（描述）、keywords（关键词）、generator（生产者）、revised（改进）、others

- content属性：指定数据的内容，和name属性关联。

```html
<meta name="keywords" content="HTML网页设计">
```

keywords：页面的关键字，用于搜索引擎来搜索的，可以同时指定多个关键字，用逗号隔开。

description：页面的描述，用于指定网站的描述相当于自我介绍。

- http-equiv属性：

  - 值：

    content-type（文档类型）、expires（期限）、refresh（刷新）、set-cookie

```html
<meta http-equiv="refresh" content="3;url=https://www.baidu.com">
```

3表示3秒后跳转到url后面这个地址。



## 10、语义化标签

**标题标签：**（块元素）

​	一共有6级标题h1、h2、h13、h4、h5、h6，一到六，一级比一级小。但是语义化标签注重看重标签的语义。搜索引擎一般也会查看h1来判断搜索 内容，一般h1只有一个。

```html
<h1>我的<font color="red">第二个</font>网页</h1>
<h2>二级标题</h2>
<h3>三级标题</h3>
<h4>四级标题</h4>
<h5>五级标题</h5>
<h6>六级标题</h6>
```

**p标签：**（块元素）

​	p标签就是段落标签，就是页面中的一个段落。

**hgroup标签：**

​	它是用来给标题标签分组。

```html
<hgroup>
    <h1>我的<font color="red">第二个</font>网页</h1>
    <h2>二级标题</h2>
</hgroup>
```

**em标签：**（行类元素）

​	用于表示语音语调的加重，效果显示为斜体。

**strong标签：**（行类元素）

​	用于表示强调重要的内容，效果显示为加粗。

**blockquote标签：**（块元素）

​	表示一个引用别人说的话，效果为自动缩进。

**q标签：**（行类元素）

​	用于引用，一般用于短引用，效果自动加引号。

**br标签：**

​	一个自结束标签，用于换行。



## 11、快元素和行类元素

**快元素：**（block element）

​	在网页中一般使用快元素进行布局。块元素就是独占一行的元素，

**行类元素：**（inline element）

​	主要用来包裹文字，可以不独占一行。

一般情况下我们会在快元素中放行类元素，但是不会在行类元素放快元素。

==注意：快元素中基本能放所有元素，但是p标签（快元素）中不能放任何的块元素。==

浏览器在进行解析代码时，会自动对代码中的一些不规范的进行纠正。例如把元素写在html外边，会自动修改。p标签中添加快元素也会自动修正。



## 12、结构化语义标签

1、header标签：表示网页的头部。

2、main标签：表示网页的主题（一个页面只有一个）。

3、footer标签：表示网页的页脚，网页最后面的部分。

4、nav标签：表示网页中的导航。

5、aside标签：和主体相关的其内容（侧边栏）。

6、article标签：表示一个独立的文章。

7、section标签：表示一个独立的标签，上面的标签不能使用的时候，就可以使用该标签。

**但是上面的标签不会使用很多，在开发中一般使用div标签（块元素），它没有语义，和section相似。**



## 13、列表

**分为有序列表、无序列表、定义列表，它们都是快元素。**

1、有序列表：使用**ol**标签嵌套**li**标签使用,li表示列表项。

```html
<ol>
    <li>结构</li>
    <li>表现</li>
    <li>行为</li>
</ol>
```

![image-20210808132441341](https://i.loli.net/2021/08/08/1KWjBMHCFcQJVmD.png)

2、无序列表：使用**ul**标签嵌套**li**标签使用。

```html
<ul>
    <li>结构</li>
    <li>表现</li>
    <li>行为</li>
</ul>
```

![image-20210808132239020](https://i.loli.net/2021/08/08/YuLtzinoWB9wEsJ.png)



3、定义列表：使用**dl**标签创建一个定义列表，**dt**表示下定义的内容， **dd**表示对内容进行解释。这个可以用来做二级菜单。

```html
<!-- 定义列表-->
<dl>
    <dt>结构</dt>
    <dd>结构表示网页的结构</dd>
</dl>
```

![image-20210808135832886](https://i.loli.net/2021/08/08/ygPhAcpB7kWxUVj.png)

**注意：列表之间可以互相嵌套。**



## 14、超链接

超链接就是用于从一个页面跳转到另一个页面，或者当前页面的指定位置。

使用**a**标签定义（行类元素）

```html
<a href="#">超链接1</a>
```

href属性的值是指定跳转的位置，#表示不做任何跳转，常用作测试。**超链接中可以嵌套除了它自己以外的所有元素。**

![image-20210808140837274](https://i.loli.net/2021/08/08/E6cTF8S14dNZKi7.png)

紫色颜色的超链接表示访问过的超链接。超链接可以访问外部的页面，也可以跳转到自己的页面。

**相对路径：**

​	  	当我们需要跳转到自己的页面时，我们都会采用相对路径，相对路径就是相当当前文件来寻找文件。**./**表示当前目录，默认为**./**可以不写。**../**表示当前文件的所在目录的上一级。

**超链接的其它应用：**

1、target属性：

​	用来指定超链接打开的位置，常见的值_self、 _blank.

_self是默认值，可以在当前中打开超链接。

_blank是在新的窗口打开超链接。

```html
<a href="./列表.html" target="_blank">超链接3</a>
```

2、回到顶部：可以给href属性添加#。

```html
<a href="#">回到顶部</a>
```

3、跳转到当前页面的任意位置（锚点）：

例如去底部：需要给底部设置一个id属性，id在网页中是唯一不重复的，每一个标签都可以添加id，是每个标签的唯一标识。

```html
<a href="#bottom">去底部</a>
```

```html
<p id="bottom">底部</p>
```

例如去某个位置，需要给这个位置设置一个id属性。

```html
<a href="#some_where">去某个地方</a>
```

```html
<p id="some_where"> 某个地方 Lorem ipsum 
```

这样点击超链接就会跳转到相应的位置。

4、一个没有任何功能的超链接：

```html
<a href="javascript:;">一个没有任何作用的超链接</a>
```



## 15、图片

使用**img**标签来引入一个外部的图片，它是一个自结束标签。img是行内块元素（替换元素），既有**块元素**的特点，也与**行类元素**的特点。

**img的属性：**

1、src属性：它是指定图片的位置，可以使用相对路径，也可以引入其它网站的图片。

```html
<img src="img/lab.png" alt="">
```

2、alt属性：是对图片的描述，当图片显示不出来的时候就会显示出来，这个主要是给搜索引擎来判断图片是什么。

![image-20210808152158908](https://i.loli.net/2021/08/08/M8Yhv1SZEHGAoKz.png)

3、width和height属性：它们分别用来指定图片的宽度和高度，一般只会设置一个，另一个就会跟着一起等比例变化。



## 16、图片的格式

格式分为，jpeg（jpg）、gif、png、webp、base64

**jpg：**支持的颜色比较丰富，不支持透明效果，不支持动图。一般显示照片。

**gif：**支持的颜色较少，支持简单的透明，支持动图。一般用于单一的图。

**png：**支持的颜色比较丰富，支持复杂的透明，不支持动图。

**webp：**是谷歌推出的专门用来表示网页中的一种图片格式，它具备其它格式的所有优点，还小。缺点就是兼容性不好。

**base64：**将图片使用base64编码，将图片转换成字符，通过文字的形式引入。一般在使用图片和网页必须一起加载的时候使用。

==注意：效果一样使用小的，效果不一样就是用效果好的。==



## 17、内联框架

使用**iframe标签**，用于向当前页面中引入一个其它页面。

```html
<iframe src="https://www.qq.com" width="500px" height="500px" frameborder="0"></iframe>
```

**iframe属性：**

1、src属性：用于指定引入的网页地址，可以使用相对路径。

2、width和height属性：用于指定内联框架的宽度和高度。

3、frameborder属性：用于指定内联框架是否有边框，0表示没有，1表示有，不能改变边框的大小。



## 18、音频和视频

**音频：**

​	使用**audio标签**，用于引入外部的音频文件，引入时默认用户不能控制播放和暂停的。

引入方式一：

```html
<audio src="resources/music1.mp3" autoplay controls loop></audio>

```

引入方式二：

```html
<!-- 引入方式二-->
<audio autoplay controls loop>
    <source src="resources/music1.mp3">
    <source src="resources/music2.mp3">
</audio>
```

**audio标签的属性：**

1、src属性：指定引入文件的路径，可以使用相对路径。

2、controls属性：用于设置用户能否控制音频的播放。

3、autoplay属性：一个没有值的属性，设置音频打开网页时自动播放，如果设置了autoplay则打开网页就会自动播放，但是大多数的浏览器不支持这个属性。

4、loop属性：这个设置当音频播放完后是否循环播放。

**拓展：**

​	audio存在很多的浏览器不兼容的情况。

这就要使用第二种引入方式来解决部分问题。

第二种方式可以指定**多个source**，但是只会加载一个音频，如果第一个支持，则第二个就不会显示。如果所有的source都不支持，则可以以下操作：

```html
<audio autoplay controls loop>
    <source src="resources/music1.mp3">
    <source src="resources/music2.mp3">
    <p>您的浏览器不支持音频播放，请升级浏览器！</p>
</audio>
```

如果都不支持，就会显示p标签中的内容。

IE8之前（包括）浏览器中支持一个**embed标签**来引入标签，之前的IE都不支持audio标签。

```html
<!-- IE8之前的音频引入-->
<embed src="resources/music1.mp3" type="audio/mp3" width="" heigth="">
```

这个可以和之前的第二种方式来嵌套使用，这样就解决兼容问题。

```html
<!-- 引入方式二-->
<audio autoplay controls loop>
    <source src="resources/music1.mp3">
    <source src="resources/music2.mp3">
    <!-- IE8之前的音频引入-->
    <embed src="resources/music1.mp3" type="audio/mp3" width="" heigth="">
    <p>您的浏览器不支持音频播放，请升级浏览器！</p>
</audio>
```

**embed标签属性：**

1、src属性：指定引入的路径，可以使用相对路径。

2、type属性：指定引入的文件类型，audio/文件类型（video/文件类型）。

3、width和height属性：指定宽和高。

**视频：**

​	使用video标签引入一个视频

引入方式和音频的引入方式一样，属性用法也是差不多的。

在IE8之前也是不支持video标签的，也是可以采用与音频一样的解决办法。

```html
<video controls autoplay loop>
    <source src="resources/video1.mp4">
    <source src="resources/video1.mp4">
    <!-- IE8之前的音频引入-->
    <embed src="resources/video1.mp4" type="video/mp3" width="" heigth="">
    <p>您的浏览器不支持音频播放，请升级浏览器！</p>
</video>
```

==注意：一般在实际开发中，我们不会把视频放在服务器上，我们通常会把视频托管在第三方，然后通过地址引入。还可以把我们的视频上传到视频软件上，例如腾讯视频、爱奇艺等==

![image-20210808172537296](https://i.loli.net/2021/08/08/LsmPjDE6alnv2KW.png)



## 19、表格

​	在现实生活中，我们经常使用表格来展示一些格式化数据，例如课程表、成绩单、人名表等。

表格使用**table标签**，嵌套**tr**，然后tr嵌套**td**来使用。tr表示行，td表示列。

```html
<table>
    <tr>
        <td>单元格</td>
        <td>单元格</td>
        <td>单元格</td>
    </tr>
    <tr>
        <td>单元格</td>
        <td>单元格</td>
        <td>单元格</td>
    </tr>
</table>
```

![image-20210808173126808](https://i.loli.net/2021/08/08/KyPmFNfzsHxhXVk.png)

**table标签的属性：**

1、border属性：来指定表格是否有边框。

2、width和height属性：来指定表格的框宽高。

3、align属性：来指定表格文字的排列方式，有center、left、right等值。

4、cellspacing属性：来指定表格边框之间的距离。

**tr有两个属性rowspan和colspan：**

1、rowspan属性：纵向合并单元格。

```html
<tr>
    <td>单元格</td>
    <td>单元格</td>
    <td rowspan="2">单元格</td>
</tr>
```

![image-20210808173839817](https://i.loli.net/2021/08/08/PLKRaVdNMZ4IAE8.png)

2、colspan属性：横向合并单元格。

```html
<tr>
    <td>单元格</td>
    <td colspan="2">单元格</td>
</tr>
```

![image-20210808173935594](https://i.loli.net/2021/08/08/5MVceAZQzjkiOYs.png)

==注意：在实际开发中一般不这么设置表格的样式，我们通常使用CSS来设置表格样式==

**表格中的thead、tbody、tfoot标签：**用来区分表格的结构。

**1、thead标签：**用于表示表格的头，依然要使用tr、td嵌套。

**2、tbody标签：**表示表格的主体。

**3、tfoot标签：**表示表格的底部，总是在表格的下边。

```html
<table border="1" cellspacing="0">
    <thead>
        <tr>
            <td>单元格</td>
            <td>单元格</td>
            <td rowspan="2">单元格</td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>单元格</td>
            <td>单元格</td>
        </tr>
    </tbody>
    <tfoot>
        <tr>
            <td>单元格</td>
            <td colspan="2">单元格</td>
        </tr>
    </tfoot>
</table>
```

**th标签：**用来代替td标签的，用作表头，默认自动加粗。

```html
<thead>
    <tr>
        <th>单元格1</th>
        <th>单元格2</th>
        <th rowspan="2">单元格3</th>
    </tr>
</thead>
```

![image-20210808175421060](https://i.loli.net/2021/08/08/luitQOfKgJ6rPHR.png)



## 20、表单

在现实生活中表单用于提交数据，**在网页中的表单将本地的数据提交到服务器。**例如注册表单、登录表单等。

使用form创建一个表单，在表单中可以添加各种**表单项input**。

```html
<form action="#">
   <!-- 添加表单项-->
</form>
```

**form标签的属性：**

1、action属性：用来指定表单提交的那里，一般提交到服务器，#用来测试，不会提交到任何地方。

**常见表单项：**

1、文本框：在文本框中可以输入数据。

```html
文本框：<input type="text">
```

![image-20210808181133325](https://i.loli.net/2021/08/08/UFI7lTcWEpyjr6S.png)

2、提交按钮：

```html
提交按钮：<input type="submit" value="提交">
```

![image-20210808181200959](https://i.loli.net/2021/08/08/fzXcbt1nUmVhsMB.png)

提交按钮默认的是提交两个字，可以设置**value属性**指定自己想要的字符。

3、密码框：

```html
密码框：<input type="password">
```

![image-20210808181318193](https://i.loli.net/2021/08/08/rAPaJqe5ZQX13Bb.png)

4、单选按钮：

```html
单选按钮：<input type="radio" name="A" value="A1" checked>A1
		<input type="radio" name="A" value="A2">A2
```

![image-20210808181638215](https://i.loli.net/2021/08/08/tVAjvHJsOZRaK1Y.png)

单选框必须指定name属性来进行分组，每组的name属性的值必须相同，自己指定。value属性是提交到服务器的值。

指定**checked**就是默认选中的。

5、多选按钮：

```html
多选按钮：<input type="checkbox" value="A1" checked>A1
		<input type="checkbox" value="A2">A2
		<input type="checkbox" value="A3">A3
		<input type="checkbox" value="A4">A4
```

![image-20210808181946452](https://i.loli.net/2021/08/08/m5yhkYpgKE92OM8.png)

value值指定提交到服务器的值，checked指定默认选中。

6、下拉列表：

 ```html
 下拉选择框：<select name="city" multiple>
 			<option value="chognqing">重庆</option>
 			<option value="beijing" slected>北京</option>
 			<option value="sahnghai">上海</option>
 		</select>
 ```

![image-20210808182515744](https://i.loli.net/2021/08/08/ZbKaSuFtTHBhRx2.png)

name指定提交到服务器的名称，multiple可以设置可以多选，每一个option标签就是一个选项，value是提交到服务器的值，slected是指定默认选中的。

7、普通按钮：就是用来点击的，可以组合超链接使用。

```html
普通按钮：<input type="button" value="按钮">
```

![image-20210808185004336](https://i.loli.net/2021/08/08/mTbMUBWhtGELD2S.png)

value是指定点击的值，默认无值。

8、重置按钮：用来重置表单中的所有表单项，恢复到默认值。

```html
重置按钮：<input type="reset" value="重置">
```

![image-20210808185302961](https://i.loli.net/2021/08/08/p9xHBYkcrdmAjaI.png)

9、button标签：

```html
<button type="submit">提交</button>
<button type="reset">重置</button>
<button type="button">按钮</button>
```

这个和上面三个按钮是一样的，一一对应。

10、color表单：用于选取颜色的表单，但是这个表单有很多浏览器不支持。

```html
<input type="color">
```

![image-20210808195829491](https://i.loli.net/2021/08/08/ndLxIUBrX1w9W5v.png)

11、email表单：提交的时候，会对输入的内容进行检查，如果不符合邮箱格式就提交失败。

```html
邮箱表单：<input type="email">
```

![image-20210808200113635](https://i.loli.net/2021/08/08/ZbXRAvIcCyOEf4a.png)

**拓展：**

1、autocomplete属性：

一些文本框中可以设置自动完成功能，就是当你聚焦输入框的时候，他会在下面给你展示你之前输入过的，你点击就会自动填写。

```html
文本框：<input type="text" autocomplete="on">
```

autocomplete属性就是设置自动完成功能，on表示开启，off表示关闭。

2、readonly属性：把表单设置为只读，不能够输入，没有值，但是还是能提交到服务器。

3、disabled属性：把表单禁用，不能够使用该表单，没有值，设置后就不能提交到服务器。

4、autofocus属性：自动获取焦点，没有值。

==注意：在表单中，如果需要提交到服务器上，就需要给表单指定name属性，name的值是自己给定，服务器通过name属性来获取表单提交的数据。==

