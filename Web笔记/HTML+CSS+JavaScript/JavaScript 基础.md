[TOC]

# JavaScript 基础



## 1、`javascript` 简介

​	它诞生于1995年，主要用于处理网页中的前端验证功能，就是检查用户输入的内容是否符合一定的规则。比如用户名的格式、长度、邮箱格式等，用于用户的交互。

- JavaScript 是 Web 的编程语言。

- 所有现代的 HTML 页面都使用 JavaScript。

- JavaScript 非常容易学。

- 本教程将教你学习从初级到高级 JavaScript 知识。

<font color=red>学习目标：</font>

​	由于`javascript`整体是由`ECMAScript`、`DOM`、`BOM`组成，所以我们主要围绕这三个部分学习。`ECMAScript`是标准、`DOM`是文档对象，操作网页、`BOM`是浏览器对象模型，操作浏览器的。

<font color=red>特点：
</font>

- 解释型语言，写完直接运行，由浏览器解释给计算机。
- 类似于其它高级语言结构。
- 动态语言。
- 基于原型的面向对象。



## 2、第一个`javascript`程序

​	`js`和`css`写的地方类似，需要写在head标签中的script标签中。

```html
<head>
	<meta charset="UTF-8">
	<title>第一个javascript网页</title>
	<script type="text/javascript">
		alert("Hello,world");
		document.write("Hello,world");
		console.log("Hello,world");
	</script>
</head>
```

1. `alert`：网页中弹出一个警告框。

![image-20210819223109890](https://i.loli.net/2021/08/19/qUACM1rhTKcinSt.png)

2. `document.write`：文档，就是网页，上面的指令是向文档中写一句话。

3. `console.log`：向浏览器控制台输出一句话。

![image-20210819223304285](https://i.loli.net/2021/08/19/LIhDoz9G5wsr2FJ.png)



## 3、`js`代码编写的位置

1. 将`js`代码写在标签的`onclick`属性中，当点击时就会执行`js`代码：

```html
<button onclick="alert('点击成功')">点击我一下</button>
```

2. 将`js`代码写在超链接的`href`属性中，当点击超链接的时候，会执行`js`代码：

```html
<a href="javascript:alert('点击了超链接');">超链接</a>
```

==注意：`js`代码必须写在`javascript:`的后面。不推荐使用上面两种方式，耦合度差。==

3. 可以写在script标签中，script标签可以写在网页的任意位置：

```html
<script type="text/javascript">
   alert("Hello,world");
   document.write("Hello,world");
   console.log("Hello,world");
</script>
```

4. 写在`js`文件中，然后通过script标签引入`js`文件：

```html
<script src="js/第一个javascript网页.js"></script>
```



## 4、`js`基本语法

1. `js`中的注释：

- 单行注释：使用 `//` 进行注释。

```js
// 单行注释
```

- 多行注释：使用 `/*  */` 注释。

```js
/*
   多行注释
   注释的内容
 */
```

- 快注释：使用 `/****/` 注释，推荐使用。

```js
/**
 * 
 * 注释的内容
 * 
 * 
 */
```

2. `js`中的注意事项：

- `js`代码严格区分大小写。
- `js`代码每一句都必须以分号结尾。
- `js`代码中会忽略多个空格和换行。



## 5、字面量和变量

1. 字面量：一些不可改变值得量。
2. 变量：可以用来保存字面量，变量得值可以任意改变。

- 声明变量：使用var来声明变量。

```js
var c;
```

- 变量赋值：使用字面量为变量赋值。

```js
var c;
c = 10;
```

- 声明变量和变量赋值可以同时进行。

```js
var c = 10;
```



##  6、标识符

​	在`js`中所有得可以由我们自定以命名得都属于标识符，例如变量名、函数名等。

<font color=red>规则：	</font>

1. 标识符中只能包含数字、字母、_、$。
2. 标识符不能以数字开头。
3. 标识符中不能使用`js`中得关键之和保留字为标识符。
4. 标识符一般采用驼峰命名法：就是首字母小写，每个单词得字母大写，其余字母小写。

<font color=red>拓展：</font>

标识符在`js`底层中实际时Unicode编码，所以所有得`utf-8`中含有得内容都可以作为标识符，例如中文也可以作为标识符，但是不建议使用。

<font color=red>js关键字：</font>

![image-20210820141041488](https://i.loli.net/2021/08/20/jUyL2BlRSO93ZgC.png)

## 7、数据类型

<font color=red>js得六种基本类型：</font>

- String：字符串类型
- Number：数值类型
- Boolean：布尔值类型
- Null：空值
- Undefined：未定义类型
- Object：对象类型

前五种属于基本数据类型，而Object属于引用数据类型。

<font color=red>typeof：</font>使用该指令来检查一个变量得类型。

语法：`typeof` 变量名

```js
console.log(typeof c);
```

在控制台输出c变量的类型。

1. String字符串：

- 在`js`中字符串使用单引号或双引号包裹起来。
- 注意引号之间不能嵌套。
- 可以使用 / 作为转义字符来书写特殊字符。

例如：/'' 表示一个双引号，/t 表示制表，/n 表示换行等

2. Number数据类型：分为整数和浮点数（小数）。

Number中的几个特殊的值：

- `Number.MAX_VALUE`：表示一个最大的值，`1.7976931348623157e+308`。
- `Number.MIN_VALUE`：表示一个大于0的最小值。`5e-324`

- `Infinity`：表示比`Number.MAX_VALUE`还要大的值，正无穷。
- `-infinity`：表示负无穷。
- `NaN`：表示一个特殊的数字，Not a Number。用于检查一个值是否为一个数字。

==注意：使用`typeof`来检查`NaN`会返回number类型。==

==注意：在`js`中整数的计算可以满足需求，但是使用浮点数计算可能会导致精确度丢失，一般不在页面进行浮点数计算，在服务器上进行浮点数计算。==

3. Boolean类型：用于逻辑判断的值，一共由两个true和false。

- true：表示真。
- false：表示假。

4. Null和Undefined数据类型：

Null：只有一个值null，表示一个为空的对象，就是这个对象不存在。使用`typeof`来检查该类型，会返回一个object类型。

Undefined：只有一个值undefined，当声明一个变量但是不给它赋值，这个变量就是undefined，表示未定义。



## 8、类型转换

​	指将一个数据类型转换成其它的数据类型，主要是把其它的数据类型转换成，String、Number、Boolean。

<font color=red>将其它数据类型转换成String：</font>

方式一：调用被转换数据类型的`toString()`方法。

- 该方法不会影响到原变量的，它会将转换的结果返回，但是可以直接用该变量来接受返回值。

```js
var a = 123;
a = a.toString();
```

这样就将a转换成String类型。

- 注意，Null和Undefined没有`toString()`方法，不能使用该方式来将Null和Undefined转换成String。

方式二：调用String()函数，将被转换的数据作为参数传递给函数。

```js
var b = 234.23;
b = String(b);
```

这样就把b转换成String类型。

- 该函数也是可以将Null和Undefined类型转换成string类型。
- String()函数的底层对于Number和Boolean直接调用的是`toString()`方法，对于Null和Undefined是直接把null转换成`"null"`，直接转换成字符串。 

<font color=red>将其它数据类型转换成Number：</font>

方式一：使用Number()函数进行转换。

- 将字符串--》数字
  1. 如果是纯数字的字符串，则直接转换成数字。
  2. 如果存在非数字的内容，则转换成`NaN`。
  3. 如果字符串是空串或者全是空格，则转换成0。

- 将Boolean--》数字：true转换成1，false转换成0。
- 将Null--》数字：0
- 将Undefined--》数字：`NaN`。

```js
var c = "1233";
c = Number(c);
```

方式二：使用`parseInt()`和`parseFloat()`函数进行转换，针对于字符串的方法，只获得有效部分，无效部分全部不要。

- `parseInt()`，将字符串中的==有效整数部分==取出来，然后转换成数字，从字符串的第一个字符开始，遇到非数字的字符就停止。

- `parseFloat()`，和上面的函数类似，不同的是它可以获得小数。

```js
c = "190.23px";
c = parseFloat(c);
```

有效部分就是190.23，则转换后就是190.23。

```js
c = "das190.23px";
c = parseFloat(c);
```

这就不存在有效部分，就转换成`NaN`。

- 对于Null、Boolean、Undefined，该函数是先转换成字符串，然后再操作，所以这些类型全部会转换成`NaN`。

<font color=red>拓展：
</font>

​	在`js`中，可以表示16、8、2进制数。

- 以`0x`开头的数字是表示16进制的数。
- 以`0`开头的数字表示8进制的数。
- 以`0b`开头的数字表示2进制数，但是二进制可能存在浏览器不兼容的情况。

所以，在进行字符串转换成数字的时候，可以给它指定一个进制。

```js
c = "070";
c = parseInt(c,8);
```

参数和进制之间使用逗号隔开，"070"字符串就会以8进制进行转化。

<font color=red>将其它数据类型转换成Boolean：</font>

- 数字--》`Boolean`：0和`NaN`转化为false，其余全都是true。

- 字符串--》Boolean：空串转化是false，其余全是true。
- Null和Undefined--》Boolean：都转换成false。
- 对象Object--》Boolean：转换成true。



## 9、运算

### 1、<font color=red>算术运算符：</font>

+（加）、-（减）、*（乘）、/（除）、%（取余）。

​	在进行非Number类型的值进行计算时，会将这些值转换成Number然后再进行计算，任何值和`NaN`计算都为`NaN`。

```js
c = "123"*12;
```

计算结果是先将`"123"`转换成Number，然后再和12相乘。

```js
c = true*12;
```

计算结果是先将`true`转换成Number，是1，然后再和12相乘。

<font color=red>拓展：</font>当字符串进行加法的时候，会进行字符串拼接，都会先转换成字符串，然后拼接字符串。

```js
c = "123"+"你好";
```

c就是String类型的"123你好"。

==注意：要注意运算顺序，`c = 1+2+"123";`会先算前面的1+2，然后进行字符串拼接，结果为"3123"。

### 2、<font color=red>一元运算符：</font>

+（正号）、-（负号）

对于非Number类型的值，它会将先转换成Number，然后进行运算，转换原理和Number()函数一样。

### 3、<font color=red>自增和自减：</font>
- 自增（++）：通过自增使变量在自身的基础上增加1，原变量的值会立即自增1。

分为前++和后++，不同点的是 a++ 表达式的值是等于原变量的值， ++a 表达式的值等于自增后的值。

- 自减（--）：和自增类似。

==注意：a = a++；a的值还是原来的值，表达式的值是赋值给a。==

### 4、<font color=red>逻辑运算符：</font>

- 非（!）：可以用来对一个值进行非运算（取反）。

对于boolean值进行取反，true变成false，false变成true。

对于非boolean值进行取反，会先把该值转换成boolean，转换方式和Boolean()函数的原理一样，然后再进行取反，返回Boolean类型。

- 与(&&)：可以对符号两侧的值进行与运算，并返回结果。

```js
var result = true && false;
```

两个值中，只要有一个值为false，就返回false。两个值都为true才返回true。

与运算是短路运算，如果第一个值为false，就不会检查第二个值，返回false。

- 或(||)：可以对符号两侧的值进行或运算，并返回结果。

两个值中，只要有一个值为true，就返回true。两个值都为false才返回false。

与运算是短路运算，如果第一个值为true，就不会检查第二个值，返回true。

<font color=red>对非boolean值得与或运算：
</font>

会先非boolean的值转换成Boolean类型，然后再进行计算，但是返回的还是之前的值。

- 与：

两个值都为true，返回后面的值；如果存在false就返回false对应的值；两个都是false则返回前面一个false的值。（短路原理）。

- 或：

两个值都为true，返回前一个的值；如果存在true则返回true对应的值；如果两个都是false则返回后一个false。（短路原理）

### 5、<font color=red>赋值运算符：</font>

=、+=、-=、*=、/=、%=。

赋值都是把右边的值赋值给左边的变量。

```js
var a = 10;
```

a+=1：简写方式，等价于a = a+1，上面的其它运算符类似。

### 6、<font color=red>关系运算符：</font>

​	>、>=、<、<=

比较两侧的关系大小，如果关系大小成立则返回true，不成立则返回false。

- 对于数值的关系比较和数学中的关系比较类似。

- 对于非数字的比较

它会先转换成数值类型的值，然后进行比较，转换原理和Number函数类似。

==注意：任意数值和`NaN`比较都是false。==

- 两个值都是字符串的比较

如果两个值都是字符串，则按照Unicode编码来进行关系运算，一位一位进行比较。

==注意：符号间不能有空格。==

### 7、<font color=red>相等运算符：</font>

==、!=、===、!==

相等（==）：比较两个值是否相等，如果相等，则返回true，不相等返回false。

- 两个数值的相等运算和数学的类似。
- 非数字的相等运算，一般情况会先转换成数字（自动转换），然后再进行运算。
- 特殊情况：
  1. null类型和0进行相等运算，虽然null转换成number是0，但是返回false。
  2. undefined和null进行相等运算，返回true。
  3. `NaN`不和任何值相等，包括自己本身。

<font color=red>拓展：
</font>判断一个数字是否`NaN`，使用`isNaN()`函数来判断。

```js
var t = NaN;
result = isNaN(t);
```

如果是`NaN`则返回true，反之返回false。

不相等（!=）：用来判断两个值是否不相等，不相等则返回true，与不相等类似。

全等（===）：用来判断两个值是否全等，不会做自动类型转换，类型不同就会返回false。

不全等（!==）：用来判断两个值是否全等，不全等则返回true，反之。

### 8、<font color=red>条件运算符：</font>

也称三元运算符。

语法：条件表达式?语句1:语句2;

如果条件成立则执行语句1并返回结果，条件不成立则执行语句2并返回结果。

如果条件表达式的返回值是非boolean值，它会自动转换成boolean类型的值。

### 9、<font color=red>运算符优先级：</font>

![image-20210830234848120](https://i.loli.net/2021/08/30/PSAeLJM9uYwROoQ.png)

- 从下到上，优先级越来越高。
- 优先级相同，则从左往右依次计算。
- 括号可以改变优先级，是最高的优先级。



## 10、语句

### 1、<font color=red>条件判断语句：</font>

- 语法一：

if(条件表达式){

​	执行的语句快

}else{

}

```js
var x  = 10;
if(x == 10){
    // 满足条件执行的语句
    alert("x等于10！");
}else {
    // 不满足条件执行的语句
    alert("x不等于10！");
}
```

如果条件表达式满足，则执行`if`后的代码块；如果不满足表达式就执行`else`后的代码快。

- 语法2：

```js
var x  = 10;
if(x == 10){
    // 满足条件执行的语句
   alert("x等于10！");
}else if (x > 10) {
    // 满足条件执行的语句
   alert("x大于10！");
}else{
    alert("x小于10！");
}
```

条件判断会从上到下依次检查，如果遇到true就执行相应的代码快，但是在该语句中只有一个代码块执行，执行完后就会结束，为false就往下继续判断。

### 2、switch语句：

​	条件分支语句

语法：

~~~javascript
switch(条件表达式){
    case 条件表达式1:
        语句1;
        break;
        
    case 条件表达式2:
        语句2;
        break;
        
    case 条件表达式3
        语句3;
        break;
        
    default:
        语句4;     
} 
~~~

表示case后面的条件表达式和switch后面的条件表达式进行==全等比较==，如果为true则执行case对应的语句，如果为false则向下继续比较。break是用于跳出循环，遇到break就跳出条件分支语句。

 default是用于没有匹配的条件表达式的执行语句。

~~~js
var score = 78;
switch (true){
    case score>60:
        alert("成绩合格!");
        break;
    default:
        alert("成绩不合格");
}
~~~

==使用prompt用于让用户输入一个值。==

```js
var num = prompt("请输入数值：");
alert(num);
```

![image-20211002212837805](https://i.loli.net/2021/10/02/Z4Rg2f5t7Yb6QzD.png)

### 3、while语句：

​	循环语句。

语法：

~~~js
while(条件表达式){
    执行的语句;
}
~~~

当满足条件表达式时，就执行while中的代码，执行完后再次判断，知道不满足条件表达式就完成while语句的执行。

==注意：可以使用 break; 来跳出while循环。==

### 4、do...while循环：

语法：

~~~js
do{
    执行的语句;
}while(条件表达式);
~~~

do...while(); 循环是执行一次循环中的语句，然后再进行判断是否满足表达式，如果为true就再执行while，如果为false就结束循环。==这个循环至少执行一次==。

==注意：while的括号后面有一个 ; 分号，不写会报错。==

### 5、for循环：

语法：

~~~js
for(初始化表达式;条件表达式;更新表达式){
    循环体;
}
~~~

执行顺序：

​	初始化-------》条件表达式------》循环体-----》更新表达式---》条件表达式----》。。。。。。

==初始化只执行一次，后面就和while类似，知道条件表达式不满足后就跳出循环。==

==这三个表达式可以省略不写，但是两个分号必须要有。==

==for循环和while循环之间可以相互转换。==

### 6、break和continue关键字

<font color=red>break：
</font>

​	==用于退出循环和switch语句。但是在循环嵌中，break只会跳出一层循环（立即终止离他最近的循环语句）。if语句中不能使用break关键之。==

为循环取名称：

​	break只可以跳出最近的一次循环，但是我们需要跳出多次循环，这就需要我们给循环取名称，然后跳出指定的循环。

```js
outer:
for(var i=0;i<5;i++){
    console.log("i == "+i);
    for(var j=0;j<5;j++){
        break outer;
        console.log("j == "+j);
    }
}
```

outer 就是外层循环的名称，里层的 break 跳出的循环就是跳出外层的循环。

<font color=red>continue：
</font>

​	==表示跳过这一次的循环，跳过这一次的下面的语句，但是不会结束循环。也是对最近的循环产生影响。==

==也可以对continue取标签，用法和break的标签类似。==



# JavaScript 基础进阶



## 1、对象

​	我们介绍了5种基本的数据类型，处理这5种的基本数据类型，还有对象这一类型。基本的数据类型之间不存在类和的关系，但是对象间存在一定的联系。

==对象属于复合的数据类型，在对象中可以保存多个不同数据类型的属性。==

<font color=red>对象的分类：
</font>

1. 内建对象：

   由ES标准中的对象，在任何ES的实现中都可以实现。比如：Math、String、Number....等

2. 宿主对象：

   由`JS`的运行环境提供的对象，主要有两组：DOM（文档模型）、`BOM`（浏览器模型）。例如`consonle`、document

等。

3. 自定义对象：

   由开发人员自己创建的对象。

### 1.1、创建对象

语法：使用 new 关键字来构建对象。

~~~js
var 对象名 = new Object();
~~~

这个就创建了一个对象，这个对象中没有属性，我们就可以往对象中添加属性了。

例如：

~~~js
var object = new Object();
~~~

<font color=red>对象中添加属性：
</font>

语法：

~~~js
对象名.属性名 = 属性值;
~~~

例如：

~~~js
object.name = "张三";
~~~

这就给 object 这个对象添加了一个 name 属性，值为张三。

<font color=red>读取对象中的属性：
</font>

语法：

~~~js
对象名.属性名;
~~~

例如：

~~~js
object.name;
~~~

这就取出了 object 这个对象中的 name 属性的值。如果这个对象中没有这个属性，读取的值就是 undefined 。

<font color=red>修改对象中的属性值：
</font>

语法：

~~~js
对象名.属性名 = 新属性值;
~~~

例如：

~~~js
object.name = "李四";
~~~

这就把object这个对象的属性值更新为李四了。

<font color=red>删除对象的属性值：
</font>

语法：

~~~js
delete 对象名.属性名;
~~~

例如：

~~~js
delete object.name;
~~~

这就把object对象的name属性删除了。

### 1.2、属性名和属性值

<font color=red>属性名：
</font>

​	在`js`中的属性的名称没有强制的规范，可以遵守标准的命名规范。

如果属性名为特殊的值，需要采用下面的方法进行添加属性：

语法：

~~~js
对象名["属性名"] = 属性值;
~~~

例如：

~~~js
object["test"] = "张三";
~~~

这就给object这个对象添加了一个test属性，它的值为张三。

==注意：[ ] 中是一个字符串，也可以是一个变量。==

如果采用上面的方法给对象添加属性，在取属性值时必须采用下面的方式来获取属性值。

~~~js
对象名["属性名"];
~~~

<font color=red>属性值：
</font>

== `js` 对象的属性值可以时任意的数据类型，也可以是一个对象。==

<font color=red>in 运算符
：
</font>用于检查一个对象中是否含有指定的属性。

语法：

~~~js
"属性名" in 对象;
~~~

例如：

~~~js
"name" in object;
~~~

表示检查object这个对象中是否含有name这个属性，如果有这个属性就返回true，没有这个属性就返回false。

### 1.3、对象字面量

​	使用对象字面量，可以在创建对象时，直接指定对象的属性。

语法：

~~~js
var obj = {
   属性名:属性值,
}
~~~

例如：

~~~js
var obj = {
    name:"张三",
    age:19,
    sex:"男"
}
~~~

==字面量的属性名可以加 "" 引号，也可以不加引号，但是使用特殊的属性名就必须加引号。==

==属性名和属性值是一组名值对结构，属性名和属性值之间使用 : 隔开，最后一个属性的后面没有逗号，其它属性的后面都需要更一个逗号。==

==对象与对象之间可以嵌套。==

例如：

~~~js
var obj = {
    name:"张三",
    age:19,
    test:{
        name:"李四",
        age:20
    }
}
~~~

上面的表示 obj 对象中有一个name属性、age属性、test对象。

### 1.4、查询对象的属性

​	在实际开发中我们一般需要知道一个对象中用于什么属性和方法，我们使用 for...in... 来查询对象中的属性和方法。

语法：

~~~js
for(var n in 对象名){
    // n就是对象每个属性的名称
    console.log(n);
    // 也可以取到每个属性对应的属性值
    console.log(对象名[n]);
}
~~~

也可以用于检查一个对对象中是否含有这个属性。

语法：

~~~js
console.log("属性名称" in 对象名称);
~~~

如果对象存在属性就返回true，否则返回false。

==如果对象的原型（后面介绍）中有该属性，也会返回true。==

==注意：属性名要加引号，是一个字符串。==



## 2、函数

​	函数也是一个对象，在函数中可以封装一些功能，在需要的时候掉用函数就执行封装的代码。

### 2.1、函数的创建

<font color=red>构造创建一个函数：
</font>使用 new 关键字来创建一个函数对象。

语法：

~~~js
var 函数名 = new Function();
~~~

==我们可以将要封装的代码以字符串的形式传给构造函数。==

例如：

~~~js
var fun = new Function("console.log('你好')");
~~~

这就创建了一个函数，当函数调用的时候就执行对应的封装代码。

==在实际开发中，我们很少几乎不适用该方法来创建一个函数对象，我们一般采用下面的两种方式。==

<font color=red>函数声明创建函数：
</font>

语法：

~~~js
function 函数名([形参]){
    函数体(封装的代码);
}
~~~

== [ ]表示可选，即可以传入参数也可以不传入参数。==

例如：

~~~js
function fun(){
    console.log("你好");
}
~~~

<font color=red>函数表达式：
</font>

语法：

~~~js
var fun = function([参数]){
    函数体;
}
~~~

==这个使用的是匿名函数的声明方式，然后把函数对象赋值给 fun 这个变量，这样使 fun 成为一个函数对象。==

### 2.2、函数的调用

语法：

~~~js
函数名();
~~~

这样就表示调用了对应的函数，就会执行函数封装的代码。

==我们使用 `typeof 函数名`它返回的是一个 function 类型的值。==

### 2.3、函数的参数

​	可以在函数的 () 中指定一个或多个形式参数，多个形参之间使用逗号隔开，相当于在函数内部声明了一个变量。

==实参是在函数调用时指定的参数，实参会赋值给形式参数。函数调用时不会检查实参的类型。==

例如：

~~~js
function sum(a,b){
    console.log(a+b);
}
~~~

==调用函数时，解析器也不会解析实参的个数，多余的实参不会赋值。如果实参数量少于形参的数量，没有对应的形参将会被赋值为 undefined 。==

==函数的实参可以是任意的数据类型。==

### 2.4、函数的返回值

​	我们使用 return 关键字来使函数返回函数的结果。

语法：

~~~js
function 函数名(参数){
    函数代码;
    return 结果;
}

var result = 函数名();
~~~

==在函数调用的时候使用一个变量来接收函数的返回值。==

例如：

~~~js
function sum(a,b){
    var r = a+b;
    return r;
}
var result = sum(1,2);
~~~

这就表示将1和2的和通过 sum 函数计算后返回给 result 变量。

==当一个函数没有返回值时，使用变量来接受返回值会接收一个 undefined 类型，当不返回一个任何值时（ `return;` ），接收到的也是一个 undefined 类型的值。==

==当函数执行了 return 后，函数就标志着结束，后面的代码就不会再继续执行了。==



## 3、作用域

​	作用域：指一个变量的作用范围。

分类：全局作用域、函数作用域。

### 3.1、全局作用域

- 直接编写在 `script` 标签中的`JS`代码都属于全局作用域。
- 全局作用域在页面打开时就创建，在页面关闭时就销毁。
- 在全局作用域中有一个全局对象 window ，可以直接使用。

==在全局作用域中创建的对象都是作为 window 对象的属性保存的，函数就作为 window 对象的方法保存。==

```js
var a = 10;
console.log(window.a);
```

==一旦声明了一个全局变量，在页面的任何位置都可以使用这个变量，即使使用外部引入的方式，但是要注意声明和调用的先后顺序。==

### 3.2、函数作用域

​	调用函数时创建函数作用域，函数执行完毕以后，函数作用域销毁。

<font color=red>注意事项：
</font>

1. ==在函数作用域中可以访问到全局作用域中的变量，全局作用域中不能访问函数作用域中的变量。==
2. ==可以在函数作用域中嵌套另一个函数作用域，相当于一个在另一个的里面。==
3. ==当函数作用域中操作变量，如果函数中有这个变量，则使用该变量，如果函数中没有这个变量，就会去上一级中找这个变量，如果找到window中也没有找到就会报错。==
4. ==如果需要直接使用全局中的变量，就可以直接使用 `window.变量名`。==
5. ==在函数作用域中也有声明提前，变量和函数都可以声明提前。==
6. ==在函数作用域中如果不适用 `var` 关键字来声明变量，就会成为一个全局变量。相当于给 window 对象添加了一个属性。==

```javascript
function fun(){
    d = 10;
}
fun();
alert("d = "+d);
```

上面的 d 在函数作用域中没有使用 var 关键字，就成为了一个全局变量，在外面也可以访问。给window添加了一个 d 这个属性。

### 3.3、声明提前

<font color=red>变量的声明提前：</font>

​	使用 var 关键字声明变量，变量会在所有代码执行前声明。

```js
<script>
   console.log(window.a);
   var a = 10;
</script>
```

a 就是变量提前了，但是输出是 undefined ，因为只是声明了变量，但是未赋值，到执行对应的赋值时，才会赋值。

上面的代码相当于是：

```js
<script>
   var a;
   console.log(window.a);
   a = 10;
</script>
```

<font color=red>函数的声明提前：
</font>

1. 使用函数声明式创建的函数	`function 函数名(){}`

==这种方式创建的函数会在代码执行前就把函数创建完成，所以我们可以在函数创建前使用该函数。推荐使用该方式来声明函数。==

2. 使用函数表达式来声明函数	`var 函数名 = function(){};`

==这种方式虽然是变量的声明提前了，但是并不是一个函数，所以不能在声明前调用函数。==

### 3.4、this 关键字

==在浏览器解析器调用函数的时候，会向函数中传入一个隐含的对象，这个对象就是 this ，this 指向一个对象，称为这个函数执行的上下文对象。==

==根据调用的对象的不同，这个 this 指向的对象也就不同。==

```js
function fun(){
    console.log(this);
}
fun();
```

这个函数的调用对象是 window 对象，所以函数中的this就是指向window这个对象。

```js
function fun(){
    console.log(this.name);
}
var object = {
    name : "张三",
   test : fun
}
object.test();
```

这个函数的调用对象是我们自己创建的对象，所以函数中的this指向我们自己创建的对象。

## 4、两种创建对象方法

### 4.1、工厂方式创建对象

​	在创建对象时，需要给多个类型相同的对象赋值属性，这样就很麻烦，我们就采用工厂的方式来创建对象。

语法：

```js
function createObject(name,age,sex){
    var obj = new Object();
    obj.name = name;
    obj.age = age;
    obj.sex = sex;
    obj.sayHello = function (){
        alert(this.name);
    }
    return obj;
}
var object = createObject("张三",18,"男")
```

1. 首先创建一个函数来创建对象，通过参数来为对象赋值。
2. 创建一个变量来接收调用函数的返回值，这样这个变量就成为了一个对象，并且赋有属性值。

### 4.2、构造函数创建对象

​	使用工厂方式创建的对象的类型全部是object类型，不利于分辨，所以有了构造函数创建对象。

语法：

```js
function Person(name,age,sex){
    this.name = name;
    this.age = age;
    this.sex = sex;
    this.sayHello = function (){
        alert(this.name);
    }
}
var person = new Person("张三",18,"男");
```

1. 创建一个函数作为构造函数，这个函数的函数名通常是大写开头。
2. 使用 new 关键字来创建对象，不使用 new 关键字的函数是直接的函数调用。
3. 可以通过传入参数来为这个对象的属性赋值。

<font color=red>构造函数的执行流程：</font>

1. 先创建一个对象。
2. 将新建的对象设置为函数中的 this ，在构造函数中可以直接使用 this 来操作新创建的对象。
3. 逐行执行函数中的代码。
4. 将新建的对象返回。

==构造函数中只有第3步是我们能够看到的，其它的步骤浏览器帮我们完成了。==

==我们通过构造函数的方式创建的对象，这个对象是这一类的对象，也称是这个类的实例。例如：我们创建的是person这类对象，而不是object对象。==

<font color=red>`ininstanceof` </font>关键字：==用于检查一个实例是否属于一类对象，如果属于就返回true，如果不属于就返回false。==

例如：

```js
alert(person instanceof Person);
```

person实例属于Person这个类，就返回true。

==任何自己创建的对象，它和 Object 这个对象使用 `instanceof` 返回的都是true。==

## 5、原型对象

​	使用构造函数创建对象时，没创建一个对象，就会创建对象的属性和方法，每一个对象的属性和方法都不一样。如果某一类对象中有重复类型的属性和方法，就会造成内存的浪费，这种解决方式就是原型对象。

<font color=red>理解原型对象：
</font>

​	==原型对象也是一个对象，它相当于一个公共区域，所有属于这一类的实例都可以访问到这个对象，我们可以将对象公有的内容放在原型独享中。==

==我们每创建一个函数，浏览器解析器都会向函数中添加一个属性 `prototype` ，这个属性对应的就是原型对象。==

当函数以普通函数调用时，这个属性没有任何作用，但是函数以构造函数调用时，它所创建的对象都会有一个隐含的属性，这个属性指向该函数的原型对象，我们可以通过`_ _proto__ __`来访问。(有两个下划线，之间没有空格)

![image-20211015212724525](https://i.loli.net/2021/10/15/tN5nsr6ZVRoHPf9.png)

==我们可以给原型对象中添加属性和方法，这样我们所有这一类的对象都可以使用这些属性和方法了。我们一般是通过函数对象给原型对象添加公共的属性和方法。其它类也是可以添加的。==

我们上面的构造函数就可以修改为：

```js
function Person(name,age,sex){
    this.name = name;
    this.age = age;
    this.sex = sex;
}
// 这是使用函数对象向原型对象中添加公共方法
Person.prototype.sayHello = function (){
    alert(this.name);
}
var person = new Person("张三",18,"男");
var person2 = new Person("李四",20,"男");
person.sayHello();
```

这样所有这一类的对象都可以使用该方法，而且只创建了一个方法，大大提高了效率。

==当我们访问一个对象的属性和方法时，首先会去该对象中去寻找，如果找到就是用对应的属性和方法，如果没有找到就回去原型对象中找，如果找到就是用，没有找到就是undefined。==

<font color=red>使用in检查对象：
</font>用于检查这个对象中是否含有某个属性和方法，如果自己对象中没有这个属性或方法，就去原型对象中找。如果有就返回true，如果没有就返回false。

语法：

```js
function Person(name,age) {
   this.name = name;
   this.age = age;
      }
      // 向原型对象中添加一个公共的方法
Person.prototype.sayHello = function (){
    alert(this.age);
}
// 实例
var person = new Person("王五",10);
// 检查person对象中是否含有sayHello这个方法
alert("sayHello" in person);
```

`sayHello`这个方法在原型对象中有，所以返回true。如果我们只想查询本身这个对象中是否有这个属性或方法，我们可以使用 `hasOwnProperty`这个方法。

语法：

```js
alert(person.hasOwnProperty("sayHello"));
```

在本身对象中没有这个`sayHello`方法，就返回false。

==注意：`hasOwnProperty`这个方法不在本身的对象中，也不在原型对象中，而是在原型对象的原型对象中的。==

==原型对象也是一个对象，所以原型对象也有一个原型对象。==

![image-20211016131423587](https://i.loli.net/2021/10/16/HhTtBZmNszCPi9b.png)

==原型对象的原型对象也是一个对象，所以它也有一个原型对象，依次下去，但是直到Object这个对象的时候，Object对象的原型就没有原型对象了。==

```js
alert(person.__proto__.__proto__.hasOwnProperty("hasOwnProperty"));
```

这个就是找的对象的原型对象的原型对象中是否含有`hasOwnProperty`方法，这个返回true。一般一个对象就两层原型对象。

## 6、toString方法

​	在打印某个对象时，通常是调用对象的toString方法来打印。

==`toString`方法是在对象的原型对象的原型对象中，我们可以在对象的原型对象中添加一个自己的`toString`方法，这样就调用我们自己的`toString`方法，从而可以自定义对象的输出格式。==

```js
alert(person.__proto__.__proto__.toString());
```

输出是一个对象，说明有这个方法。

我们修改重新定义`toString`方法。

```js
function Person(name,age) {
    this.name = name;
    this.age = age;
}
// 修改toString方法
Person.prototype.toString = function (){
    return "[Person-->"+"name="+this.name+",age="+this.age+"]";
}
// 实例
var person = new Person("王五",10);
alert(person)
```

上面就输出为：`[Person-->name=王五,age=10]`这个我们自己定义的输出方式。

## 7、数组

​	数组也是一个对象，用于存储数据的对象，可以存储任意类型的数据，每一个元素与元素之间独立，类型可以不一样。

### 7.1、基本语法

<font color=red>创建数组：
</font>

语法：

```js
var array = new Array();
```

这就创建了一个数组对象，但是这个数组对象中没有任何数据。

==数组对象其实是一个特殊对象，他拥有普通对象的全部特性，所以不止负索引，小数索引，连字符串索引都是可以的，只是用这些索引向数组添加属性时，数组的length不会增加罢了，而只有当索引是正整数或其对应数字串时，length才会增加。==

<font color=red>数组中添加元素：
</font>通过下标给数组添加元素。

语法：

```js
array[0] = 10;
array[1] = 20;
```

给array数组元素10，20。

我们一般是把数据添加到数组的最后一个元素，所以有下面的操作。

~~~js
array[array.length] = 30;
~~~

==`array.lenth`是获取到数组的长度==，它比数组中最后一个下标大1，因为下标从0开始的。

==如果通过下标来给数组添加元素，下标的值如果不连续的话，会创建一个不连续的数组，即中间没有的数据全是undefined，不推荐创建不连续的数组。==

例如：

~~~js
array[100] = 100;
~~~

我给下标100添加一个100这个元素，那么2~99中没有添加元素，这些元素的值全是undefined。

<font color=red>访问数组中的元素：
</font>也是通过下标来访问的。

语法：

~~~js
alert(array[1]);
~~~

这就访问到下标为 1 的元素，如果访问的下标超出了数组的长度大小，返回的是undefined。

<font color=red>length：
</font>我们可以通过length获取数组的长度和修改数组的长度。

- 如果我们修改的length大于原长度，多出的部分就是undefined。
- 如果我们修改的length小于原长度，则多出的元素就会被删除。

==特殊的下标不会增加数组的长度，但是也可以通过特殊的下标来访问对应的元素。==

例如：

```js
var array = new Array();
array[0] = 10;
array[1] = 20;
array[2] = 30;
array[3] = 40;
console.log(array);
// 修改数组长度,原长度是4
array.length = 3;
// 现在数组的长度为3，第4个元素就被删除了
console.log(array);
```

输出为：

![image-20211016152532001](https://i.loli.net/2021/10/16/3Hweni5vjcVtPpa.png)

### 7.2、数组的字面量

​	使用数组字面量的方式创建一个数组，可以对数组进行初始化。

语法：

~~~js
var array = [1,2,3,4];
~~~

创建了一个数组，数组中初始化了元素：1，2，3，4

我们也可以通过构造函数方式创建数组时对数组进行初始化，通过参数的方式把初始化内容传入。

~~~js
var array = new Array(1,2,3,4);
~~~

我们也创建了一个数组，数据初始化元素为：1，2，3，4。

==如果使用构造函数创建数组时传入的参数只有一个整数，那么这个整数是指定数组的长度，而不是元素。==

~~~js
var array = new Array(10); // 这是初始化数组的长度为10，而不是元素为10
~~~

### 7.3、数组的常用方法

<font color=red>push方法：
</font>用于向数组的最后面添加一个或多个元素，它的返回值是新数组的长度。

语法：

```js
array.push(1,2,3,4);
```

括号中就是向数组中添加的元素，元素与元素之间使用逗号隔开。

<font color=red>pop方法：
</font>用于删除数组的最后面的一个元素，并返回被删除的元素。

语法：

~~~js
var result = array.pop();
~~~

这就删除了数组中最后一个元素4，返回的就也是4。

<font color=red>unshift方法：
</font>用于向数组最前面添加一个或多个数组，并返回数组新的长度。

语法：

~~~js
array.unshift(5,6,7);
~~~

括号中是添加的元素，元素与元素之间使用逗号隔开。

<font color=red>shift方法：
</font>用于删除数组中第一个元素，并返回被删除的元素

语法：

~~~js
var result = array.shift();
~~~

<font color=red>forEach方法：
</font>用于遍历数组对象。

语法1：

```js
array.forEach(element => console.log(element));
```

==element是自己取的名称，这个就是遍历出来的每一个元素。可以在后面添加一个函数来操作遍历到的数据，但是这种方式不建议使用==

语法2：

~~~js
array.forEach(function (value,index,arrayObj){
    console.log(value);
});
~~~

==在括号中写一个函数，这个函数被称为回调函数，这个函数我们自己没有调用，是浏览器帮我们调用的，数组中有多少个元素，这个函数就会执行多少次。==

函数调用会传入几个参数：

- value：这个就是遍历到的数组中的元素，函数每调用一次，value就对应到被遍历到的元素。
- index：这个就是每个元素对应的下标。
- `arrayObj`：就是调用`forEach`方法对应得数组对象，可以同个这个对象操作调用这个方法得数组对象。

```js
array.forEach(function (value,index,arrayObj){
    arrayObj[index] = "已经被修改";
});
```

上面表示将所有得数组中所有得元素全部修改为 "已经被修改"。

==这个方法对于`IE8`及以下不支持，其它大部分浏览器都支持。==

<font color=red>slice方法：
</font>用于提取数组中指定的元素，封装到一个新的数组中返回，不会修改原数组，相当于切片，切下来的作为一个新数组。

语法：

```js
var array = [10,20,30,40];
var newArray = array.splice(0,2);
console.log(newArray);
```

这个方法有两个参数：

- 第一个参数：下标开始的值，包含这个下标对应的元素。
- 第二个参数：下标结束的值，不包含这个下标对应的元素。

上面表示从下标0开始，到下标2结束，但是不包括下标2对应的元素，所以输出为：10，20

==如果两个参数值相等，则返回一个空的数组，下标的值可以为负数，为负数时的，下标从后面开始计算，开始值为-1，例如倒数第一个元素的负下标就是-1，倒数第二个元素的负下标就是-2。==

==第二个参数可以不写，不写表示截取从开始下标及后面的所有元素。==

```js
var newArray = array.splice(-1);
console.log(newArray);
```

这个截取到的就是对后面的一个元素40。

<font color=red>splice方法：
</font>用于删除数组中指定的元素，并添加新的元素到数组中，返回一个由被删除元素组成的新数组，这个方法会修改原数组。

语法：

```js
array = [10,20,30,40];
var newArray = array.splice(1,2,"我是新元素1","我是新元素2");
console.log(newArray);
console.log(array);
```

这个方法的前两个参数是指定删除的元素，后面其它的参数是添加到原数组的元素。

- 参数1：被删除元素的开始下标。
- 参数2：要删除元素的个数。
- 后面的参数：这些参数会添加到原数组中参数1下标以前的位置。

上面就表示删除从下标1开始，删除两个元素，将后面的其它参数添加到原来参数1下标以前的位置。

<font color=red>concat方法：
</font>用于拼接两个或多个数组，并返回一个新数组，不会改变原有的数组。

语法：

```js
array1 = [1,2,3,4];
array2 = [5,6,7,8];
array3 = [9,10,11,12];
var result = array1.concat(array2,array3);
console.log(result)
```

==不但可以拼接数组，还可以拼接元素，之间使用逗号隔开。==

```js
var result = array1.concat(array2,"你好",array3,"再见");
```

<font color=red>reverse：</font>用于将数组反转，改变原数组。

语法：

~~~js
array.reverse();
~~~

<font color=red>sort方法：
</font>用于给数组排序，改变原数组，默认是升序排序。

语法：

~~~js
array.sort(); // 如果字符串需要降序排序，就再使用数组反转就可以了
~~~

==由于sort采用的是`unicode`编码的方式进行排序，对于字符串是没有问题的，但是对于纯数字的数组进行排序就会出问题。所以我们采用下面的方式给纯数字进行排序。需要给方法一个回调函数。==

语法：

~~~js
array.sort(function(a,b){
    // a、b就是传入的需要排序的元素
    // a-b 是升序排列
    // b-a 是降序排列
   	return a-b; 
});
~~~

我们由这样一个需求，我很多对象Person，对象中有一个age属性，对象存放再数组中，我们需要这些对象按照age升序（降序）排序。

```js
// 构造函数
function Person(name,age) {
    this.name = name;
    this.age = age;
}
// 实例对象
var p1 = new Person("张三",19);
var p2 = new Person("李四",25);
var p3 = new Person("王五",10);
var p4 = new Person("赵六",68);
var p5 = new Person("田七",13);
// 创建数组存放对象
var persons = [p1,p2,p3,p4,p5];
// 进行排序
persons.sort(function (a,b){
    // 升序排序
    return a.age - b.age;
    // 降序排列
    // return b.age - a.age;
});
console.log(persons);
```

<font color=red>every方法：
</font>用于检查所有元素是否都通过测试，不改变原数组。

语法：

```js
var flag = array.every(function (value,index,arrayObj){
    console.log(value < 13);
    return value < 13;
});
```

这个回调函数和`forEach`的回调函数类似，这个函数会将返回值自动转换成boolean类型的值。

- 如果函数返回 false 值的数组元素，every() 返回 false（并且不检查剩余值）。
- 如果没有出现 false，every() 返回 true。

上面的表示检查数组中的所有值是否都小于13。

<font color=red>filter方法：</font>用于将检查通过的元素重新组成一个新的数组，不改变原数组。

语法：

~~~js
var result = array.filter(function(value,index,arrayObj){
   return  value < 10;
});
~~~

表示将小于10的元素重新组成一个新的数组。

<font color=red>findIndex方法：
</font>用于返回第一个通过测试元素的下标，不改变原数组。

语法：

~~~js
var result = array.filter(function(value,index,arrayObj){
   return  value < 10;
});
~~~

- 如果找到函数返回 true 值的数组元素，则 `findIndex()` 返回该数组元素的索引（并且不检查剩余值）
- 否则返回 -1

<font color=red>includes方法：</font>用于查询数组中是否含有某个指定的元素，如果存在返回true，不存在就返回false。

语法：

```js
var flag = result.includes(元素);
console.log(flag);
```

<font color=red>indexOf方法：
</font>用于搜索指定元素，如果存在元素就返回元素下标，不存在就返回false，可以指定查找开始的位置，不指定就是从0开始。

语法：

~~~js
var result = result.indexOf(元素,开始下标);
console.log(result);
~~~

==还有一个方法`lastIndexOf`和该方法的功能相同，但是它是从尾向头查询。==

<font color=red>isArray方法：
</font>用于检验一个对象是否是数组，如果是就返回true，不是就返回false。

语法

~~~js
Array.isArray(被检验的对象);
~~~

<font color=red>join方法：
</font>用于将一个数组的元素作为拼接为字符串返回，可以指定分隔符，默认分隔符是逗号。

语法：

~~~js
var result = array.join(分隔符);
~~~

==部分方法都是比较`ES6、ES7`的新方法，注意版本的兼容问题。==

## 8、call和apply

​	call()和apply()都是函数方法，需要通过函数对象来调用，函数调用时多指定的对象一般是window，但是使用这两个方法可以指定函数调用的对象，这个对象将成为函数执行的this。

语法：

~~~js
函数名称.call(指定的对象);
函数名称.apply(指定的对象);
~~~

<font color=red>不同点：
</font>

- call方法可以将实参在对象后面依次传递。

~~~js
函数.call(对象,参数1,参数2);
~~~

这样在函数中就可以使用这些参数。

- apply方法需要将实参封装到一个数组中传递。

~~~js
函数.apply(对象,参数数组);
~~~

<font color=red>arguments：</font>在函数调用时，会传入两个隐含的参数，一个是this，还有一个就是arguments。

​	==arguments是用于封装实参对象的，是一个类数组，和数组相似，但是不是数组。在函数调用时，所有的实参都是封装到它里面的。==

所以，我们可以通过创建形参来接收参数，也可以通过arguments来获取参数，只不过这种方式比较麻烦。

```js
function fun() {
    // 获取第一个实参
    console.log(arguments[0]);
}
fun(1,2,3);
```

==arguments还有一个属性callee，这个属性表示的就是当前这个函数对象。==

```js
function fun() {
    // 获取第一个实参
    console.log(arguments[0]);
    // 当前这个函数对象
    console.log(arguments.callee);
}
fun(1,2,3);
```

## 9、Date对象

### 9.1、Date对象的创建

​	使用构造函数创建Date对象

语法1：

~~~js
var d = new Date();
~~~

==该方法创建的时间对象时当前系统的时间。==

语法2：我们可以指定一个日期，然后创建一个日期对象。

~~~js
var d = new Date("2021/10/2 10:20:10");
~~~

需要传入一个时间字符串的参数，这个参数的格式是：年/月/日 时:分:秒   也可是：月/日/年 时:分:秒 。

### 9.2、日期对象的常用方法

| 方法                                 | 解释                                                         |
| ------------------------------------ | ------------------------------------------------------------ |
| `getDate()`                          | 返回对象是第几天，1~31，相当于日                             |
| `getDay()`                           | 返回对象是星期几，0~6，0表示星期日，1~6表示周一~周六         |
| `getFullYear()`                      | 返回对象的年份，是4位的年份。                                |
| `getMonth()`                         | 返回对象的月份，0~11，0表示一月份。                          |
| `getHours()`                         | 返回对象的时，0~23                                           |
| `getMinutes()`                       | 返回对象的分，0~59                                           |
| `getSeconds()`                       | 返回对象的秒，0~59                                           |
| `getTime()`                          | 返回自 1970 年 1 月 1 日午夜以来与指定日期的毫秒数。         |
| `now()`                              | 返回自 1970 年 1 月 1 日午夜以来的毫秒数。                   |
| `setDate(day)`                       | 设置 Date 对象中月的某一天。需要传入参数day来设置天数，参数需要合法。 |
| `setFullYear(year, month, day)`      | 设置日期对象的年份，可以只设置年份，也可以月和日，需要参数，参数需要合法 |
| `setHours(hour, min, sec, millisec)` | 设置日期对象的小时，可以只设置时，也可以分、秒、毫秒，需要参数，参数需要合法 |
| `setMinutes(min, sec, millisec)`     | 设置日期对象的分，可以只设置分，也可以秒、毫秒，需要参数，参数需要合法 |
| `setSesconds(sec, millisec)`         | 设置日期对象的秒，可以只设置秒，也可以毫秒，需要参数，参数需要合法 |
| `setMillisecondes(millisec)`         | 设置日期对象的毫秒，需要参数，参数需要合法                   |



## 10、Math对象

​	Math对象是一个工具对象，不需要构造函数来创建，可以直接使用。

语法：

~~~js
Math.方法名/属性;
~~~

<font color=red>Math对象的属性：
</font>

| 属性      | 描述                                    |
| :-------- | :-------------------------------------- |
| `E`       | 返回欧拉数（约 2.718）。                |
| `LN2`     | 返回 2 的自然对数（约 0.693）。         |
| `LN10`    | 返回 10 的自然对数（约 2.302）。        |
| `LOG2E`   | 返回 E 的以 2 为底的对数（约 1.442）。  |
| `LOG10E`  | 返回 E 的以 10 为底的对数（约 0.434）。 |
| `PI`      | 返回 PI（约 3.14）。                    |
| `SQRT1_2` | 返回 1/2 的平方根（约 0.707）。         |
| `SQRT2`   | 返回 2 的平方根（约 1.414）。           |

<font color=red>Math对象的方法：
</font>

| 方法                                                         | 描述                                                       |
| :----------------------------------------------------------- | :--------------------------------------------------------- |
| abs(x)                                                       | 返回 x 的绝对值。                                          |
| `acos(x)`                                                    | 返回 x 的反余弦值，以弧度为单位。                          |
| `acosh(x)`                                                   | 返回 x 的双曲反余弦值。                                    |
| [asin(*x*)](https://www.w3school.com.cn/jsref/jsref_asin.asp) | 返回 x 的反正弦值，以弧度为单位。                          |
| [asinh(*x*)](https://www.w3school.com.cn/jsref/jsref_asinh.asp) | 返回 x 的双曲反正弦值。                                    |
| [atan(*x*)](https://www.w3school.com.cn/jsref/jsref_atan.asp) | 返回 x 的反正切值，返回的值是 -PI/2 到 PI/2 之间的弧度值。 |
| [atan2(*y*, *x*)](https://www.w3school.com.cn/jsref/jsref_atan2.asp) | 返回其参数商的反正切值。                                   |
| [atanh(*x*)](https://www.w3school.com.cn/jsref/jsref_atanh.asp) | 返回 x 的双曲反正切值。                                    |
| [cbrt(*x*)](https://www.w3school.com.cn/jsref/jsref_cbrt.asp) | 返回 x 的三次方根。                                        |
| [ceil(*x*)](https://www.w3school.com.cn/jsref/jsref_ceil.asp) | 返回 x，向上舍入为最接近的整数。                           |
| [clz32(*x*)](https://www.w3school.com.cn/jsref/jsref_clz32.asp) | 返回 x 的 32 位二进制表示中前导零的数量。                  |
| [cos(*x*)](https://www.w3school.com.cn/jsref/jsref_cos.asp)  | 返回 x 的余弦值（x 以弧度为单位）。                        |
| [cosh(*x*)](https://www.w3school.com.cn/jsref/jsref_cosh.asp) | 返回 x 的双曲余弦值。                                      |
| [exp(*x*)](https://www.w3school.com.cn/jsref/jsref_exp.asp)  | 返回 Ex 的值。                                             |
| [expm1(*x*)](https://www.w3school.com.cn/jsref/jsref_expm1.asp) | 返回 Ex 减去 1 的值。                                      |
| [floor(*x*)](https://www.w3school.com.cn/jsref/jsref_floor.asp) | 返回 x，向下舍入为最接近的整数。                           |
| [fround(*x*)](https://www.w3school.com.cn/jsref/jsref_fround.asp) | 返回数的最接近的（32 位单精度）浮点表示。                  |
| [log(*x*)](https://www.w3school.com.cn/jsref/jsref_log.asp)  | 返回 x 的自然对数。                                        |
| [log10(*x*)](https://www.w3school.com.cn/jsref/jsref_log10.asp) | 返回 x 的以 10 为底的对数。                                |
| [log1p(*x*)](https://www.w3school.com.cn/jsref/jsref_log1p.asp) | 返回 1 + x 的自然对数。                                    |
| [log2(*x*)](https://www.w3school.com.cn/jsref/jsref_log2.asp) | 返回 x 的以 2 为底的对数。                                 |
| [max(*x*, *y*, *z*, ..., *n*)](https://www.w3school.com.cn/jsref/jsref_max.asp) | 返回值最高的数字。                                         |
| [min(*x*, *y*, *z*, ..., *n*)](https://www.w3school.com.cn/jsref/jsref_min.asp) | 返回值最小的数字。                                         |
| [pow(*x*, *y*)](https://www.w3school.com.cn/jsref/jsref_pow.asp) | 返回 x 的 y 次幂值。                                       |
| [random()](https://www.w3school.com.cn/jsref/jsref_random.asp) | 返回 0 到 1 之间的随机数。                                 |
| [round(*x*)](https://www.w3school.com.cn/jsref/jsref_round.asp) | 将 x 舍入为最接近的整数。                                  |
| [sign(*x*)](https://www.w3school.com.cn/jsref/jsref_sign.asp) | 返回数的符号（检查它是正数、负数还是零）。                 |
| [sin(*x*)](https://www.w3school.com.cn/jsref/jsref_sin.asp)  | 返回 x 的正弦值（x 以弧度为单位）。                        |
| [sinh(*x*)](https://www.w3school.com.cn/jsref/jsref_sinh.asp) | 返回 x 的双曲正弦值。                                      |
| [sqrt(*x*)](https://www.w3school.com.cn/jsref/jsref_sqrt.asp) | 返回 x 的平方根。                                          |
| [tan(*x*)](https://www.w3school.com.cn/jsref/jsref_tan.asp)  | 返回角度的正切值。                                         |
| [tanh(*x*)](https://www.w3school.com.cn/jsref/jsref_tanh.asp) | 返回数的双曲正切值。                                       |
| [trunc(*x*)](https://www.w3school.com.cn/jsref/jsref_trunc.asp) | 返回数字 (x) 的整数部分。                                  |



## 11、字符串

| 方法                                                         | 描述                                                 |
| :----------------------------------------------------------- | :--------------------------------------------------- |
| [anchor()](https://www.w3school.com.cn/jsref/jsref_anchor.asp) | 创建 HTML 锚。                                       |
| [big()](https://www.w3school.com.cn/jsref/jsref_big.asp)     | 用大号字体显示字符串。                               |
| [blink()](https://www.w3school.com.cn/jsref/jsref_blink.asp) | 显示闪动字符串。                                     |
| [bold()](https://www.w3school.com.cn/jsref/jsref_bold.asp)   | 使用粗体显示字符串。                                 |
| [charAt()](https://www.w3school.com.cn/jsref/jsref_charAt.asp) | 返回在指定位置的字符。                               |
| [charCodeAt()](https://www.w3school.com.cn/jsref/jsref_charCodeAt.asp) | 返回在指定的位置的字符的 Unicode 编码。              |
| [concat()](https://www.w3school.com.cn/jsref/jsref_concat_string.asp) | 连接字符串。                                         |
| [fixed()](https://www.w3school.com.cn/jsref/jsref_fixed.asp) | 以打字机文本显示字符串。                             |
| [fontcolor()](https://www.w3school.com.cn/jsref/jsref_fontcolor.asp) | 使用指定的颜色来显示字符串。                         |
| [fontsize()](https://www.w3school.com.cn/jsref/jsref_fontsize.asp) | 使用指定的尺寸来显示字符串。                         |
| [fromCharCode()](https://www.w3school.com.cn/jsref/jsref_fromCharCode.asp) | 从字符编码创建一个字符串。                           |
| [indexOf()](https://www.w3school.com.cn/jsref/jsref_indexOf.asp) | 检索字符串。                                         |
| [italics()](https://www.w3school.com.cn/jsref/jsref_italics.asp) | 使用斜体显示字符串。                                 |
| [lastIndexOf()](https://www.w3school.com.cn/jsref/jsref_lastIndexOf.asp) | 从后向前搜索字符串。                                 |
| [link()](https://www.w3school.com.cn/jsref/jsref_link.asp)   | 将字符串显示为链接。                                 |
| [localeCompare()](https://www.w3school.com.cn/jsref/jsref_localeCompare.asp) | 用本地特定的顺序来比较两个字符串。                   |
| [match()](https://www.w3school.com.cn/jsref/jsref_match.asp) | 找到一个或多个正则表达式的匹配。                     |
| [replace()](https://www.w3school.com.cn/jsref/jsref_replace.asp) | 替换与正则表达式匹配的子串。                         |
| [search()](https://www.w3school.com.cn/jsref/jsref_search.asp) | 检索与正则表达式相匹配的值。                         |
| [slice()](https://www.w3school.com.cn/jsref/jsref_slice_string.asp) | 提取字符串的片断，并在新的字符串中返回被提取的部分。 |
| [small()](https://www.w3school.com.cn/jsref/jsref_small.asp) | 使用小字号来显示字符串。                             |
| [split()](https://www.w3school.com.cn/jsref/jsref_split.asp) | 把字符串分割为字符串数组。                           |
| [strike()](https://www.w3school.com.cn/jsref/jsref_strike.asp) | 使用删除线来显示字符串。                             |
| [sub()](https://www.w3school.com.cn/jsref/jsref_sub.asp)     | 把字符串显示为下标。                                 |
| [substr()](https://www.w3school.com.cn/jsref/jsref_substr.asp) | 从起始索引号提取字符串中指定数目的字符。             |
| [substring()](https://www.w3school.com.cn/jsref/jsref_substring.asp) | 提取字符串中两个指定的索引号之间的字符。             |
| [sup()](https://www.w3school.com.cn/jsref/jsref_sup.asp)     | 把字符串显示为上标。                                 |
| [toLocaleLowerCase()](https://www.w3school.com.cn/jsref/jsref_toLocaleLowerCase.asp) | 把字符串转换为小写。                                 |
| [toLocaleUpperCase()](https://www.w3school.com.cn/jsref/jsref_toLocaleUpperCase.asp) | 把字符串转换为大写。                                 |
| [toLowerCase()](https://www.w3school.com.cn/jsref/jsref_toLowerCase.asp) | 把字符串转换为小写。                                 |
| [toUpperCase()](https://www.w3school.com.cn/jsref/jsref_toUpperCase.asp) | 把字符串转换为大写。                                 |
| toSource()                                                   | 代表对象的源代码。                                   |
| [toString()](https://www.w3school.com.cn/jsref/jsref_toString_string.asp) | 返回字符串。                                         |
| [valueOf()](https://www.w3school.com.cn/jsref/jsref_valueOf_string.asp) | 返回某个字符串对象的原始值。                         |



## 12、正则表达式

​	用于定义一些字符串的规则。

### 12.1、创建正则表达式对象

语法1：

~~~js
var reg = new RegExp("正则表达式","匹配模式");
~~~

==该构造函数有两个参数，第一个参数是定义的正则表达式的规则，第二个是匹配模式。这两个都是字符串==

==匹配模式有两个可选择值：`i` 、`g`、`m`==

- i：表示匹配不区分大小写。
- g：表示全局匹配。
- m：表示多行匹配。

==正则表达式可以设置多个匹配模式，没有顺序要求。==

~~~js
var reg = new RegExp("a","ig");
~~~

语法2：使用字面量创建正则表达式，但是不推荐使用这种方式创建，不灵活。

~~~js
var reg = /正则表达式/匹配模式;
~~~

### 12.2、正则表达式的方法

<font color=red>test方法：
</font>用于测试一个字符串是否满足正则表达式，如果符合就返回true，不符合就返回false。

语法：

~~~js
// 创建一个正则表达式对象，不区分大小写
var reg = new RegExp("a","i");
// 需要测试的字符串
var str = "dadjkbdskfgb";
// 执行测试
var result = reg.test(str); // 返回为true，含有a
~~~

### 12.3、字符串与正则表达式

<font color=red>split方法：
</font>用于将字符串拆分成数组。

方法中可以传入一个字符串作为分隔符，也可以传入一个正则表达式作为分割符，满足正则表达式的元素就作为分隔符。

语法：

~~~js
var str = "1a2c3f4k7d";
// 将字符串拆分成数组
var array = str.split(/[A-z]/);
console.log(array);
~~~

<font color=red>search方法：
</font>用于检索指定的字符串，如果有就返回第一次出现的索引，没有就返回-1。

方法可以传入一个字符串作为参数，检索指定的字符串，也可以传入一个正则表达式作为参数，检索与正则表达式匹配的字符串。

语法：

~~~js
var str = "1a2c3f4k7d";
var result = str.search(/3f/);
console.log(result);
~~~

检索`3f`这个字符串。

<font color=red>match方法:
</font>将一个字符串中符合条件的内容提取出来，将提取出来的内容作为一个数组存储。

方法可以传入一个字符串作为参数，检索指定的字符串，也可以传入一个正则表达式作为参数，检索与正则表达式匹配的字符串。

语法：

~~~js
var str = "1a2c3f4k7d";
var result = str.match(/[0-9]/);
console.log(result);
~~~

表示将第一个匹配到的数字取出来，这个方法只会匹配到第一个符合条件的元素，但是我们也可以设置让它匹配所有元素，只需改为全局匹配就可以。

~~~js
var str = "1a2c3f4k7d";
var result = str.match(/[0-9]/g);
console.log(result);
~~~

这就匹配字符串中的所有数字。

<font color=red>replace方法：
</font>可以将字符串中指定的内容替换成新的内容，返回值是一个新的字符串。

方法可以传入一个字符串作为参数，检索指定的字符串，也可以传入一个正则表达式作为参数，替换与正则表达式匹配的字符串。

语法：

~~~js
var str = "1a2c3f4k7d";
var s = str.replace(/[a-z]/,"新内容");
console.log(s);
~~~

这个方法只会匹配到第一个符合条件的元素，但是我们也可以设置让它匹配所有元素，只需改为全局匹配就可以。

~~~js
var str = "1a2c3f4k7d";
var s = str.replace(/[a-z]/g,"新内容");
console.log(s);
~~~

### 12.4、正则表达式的语法

<font color=red>量词：
</font>

| 量词                                                         | 描述                                        |
| :----------------------------------------------------------- | :------------------------------------------ |
| [n+](https://www.w3school.com.cn/jsref/jsref_regexp_onemore.asp) | 匹配任何包含至少一个 n 的字符串。           |
| [n*](https://www.w3school.com.cn/jsref/jsref_regexp_zeromore.asp) | 匹配任何包含零个或多个 n 的字符串。         |
| [n?](https://www.w3school.com.cn/jsref/jsref_regexp_zeroone.asp) | 匹配任何包含零个或一个 n 的字符串。         |
| [n{X}](https://www.w3school.com.cn/jsref/jsref_regexp_nx.asp) | 匹配包含 X 个 n 的序列的字符串。            |
| [n{X,Y}](https://www.w3school.com.cn/jsref/jsref_regexp_nxy.asp) | 匹配包含 X 至 Y 个 n 的序列的字符串。       |
| [n{X,}](https://www.w3school.com.cn/jsref/jsref_regexp_nxcomma.asp) | 匹配包含至少 X 个 n 的序列的字符串。        |
| [n$](https://www.w3school.com.cn/jsref/jsref_regexp_ndollar.asp) | 匹配任何结尾为 n 的字符串。                 |
| [^n](https://www.w3school.com.cn/jsref/jsref_regexp_ncaret.asp) | 匹配任何开头为 n 的字符串。                 |
| [?=n](https://www.w3school.com.cn/jsref/jsref_regexp_nfollow.asp) | 匹配任何其后紧接指定字符串 n 的字符串。     |
| [?!n](https://www.w3school.com.cn/jsref/jsref_regexp_nfollow_not.asp) | 匹配任何其后没有紧接指定字符串 n 的字符串。 |

==量词只能对它前面后后面的一个内容起作用。==

```js
var reg = /^a$/; // 这个表示这个字符串只能是a，并不是以a开头，a结尾的任意字符串
var result;
result = reg.test("afdga");
console.log(result);
```

~~~js
var reg = /^a|a$/ // 这才是以a开头，a结尾的任意字符串
~~~

手机号的规则：

- 第一位是1。
- 第二位是3~9的任意数。
- 后面的其他位是0~9的任意数。
- 一共11位。

所以手机号的正则表达式就因该为：`^1[3-9][0-9]{9}$`

<font color=red>元字符：
</font>

| 元字符                                                       | 描述                                        |
| :----------------------------------------------------------- | :------------------------------------------ |
| [.](https://www.w3school.com.cn/jsref/jsref_regexp_dot.asp)  | 查找单个字符，除了换行和行结束符。          |
| [\w](https://www.w3school.com.cn/jsref/jsref_regexp_wordchar.asp) | 查找单词字符。                              |
| [\W](https://www.w3school.com.cn/jsref/jsref_regexp_wordchar_non.asp) | 查找非单词字符。                            |
| [\d](https://www.w3school.com.cn/jsref/jsref_regexp_digit.asp) | 查找数字。                                  |
| [\D](https://www.w3school.com.cn/jsref/jsref_regexp_digit_non.asp) | 查找非数字字符。                            |
| [\s](https://www.w3school.com.cn/jsref/jsref_regexp_whitespace.asp) | 查找空白字符。                              |
| [\S](https://www.w3school.com.cn/jsref/jsref_regexp_whitespace_non.asp) | 查找非空白字符。                            |
| [\b](https://www.w3school.com.cn/jsref/jsref_regexp_begin.asp) | 匹配单词边界。                              |
| [\B](https://www.w3school.com.cn/jsref/jsref_regexp_begin_not.asp) | 匹配非单词边界。                            |
| \0                                                           | 查找 NUL 字符。                             |
| [\n](https://www.w3school.com.cn/jsref/jsref_regexp_newline.asp) | 查找换行符。                                |
| \f                                                           | 查找换页符。                                |
| \r                                                           | 查找回车符。                                |
| \t                                                           | 查找制表符。                                |
| \v                                                           | 查找垂直制表符。                            |
| [\xxx](https://www.w3school.com.cn/jsref/jsref_regexp_octal.asp) | 查找以八进制数 xxx 规定的字符。             |
| [\xdd](https://www.w3school.com.cn/jsref/jsref_regexp_hex.asp) | 查找以十六进制数 dd 规定的字符。            |
| [\uxxxx](https://www.w3school.com.cn/jsref/jsref_regexp_unicode_hex.asp) | 查找以十六进制数 xxxx 规定的 Unicode 字符。 |

注意：

- 如果需要匹配 `.` 这个字符需要使用转义字符。

~~~js
var reg = /\./; // 匹配字符串中是否含有点
var reg = new RegExp("\\.");
~~~

==在字符串中，如果需要使用到特殊的字符需要使用转义字符，不然会当成特殊的字符使用。==

常见正则表达式的网址：[https://c.runoob.com/front-end/854/](https://c.runoob.com/front-end/854/)



## 13、DOM对象

### 13.1、简介

​	DOM，（Document Object Model）文档对象模型，在`js`中通过DOM来对HTML文档进行操作。

- 文档：表示整个的HTML页面。
- 对象：将页面的每一个部分都转换成了对象。
- 模型：使用模型来表示对象与对象之间的关系。

![image-20211018195341437](https://i.loli.net/2021/10/18/fSnXeiHvc5UENCp.png)

<font color=red>节点：
</font>是构成网页的最基础组成部分，在网页中的每个部分都可以成为节点，例如标签、属性、文本、注释等等。虽然它们都是节点，但是它们的具体类型是不一样的。

分类：

- 文档节点：整个HTML文档。
- 元素节点：HTML中的标签。
- 属性节点：元素的属性。
- 文本节点：HTML中的文本内容。

节点的属性：

![image-20211018195745527](https://i.loli.net/2021/10/18/f8ugPXExiwCTWAK.png)

### 13.2、document节点

<font color=red>属性：
</font>

| 属性                                                         | 描述                                                         |
| :----------------------------------------------------------- | :----------------------------------------------------------- |
| body                                                         | 提供对 <body> 元素的直接访问。对于定义了框架集的文档，该属性引用最外层的 <frameset>。 |
| [cookie](https://www.w3school.com.cn/jsref/prop_doc_cookie.asp) | 设置或返回与当前文档有关的所有 cookie。                      |
| [domain](https://www.w3school.com.cn/jsref/prop_doc_domain.asp) | 返回当前文档的域名。                                         |
| [lastModified](https://www.w3school.com.cn/jsref/prop_doc_lastmodified.asp) | 返回文档被最后修改的日期和时间。                             |
| [referrer](https://www.w3school.com.cn/jsref/prop_doc_referrer.asp) | 返回载入当前文档的文档的 URL。                               |
| [title](https://www.w3school.com.cn/jsref/prop_doc_title.asp) | 返回当前文档的标题。                                         |
| [URL](https://www.w3school.com.cn/jsref/prop_doc_url.asp)    | 返回当前文档的 URL。                                         |
| `documentElement`                                            | 返回HTML这个根标签。                                         |
| `all`                                                        | 返回页面中的所有元素。                                       |

<font color=red>方法：
</font>

| 方法                                                         | 描述                                                         |
| :----------------------------------------------------------- | :----------------------------------------------------------- |
| [close()](https://www.w3school.com.cn/jsref/met_doc_close.asp) | 关闭用 document.open() 方法打开的输出流，并显示选定的数据。  |
| [getElementById()](https://www.w3school.com.cn/jsref/met_doc_getelementbyid.asp) | 返回对拥有指定 id 的第一个对象的引用。                       |
| [getElementsByName()](https://www.w3school.com.cn/jsref/met_doc_getelementsbyname.asp) | 返回带有指定的name属性的对象集合，返回的是一个类数组对象。   |
| [getElementsByTagName()](https://www.w3school.com.cn/jsref/met_doc_getelementsbytagname.asp) | 返回带有指定标签名的对象集合，返回的是一个类数组对象。       |
| [open()](https://www.w3school.com.cn/jsref/met_doc_open.asp) | 打开一个流，以收集来自任何 document.write() 或 document.writeln() 方法的输出。 |
| [write()](https://www.w3school.com.cn/jsref/met_doc_write.asp) | 向文档写 HTML 表达式 或 JavaScript 代码。                    |
| [writeln()](https://www.w3school.com.cn/jsref/met_doc_writeln.asp) | 等同于 write() 方法，不同的是在每个表达式之后写一个换行符。  |
| `getElementsByClassName()`                                   | 根据元素的class属性查询一组对象，返回值是类数组。            |
| `querySelector()`                                            | 需要传入一个css选择器的字符串，根据选择器来返回一个元素节点对象，但是这个只能返回一个，是第一个。 |
| `querySelectorAll()`                                         | 用法和`querySelector()`类似，但是它是返回所有满足条件的元素节点，封装到一个类数组中。 |

### 13.3、元素节点

<font color=red>方法和属性：
</font>

| 方法名                   | 解释                                                       |
| ------------------------ | ---------------------------------------------------------- |
| `getElementsByTagName()` | 返回当前节点指定标签名的所有后代节点，返回的是一个类数组。 |
| `childNodes`             | 返回当前节点的所有子节点，返回一个类数组。                 |
| `children`               | 返回当前节点下的所有子元素，返回的是一个类数组。           |
| `firstChild`             | 返回当前节点的第一个子节点。                               |
| `firstElementChild`      | 返回当前节点的第一个子元素。                               |
| `lastChild`              | 返回当前节点的最后一个子节点。                             |
| `lastElementChild`       | 返回当前节点的最后一个字元素。                             |
| `parentNode`             | 返回当前元素的父节点，由于是父节点，所以只能返回一个元素。 |
| `previousSibling`        | 返回当前节点的前一个兄弟节点。                             |
| `previousElementSibling` | 返回当前节点的前一个兄弟元素。                             |
| `nextSibling`            | 返回当前节点的前一个节点。                                 |
| `nextElementSibing`      | 返回当前节点的前一个元素。                                 |
| `innerHTML`              | 返回当前节点的所有内容，不忽略HTML标签。                   |
| `innerText`              | 返回当前节点的所有内容，忽略HTML标签。                     |

<font color=red>节点的增删改：
</font>

| 方法                             | 解释                                   |
| -------------------------------- | -------------------------------------- |
| `appendChild(子节点)`            | 父节点中增加一个子节点。               |
| `removeChild(子节点)`            | 父节点中删除一个存在的子节点。         |
| `replaceChild(新节点,旧节点)`    | 父节点中把指定的旧节点换成新的节点。   |
| `insertBefor(新节点,指定节点)`   | 父节点中把新节点插入到指定节点的前面。 |
| `createElement(标签字符串)`      | 创建一个元素节点。                     |
| `createTextNode(文本内容字符串)` | 创建一个文本节点。                     |

<font color=red>注意：
</font>

- 根据DOM标签中空白也算一个节点，元素与元素之间的换行也是节点，获取为文本节点。
- 注意节点与元素的区别。

### 13.4、`js`操作样式

<font color=red>操作内联样式：
</font>

语法：

~~~js
元素.style.样式的名称 = 样式值;
~~~

==通过元素的style这个属性可以设置和获取元素的内联样式。==

例如：

~~~js
boxObj.style.width = "100px";
~~~

表示将`boxObj`这个元素的宽度设置为`100px`。

- 样式值必须以样式的字符串的形式赋值给元素。
- 如果样式名称中带有 `-` ，需要将样式的名称转换成驼峰的命名规范。
- 只能获取或修改内联样式，不能获取和修改样式表中的样式。

<font color=red>获取元素的当前样式：
</font>

语法1：

~~~js
var result = 元素.currentStyle.样式名;
~~~

==但是这种方式只能在IE中使用，其它浏览器不支持。==

语法2：

~~~js
var obj = getComputedStyle(元素,null);
var result = obj.样式名;
~~~

- 这个方法是window的方法，可以直接使用。
- 这个方法需要传两个参数，第一个是获取样式的元素，第二个参数是一个伪元素，但是一般都是传入null。
- 该方法会返回一个对象，对象中封装了元素当前对应的样式。
- 这两种方式都是获取的是当前元素显示的样式，无论是内联样式还是样式表中的样式。
- 这样两种方式只能读取当前元素的样式，不能修改当前元素的样式，只能通过`元素.style`这种方式取修改样式。

### 13.3、事件

​	就是文档或浏览器窗口发生的一些特定的交互瞬间，例如点击、数鼠标移动、按键等。

<font color=red>事件类型：</font>

| 事件         | 解释                                                   |
| ------------ | ------------------------------------------------------ |
| `mousedown`  | 当用户在元素上按下鼠标按钮时，发生此事件。             |
| `mouseenter` | 当指针移动到元素上时，发生此事件。                     |
| `mouseleave` | 当指针从元素上移出时，发生此事件。                     |
| `mousemove`  | 当指针在元素上方移动时，发生此事件。                   |
| `mouseout`   | 当用户将鼠标指针移出元素或其中的子元素时，发生此事件。 |
| `mouseover`  | 当指针移动到元素或其中的子元素上时，发生此事件。       |
| `mouseup`    | 当用户在元素上释放鼠标按钮时，发生此事件。             |

语法：

~~~js
元素.事件名称 = function(event){
  alert("你好");  
};
~~~

==当触发响应的事件时，就会执行对应的回调函数。==

事件对象：

==当事件函数触发时，浏览器每次会将一个事件对象作为实参传给回调函数，在事件对象中封装了当前事件的一切相关信息，例如鼠标的坐标、哪个键被按下等。上面的函数中使用event来接收这个事件对象。==

<font color=red>事件对象的属性：
</font>

| 属性/方法                                                    | 描述                                                         | 属于                                                         |
| :----------------------------------------------------------- | :----------------------------------------------------------- | :----------------------------------------------------------- |
| [altKey](https://www.w3school.com.cn/jsref/event_altkey.asp) | 返回触发鼠标事件时是否按下了 "ALT" 键。                      | [MouseEvent](https://www.w3school.com.cn/jsref/obj_mouseevent.asp) |
| [altKey](https://www.w3school.com.cn/jsref/event_key_altkey.asp) | 返回触发按键事件时是否按下了 "ALT" 键。                      | [KeyboardEvent](https://www.w3school.com.cn/jsref/obj_keyboardevent.asp)[TouchEvent](https://www.w3school.com.cn/jsref/obj_touchevent.asp) |
| [animationName](https://www.w3school.com.cn/jsref/event_animation_animationName.asp) | 返回动画的名称。                                             | [AnimationEvent](https://www.w3school.com.cn/jsref/obj_animationevent.asp) |
| [bubbles](https://www.w3school.com.cn/jsref/event_bubbles.asp) | 返回特定事件是否为冒泡事件。                                 | [Event](https://www.w3school.com.cn/jsref/obj_event.asp)     |
| [button](https://www.w3school.com.cn/jsref/event_button.asp) | 返回触发鼠标事件时按下的鼠标按钮。                           | [MouseEvent](https://www.w3school.com.cn/jsref/obj_mouseevent.asp) |
| [buttons](https://www.w3school.com.cn/jsref/event_buttons.asp) | 返回触发鼠标事件时按下的鼠标按钮。                           | [MouseEvent](https://www.w3school.com.cn/jsref/obj_mouseevent.asp) |
| [cancelBubble](https://www.w3school.com.cn/jsref/event_cancelbubble.asp) | 设置或返回事件是否应该向上层级进行传播。                     |                                                              |
| [cancelable](https://www.w3school.com.cn/jsref/event_cancelable.asp) | 返回事件是否可以阻止其默认操作。                             | [Event](https://www.w3school.com.cn/jsref/obj_event.asp)     |
| changeTouches                                                | 返回在上一触摸与该触摸之间其状态已更改的所有触摸对象的列表   | [TouchEvent](https://www.w3school.com.cn/jsref/obj_touchevent.asp) |
| [charCode](https://www.w3school.com.cn/jsref/event_key_charcode.asp) | 返回触发 onkeypress 事件的键的 Unicode 字符代码。            | [KeyboardEvent](https://www.w3school.com.cn/jsref/obj_keyboardevent.asp) |
| [clientX](https://www.w3school.com.cn/jsref/event_clientx.asp) | 返回触发鼠标事件时，鼠标指针相对于当前窗口的水平坐标。       | [MouseEvent](https://www.w3school.com.cn/jsref/obj_mouseevent.asp)[TouchEvent](https://www.w3school.com.cn/jsref/obj_touchevent.asp) |
| [clientY](https://www.w3school.com.cn/jsref/event_clienty.asp) | 返回触发鼠标事件时，鼠标指针相对于当前窗口的垂直坐标。       | [MouseEvent](https://www.w3school.com.cn/jsref/obj_mouseevent.asp)[TouchEvent](https://www.w3school.com.cn/jsref/obj_touchevent.asp) |
| clipboardData                                                | 返回对象，其中包含受剪贴板操作影响的数据。                   | [ClipboardData](https://www.w3school.com.cn/jsref/obj_clipboardevent.asp) |
| [code](https://www.w3school.com.cn/jsref/event_key_code.asp) | 返回触发事件的键的代码。                                     | [KeyboardEvent](https://www.w3school.com.cn/jsref/obj_keyboardevent.asp) |
| composed                                                     | 指示该事件是否可以从 Shadow DOM 传递到一般的 DOM。           | [Event](https://www.w3school.com.cn/jsref/obj_event.asp)     |
| [composedPath()](https://www.w3school.com.cn/jsref/event_composedpath.asp) | 返回事件的路径。                                             |                                                              |
| [createEvent()](https://www.w3school.com.cn/jsref/event_createevent.asp) | 创建新事件。                                                 | [Event](https://www.w3school.com.cn/jsref/obj_event.asp)     |
| [ctrlKey](https://www.w3school.com.cn/jsref/event_ctrlkey.asp) | 返回触发鼠标事件时是否按下了 "CTRL" 键。                     | [MouseEvent](https://www.w3school.com.cn/jsref/obj_mouseevent.asp) |
| [ctrlKey](https://www.w3school.com.cn/jsref/event_key_ctrlkey.asp) | 返回按键鼠标事件时是否按下了 "CTRL" 键。                     | [KeyboardEvent](https://www.w3school.com.cn/jsref/obj_keyboardevent.asp)[TouchEvent](https://www.w3school.com.cn/jsref/obj_touchevent.asp) |
| [currentTarget](https://www.w3school.com.cn/jsref/event_currenttarget.asp) | 返回其事件侦听器触发事件的元素。                             | [Event](https://www.w3school.com.cn/jsref/obj_event.asp)     |
| [data](https://www.w3school.com.cn/jsref/event_inputevent_data.asp) | 返回插入的字符。                                             | [InputEvent](https://www.w3school.com.cn/jsref/obj_inputevent.asp) |
| dataTransfer                                                 | 返回一个对象，其中包含被拖放或插入/删除的数据。              | [DragEvent](https://www.w3school.com.cn/jsref/obj_dragevent.asp)[InputEvent](https://www.w3school.com.cn/jsref/obj_inputevent.asp) |
| [defaultPrevented](https://www.w3school.com.cn/jsref/event_defaultprevented.asp) | 返回是否为事件调用 preventDefault() 方法。                   | [Event](https://www.w3school.com.cn/jsref/obj_event.asp)     |
| [deltaX](https://www.w3school.com.cn/jsref/event_wheel_deltax.asp) | 返回鼠标滚轮的水平滚动量（x 轴）。                           | [WheelEvent](https://www.w3school.com.cn/jsref/obj_wheelevent.asp) |
| [deltaY](https://www.w3school.com.cn/jsref/event_wheel_deltay.asp) | 返回鼠标滚轮的垂直滚动量（y 轴）。                           | [WheelEvent](https://www.w3school.com.cn/jsref/obj_wheelevent.asp) |
| [deltaZ](https://www.w3school.com.cn/jsref/event_wheel_deltaz.asp) | 返回鼠标滚轮的 Z 轴滚动量。                                  | [WheelEvent](https://www.w3school.com.cn/jsref/obj_wheelevent.asp) |
| [deltaMode](https://www.w3school.com.cn/jsref/event_wheel_deltamode.asp) | 返回数字，代表增量值（像素、线或页面）的度量单位。           | [WheelEvent](https://www.w3school.com.cn/jsref/obj_wheelevent.asp) |
| [detail](https://www.w3school.com.cn/jsref/event_detail.asp) | 返回数字，指示鼠标被单击了多少次。                           | [UiEvent](https://www.w3school.com.cn/jsref/obj_uievent.asp) |
| [elapsedTime](https://www.w3school.com.cn/jsref/event_animation_elapsedtime.asp) | 返回动画已运行的秒数。                                       | [AnimationEvent](https://www.w3school.com.cn/jsref/obj_animationevent.asp) |
| [elapsedTime](https://www.w3school.com.cn/jsref/event_transition_elapsedtime.asp) | 返回过渡已运行的秒数。                                       |                                                              |
| [eventPhase](https://www.w3school.com.cn/jsref/event_eventphase.asp) | 返回当前正在评估事件流处于哪个阶段。                         | [Event](https://www.w3school.com.cn/jsref/obj_event.asp)     |
| [getModifierState()](https://www.w3school.com.cn/jsref/event_mouse_getmodifierstate.asp) | 返回包含目标范围的数组，此范围将受到插入/删除的影响。        | [MouseEvent](https://www.w3school.com.cn/jsref/obj_mouseevent.asp) |
| getTargetRanges()                                            | 返回包含目标范围的数组，此范围将受到插入/删除的影响。        | [InputEvent](https://www.w3school.com.cn/jsref/obj_inputevent.asp) |
| [inputType](https://www.w3school.com.cn/jsref/event_inputevent_inputtype.asp) | 返回更改的类型（即 "inserting" 或 "deleting"）。             | [InputEvent](https://www.w3school.com.cn/jsref/obj_inputevent.asp) |
| isComposing                                                  | 返回事件的状态是否正在构成。                                 | [InputEvent](https://www.w3school.com.cn/jsref/obj_inputevent.asp)[KeyboardEvent](https://www.w3school.com.cn/jsref/obj_keyboardevent.asp) |
| [isTrusted](https://www.w3school.com.cn/jsref/event_istrusted.asp) | 返回事件是否受信任。                                         | [Event](https://www.w3school.com.cn/jsref/obj_event.asp)     |
| [key](https://www.w3school.com.cn/jsref/event_key_key.asp)   | 返回事件表示的键的键值。                                     | [KeyboardEvent](https://www.w3school.com.cn/jsref/obj_keyboardevent.asp) |
| key                                                          | 返回更改后的存储项的键。                                     | [StorageEvent](https://www.w3school.com.cn/jsref/obj_storageevent.asp) |
| [keyCode](https://www.w3school.com.cn/jsref/event_key_keycode.asp) | 返回触发 onkeypress、onkeydown 或 onkeyup 事件的键的 Unicode 字符代码。 | [KeyboardEvent](https://www.w3school.com.cn/jsref/obj_keyboardevent.asp) |
| [location](https://www.w3school.com.cn/jsref/event_key_location.asp) | 返回键盘或设备上按键的位置。                                 | [KeyboardEvent](https://www.w3school.com.cn/jsref/obj_keyboardevent.asp) |
| lengthComputable                                             | 返回进度的长度是否可计算。                                   | [ProgressEvent](https://www.w3school.com.cn/jsref/obj_progressevent.asp) |
| loaded                                                       | 返回已加载的工作量。                                         | [ProgressEvent](https://www.w3school.com.cn/jsref/obj_progressevent.asp) |
| [metaKey](https://www.w3school.com.cn/jsref/event_metakey.asp) | 返回事件触发时是否按下了 "META" 键。                         | [MouseEvent](https://www.w3school.com.cn/jsref/obj_mouseevent.asp) |
| [metaKey](https://www.w3school.com.cn/jsref/event_key_metakey.asp) | 返回按键事件触发时是否按下了 "META" 键。                     | [KeyboardEvent](https://www.w3school.com.cn/jsref/obj_keyboardevent.asp)[TouchEvent](https://www.w3school.com.cn/jsref/obj_touchevent.asp) |
| MovementX                                                    | 返回相对于上一 mousemove 事件的位置的鼠标指针的水平坐标      | [MouseEvent](https://www.w3school.com.cn/jsref/obj_mouseevent.asp) |
| MovementY                                                    | 返回相对于上一 mousemove 事件的位置的鼠标指针的垂直坐标      | [MouseEvent](https://www.w3school.com.cn/jsref/obj_mouseevent.asp) |
| [newURL](https://www.w3school.com.cn/jsref/event_hashchange_newurl.asp) | 返回更改 hash 后的文档 URL。                                 | [HasChangeEvent](https://www.w3school.com.cn/jsref/obj_hashchangeevent.asp) |
| newValue                                                     | 返回更改后的存储项目的新值。                                 | [StorageEvent](https://www.w3school.com.cn/jsref/obj_storageevent.asp) |
| [offsetX](https://www.w3school.com.cn/jsref/event_offsetx.asp) | 返回鼠标指针相对于目标元素边缘位置的水平坐标。               | [MouseEvent](https://www.w3school.com.cn/jsref/obj_mouseevent.asp) |
| [offsetY](https://www.w3school.com.cn/jsref/event_offsety.asp) | 返回鼠标指针相对于目标元素边缘位置的垂直坐标。               | [MouseEvent](https://www.w3school.com.cn/jsref/obj_mouseevent.asp) |
| [oldURL](https://www.w3school.com.cn/jsref/event_hashchange_oldurl.asp) | 返回更改 hash 前的文档 URL。                                 | [HasChangeEvent](https://www.w3school.com.cn/jsref/obj_hashchangeevent.asp) |
| oldValue                                                     | 返回更改后的存储项目的旧值。                                 | [StorageEvent](https://www.w3school.com.cn/jsref/obj_storageevent.asp) |
| onemptied                                                    | 当发生不良情况且媒体文件突然不可用时，发生此事件。           |                                                              |
| [pageX](https://www.w3school.com.cn/jsref/event_pagex.asp)   | 返回触发鼠标事件时鼠标指针相对于文档的水平坐标。             | [MouseEvent](https://www.w3school.com.cn/jsref/obj_mouseevent.asp) |
| [pageY](https://www.w3school.com.cn/jsref/event_pagey.asp)   | 返回触发鼠标事件时鼠标指针相对于文档的垂直坐标。             | [MouseEvent](https://www.w3school.com.cn/jsref/obj_mouseevent.asp) |
| [persisted](https://www.w3school.com.cn/jsref/event_pagetransition_persisted.asp) | 返回网页是否被浏览器缓存。                                   | [PageTransitionEvent](https://www.w3school.com.cn/jsref/obj_pagetransitionevent.asp) |
| [preventDefault()](https://www.w3school.com.cn/jsref/event_preventdefault.asp) | 如果可以取消事件，则将其取消，不执行属于该事件的默认操作。   | [Event](https://www.w3school.com.cn/jsref/obj_event.asp)     |
| [propertyName](https://www.w3school.com.cn/jsref/event_transition_propertyName.asp) | 返回与动画或过渡相关联的 CSS 属性的名称。                    | [AnimationEvent](https://www.w3school.com.cn/jsref/obj_animationevent.asp)[TransitionEvent](https://www.w3school.com.cn/jsref/obj_transitionevent.asp) |
| pseudoElement                                                | 返回动画或过渡的伪元素的名称。                               | [AnimationEvent](https://www.w3school.com.cn/jsref/obj_animationevent.asp)[TransitionEvent](https://www.w3school.com.cn/jsref/obj_transitionevent.asp) |
| [relatedTarget](https://www.w3school.com.cn/jsref/event_relatedtarget.asp) | 返回与触发鼠标事件的元素相关的元素。                         | [MouseEvent](https://www.w3school.com.cn/jsref/obj_mouseevent.asp) |
| [relatedTarget](https://www.w3school.com.cn/jsref/event_focus_relatedtarget.asp) | 返回与触发事件的元素相关的元素。                             | [FocusEvent](https://www.w3school.com.cn/jsref/obj_focusevent.asp) |
| repeat                                                       | 返回是否重复按住某个键。                                     | [KeyboardEvent](https://www.w3school.com.cn/jsref/obj_keyboardevent.asp) |
| [screenX](https://www.w3school.com.cn/jsref/event_screenx.asp) | 返回窗口/鼠标指针相对于屏幕的水平坐标。                      | [MouseEvent](https://www.w3school.com.cn/jsref/obj_mouseevent.asp) |
| [screenY](https://www.w3school.com.cn/jsref/event_screeny.asp) | 返回窗口/鼠标指针相对于屏幕的垂直坐标。                      | [MouseEvent](https://www.w3school.com.cn/jsref/obj_mouseevent.asp) |
| [shiftKey](https://www.w3school.com.cn/jsref/event_shiftkey.asp) | 返回事件触发时是否按下了 "SHIFT" 键。                        | [MouseEvent](https://www.w3school.com.cn/jsref/obj_mouseevent.asp) |
| [shiftKey](https://www.w3school.com.cn/jsref/event_key_shiftkey.asp) | 返回按键事件触发时是否按下了 "SHIFT" 键。                    | [KeyboardEvent](https://www.w3school.com.cn/jsref/obj_keyboardevent.asp)[TouchEvent](https://www.w3school.com.cn/jsref/obj_touchevent.asp) |
| state                                                        | 返回包含历史记录条目副本的对象。                             | [PopStateEvent](https://www.w3school.com.cn/jsref/obj_popstateevent.asp) |
| [stopImme...()](https://www.w3school.com.cn/jsref/event_stopimmediatepropagation.asp) | 防止同一事件的其他侦听器被调用。                             | [Event](https://www.w3school.com.cn/jsref/obj_event.asp)     |
| [stopPropagation()](https://www.w3school.com.cn/jsref/event_stoppropagation.asp) | 防止事件在事件流中进一步传播。                               | [Event](https://www.w3school.com.cn/jsref/obj_event.asp)     |
| storageArea                                                  | 返回代表受影响的存储对象的对象。                             | [StorageEvent](https://www.w3school.com.cn/jsref/obj_storageevent.asp) |
| [target](https://www.w3school.com.cn/jsref/event_target.asp) | 返回触发事件的元素。                                         | [Event](https://www.w3school.com.cn/jsref/obj_event.asp)     |
| [targetTouches](https://www.w3school.com.cn/jsref/event_touch_targettouches.asp) | 返回包含仍与触摸面接触的所有触摸点的Touch对象的TouchList列表 | [TouchEvent](https://www.w3school.com.cn/jsref/obj_touchevent.asp) |
| [timeStamp](https://www.w3school.com.cn/jsref/event_timestamp.asp) | 返回创建事件的时间（相对于纪元的毫秒数）。                   | [Event](https://www.w3school.com.cn/jsref/obj_event.asp)     |
| total                                                        | 返回将要加载的工作总量。                                     | [ProgressEvent](https://www.w3school.com.cn/jsref/obj_progressevent.asp) |
| [touches](https://www.w3school.com.cn/jsref/event_touch_touches.asp) | 返回当前与表面接触的所有 touch 对象的列表。                  | [TouchEvent](https://www.w3school.com.cn/jsref/obj_touchevent.asp) |
| [type](https://www.w3school.com.cn/jsref/event_type.asp)     | 返回事件名称。                                               | [Event](https://www.w3school.com.cn/jsref/obj_event.asp)     |
| url                                                          | 返回已更改项目的所在文档的 URL。                             | [StorageEvent](https://www.w3school.com.cn/jsref/obj_storageevent.asp) |
| [which](https://www.w3school.com.cn/jsref/event_which.asp)   | 返回触发鼠标事件时按下的鼠标按钮。                           | [MouseEvent](https://www.w3school.com.cn/jsref/obj_mouseevent.asp) |
| [which](https://www.w3school.com.cn/jsref/event_key_which.asp) | 返回触发 onkeypress 事件的键的 Unicode 字符码，或触发 onkeydown 或 onkeyup 事件的键的 Unicode 键码 | [KeyboardEvent](https://www.w3school.com.cn/jsref/obj_keyboardevent.asp) |
| [view](https://www.w3school.com.cn/jsref/event_view.asp)     | 返回对发生事件的 Window 对象的引用。                         | [UiEvent](https://www.w3school.com.cn/jsref/obj_uievent.asp) |

<font color=red>事件冒泡：
</font>

​	就是事件的向上传递，当后代元素被触发时，其祖先元素的==相同事件==也会触发。

例如：

```html
<!DOCTYPE html>
<html lang="zh">
    <head>
        <meta charset="UTF-8">
        <title>事件冒泡</title>
        <style>
            #box1{
                width: 200px;
                height: 200px;
                background-color: #bfa;
            }
            #box2{
                width: 100px;
                height: 100px;
                background-color: red;
            }
        </style>

        <script>
            window.onload = function () {
                var box1 = document.getElementById("box1");
                var box2 = document.getElementById("box2");
                // 给box1绑定点击事件
                box1.onclick = function (event) {
                    alert("我是box1");
                };
                // 给box2绑定点击事件
                box2.onclick = function (event) {
                    alert("我是box2");
                }
            };
        </script>
    </head>
    <body>
        <div id="box1">
            <div id="box2"></div>
        </div>
    </body>
</html>
```

当我们点击`box2`时，由于事件冒泡，`box1`的点击事件也会触发。

==在开发中大部分的冒泡是对我们有利的，但是有部分的冒泡没有用处。==

取消事件冒泡：

语法：

~~~js
box2.onclick = function (event) {
    // 取消事件冒泡
    event.cancelBubble = true;
    alert("我是box2");
}
~~~

==利用事件对象可以来取消冒泡。==

<font color=red>事件的委派：
</font>

​	我们在为一些元素绑定事件时可能需要一个元素就需要绑定一个事件，这种操作比较麻烦，对于新添加的元素也需要重新绑定事件。我们希望只绑定一个事件，就可以应用在多个元素上，即使元素是后添加的。

==我们需要的给这些元素的事件绑定给元素的共同祖先元素，这样就可以通过事件的冒泡，当点击后代元素就会冒泡到祖先元素，就会执行祖先元素绑定的事件。这就是事件的委派，可以大大减少事件委派的次数，提高效率。==

```js
<!DOCTYPE html>
<html lang="zh">
<head>
   <meta charset="UTF-8">
   <title>事件委派</title>
   <script>
      window.onload = function () {
         // 获取btn元素
         var btn = document.getElementById("btn");
         // 获取ul元素
         var u = document.getElementById("list");
         btn.onclick = function () {
             var li = document.createElement("li");
             li.innerHTML = "<a href='javascript:;'>新链接</a>"
            u.appendChild(li);
            };

         u.onclick = function () {
            alert("你好");
            };
        };
   </script>
</head>
<body>
   <button id="btn">添加元素</button>
   <ul id="list">
      <li><a href="javascript:;">链接1</a></li>
      <li><a href="javascript:;">链接2</a></li>
      <li><a href="javascript:;">链接3</a></li>
      <li><a href="javascript:;">链接4</a></li>
   </ul>
</body>
</html>
```

上面就是只给 `ul` 这个祖先元素绑定了一次事件，当点击其中的`a`元素时，也会执行点击事件，即使是新添加的`a`元素。

但是，上面的中是把事件绑定给祖先元素的，如果点击祖先元素下的任何子元素包括祖先元素，都会执行对应的响应函数，需要进行改进。

```js
btn.onclick = function () {
    var li = document.createElement("li");
    li.innerHTML = "<a href='javascript:;' class='test'>新链接</a>"
    u.appendChild(li);
};

u.onclick = function (event) {
    if(event.target.className === "test"){
        alert("你好");
    }
};
```

给每个a元素添加一个class属性，然后通过事件对象获取触发事件的元素，如果触发的元素中含有test这个class属性就执行对应的事件函数，否则就不执行事件函数。

==`event.target`这个是获取触发事件的元素对象。==

<font color=red>事件的绑定：
</font>

在之前的事件绑定使用的是 `元素.事件名 = 响应函数`这种方式，这种方式只能绑定一个同类型事件，如果绑定多个同类型的事件，后面绑定的事件会覆盖前面所绑定的事件。如果需要绑定多个同类型的事件时，我们使用下面这种方式进行绑定。

```html
<!DOCTYPE html>
    <html lang="zh">
        <head>
        <meta charset="UTF-8">
           <title>事件绑定</title>
<script>
            window.onload = function () {
            var btn = document.getElementById("btn");

            btn.addEventListener("click",function () {
                alert("成功被点击!1");
            },false);

            btn.addEventListener("click",function () {
                alert("成功被点击!2");
            },false);
        };
</script>
</head>
<body>
    <button id="btn">我是按钮</button>
</body>
</html>
```

上面就表示给`btn`绑定了两个点击事件函数。

`addEventListener`方法的参数：

- 第一个参数：事件的名称，但是不要加上on。
- 第二个参数：响应函数。
- 第三个参数：是否在捕获阶段触发事件，需要一个boolean值，一般传false。

==改方法可以为一个元素的相同事件绑定多个响应函数，响应函数的执行顺序是，先绑定先执行。==

<font color=red>事件的三个阶段：
</font>

- 捕获阶段：在捕获阶段时是从最外层的祖先元素，向目标元素进行事件的捕获，但是默认现阶段事件是不会触发的。
- 目标阶段：事件捕获到目标元素，捕获结束开始在目标元素上触发事件。
- 冒泡阶段：事件从目标元素向祖先元素传递，依次触发捕获到是事件。



## 14、`BOM`对象

​	浏览器对象模型，我们可以通过`js`来操作浏览器，在`BOM`中提供了一组对象用于对浏览器的操作。

- Window对象：代表整个浏览器窗口，同时window是网页中的全局对象。
- Navigator对象：代表当前浏览器的信息，通过该对象来识别不同的浏览器。
- History对象：代表浏览器的历史记录，通过该对象可以操作浏览器的历史记录，但是由于隐私原因，不能获得具体的历史记录，只能操作浏览器前后翻页，而且只有当前访问有效。
- Screen对象：代表用户的屏幕信息，通过该对象可以获取用户的显示器的相关信息。

==这些`BOM`对象都是作为Window对象的属性保存的，可以直接使用。==

### 14.1、Window对象

​	Window 对象表示浏览器中打开的窗口。

==如果文档包含框架（frame 或 `iframe` 标签），浏览器会为 HTML 文档创建一个 window 对象，并为每个框架创建一个额外的 window 对象。==

### 14.2、Navigator对象

​	代表用户浏览器的信息，通过该对象来便是不同的浏览器。由于历史原因，Navigator对象的大部分属性都不能帮助我们来辨别浏览器，我们一般只会使用`userAgent`是一个字符串来便是浏览器，不同浏览器的`userAgent`不同。

谷歌：`Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36`

IE：`Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36 Edg/94.0.992.50`

火狐：`Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) firfox/94.0.4606.81 Safari/537.36`

### 14.3、History对象

​	该对象用于操作浏览器向前或向后跳转页面。

<font color=red>属性：
</font>

- length：返回浏览器中历史列表中的URL数量。

<font color=red>方法：
</font>

- back()：向后跳转一个页面，相当于是浏览器的回退箭头。
- forward()：向前跳转一个页面，相当于是浏览器的前进箭头。
- go()：用于跳转浏览器历史记录中指定的页面，参数需要一个整数，1表示向前跳转一个页面，2表示向前跳转两个页面，依次类推。如果是负数，就是向后跳转相应的页面数。

### 14.4、Location对象

​	该对象封装了浏览器地址栏的相关信息，如果直接打印Location对象，会把页面的URL打印出来。

==如果将location对象修改为一个完整的URL地址，或者是相对路径，则我们的页面就会跳转到对应的页面中。并生成历史记录。==

<font color=red>属性：
</font>

| 属性                                                         | 描述                                          |
| :----------------------------------------------------------- | :-------------------------------------------- |
| [hash](https://www.w3school.com.cn/jsref/prop_loc_hash.asp)  | 设置或返回从井号 (#) 开始的 URL（锚）。       |
| [host](https://www.w3school.com.cn/jsref/prop_loc_host.asp)  | 设置或返回主机名和当前 URL 的端口号。         |
| [hostname](https://www.w3school.com.cn/jsref/prop_loc_hostname.asp) | 设置或返回当前 URL 的主机名。                 |
| [href](https://www.w3school.com.cn/jsref/prop_loc_href.asp)  | 设置或返回完整的 URL。                        |
| [pathname](https://www.w3school.com.cn/jsref/prop_loc_pathname.asp) | 设置或返回当前 URL 的路径部分。               |
| [port](https://www.w3school.com.cn/jsref/prop_loc_port.asp)  | 设置或返回当前 URL 的端口号。                 |
| [protocol](https://www.w3school.com.cn/jsref/prop_loc_protocol.asp) | 设置或返回当前 URL 的协议。                   |
| [search](https://www.w3school.com.cn/jsref/prop_loc_search.asp) | 设置或返回从问号 (?) 开始的 URL（查询部分）。 |

<font color=red>方法：
</font>

- assign()：用于跳转到其它页面，和直接修改location效果一样。
- reload()：用于重新加载当前页面，作用和刷新一样，但是不会清理缓存，如果需要清理缓存刷新，可以传入一个true作为参数，这样就会强制清空缓存。
- replace()：用于新的页面替换当前页面，和assign方法效果一样，但是不会生成历史记录。



## 15、定时器

​	如果我们希望一段程序每隔一段时间执行一次，我们就可以使用定时器来完成。

<font color=red>开启定时器：
</font>我们使用`setInterval()`方法来开启定时器。

语法：

```js
var timer = setInterval(function () {
    // 需要执行的代码
},1000)
```

- 该方法是window对象的方法，可以随时使用。
- 该方法有两个参数：①一个回调函数，需要定时的执行代码。②一个整数，表示每隔多少时间回调函数执行一次，单位是毫秒。
- 该方法的返回值是一个整数，是这个定时器的唯一标识。

==这个定时器就表示每个1秒，执行一次回调函数。==

<font color=red>关闭定时器：
</font>我们使用`clearInterval()`方法来关闭定时器。

语法：

```js
var num = 0;
var timer = setInterval(function () {
    num++;
    h.innerHTML = num;
    if(num === 10){
        clearInterval(timer);
    }
},1000)
```

==该方法需要一个参数，这个参数就是开启定时器时的返回值，这个返回值是个整数，但是唯一标识的整数。这个参数其实可以是任意类型的值，null和undefined也可以，但是如果参数不是一个有效的定时器表示，什么也不会发生，如果是一个有效的定时器标识，就会停止对应的定时器。==

==表示，当执行10秒后，就清除定时器，就不再继续执行。==

==注意：当开启一个定时器时需要将之前给当前元素绑定的定时器关闭，不然会创建多个定时器，导致间隔时间变小且不能关闭。==

<font color=red>开启延时调用：
</font>我们使用`setTimeout()`来开启一个延时调用。

语法：

```js
var timer = setTimeout(function () {
    // 执行的代码
},3000);
```

==延时调用和定时器类似，不同的就是延时调用是一段时间后执行对应的程序。只会执行一次。==

<font color=red>关闭延时调用：
</font>我们`clearTimeout()`方法来关闭延时调用。

语法：

```js
clearTimeout(timer);
```

参数也是以一个延时调用的唯一标识，和关闭定时器类似。



## 16、`JSON`

​	`JSON`（`javaScript` Object Notation），本质是一个字符串，但是是一个`js`对象表示的字符串。一种数据交换的通用格式，用于数据交换的。

`JSON`分为：对象`JSON`、数组`JSON`。

<font color=red>对象json字符串：
</font>

语法：

```js
var json = '{"属性名":属性值,"属性名":属性值}';
```

==属性名必须使用双引号包裹，`json`本身是一个字符串的格式。==

<font color=red>数组json字符串：
</font>

语法：

```js
var jsonArr = '[值,值,值]';
```

<font color=red>json允许的值：
</font>并不是所有类型的值都可以作为`json`的值，有部分不能作为`json`的值。

- 字符串
- number
- 布尔值
- null
- 对象（只能是普通对象，不能是函数对象）
- 数组

<font color=red>json字符串与js对象相互转换：
</font>在`js`中提供了一个`JSON`工具用于将`json`字符串和`js`对象的相互转换。

- `json`字符串---》`js`对象：

语法：

```js
// 一个json字符串
var json = '{"name":"张三","age":18}';
// 转换成js对象
var obj = JSON.parse(json);
console.log(obj.name);
```

==`JSON.parse()`方法需要一个`json`字符串，把这个`json`字符串转换成一个`js`对象，这样就可以在`js`中操作该对象。==

==数组`json`字符串就会转换成一个`js`数组对象。==

- `js`对象---》`json`字符串：

语法：

```js
var json1 = JSON.stringify(obj);
console.log(json1);
```

==`JSON.stringify()`方法需要一个`js`对象，把这个`js`对象转换成一个`json`字符串。==





































































































































































































































































 

































































































































































































































