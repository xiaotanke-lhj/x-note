[TOC]

# Vue 进阶学习



## 1、webpack 打包

​	webpack 是代码编译工具，有入口、出口、loader 和插件。webpack 是一个用于现代JavaScript应用程序的静态模块打包工具。当 webpack 处理应用程序时，它会在内部构建一个依赖图(dependency graph)，此依赖图对应映射到项目所需的每个模块，并生成一个或多个 *bundle*。就是用于将符合ES6的js代码打包为ES5的js代码，供浏览器使用。

1. 安装webpack环境

使用命令：

- `npm install webpack -g`
- `npm install webpack-cli -g`

![image-20211115200804490](https://i.loli.net/2021/11/15/D57I9tkq21PShNp.png)

2. 构建打包格式

```js
// 导出模板
module.exports = {
    // 指定需要打包的文件js文件的路径，这个文件名必须是main.js
    entry: './modules/main.js',
    output: {
        // 将main.js打包到js这个目录下，打包的文件就是bundle.js，如果不存在这个目录就会新建这个目录
        filename: './js/bundle.js'
    }
};
```

3. 打包

在idea中的终端使用webpack命令进行打包，可能会出现这个错误

![image-20211115201118126](https://i.loli.net/2021/11/15/hmGYUrZuBcLe67N.png)

需要以管理员身份运行powershell，输入命令`set-ExecutionPolicy RemoteSigned`

![image-20211115201226291](https://i.loli.net/2021/11/15/z3I7GSlBEmQ4eV9.png)

再进行打包：

![image-20211115201308869](https://i.loli.net/2021/11/15/KIevqlFx7186nsi.png)

打包后会生成对应的js文件，这样页面就可以直接引用

![image-20211115201402100](https://i.loli.net/2021/11/15/71cIlpDQfjwr9sm.png)



## 2、vue-router 路由

​	在Vue中用也页面的跳转的。

### 2.1、安装 vue-router 路由

命令：`npm install vue-router -g` （全局安装）或者 `npm install vue-router --save-dev`（当前项目安装）

==安装完成后，在`node_modules`这个文件下有vue-router这个文件夹。==

![image-20211122144446358](https://i.loli.net/2021/11/22/wmzhRPeWiM4px6r.png)

### 2.2、创建一个router文件夹

​	这个文件夹中放置路由的配置，在这个文件夹中创建一个index.js来配置路由，名称为index后之后的导入可以省略名称，它可以自动扫描index.js这个文件。

```js
// 导入Vue这个模块
import Vue from 'vue'
// 导入vue-router这个模块
import VueRouter from "vue-router";

// 安装路由
Vue.use(VueRouter);

// 配置路由
export default new VueRouter({
  // 是一个数组，数组的的每一个对象就是一个组件的路由配置
  routes:[
    {
      // 访问的路径
      path: "/main",
      // 这个路由配置的名称
      name: "main",
      // 跳转的组件，需要提前引入，值是导入的组件名称
      component: Main
    }
  ]
})
```

### 2.2、将路由的配置文件导入到main.js中

```js
import Vue from 'vue'
import App from './App'
// 导入路由的配置
import router from './router' // 如果配置文件是index.js的话，它会自动扫描

Vue.config.productionTip = false

new Vue({
  el: '#app',
  // 配置路由
  router,
  render: h => h(App)
})
```

### 2.3、创建一个Main组件，并将组件配置到router中

```vue
<template>
  <h1>这是主页面!!!</h1>
</template>

<script>
export default {
  name: "Main"
}
</script>

<style scoped>

</style>
```

```js
// 导入Vue这个模块
import Vue from 'vue'
// 导入vue-router这个模块
import VueRouter from "vue-router";
// 导入Main组件
import Main from '../components/Main'

// 安装路由
Vue.use(VueRouter);

// 配置路由
export default new VueRouter({
  // 是一个数组，数组的的每一个对象就是一个组件的路由配置
  routes:[
    {
      // 访问的路径
      path: "/main",
      // 这个路由配置的名称
      name: "main",
      // 跳转的组件，需要提前引入，值是导入的组件名称
      component: Main
    }
  ]
})
```

### 2.4、使用router路由

​	在APP这个组件中创建一个跳转链接，跳转到主页面。

```vue
<template>
  <div id="app">
    <h1>你好 ==== Vue!</h1>
    <!-- 这是一个跳转链接，相当于是a标签，to就是指定在路由中配置的路径-->
    <router-link to="/main">主页</router-link>
    <!-- 用于显示跳转的组件-->
    <router-view></router-view>
  </div>
</template>

<script>

export default {
  name: 'App'
}
</script>

<style>
  #app {
    width: 400px;
    height: 300px;
    margin: 0 auto;
  }
</style>
```

### 2.5、测试

![image-20211122152520568](https://i.loli.net/2021/11/22/Q8y4Sh3putTM2qO.png)

![image-20211122152539622](https://i.loli.net/2021/11/22/aBAShOjyQZK5Uxz.png)

步骤：

-  配置路由的index.js文件，将需要跳转的组件导入并配置。
- 在APP.vue中导入路由配置，并使用路由。
- 在组件中使用router-link来跳转，使用router-view来显示跳转的组件。



## 3、Element-UI

### 3.1、安装element-ui

命令：`npm i element-ui -S` ，如果在node_module中存在element-ui就可以了。

### 3.2、在main.js中引入element-ui模块

```js
import Vue from 'vue'
import App from './App'

Vue.config.productionTip = false

import VueRouter from "./router";
// 导入elementui
import ElementUI from 'element-ui';
// 还需要导入一个css文件
import 'element-ui/lib/theme-chalk/index.css';

// 声明使用elementui
Vue.use(ElementUI);

new Vue({
  el: '#app',
  VueRouter,
  render: h => h(App)
})
```

### 3.3、使用element-ui组件

​	在element-ui的官网上去复制自己需要的组件，然后在自己的组件中复制过来就可以。

![image-20211122201611980](https://i.loli.net/2021/11/22/vqfRz7ragmK9JT6.png)

创建自己的组件，复制拷贝的element-ui组件

```vue
<template>
  <div id="banner">
    <el-carousel indicator-position="outside">
      <el-carousel-item v-for="item in items" :key="item">
        <img :src="item" alt="这是图片">
      </el-carousel-item>
    </el-carousel>
  </div>
</template>

<script>
  export default {
    name: "Main",
    model: "#banner",
    data() {
      return {
        items: ["../../static/images/5.图片6.jpg",
          "../../static/images/5.图片5.jpg",
          "../../static/images/5.图片4.jpg",
          "../../static/images/5.图片3.jpg",
          "../../static/images/5.图片2.png",
          "../../static/images/5.图片1.jpg"]
      }
    }
  }
</script>

<style scoped>
  .el-carousel__item h3 {
    color: #475669;
    font-size: 18px;
    opacity: 0.75;
    line-height: 300px;
    margin: 0;
  }
  .el-carousel__item:nth-child(2n) {
    background-color: #99a9bf;
  }
  .el-carousel__item:nth-child(2n+1) {
    background-color: #d3dce6;
  }
</style>
```



## 4、路由传参

### 4.1、vue-router传参

```vue
<!-- 这是一个跳转链接，相当于是a标签，to就是指定在路由中配置的路径，name指定在路由中配置的组件的名称-->
<router-link :to="{name: 'main',params: {id: 10}}">主页</router-link>
```

==需要使用 v-bind 指令绑定to属性，属性值是一个对象，name属性是自定跳转的路由的名称，params属性是参数，参数值也是一个对象。==

### 4.2、在路由配置中接受参数

<font color=red>方式一：
</font>

```js
// 访问的路径，直接在path的后面添加 :参数名，可以跟多个参数
path: "/main/:id",
```

<font color=red>方式二：
</font>

```js
{
  // 访问的路径，直接在path的后面添加 :参数名，可以跟多个参数
  path: "/main/:id",
  // 这个路由配置的名称
  name: "main",
  // 跳转的组件，需要提前引入，值是导入的组件名称
  component: Main,
  props: true  // 增加一个props让他支持使用props来接受参数
},
```

### 4.3、组件中获取传递的参数

==如果在路由配置中采用第一个方式接受参数，则使用下面这种方法：==

```vue
<!-- 在组件中获取传递的参数-->
<h1>这是主页面!!!{{$route.params.id}}</h1>
```

==如果在路由配置中采用第二个方式接受参数，则使用下面这种方法：==

```vue
<template>
  <!-- 在组件中获取传递的参数，直接使用-->
  <h1>这是主页面!!!{{id}}</h1>
</template>

<script>
export default {
  props: ["id"], // 接受参数
  name: "Main"
}
</script>

<style scoped>
</style>
```



## 5、重定向

​	在路由的配置中，可以使用 redirect 重定向到某个组件中。

```js
{
  path: "/goHome",
  redirect: "/main"
}
```

==当访问 /goHome 这个请求时，就会直接跳转到 /main 这个路由对应的组件中，并且url的地址变成 /main。==



## 6、路由钩子函数

### 6.1、beforeRouteEnter  进入路由前

​	一般在这个路由的钩子函数中进行异步请求。

```vue
<script>
export default {
  props: ["id"], // 接受参数
  name: "Main",
  beforeRouteEnter: function (to, from, next) {
    console.log("进入路由前");
    next();
  }
}
</script>
```

==这个钩子函数存在三个参数，to、from、next，这三个参数不能缺少。==

- to：路由将要跳转的路劲信息。
- from：路径跳转前的路径信息。
- next：路由的控制参数。
- next()：跳入路由对应的组件。
  1. next("path")：跳入到指定的路径中。
  2. next(false)：返回之前的跳转页面。
  3. next( (vm) => {})：仅在 beforeRouteEnter 中可以使用，vm就是这个跳转的实例对象。

### 6.2、beforeRouteLeave 离开路由前

```js
beforeRouteLeave: function (to, from, next) {
  
}
```

==这三个参数与 beforeRouteEnter  这个钩子函数的参数作用一样。==

### 6.3、beforeRouteEnter   应用

​	在进入路由之前，通过这个钩子函数异步请求数据。

```vue
<template>
  <div>
    <!-- 在组件中获取传递的参数，直接使用-->
    <h1>这是主页面!!!{{ id }}</h1>
    <h1>展示的数据：</h1>
    <div v-for="item in items">
      <p>id=={{ item.id }}</p>
      <p>name=={{ item.name }}</p>
      <p>age=={{ item.age }}</p>
    </div>
  </div>
</template>

<script>
import axios from "axios";

export default {
  props: ["id"], // 接受参数
  name: "Main",
  beforeRouteEnter: function (to, from, next) {
    next(vm => {
      // vm就是这个组件对象，这个组件对象中调用vm中的方法
      vm.getData();
    });
  },
  data(){
    return{
      items: {
          id: "",
          name: "",
          age: ""
      }
    }
  },
  methods: {
    getData: function () {
      axios({
        method: "get",
        url: "http://localhost:8080/static/data.json"
      }).then(response =>{
          this.items = response.data;
      });
    }
  }
}
</script>
<style scoped>
</style>
```

































