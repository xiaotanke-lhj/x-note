[TOC]

# `Vue2`基础学习笔记

​	`Vue` 是一套用于构建用户界面的渐进式JavaScript框架。与其它大型框架不同的是，`Vue` 被设计为可以自底向上逐层应用。`Vue` 的核心库只关注视图层，不仅易于上手，还便于与第三方库或既有项目整合。另一方面，当与现代化的工具链以及各种支持类库结合使用时，`Vue` 也完全能够为复杂的单页应用提供驱动，`Vue`是一个中国人尤雨溪开发的。



## 1、`MVVM`模式

`MVVM`是`Model-View-ViewModel`的简写。它本质上就是`MVC` 的改进版。`MVVM` 就是将其中的View 的状态和行为抽象化，让我们将视图 `UI` 和业务逻辑分开。

![image-20211111142148300](https://i.loli.net/2021/11/11/qHILtMGl47wcAva.png)

- 低耦合：视图可以独立于Model变化和修改，一个`ViewModel`可以绑定到不同的View上，当View变化时，Model可以不改变，当Model变化时，View也可以不改变。
- 可复用：可以将一些View逻辑放在一个`ViewModel`中，让很多的View可以重复使用这些逻辑。
- 独立开发：业务员可以专注到业务逻辑的开发，设计人员专注到网页的设计上。



## 2、第一个`Vue`程序

[Vue官网：https://cn.vuejs.org/](https://cn.vuejs.org/)

1. 引入`Vue.js`，你可以使用`CDN`或者去`GitHub`上下载`vue`的源码，然后自己手动导入`vue.js`文件
2. 编写第一个`vue`程序。

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>第一个Vue程序</title>
    <script src="../js/vue.min.js"></script>
</head>
<body>

    <div id="box">
        <!-- 获取绑定的数据-->
        {{msg}}
    </div>
</body>
<script>
    // ViewModel绑定数据
    var app = new Vue({
        // 绑定的元素
        el: "#box",
        // 绑定的数据，以对象形式传入
        data: {
            msg: "你好,Vue!"
        }
    });
</script>
</html>
```



## 3、`Vue`的基本语法

​	在`vue`中，以`v-`开头的都是`vue`的指令。

### 3.1、v-bind指令

​	将一个`vue`对象的数据绑定给一个元素的属性。

```html
<body>

    <div id="box">
        <!-- 获取数据，绑定给h1的title属性-->
        <h1 v-bind:title="msg">你好</h1>
    </div>
</body>
<script>
    // ViewModel绑定数据
    var app = new Vue({
        // 绑定的元素
        el: "#box",
        // 绑定的数据，以对象形式传入
        data: {
            msg: "你好,Vue!"
        }
    });
</script>
```

==可以简写为：`:title="msg"`。==

### 3.2、v-if 指令

​	根据数据的true和false来决定一个元素是否显示。

```html
<body>
    <div id="box">
        <!-- 如果数据为true这个元素就显示，如果数据为false这个元素就不显示-->
        <h1 v-if="msg">你好</h1>
    </div>
</body>
<script>
    // ViewModel绑定数据
    var app = new Vue({
        // 绑定的元素
        el: "#box",
        // 绑定的数据，以对象形式传入
        data: {
            msg: true
        }
    });
</script>
```

### 3.3、v-else 指令

​	距离最近的 `v-if` 指令，如果if不成立，则这个指令成立，就显示这个元素。

```html
<body>
    <div id="box">
        <h1 v-if="msg">你好</h1>
        <!-- 前面的v-if不成立，就显示下面的v-else-->
        <h1 v-else>不好</h1>
    </div>
</body>
<script>
    // ViewModel绑定数据
    var app = new Vue({
        // 绑定的元素
        el: "#box",
        // 绑定的数据，以对象形式传入
        data: {
            msg: false
        }
    });
</script>
```

### 3.4、v-else-if 指令

​	相当于判断语句中的`else if`，如果这个条件满足就显示这个元素。

```html
<body>
    <div id="box">
        <h1 v-if="msg === 'A'">A</h1>
        <h1 v-else-if="msg === 'B'">B</h1>
        <h1 v-else>C</h1>
    </div>
</body>
<script>
    // ViewModel绑定数据
    var app = new Vue({
        // 绑定的元素
        el: "#box",
        // 绑定的数据，以对象形式传入
        data: {
            msg: "B"
        }
    });
</script>
```

### 3.5、v-for 指令

​	用于遍历一个数组，将遍历的数据显示，遍历到一个数据就显示一个元素。

```html
<body>
    <div class="app">
        <!-- item就是遍历到的每一个数据，index是每一个数据的下标，每遍历到一个数据就会创建一个li元素来展示数据-->
        <li v-for="(item, index) in users">
            {{item.name}} --- > {{index}}
        </li>
    </div>
</body>
<script>
    // ViewModel绑定数据
    var app = new Vue({
        // 绑定的元素
        el: ".app",
        // 绑定的数据，以对象形式传入
        data: {
            users: [
                {name: "张三"},
                {name: "李四"},
                {name: "王五"}
            ]
        }
    });
</script>
```

### 3.6、v-on 指令

​	用于绑定元素的事件。

```html
<body>
    <div id="app">
        <!-- click是事件的名称，test是在Vue对象中methods中注册是响应事件函数的名称-->
        <button v-on:click="test">点击我</button>
    </div>
</body>
<script>
    var app = new Vue({
        el: "#app",
        data: {
            msg: "你好,Vue!"
        },
        methods: {
            // 设置一个响应事件函数，响应函数必须定义在methods这个属性中
            "test": function () {
                alert("点击成功!");
            }
        }
    });
</script>
```

==可以简写为：`@click="test"`。==

### 3.7、v-model 指令

​	用于数据于元素之间的双向绑定，当元素的值发生改变时，数据也会发生改变，通常用于表单、文本、下拉框。

```html
<body>
    <div id="app">
        <span>{{msg}}</span><br />
        <!-- 使用v-model实现双向绑定，当input的值改变时，Vue中的msg就改变为对应的值-->
        <input type="text" v-model="msg"/>
    </div>
</body>

<script>
    var app = new Vue({
        el: "#app",
        data: {
            msg: ""
        }
    });
</script>
```

### 3.8、v-once 指令

通过使用该指令，你也能执行一次性地插值，当数据改变时，插值处的内容不会更新。但请留心这会影响到该节点上的其它数据绑定：

```html
<div id="app">
    <!-- 这个值绑定后后面就不能改变了。-->
    <span v-once>{{msg}}</span>
</div>
```

### 3.9、`v-html`指令

双大括号会将数据解释为普通文本，而非 HTML 代码。为了输出真正的 HTML，你需要使用 `v-html`。

```html
<div id="app">
    <span v-html="msg">{{msg}}</span>
</div>
```

### 3.10、v-show 指令

另一个用于根据条件展示元素的选项是 `v-show` 指令，这个指令只是简单的将元素的display属性设置为none。用法大致一样：

```html
<div id="app">
    <!-- 如果ok的值为true就显示，如果为false就不显示-->
    <span v-show="ok">{{msg}}</span>
</div>
```







## 4、组件

​	组件系统是 `vue` 的另一个重要概念，因为它是一种抽象，允许我们使用小型、独立和通常可复用的组件构建大型应用。组件就是自定义标签，组件相同的标签可以复用。

<font color=red>注册组件：</font>

```html
<body>
    <div id="app">
        <!-- 使用自定义的组件，使用v-for指令获取数据，将遍历的item数据绑定给自定义组件的自定义属性li_data中-->
        <my-component v-for="item in items" v-bind:li_data="item"></my-component>
    </div>
</body>
<script>
    // 注册组件，第一个参数是组件的名称，第二个参数是一个对象
    Vue.component("my-component",{
        props: ['li_data'],  //定义一个属性接收数据
        template: '<li>我是li第{{li_data}}个</li>'  // 组件的模板
    });
    var app = new Vue({
        el: "#app",
        data: {
            items: [1,2,3,4]
        }
    });
</script>
```

==如果组件中模板中存在多个不同的标签元素，需要在外面添加一个div标签进行包裹，不然只会显示第一个标签元素。==

## 5、计算属性

​	计算属性就是一个函数，它可以将函数的结果缓存起来，如果数据不发生改变，再次调用这个函数时就直接从缓存在拿取结果，不会再执行一次函数，如果数据发生改变就会重新计算。可以把它想象成一个缓存。

```html
<body>
    <div id="app">
        <!-- 调用函数-->
        <span>{{returnTime()}}</span><br />
        <!-- 计算属性-->
        <span>{{returnTime1}}</span>
    </div>
</body>
<script>
    var app = new Vue({
        el: "#app",
        data: {
            msg: "你好"
        },
        methods: {
            "returnTime": function () {
                return Date.now();
            }
        },
        // 加载后这个计算属性的值就不会改变，保存在缓存中
        computed: {
            "returnTime1": function (){
                return Date.now();
            }
        }
    });
</script>
```

==计算属性的名称和方法的名称不能重复，如果重复，调用的只能是方法。计算属性就是为了将不经常变化得结果进行缓存，节约系统得开销，提高性能。==



## 6、插槽（slot）

​	我们使用slot作为承载分发内容的出口，可以用于组合组件的场景中。

```html
<body>
<div id="app">
    <!-- 使用插槽组件-->
    <my_slot>
        <!-- 将组件插入插槽，组件使用slot属性指定一个插入的插槽，值必须和定义插槽的name相同-->
        <my_title :data="title" slot="title_slot"></my_title>
        <my_content v-for="item in items" :data="item" slot="content_slot"></my_content>
    </my_slot>
</div>
</body>
<script>
    // 定义一个具有插槽的组件，每一个插槽需要指定一个name属性，用于其它组件进行插入
    Vue.component("my_slot",{
        template: "<div>\
                        <slot name='title_slot'></slot>\
                        <ul>\
                            <slot name='content_slot'></slot>\
                        </ul>\
                    </div>"
    });
    // 定义一个插入插槽的组件
    Vue.component("my_title",{
        props: ["data"],
        template: "<h2>{{data}}</h2>"
    });
    // 定义一个插入插槽的组件
    Vue.component("my_content",{
       props: ["data"],
       template: "<li>{{data}}</li>"
    });
    var app = new Vue({
        el: "#app",
        data: {
            title: "我最帅",
            items: ["曾小贤","胡一菲","吕子桥","关谷神奇","唐悠悠"]
        }
    });
</script>
```

## 7、`Vue` 实例

### 7.1、创建`Vue`对象

```js
var app = new Vue({
    // 它有自己的属性
});
```

### 7.2、`Vue` 的7大属性

| 属性名   | 解释                                                         |
| -------- | ------------------------------------------------------------ |
| el       | 用来指示`vue`编译器从什么地方开始解析 `vue`的语法，可以说是一个占位符。 |
| data     | 视图绑定的数据存放再这个属性中，数据以对象形式存在。         |
| template | 用来设置模板，会替换页面元素，包括占位符。                   |
| methods  | 写业务逻辑，写函数供视图层调用。                             |
| render   | 创建真正的Virtual Dom                                        |
| computed | 用于计算属性，将计算的结果保存在缓存中。                     |
| watch    | 以通过 watch 来响应数据的变化。`watch:function(new,old){}` 监听data中数据的变化两个参数，一个返回新值，一个返回旧值。 |

### 7.3、数据与方法

当一个 `vue` 实例被创建时，它将 `data` 对象中的所有的 属性 加入到 `vue` 的**响应式系统**中。当这些 属性 的值发生改变时，视图将会产生“响应”，即匹配更新为新的值。

```html
<body>
    <div id="app">
        {{data.name}}
    </div>
</body>
<script>
    var data = {
        name: "张三",
        age: 18,
    }
    var app = new Vue({
        el: "#app",
        // 将js的一个data对象赋值给vue的data属性，那么app这个vue对象也会有data这个js对象的全部属性及属性值
        // 并且，当js中的data发生改变时，app这个对象对应的属性也会改变，使用app这个对象改变对应的属性时，原生的data数据也会改变
        data: data
    });
    // 改变data对象的name属性
    data.name = "王五";   // 这时app对象的name属性也发生了改变
    // 改变app对象的name属性
    app.name = "李四";    // 这时data的name属性也会改变
</script>
```

==注意：当一个`vue`实例已经完成后，再向data这个对象中添加属性，`app`这个对象中不会有这个属性。==

==这里唯一的例外是使用 `Object.freeze()`，这会阻止修改现有的 property，也意味着响应系统无法再*追踪*变化。==

```js
// data中的属性值就不能改变
Object.freeze(data);
```

### 7.4、 `Vue` 实例的生命周期

![20200616222020393](https://i.loli.net/2021/11/22/ZPMi6wmN5eB8fdV.png)

~~~js
		beforeCreate() {
            console.log('------初始化前------')
            console.log(this.message)
            console.log(this.$el)
        },
        created () {
            console.log('------初始化完成------')
            console.log(this.message)
            console.log(this.$el)
        },
        beforeMount () {
            console.log('------挂载前---------')
            console.log(this.message)
            console.log(this.$el)
        },
        mounted () {
            console.log('------挂载完成---------')
            console.log(this.message)
            console.log(this.$el)
        },
        beforeUpdate () {
            console.log('------更新前---------')
            console.log(this.message)
            console.log(this.$el)
        },
        updated() {
            console.log('------更新后---------')
            console.log(this.message)
            console.log(this.$el)
        }
~~~



## 8、自定义事件内容分发

​	在组件中有自己定义的事件，在`vue`这个对象中也有自己定义的事件，但是组件中不能使用vue定义的事件

```html
<script>
    Vue.component("my-todo",{
        template: "<div>\
                        <slot name='title'></slot>\
                        <ul>\
                            <slot name='item'><slot/>\
                        </ul>\
                    </div>"
    });
    Vue.component("title-todo",{
        props: ["title"],
        template: "<h1>{{title}}</h1>",

    });
    Vue.component("todo-items",{
        props: ["item","index"],
        // 只能调用组件中定义的方法，而不能直接使用vue对象的方法，不能调用removeItem方法
        template: "<li>{{item}}---{{index}}<button @click='remove'>删除</button></li>",
        methods: {
            remove: function () {
                alert("组件中的方法!");
            }
        }
    });

    var app = new Vue({
        el: "#app",
        data: {
            title: "蔬菜",
            items: ["白菜","萝卜","西红柿"]
        },
        methods: {
            removeItem: function () {
                alert("vue中的方法!")
            }
        }
    });
</script>
```

这样就不能通过组件中的方法操作`vue`对象中的data数据，但是我们可以通过自定义事件分发，自定义一个标签，然后在组件中通过`$.emit("自定事件名",参数)`，而自定义事件时可以调用`vue`对象中的方法的。

```html
<body>
    <div id="app">
        <my-todo>
            <title-todo slot="title" :title="title"></title-todo>
            <!-- 自定义了一个remove事件，通过这个自定义事件调用vue对象中的方法-->
            <todo-items slot="item" v-for="(item,index) in items" :item="item" :index="index" @remove="removeItem(index)"></todo-items>
        </my-todo>
    </div>
</body>
<script>
    Vue.component("my-todo",{
        template: "<div>\
                        <slot name='title'></slot>\
                        <ul>\
                            <slot name='item'><slot/>\
                        </ul>\
                    </div>"
    });
    Vue.component("title-todo",{
        props: ["title"],
        template: "<h1>{{title}}</h1>",

    });
    Vue.component("todo-items",{
        props: ["item","index"],
        template: "<li>{{item}}---{{index}}<button @click='remove'>删除</button></li>",
        methods: {
            remove: function (index){
                // 通过$.emit()来通过自定义事件调用vue对象中的方法
               this.$emit("remove",index);
            }
        }
    })

    var app = new Vue({
        el: "#app",
        data: {
            title: "蔬菜",
            items: ["白菜","萝卜","西红柿"]
        },
        methods: {
            removeItem: function (index) {
                this.items.splice(index,1);
            }
        }
    });
</script>
```



## 9、使用`axios`请求数据并展示

```html
<body>
    <div id="app">
        <table border="1" style="border-spacing: 0"> <!-- 展示数据-->
            <tr v-for="item in items">
                <td>{{item.name}}</td>
                <td>{{item.age}}</td>
                <td>{{item.school}}</td>
            </tr>
        </table>
    </div>
</body>
<script>
    var app = new Vue({
        el: "#app",
        data(){  // 使用data函数来接受数据
            return{
                items:
                    {
                        name: "",
                        age: "",
                        school: ""
                    }
            }
        },
        mounted(){
            // 使用axios进行请求
            axios({
                url: "../data.json" 
            }).then(response =>{ // 将返回的数据进行赋值
                this.items = response.data;
            })
            // 不能采用这种方式，会报错
            // function (response) {
            //     this.items = response.data;
            // }
        }
    });
</script>
```

<font color=red>解决闪烁问题：
</font>

​	由于网速的原因，可以一些数据没有加载，它会显示一个模板出来，例如它会显示`{{item.name}}`，这样影响不好，所以需要解决这个闪烁问题。使用`v-clock`解决，在开始解析的地方，使用这个指令。然后在style中将这个属性的display设置为none。

```html
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <script src="https://cdn.jsdelivr.net/npm/vue@2.5.21/dist/vue.min.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <style>
        /*
            将这个属性的display设置为none
         */
        [v-clock]{
            display: none;
        }
    </style>
</head>
<body>
    <!-- 设置v-clock-->
    <div id="app" v-clock>
        <table border="1" style="border-spacing: 0"> <!-- 展示数据-->
            <tr v-for="item in items">
                <td>{{item.name}}</td>
                <td>{{item.age}}</td>
                <td>{{item.school}}</td>
            </tr>
        </table>
    </div>
</body>
```









