[TOC]

# elementUI 框架学习笔记



## 1、创建一个vue-cli脚手架

- 初始化：`vue init webpack`
- 安装包：`npm install`
- 安装elementui：`npm i element-ui -S`
- 安装axios：`npm install axios -S`
- 安装router：`npm install vue-router -S`，并配置路由。
- 引入组件，配置路由。
- 启动项目：`npm run dev`



## 2、Container 布局容器

用于布局的容器组件，方便快速搭建页面的基本结构：

- `<el-container>`：外层容器。当子元素中包含 `<el-header>` 或 `<el-footer>` 时，全部子元素会垂直上下排列，否则会水平左右排列。
  - 属性：
    - `direction`：指定子元素的排列方式，string类型，属性值：`horizontal / vertical`
- `<el-header>`：顶栏容器。
  - 属性：
    - `height`：顶部栏的高度，默认是60px，string类型。
- `<el-aside>`：侧边栏容器。
  - 属性：
    - `width`：侧边栏的宽度，默认是300px，string类型。
- `<el-main>`：主要区域容器。
- `<el-footer>`：底栏容器。
  - 属性：
    - `height`：页脚的高度，默认值是60px，string类型。

```vue
<template>
    <el-container direction="vertical">
        <el-header height="100px">
            <h1>头部内容</h1>
        </el-header>
        <el-container>
            <el-aside width="800px">
                <h1>侧边内容</h1>
            </el-aside>
            <el-main>
                <h1>主要内容</h1>
            </el-main>
        </el-container>
        <el-footer height="200px">
            <h1>页脚</h1>
        </el-footer>
    </el-container>
</template>

<style>
.el-container{
    background: #B3C0D1;
    text-align: center;
    border: 1px solid red;
}
.el-header{
    background: #B3C0D1;
    text-align: center;
    border: 1px solid blue;
}
.el-aside{
    background: #B3C0D1;
    text-align: center;
    border: 1px solid green;
}
.el-main{
    background: #B3C0D1;
    text-align: center;
    border: 1px solid yellow;
}
.el-footer{
    background: #B3C0D1;
    text-align: center;
    border: 1px solid #bfa;
}
</style>

<script>
export default {
    name: "Demo01",
};
</script>

```

![image-20211123152903401](https://i.loli.net/2021/11/23/goUaxiDIn2BmpJw.png)



## 3、Layout 布局

​	通过基础的 24 分栏，迅速简便地创建布局。通过 row 和 col 组件，并通过 col 组件的 `span` 属性我们就可以自由地组合布局。如果一行中的列数之和大于24，则自动换行。

```vue
<el-row :gutter="0">
    <el-col :span="12">12</el-col>
    <el-col :span="12">12</el-col>
</el-row>
```

==一行分为24分，其中有2列，每一列占12分。==

<font color=red>el-row属性：
</font>

| 属性名  | 解释                                                         | 值类型                      | 默认值 |
| ------- | ------------------------------------------------------------ | --------------------------- | ------ |
| :gutter | 每一列之间的间隔距离                                         | number                      | 0      |
| type    | 布局模式，可选flex，这样就可以通过`justify` 属性来指定 start, center, end, space-between, space-around 其中的值来定义子元素的排版方式。 | string                      |        |
| align   | flex 布局下的垂直排列方式                                    | string（top/middle/bottom） |        |

<font color=red>el-col属性：</font>

| 属性名  | 解释               | 值类型 | 默认值 |
| ------- | ------------------ | ------ | ------ |
| :span   | 栅格占据的列数     | number | 24     |
| :offset | 栅格左侧的间隔格数 | number | 0      |
| :push   | 栅格向右移动格数   | number | 0      |
| :pull   | 栅格向左移动格数   | number | 0      |



## 4、按钮 button

​	使用 `el-button`标签来定义一个按钮，使用`type`、`plain`、`round`和`circle`属性来定义 Button 的样式。存在多种按钮，例如基础按钮、文字按钮、图标按钮、按钮组、加载按钮。

<font color=red>基础按钮：
</font>

​	直接使用 `el-button` 标签，然后使用type使用执行按钮的类型。

```vue
<el-button type="danger">结束考试</el-button>
```

<font color=red>文字按钮：</font>

​	直接将 `type` 属性指定为 `text` 即可。

```vue
<el-button type="text">结束考试</el-button>
```

<font color=red>图标按钮：
</font>

​	设置`icon`属性即可，icon 的列表可以参考 Element 的 icon 组件，也可以设置在文字右边的 icon ，只要使用`i`标签即可，可以使用自定义图标。

```vue
<el-button type="primary" icon="el-icon-delete"></el-button> <!-- 普通的图标按钮-->
<el-button type="primary" icon="el-icon-search">搜索</el-button>  <!-- 文字在右边的按钮-->
<el-button type="primary">上传<i class="el-icon-upload el-icon--right"></i></el-button> <!-- 文字在左边的按钮-->
```

<font color=red>按钮组：
</font>

​	使用`<el-button-group>`标签来嵌套你的按钮。

```vue
<el-button-group>
    <el-button type="primary" icon="el-icon-arrow-left">上一页</el-button>
    <el-button type="primary">下一页<i class="el-icon-arrow-right el-icon--right"></i></el-button>
</el-button-group>
```

<font color=red>加载按钮：
</font>

​	要设置为 loading 状态，只要设置`loading`属性为`true`即可。

```vue
<el-button type="primary" :loading="true">加载中</el-button>
```

<font color=red>按钮的属性：
</font>

| 属性名      | 解释           | 属性值类型                                                   | 默认值 |
| ----------- | -------------- | ------------------------------------------------------------ | ------ |
| size        | 尺寸           | string（medium / small / mini）                              |        |
| type        | 指定按钮类型   | string（primary / success / warning / danger / info / text） |        |
| plain       | 是否朴素按钮   | boolean                                                      | false  |
| round       | 是否圆角按钮   | boolean                                                      | false  |
| circle      | 是否圆形按钮   | boolean                                                      | false  |
| loading     | 是否加载中状态 | boolean                                                      | false  |
| disabled    | 是否禁用状态   | boolean                                                      | false  |
| icon        | 图标类名       | string                                                       |        |
| autofocus   | 是否默认聚焦   | boolean                                                      | false  |
| native-type | 原生 type 属性 | string（button / submit / reset）                            | button |



## 5、链接

​	通过 `el-link` 这个标签就行链接和跳转。

```vue
<el-link href="https://www.baidu.com" target="_blank">默认链接</el-link>
```

<font color=red>链接属性：
</font>

| 属性名     | 解释           | 属性值类型                                            | 默认值  |
| ---------- | -------------- | ----------------------------------------------------- | ------- |
| type       | 执行链接的类型 | string（primary / success / warning / danger / info） | default |
| :underline | 是否下划线     | boolean                                               | true    |
| disabled   | 是否禁用状态   | boolean                                               | false   |
| href       | 跳转的地址     | string                                                |         |
| icon       | 图标类名       | string                                                |         |



## 6、表单系列

### 6.1、input 输入框

​	使用 `el-input`标签来定义一个输入框，用于接收用户的输入的数据。一般输入框会绑定一个 `v-model`来双向绑定数据。

#### 6.1.1、基础用法

```vue
<template>
    <div>
        <el-row :gutter="10">
            <el-col :span="5">
                <el-input v-model="username" type="text" placeholder="请输入用户名" clearable></el-input>
                用户名:{{username}}
            </el-col>
        </el-row>
        <el-row :gutter="10">
            <el-col :span="5">
                <el-input v-model="password" type="password" placeholder="请输入密码" clearable></el-input>
                密码:{{password}}
            </el-col>
        </el-row>
    </div>
</template>
<script>
export default {
    name: "Demo02",
    data(){
        return{
            username: "",
            password: ""
        }
    }
}
</script>
```

#### 6.1.2、带icon的输入框

<font color=red>prefix-icon：
</font>在input内的前面添加一个icon图标。

```vue
<el-input
    placeholder="请输入内容"
    prefix-icon="el-icon-search"
    v-model="msg">
</el-input>
```

<font color=red>suffix-icon：</font>在input内的最后面添加一个icon图标。

```vue
<el-input
    placeholder="请选择日期"
    suffix-icon="el-icon-date"
    v-model="date">
</el-input>
```

<font color=red>slot：</font>

```vue
<el-input
    placeholder="请选择日期"
    v-model="input3">
    <!-- 在输入框的后面添加一个图标-->
    <i slot="suffix" class="el-input__icon el-icon-date"></i>
</el-input>
<el-input
    placeholder="请输入内容"
    v-model="input4">
    <!-- 在输入框的前面添加一个图标-->
    <i slot="prefix" class="el-input__icon el-icon-search"></i>
</el-input>
```

#### 6.1.3、文本域输入框

```vue
<el-input
    type="textarea"
    :rows="2"
    placeholder="请输入内容"
    v-model="textarea">
</el-input>
```

<font color=red>属性：</font>

| 属性名          | 解释                                                         | 属性值类型 | 默认值 |
| --------------- | ------------------------------------------------------------ | ---------- | ------ |
| :rows           | 指定文本域的行数，超过这个行数就形成滚动条                   | number     |        |
| :autosize       | 指定文本域可以自动调整大小，当超过指定的最大值也会出现滚动条 | object     |        |
| maxlength       | 限制最大的输入长度                                           | string     |        |
| show-word-limit | 在文本域的右下角显示字数的统计，配合maxlength使用            |            |        |

#### 6.1.4、复合型输入框

​	可前置或后置元素，一般为标签或按钮。

```vue
<!-- 前置内容-->
<el-input type="text"
          v-model="url"
          placeholder="请输入网址">
    <template slot="prepend">Http://</template>
</el-input>
<!-- 后置内容-->
<el-input type="text"
          v-model="url"
          placeholder="请输入网址">
    <template slot="append">Http://</template>
</el-input>
```

一般还可以使用按钮进行组合复合输入框。

```vue
<el-input placeholder="请输入内容" v-model="input3">
    <el-select v-model="select" slot="prepend" placeholder="请选择">
        <el-option label="餐厅名" value="1"></el-option>
        <el-option label="订单号" value="2"></el-option>
        <el-option label="用户电话" value="3"></el-option>
    </el-select>
    <el-button slot="append" icon="el-icon-search"></el-button>
</el-input>
```

### 6.2、单选按钮

​	使用el-radio标签用来表示一个单选按钮。

```vue
<template>
    <div>
        <el-radio v-model="radio" :label="1">备选项</el-radio>
        <el-radio v-model="radio" :label="2">备选项</el-radio>
    </div>
</template>

<script>
export default {
    name: "Demo01",
    data(){
        return{
            radio: 1
        }
    }
}
</script>
```

==`label`属性就是绑定单选按钮的值，它的值类型可以是number、string、boolean。除了使用string类型，其他两个都要使用 `:label` 进行绑定。==

##### 6.2.1、单选按钮组

​	使用`el-radio-group`标签，将单选按钮进行分组。

```vue
<el-radio-group v-model="radio1">
  <el-radio :label="1">选项1</el-radio>
  <el-radio :label="2">选项2</el-radio>
  <el-radio :label="3">选项3</el-radio>
  <el-radio :label="4">选项4</el-radio>
</el-radio-group>
```

==在一组的选项中，只要选择一个选项，其他选项就会进行排斥。==

单选按钮组还给我们提供了一个change事件，当选择发生改变时就会触发对应的函数，它有这个参数是单选选中的值：

```vue
<el-radio-group v-model="radio1" @change="action">
  <el-radio :label="1">选项1</el-radio>
  <el-radio :label="2">选项2</el-radio>
  <el-radio :label="3">选项3</el-radio>
  <el-radio :label="4">选项4</el-radio>
</el-radio-group>
<script>
export default {
    name: "Demo01",
    data(){
        return{
            radio1: 1,
            radio2: 1,
            radio3: 1

        }
    },
    methods: {
        // 会将选中的值作为参数传入
        action(value){
            alert(value);
        }
    }
}
</script>
```

##### 6.2.2、按钮单选

​	只需要把`el-radio`元素换成`el-radio-button`元素即可，此外，Element 还提供了`size`属性。

```vue
<el-radio-group v-model="radio3">
  <el-radio-button :label="1">按钮1</el-radio-button>
  <el-radio-button :label="2">按钮2</el-radio-button>
  <el-radio-button :label="3">按钮3</el-radio-button>
  <el-radio-button :label="4">按钮4</el-radio-button>
</el-radio-group>
```

##### 6.2.3、属性

<font color=red>el-radio属性：
</font>

| 属性名  | 解释           | 属性值类型              | 默认值 |
| ------- | -------------- | ----------------------- | ------ |
| label   | 单选项对应的值 | string、number、boolean |        |
| disable | 是否被禁用     | boolean                 | false  |
| border  | 是否显示边框   | boolean                 | false  |

<font color=red>单选change 事件：</font>当绑定的值发生改变时，触发该事件，事件会有一个选中的选项的值作为参数传递给回调函数。

### 6.3、多选按钮

​	使用 `el-checkbox` 标签，实现可以选择多个选项。

```vue
<el-checkbox v-model="hobby" :label="1">篮球</el-checkbox>
<el-checkbox v-model="hobby" :label="2">羽毛球</el-checkbox>
<el-checkbox v-model="hobby" :label="3">游戏</el-checkbox>
<el-checkbox v-model="hobby" :label="4">女朋友</el-checkbox>
```

#### 6.3.1、多选按钮组

​	使用 `el-checkbox-group`标签将不同的多选按钮进行分组。

```vue
<el-checkbox-group v-model="hobby"
                   min="1"
                   max="2">
    <el-checkbox :label="1">篮球</el-checkbox>
    <el-checkbox :label="2">羽毛球</el-checkbox>
    <el-checkbox :label="3">游戏</el-checkbox>
    <el-checkbox :label="4">女朋友</el-checkbox>
</el-checkbox-group>
```

==可以指定`min`和`max`属性，设置为最少选几个和最多选几个。==

#### 6.3.2、indeterminate 状态

`	indeterminate` 属性用以表示 checkbox 的不确定状态，一般用于实现全选的效果。实现一个全选和半全选功能。

```vue
<template>
    <div>
        <el-checkbox :indeterminate="isIndeterminate" v-model="checkAll" @change="checkAllAction">全选</el-checkbox>
        <br />
        <el-checkbox-group v-model="items" @change="checkItemAction">
          <el-checkbox v-for="item in city" :label="item">{{item}}</el-checkbox>
        </el-checkbox-group>
    </div>
</template>

<script>
const citys = ["重庆","上海","北京","大连","成都"];
export default {
    name: "Demo02",
    data(){
        return{
            checkAll: false,
            city: citys,
            items: [],
            isIndeterminate: false
        };
    },
    methods: {
        // 检车全选按钮是否发生改变
        checkAllAction(value){
            this.items = value ? this.city : [];
            // 如果改变就将不确定状态改为false
            this.isIndeterminate = false;
        },
        checkItemAction(value){
            let length = value.length;
            // 判断是否全部被选中
            this.checkAll = length === this.city.length;
            // 如果部分选中就激活不确定状态，反之
            this.isIndeterminate = length > 0 && length < this.city.length;
        }
    }
}
</script>
```

#### 6.3.3、按钮多选

​	使用 `el-checkbox-button` 将多选的设置为按钮选择。

```vue
<el-checkbox-group v-model="hobby">
    <el-checkbox-button label="篮球">篮球</el-checkbox-button>
    <el-checkbox-button label="羽毛球">羽毛球</el-checkbox-button>
    <el-checkbox-button label="游戏">游戏</el-checkbox-button>
    <el-checkbox-button label="女朋友">女朋友</el-checkbox-button>
</el-checkbox-group>
```

#### 6.3.4、属性

<font color=red>checkbox属性：</font>

| 属性名        | 解释                                    | 属性值类型     | 默认值 |
| ------------- | --------------------------------------- | -------------- | ------ |
| true-label    | 选中时的值                              | string、number |        |
| false-label   | 没有选中时的值                          | string、number |        |
| disabled      | 是否被禁用                              | boolean        | false  |
| border        | 是否带有边框                            | boolean        | false  |
| checked       | 当前是否勾选                            | boolean        | false  |
| indeterminate | 设置 indeterminate 状态，只负责样式控制 | boolean        | false  |

### 6.4、input-number计数器

 只需要在`el-input-number`元素中使用`v-model`绑定变量即可，变量的初始值即为默认值。

```vue
<el-input-number v-model="number" @change="numberCheckAction" :min="1" :max="10" label="label"></el-input-number>
```

<font color=red>属性：</font>

| 属性名            | 解释                     | 属性值类型      | 默认值 |
| ----------------- | ------------------------ | --------------- | ------ |
| step              | 计数器步长               | number          | 1      |
| step-strictly     | 是否只能输入 step 的倍数 | boolean         | false  |
| precision         | 数值精度                 | number          |        |
| controls-position | 控制按钮位置             | string（right） |        |

<font color=red>事件：
</font>

| 事件名 | 解释                        |
| ------ | --------------------------- |
| change | 绑定值被改变时触发          |
| blur   | 在组件 Input 失去焦点时触发 |
| focus  | 在组件 Input 获得焦点时触发 |

### 6.5、switch开关

```vue
<el-switch v-model="active"
           active-color="#13ce66"
           inactive-color="#ff4949"
></el-switch>
```

<font color=red>属性：
</font>

| 属性名              | 解释                                         | 属性值类型                | 默认值   |
| ------------------- | -------------------------------------------- | ------------------------- | -------- |
| active-color        | 开时的背景颜色                               | string                    | \#409EFF |
| inactive-color      | 关时的背景颜色                               | string                    | \#C0CCDA |
| active-text         | 开时显示的文字                               | string                    |          |
| inactive-text       | 关时显示的文字                               | string                    |          |
| active-value        | 开时对应的值                                 | boolean / string / number | true     |
| inactive-value      | 关闭时的值                                   | boolean / string / number | false    |
| width               | switch的宽度                                 | number                    | 40       |
| active-icon-class   | 开时显示的图标，设置此项会忽略 `active-text` | string                    |          |
| inactive-icon-class | 关时显示的图标，设置此项会忽略 `active-text` | string                    |          |

### 6.6、Select 选择器

​	使用 `el-select`标签来提供用户来选择，子元素为 `el-option`标签。

```vue
<el-select v-model="selectValue" placeholder="请选择">
    <el-option
        v-for="item in select"
        :label="item"
        :value="item"
    ></el-option>
</el-select>
```

<font color=red>select的属性：
</font>

| 属性名         | 解释                                                     | 属性值类型                  | 默认值     |
| -------------- | -------------------------------------------------------- | --------------------------- | ---------- |
| v-model        | 绑定当前选择的属性值                                     | boolean 、 string 、 number |            |
| multiple       | 是否多选，此时绑定的v-model就是选中组成的数组            | boolean                     | false      |
| collapse-tags  | 如果设置了多选，选择多个会将多个数据组成一个字符串显示   | boolean                     | false      |
| multiple-limit | 如果设置多选，指定用户最多选择的数量，如果为0就是不限制  | number                      | 0          |
| filterable     | 是否可以搜索                                             | boolean                     | false      |
| allow-create   | 是否允许用户创建新条目，需配合 `filterable` 使用         | boolean                     | false      |
| filter-method  | 自定义搜索方法                                           | function                    |            |
| remote         | 是否支持远程搜索                                         | boolean                     | false      |
| remote-method  | 远程搜索方法                                             | function                    |            |
| loading        | 是否正在从远程获取数据                                   | boolean                     | false      |
| no-match-text  | 搜索条件无匹配时显示的文字，也可以使用`slot="empty"`设置 | string                      | 无匹配数据 |
| loading-text   | 远程加载时显示的文字                                     | string                      | 加载中     |
| no-data-text   | 选项为空时显示的文字，也可以使用`slot="empty"`设置       | string                      | 无数据     |

<font color=red>select的事件：
</font>

| 事件名         | 解释                                     | 回调参数                                   |
| -------------- | ---------------------------------------- | ------------------------------------------ |
| change         | 当选中的值发生改变时                     | 目前选中的值，如果存在多个参数就是一个数组 |
| visible-change | 下拉框出现/隐藏时触发                    | 显示参数就是true，隐藏参数就是false        |
| remove-tag     | 多选模式下移除tag时触发                  | 移除的tag值                                |
| clear          | 可清空的单选模式下用户点击清空按钮时触发 |                                            |
| blur           | 当 input 失去焦点时触发                  | (event: Event)                             |
| focus          | 当 input 获得焦点时触发                  | (event: Event)                             |

<font color=red>el-option的属性：
</font>

| 属性名   | 解释                                                      | 属性值类型           | 默认值 |
| -------- | --------------------------------------------------------- | -------------------- | ------ |
| value    | 选项的值                                                  | string/number/object |        |
| label    | 选项的标签，若不设置则默认与 `value` 相同，就是显示的选项 | string/number        |        |
| disabled | 是否禁用该选项                                            | boolean              | false  |

<font color=red>自定义选项的模板：</font>

​	将自定义的 HTML 模板插入`el-option`的 slot 中即可，显示自己想要的选项模板

```vue
<el-option
    v-for="(item,index) in select"
    :label="item"
    :value="item">
    <span style="float: left">{{ item}}</span>
    <span style="float: right; color: #8492a6; font-size: 13px">{{ index }}</span>
</el-option>
```

### 6.7、Cascader 连级选择器

​	只需为 Cascader 的`options`属性指定选项数组即可渲染出一个级联选择器。通过`props.expandTrigger`可以定义展开子级菜单的触发方式。

```vue
<template>
    <div>
        <el-cascader
            v-model="result"
            :options="items">
        </el-cascader>
    </div>
</template>
<script>
export default {
    name: "Demo04",
    data(){
        return{
            items: [
                {
                    value: 1,
                    label: "运动",
                    children: [
                        {
                            value: "篮球",
                            label: "篮球"
                        },
                        {
                            value: "羽毛球",
                            label: "羽毛球"
                        },
                        {
                            value: "乒乓球",
                            label: "乒乓球"
                        }
                    ]
                },
                {
                    value: 2,
                    label: "食物",
                    children: [
                        {
                            value: "鸡肉",
                            label: "鸡肉"
                        },
                        {
                            value: "鱼肉",
                            label: "鱼肉"
                        },
                        {
                            value: "鸭肉",
                            label: "鸭肉"
                        }
                    ]
                },
                {
                    value: 3,
                    label: "游戏",
                    children: [
                        {
                            value: "LoL",
                            label: "LoL"
                        },
                        {
                            value: "穿越火线",
                            label: "穿越火线"
                        },
                        {
                            value: "QQ飞车",
                            label: "QQ飞车"
                        }
                    ]
                }
            ],
            result: []
        }
    }
}
</script>
```

== `options` 属性的属性值必须是一个对象数组，一个对象表示一个选项，如果存在子级选项，就在选项对象中添加一个 `children`属性，这个属性也是一个数组，数组中包含子级的对象，每一个对象中都要包含 `value `和 `label` 属性，`value`属性就是选中时的值，`label`就是显示的文本。==

==这个选择器的触发方式有两种，默认是点击时触发，可以设置`props.expandTrigger`属性，设置为鼠标移入就显示，`:props="{ expandTrigger: 'hover' }"`。==

<font color=red>自定义节点内容：
</font>

​		可以通过`scoped slot`对级联选择器的备选项的节点内容进行自定义，scoped slot会传入两个字段 `node` 和 `data`，分别表示当前节点的 Node 对象和数据。

```vue
<el-cascader
    v-model="result"
    :props="props"
    :show-all-levels="false"
    collapse-tags
    :options="items">
    <template slot-scope="{node,data}">
        <span>{{data.label}}</span>
		<!-- 判断是否为叶子节点-->
        <span v-if="!node.isLeaf">{{data.children.length}}</span>
    </template>
</el-cascader>
```

<font color=red>级联面板：
</font>

```vue
<el-cascader-panel :options="items"></el-cascader-panel>
```

用法与级联选择器一样。

<font color=red>el-cascader的属性：
</font>

| 属性名          | 解释                                                         | 属性值类型             | 默认值 |
| --------------- | ------------------------------------------------------------ | ---------------------- | ------ |
| options         | 指定选项                                                     | object                 |        |
| show-all-levels | 定义了是否显示完整的路径                                     | boolean                | true   |
| props           | 用于一些级联配置                                             | object                 |        |
| collapse-tags   | 多选模式下是否折叠Tag                                        | boolean                | false  |
| separator       | 选项分隔符                                                   | string                 | " / "  |
| filterable      | 是否可以搜索                                                 | boolean                | false  |
| filter-method   | 自定义搜索的方法，第一个参数是节点`node`，第二个参数是搜索关键词`keyword`，通过返回布尔值表示是否命中 | function(node,keyword) |        |

<font color=red>el-cascader的方法：
</font>

`getCheckedNodes()`，获取选中的节点，参数是一个布尔值，表示是否只是叶子节点，默认值为 `false`。

<font color=red>props的配置：</font>

| 属性名        | 解释                                                         | 属性值类型              | 默认值 |
| ------------- | ------------------------------------------------------------ | ----------------------- | ------ |
| expandTrigger | 次级菜单的展开方式                                           | string（click 、hover） | click  |
| multiple      | 是否多选                                                     | boolean                 | false  |
| checkStrictly | 是否严格的遵守父子节点不互相关联，父节点也可以作为选项       | boolean                 | false  |
| emitPath      | 在选中节点改变时，是否返回由该节点所在的各级菜单的值所组成的数组，若设置 false，则只返回该节点的值 | boolean                 | false  |

### 6.8、TimePicker 时间选择器

<font color=red>固定时间：</font>

​	使用 el-time-select 标签，分别通过`start`、`end`和`step`指定可选的起始时间、结束时间和步长

```vue
<template>
    <div>
        <el-time-select v-model="value"
                        :picker-options="picker"
                        placeholder="time">
        </el-time-select>
    </div>
</template>

<script>
export default {
    name: "Demo05",
    data(){
        return{
            value: "",
            picker: {
                start: '00:00',  // 开始的时间
                step: '01:00',   // 步长
                end: '23:59'     // 结束的时间，如果结束的时间比开始的时间大，就不会显示
            }
        }
    }
}
</script>
```

<font color=red>任意时间点：</font>

```vue
<template>
    <div>
        <el-time-picker v-model="value"
                        :picker-options="picker"
                        placeholder="time">
        </el-time-picker>
    </div>
</template>

<script>
export default {
    name: "Demo05",
    data(){
        return{
            value: "",
            picker: {
                selectableRange: '00:00:00 - 23:59:59', // 必须精确到秒
            }
        }
    }
}
</script>
```

==选择方式有两种，默认是滑动鼠标进行选择。另一种是使用箭头进行选择，只需在标签中添加 `arrow-control `属性。==

<font color=red>任意时间范围：
</font>

​	添加`is-range`属性即可选择时间范围，同样支持`arrow-control`属性。

```vue
<el-time-picker v-model="value"
                is-range
                start-placeholder="开始时间"
                end-placeholder="结束时间"
                range-separator="至"
                placeholder="time">
</el-time-picker>
```

<font color=red>属性：
</font>

| 属性名   | 解释         | 属性值类型 | 默认值 |
| -------- | ------------ | ---------- | ------ |
| readonly | 完全只读     | boolean    | false  |
| editable | 文本框可输入 | boolean    | true   |

### 6.9、DatePicker 日期选择器

​	使用 `el-date-picker`标签，进行日期的选择。

```vue
<el-date-picker v-model="date"
                type="date"
                placeholder="日期">

</el-date-picker>
```

<font color=red>选择日期范围：
</font>

```vue
<el-date-picker v-model="date"
                type="daterange"
                range-separator=" - "
                start-placeholder="开始日期"
                end-placeholder="结束日期"
                @change="dateAction"
                placeholder="日期">

</el-date-picker>
```

<font color=red>日期格式：
</font>

​	使用`format`指定输入框的格式；使用`value-format`指定绑定值的格式。

```vue
<el-date-picker v-model="date"
                type="daterange"
                range-separator=" - "
                start-placeholder="开始日期"
                end-placeholder="结束日期"
                format="dd-MM-yyyy"
                value-format="yyyy-MM-dd"
                @change="dateAction"
                placeholder="日期">
</el-date-picker>
```

### 6.10、DateTimePicker 日期时间选择器

​	通过设置`type`属性为`datetime`，即可在同一个选择器里同时进行日期和时间的选择。快捷选项的使用方法与 Date Picker 相同。

```vue
<el-date-picker v-model="date"
                type="datetime"
                value-format="yyyy-MM-dd hh:mm:ss"
                @change="dateAction"
                placeholder="time">
    
</el-date-picker>
```

<font color=red>日期和时间范围：
</font>

​	设置`type`为`datetimerange`即可选择日期和时间范围。

```vue
<el-date-picker v-model="date"
                type="datetimerange"
                value-format="yyyy-MM-dd hh:mm:ss"
                @change="dateAction"
                start-placeholder="开始时间"
                end-placeholder="结束时间"
                placeholder="时间">

</el-date-picker>
```

<font color=red>属性：
</font>

| 属性名       | 解释                                           | 属性值 类型                                                  | 默认值              |
| ------------ | ---------------------------------------------- | ------------------------------------------------------------ | ------------------- |
| type         | 显示的类型                                     | string（year/month/date/week/ datetime/datetimerange/daterange） | date                |
| format       | 显示在输入框中的格式                           | string                                                       | yyyy-MM-dd HH:mm:ss |
| value-format | 可选，绑定值的格式。不指定则绑定值为 Date 对象 | string                                                       |                     |
| prefix-icon  | 自定义头部图标的类名                           | string                                                       |                     |
| clear-icon   | 自定义清空图标的类名                           | string                                                       |                     |

### 6.11、upload上传

​	使用 `el-upload`标签来进行用户的文件的上传。

```vue
<el-upload
    class="upload-demo"
    action="https://www.demo.com/posts/"
    :limit="3"
    :file-list="[{name: 'name', url: 'url'}, {name: 'name2', url: 'url'}]" multiple="">
    <el-button size="small" type="primary">点击上传</el-button>
    <div slot="tip" class="el-upload__tip">只能上传jpg/png文件，且不超过500kb</div>
</el-upload>
```

<font color=red>属性：
</font>

| 属性名         | 解释                                                         | 属性值类型                                                   | 默认值 |
| -------------- | ------------------------------------------------------------ | ------------------------------------------------------------ | ------ |
| class          | 执行上传的样式                                               | string（upload-demo普通上传、avatar-uploader头像上传）       |        |
| action         | 上传的url地址                                                | string                                                       |        |
| :limit         | 限制上传的文件个数                                           | number                                                       |        |
| :on-exceed     | 当超出限制时的行为                                           | function                                                     |        |
| multiple       | 是否支持多文件上传                                           | boolean                                                      |        |
| data           | 上传额外带的参数                                             | object                                                       |        |
| name           | 上传的文件字段名                                             | string                                                       | file   |
| drag           | 是否启动拖拽上传                                             | boolean                                                      | false  |
| show-file-list | 是否显示上传的文件列表                                       | boolean                                                      | true   |
| on-preview     | 点击文件列表中已上传的文件时的钩子                           | function(file)，file是对应的文件对象                         |        |
| on-remove      | 文件列表移除文件时的钩子                                     | function(file, fileList)，file是移除的文件对象，fileList移除后剩余的文件列表 |        |
| on-success     | 文件上传成功时的钩子                                         | function(response, file, fileList)                           |        |
| on-error       | 文件上传失败时的钩子                                         | function(err, file, fileList)                                |        |
| before-remove  | 删除文件之前的钩子，参数为上传的文件和文件列表，若返回 false 或者返回 Promise 且被 reject，则停止删除。 | function(file, fileList)                                     |        |
| auto-upload    | 是否在选取文件后立即进行上传                                 | boolean                                                      | true   |

### 6.12、评分

​	使用 `el-rate`标签进行选择评分。

```vue
<el-rate v-model="rate"
         @change="changeAction">
</el-rate>
```

​		评分默认被分为三个等级，可以利用颜色数组对分数及情感倾向进行分级（默认情况下不区分颜色）。三个等级所对应的颜色用`colors`属性设置，而它们对应的两个阈值则通过 `low-threshold` 和 `high-threshold` 设定。你也可以通过传入颜色对象来自定义分段，键名为分段的界限值，键值为对应的颜色。

<font color=red>属性：
</font>

| 属性名          | 解释                                            | 属性值类型 | 默认值                       |
| --------------- | ----------------------------------------------- | ---------- | ---------------------------- |
| v-model         | 绑定的值（1-5）                                 | number     | 0                            |
| colors          | 三个等级对应的颜色显示                          | object数组 |                              |
| show-text       | 是否显示辅助文字                                | boolean    | false                        |
| texts           | 设置显示的辅助文字，可以设置5个值，对应绑定的值 | object数组 | 极差、失望、一般、满意、惊喜 |
| icon-classes    | 自定义不同分段的图标，可以定义三个              | object数组 |                              |
| void-icon-class | 指定了未选中时的图标类名                        | string     |                              |
| disabled        | 设置组件只读                                    |            |                              |
| show-score      | 在只读的基础上显示分数                          | boolean    | false                        |
| score-template  | 显示分数的模板                                  | string     |                              |
| max             | 设置最大的分值                                  | number     |                              |
| allow-half      | 是否允许半选                                    | boolean    | false                        |

### 6.13、Form 表单

​	在 Form 组件中，每一个表单域由一个 Form-Item 组件构成，表单域中可以放置各种类型的表单控件，包括 Input、Select、Checkbox、Radio、Switch、DatePicker、TimePicker。

```vue
<template>
    <el-form ref="form" :model="form"  label-width="80px">
        <el-form-item label="活动名称">
            <el-input v-model="form.name"></el-input>
        </el-form-item>
        <el-form-item label="活动区域">
            <el-select v-model="form.region" placeholder="请选择活动区域">
                <el-option label="区域一" value="shanghai"></el-option>
                <el-option label="区域二" value="beijing"></el-option>
            </el-select>
        </el-form-item>
        <el-form-item label="活动时间">
            <el-col :span="11">
                <el-date-picker type="date" placeholder="选择日期" v-model="form.date1" style="width: 100%;"></el-date-picker>
            </el-col>
            <el-col class="line" :span="2">-</el-col>
            <el-col :span="11">
                <el-time-picker placeholder="选择时间" v-model="form.date2" style="width: 100%;"></el-time-picker>
            </el-col>
        </el-form-item>
        <el-form-item label="即时配送">
            <el-switch v-model="form.delivery"></el-switch>
        </el-form-item>
        <el-form-item label="活动性质">
            <el-checkbox-group v-model="form.type">
                <el-checkbox label="美食/餐厅线上活动" name="type"></el-checkbox>
                <el-checkbox label="地推活动" name="type"></el-checkbox>
                <el-checkbox label="线下主题活动" name="type"></el-checkbox>
                <el-checkbox label="单纯品牌曝光" name="type"></el-checkbox>
            </el-checkbox-group>
        </el-form-item>
        <el-form-item label="特殊资源">
            <el-radio-group v-model="form.resource">
                <el-radio label="线上品牌商赞助"></el-radio>
                <el-radio label="线下场地免费"></el-radio>
            </el-radio-group>
        </el-form-item>
        <el-form-item label="活动形式">
            <el-input type="textarea" v-model="form.desc"></el-input>
        </el-form-item>
        <el-form-item>
            <el-button type="primary" @click="onSubmit">立即创建</el-button>
            <el-button>取消</el-button>
        </el-form-item>
    </el-form>
</template>

<script>
export default {
    name: "Demo02",
    data(){
        return{
            form: {
                name: '',
                region: '',
                date1: '',
                date2: '',
                delivery: false,
                type: [],
                resource: '',
                desc: ''
            }
        }
    },
    methods: {
        onSubmit(){
            alert("提交成功!");
        }
    }
}
</script>
```

<font color=red>Form表单的属性：
</font>

| 属性名                  | 解释                                      | 属性值类型                      | 默认值 |
| ----------------------- | ----------------------------------------- | ------------------------------- | ------ |
| inline                  | 表单变成行类表单                          | boolean                         | false  |
| label-position          | 设置表单域标签的位置                      | string（right/left/top）        | right  |
| label-width             | 设置表单域标签的宽度，需要带单位          | string                          |        |
| hide-required-asterisk  | 是否隐藏必填字段的标签旁边的红色星号      | boolean                         | false  |
| show-message            | 是否显示校验错误信息                      | boolean                         | true   |
| inline-message          | 是否以行内形式展示校验信息                | boolean                         | false  |
| status-icon             | 是否在输入框中显示校验结果反馈图标        | boolean                         | false  |
| validate-on-rule-change | 是否在 `rules` 属性改变后立即触发一次验证 | boolean                         | true   |
| size                    | 组件的尺寸                                | string（medium / small / mini） |        |

<font color=red>表单校验：
</font>

- 创建一个校验规则对象

```js
data(){
    return{
        formValue:{
            username: "",
            password: "",
        },
        // 表单校验规则对象
        formRule:{
            // 每一个校验的对象，与表单中每项绑定的值一一对应
            // 可以有多个校验规则，所以是个数组，数组中存放校验对象
            // required必填，message:校验失败后的提示文字；trigger：触发方式，blur表示失去焦点时触发
            username: [{required: true, message: '输入的登录账号不能为空', trigger: 'blur'}],
            password: [{required: true, message: '输入的登录账号不能为空', trigger: 'blur'}]
        }
    }
},
```

- 表单中注入规则

在表单中通过rules属性，将创建的表单校验规则对象注入进去。

```vue
<el-form ref="form"
         :model="formValue"
         label-position="left"
         :rules="formRule"
         label-width="55px">
```

- 在每一表单项上，通过prop属性指定校验规则，指定的是校验规则对象中的属性。

```vue
<el-form-item label="账号"
              prop="username">
    <el-input v-model="formValue.username"
              placeholder="请输入账号"></el-input>
</el-form-item>
<el-form-item label="密码"
              prop="password">
    <el-input v-model="formValue.password"
              show-password
              placeholder="请输入密码"></el-input>
</el-form-item>
```

- 全局校验：当点击提交按钮是通过点击事件来进行表单的校验。

```js
submitAction(){
// 获取表单，然后通过validate方法进行校验，它有一个回调函数，回调参数就是校验的结果，如果通过校验就返回true，否则返回false
    this.$refs['form'].validate((result)=>{
        if (result){
            alert("通过校验");
        }else {
            alert("校验失败");
        }
    });
}
```

<font color=red>自定义校验规则：
</font>

- 在data中创建一个函数对象。

```js
data(){
    // 自定义的一个校验函数
    // rule可忽略，value需要校验的值，callback回调函数
    let checkPassword = (rule,value,callback)=>{
        if (value.trim()=== ""){
            // 如果不校验通过可以通过new Error对象，将提示文字通过参数传入
            callback(new Error("输入登录密码不能为空"));
        }else {
            // 如果callback的参数为空就表示校验通过
            callback();
        }
    };
},
```

- 使用自定义的校验规则

```js
formRule:{
    // 每一个校验的对象，与表单中每项绑定的值一一对应
    // 可以有多个校验规则，所以是个数组，数组中存放校验对象
    // required必填，message:校验失败后的提示文字；trigger：触发方式，blur表示失去焦点时触发
    username: [{required: true, message: '输入的登录账号不能为空', trigger: 'blur'}],
    password: [{validator:checkPassword, trigger: 'blur'}]
}
```

==通过使用校验对象的`validator`属性，属性值为自定义的校验函数。==

<font color=red>表单的方法：
</font>

| 方法名        | 解释                                                       | 参数                                          |
| ------------- | ---------------------------------------------------------- | --------------------------------------------- |
| validate      | 对表单进行校验，参数是一个回调函数                         | Function(callback: Function(boolean, object)) |
| resetFields   | 对整个表单进行重置，将所有字段值重置为初始值并移除校验结果 |                                               |
| clearValidate | 移除表单项的校验结果                                       |                                               |

## 7、表格

​		当`el-table`元素中注入`data`对象数组后，在`el-table-column`中用`prop`属性来对应对象中的键名即可填入数据，用`label`属性来定义表格的列名。可以使用`width`属性来定义列宽。

```vue
<template>
    <div>
        <el-table
            :data="items"
            style="width: 100%">
            <el-table-column
                prop="date"
                label="日期"
                width="180">
            </el-table-column>
            <el-table-column
                prop="name"
                label="姓名"
                width="180">
            </el-table-column>
            <el-table-column
                prop="address"
                label="地址">
            </el-table-column>
        </el-table>
    </div>
</template>
<script>
export default {
    name: "Demo04",
    data() {
        return{
            items:[
                {date:'2019-10-01',name:'张三',address:'重庆科技学院'},
                {date:'2019-10-02',name:'李四',address:'重庆科技学院'},
                {date:'2019-10-03',name:'王五',address:'重庆科技学院'},
                {date:'2019-10-04',name:'赵六',address:'重庆科技学院'},
                {date:'2019-10-05',name:'田七',address:'重庆科技学院'}
            ]
        }
    }
}
</script>
```

<font color=red>el-table的属性：
</font>

| 属性名                | 解释                                                     | 属性值类型     | 默认值 |
| --------------------- | -------------------------------------------------------- | -------------- | ------ |
| data                  | 绑定的表格的数据                                         | object数组对象 |        |
| stripe                | 是否带有斑马线效果                                       | boolean        | false  |
| border                | 表格是否带有边框                                         | boolean        | false  |
| height                | 指定表格的高度，当超过这个高度就出现滚动条，但是表头固定 | string         |        |
| max-height            | 指定表格的最大高度，超过这个高度出现滚动条               | string         |        |
| fit                   | 列的宽度是否自撑开                                       | boolean        | false  |
| highlight-current-row | 是否要高亮当前行                                         | boolean        | false  |

<font color=red>el-table-column的属性：
</font>

| 属性名   | 解释                                           | 属性值类型                                         | 默认值 |
| -------- | ---------------------------------------------- | -------------------------------------------------- | ------ |
| fixed    | 固定列，当宽度不够，改列固定，其他列出现滚动条 | boolean或者left（固定在左边）、right（固定在右边） | false  |
| sortable | 实现以该列为基准的排序                         | boolean                                            | false  |

<font color=red>表格序号：
</font>

​	如果需要显示索引，可以增加一列`el-table-column`，设置`type`属性为`index`即可显示从 1 开始的索引号。

```vue
<el-table-column
    type="index"
    label="序号"
    width="50">
</el-table-column>
```

<font color=red>选择框：
</font>

​	设`type`属性为`selection`即可。

```vue
<el-table-column
    type="selection"
    width="50">
</el-table-column>
```

<font color=red>自定义表头：
</font>

​	通过设置 Scoped slo 来自定义表头。

```vue
<el-table-column
    align="right">
    <template slot-scope="scope" slot="header">
        <el-input v-model="searchValue"
                  size="mini"
                  placeholder="请输入关键字!"/>
    </template>
    <template slot-scope="scope">
        <el-button
            size="mini">修改</el-button>
        <el-button
            size="mini"
            type="danger">删除</el-button>
    </template>
</el-table-column>
```

<font color=red>表格的事件：
</font>

| 事件名                                     | 解释                                         | 参数                                                         |
| ------------------------------------------ | -------------------------------------------- | ------------------------------------------------------------ |
| select(selection,row)                      | 当用户手动勾选数据行的 Checkbox 时触发的事件 | 第一个参数是已经被选中数据，第二个参数是对应行对象其中包含数据， |
| select-all(selection)                      | 当用户手动勾选全选 Checkbox 时触发的事件     | 已经被选中的行对象对应的数据                                 |
| selection-change(selection)                | 当选择项发生变化时会触发该事件               | 已经被选中的行对象对应的数据                                 |
| cell-mouse-enter(row, column, cell, event) | 当单元格 hover 进入时会触发该事件            | 行中的数据对象、列对象、单元格对象这个Dom对象、事件对象      |
| cell-mouse-leave(row, column, cell, event) | 当单元格 hover 退出时会触发该事件            | 行中的数据对象、列对象、单元格对象这个Dom对象、事件对象      |
| row-click(row, column, event)              | 当某一行被点击时会触发该事件                 |                                                              |

## 8、Tag标签

​	使用 `el-tag`定义一个标签。

```vue
<el-tag type="success">标签二</el-tag>
```

<font color=red>属性：
</font>

| 属性名   | 解释                 | 属性值类型                            | 默认值 |
| -------- | -------------------- | ------------------------------------- | ------ |
| type     | 指定标签的类型       | string（success/info/warning/danger） |        |
| color    | 指定背景颜色         | string                                |        |
| closable | 设置是否可以关闭标签 | boolean                               | false  |
| hit      | 是否有边框描边       | boolean                               | false  |
| effect   | 设置主题             | string（dark / light / plain）        | light  |

<font color=red>事件：
</font>

| 事件名 | 解释                   | 参数 |
| ------ | ---------------------- | ---- |
| click  | 点击时的事件           |      |
| close  | 设置可关闭后的关闭事件 |      |

## 9、Tree 树形控件

​	使用`el-tree`创建一个树形控件。

```vue
<el-tree
    :data="data"></el-tree>
```

<font color=red>属性：
</font>

| 属性名     | 解释                                                 | 属性值类型 | 默认值 |
| ---------- | ---------------------------------------------------- | ---------- | ------ |
| data       | 展示的数据                                           | array      |        |
| empty-text | 内容为空的时候展示的文本                             | string     |        |
| node-key   | 每个树节点用来作为唯一标识的属性，整棵树应该是唯一的 | String     |        |

<font color=red>事件：</font>

| 事件名称         | 说明                                                  | 回调参数                                                     |
| :--------------- | :---------------------------------------------------- | :----------------------------------------------------------- |
| node-click       | 节点被点击时的回调                                    | 共三个参数，依次为：传递给 `data` 属性的数组中该节点所对应的对象、节点对应的 Node、节点组件本身。 |
| node-contextmenu | 当某一节点被鼠标右键点击时会触发该事件                | 共四个参数，依次为：event、传递给 `data` 属性的数组中该节点所对应的对象、节点对应的 Node、节点组件本身。 |
| check-change     | 节点选中状态发生变化时的回调                          | 共三个参数，依次为：传递给 `data` 属性的数组中该节点所对应的对象、节点本身是否被选中、节点的子树中是否有被选中的节点 |
| check            | 当复选框被点击的时候触发                              | 共两个参数，依次为：传递给 `data` 属性的数组中该节点所对应的对象、树目前的选中状态对象，包含 checkedNodes、checkedKeys、halfCheckedNodes、halfCheckedKeys 四个属性 |
| current-change   | 当前选中节点变化时触发的事件                          | 共两个参数，依次为：当前节点的数据，当前节点的 Node 对象     |
| node-expand      | 节点被展开时触发的事件                                | 共三个参数，依次为：传递给 `data` 属性的数组中该节点所对应的对象、节点对应的 Node、节点组件本身 |
| node-collapse    | 节点被关闭时触发的事件                                | 共三个参数，依次为：传递给 `data` 属性的数组中该节点所对应的对象、节点对应的 Node、节点组件本身 |
| node-drag-start  | 节点开始拖拽时触发的事件                              | 共两个参数，依次为：被拖拽节点对应的 Node、event             |
| node-drag-enter  | 拖拽进入其他节点时触发的事件                          | 共三个参数，依次为：被拖拽节点对应的 Node、所进入节点对应的 Node、event |
| node-drag-leave  | 拖拽离开某个节点时触发的事件                          | 共三个参数，依次为：被拖拽节点对应的 Node、所离开节点对应的 Node、event |
| node-drag-over   | 在拖拽节点时触发的事件（类似浏览器的 mouseover 事件） | 共三个参数，依次为：被拖拽节点对应的 Node、当前进入节点对应的 Node、event |
| node-drag-end    | 拖拽结束时（可能未成功）触发的事件                    | 共四个参数，依次为：被拖拽节点对应的 Node、结束拖拽时最后进入的节点（可能为空）、被拖拽节点的放置位置（before、after、inner）、event |
| node-drop        | 拖拽成功完成时触发的事件                              | 共四个参数，依次为：被拖拽节点对应的 Node、结束拖拽时最后进入的节点、被拖拽节点的放置位置（before、after、inner）、event |

## 10、分页

​	使用 `el-pagination`来创建一个分页组件。

```vue
<el-pagination
    layout="prev, pager, next"
    :total="20">
</el-pagination>
```

​		设置`layout`，表示需要显示的内容，用逗号分隔，布局元素会依次显示。`prev`表示上一页，`next`为下一页，`pager`表示页码列表，除此以外还提供了`jumper`和`total`，`size`和特殊的布局符号`->`，`->`后的元素会靠右显示，`jumper`表示跳页元素，`total`表示总条目数，`size`用于设置每页显示的页码数量。一般吧llayout设置为 `layout="sizes,prev, pager, next,jumper,->,total"`

<font color=red>属性：
</font>

| 属性名       | 解释                                     | 类型                                      | 默认值                    |
| ------------ | ---------------------------------------- | ----------------------------------------- | ------------------------- |
| total        | 总共的条数                               | number                                    |                           |
| layout       | 设置分页的样式                           | string                                    |                           |
| page-size    | 每页显示的条数                           | number                                    | 10                        |
| pager-count  | 页码按钮的数量，当总页数超过该值时会折叠 | number（大于等于 5 且小于等于 21 的奇数） | 7                         |
| current-page | 当前页                                   | number                                    | 1                         |
| popper-class | 每页显示个数选择器的下拉框类名           | string                                    |                           |
| prev-text    | 替代图标显示的上一页文字                 | string                                    |                           |
| next-text    | 替代图标显示的下一页文字                 | string                                    |                           |
| page-sizes   | 每页显示个数选择器的选项设置             | number[]                                  | [10, 20, 30, 40, 50, 100] |
| background   | 是否设置背景颜色                         | boolean                                   | false                     |

<font color=red>事件：
</font>

| 事件名称       | 说明                               | 回调参数 |
| :------------- | :--------------------------------- | :------- |
| size-change    | page-size 改变时会触发             | 每页条数 |
| current-change | current-page 改变时会触发          | 当前页   |
| prev-click     | 用户点击上一页按钮改变当前页后触发 | 当前页   |
| next-click     | 用户点击下一页按钮改变当前页后触发 | 当前页   |

## 11、标记

​	使用 `el-badge` 标签创建一个标记。

```vue
<el-badge :value="12" class="item">
    <el-button size="small">评论</el-button>
</el-badge>
```

<font color=red>属性：
</font>

| 属性名 | 解释                                                    | 类型                                                  | 默认值 |
| ------ | ------------------------------------------------------- | ----------------------------------------------------- | ------ |
| value  | 显示的标记的值                                          | string、number                                        |        |
| type   | 修改标记的类型                                          | string（primary / success / warning / danger / info） |        |
| max    | 当标记的数大于max，会出现+标记，只能在value为number使用 | number                                                |        |
| is-dot | 标记一个红点，用于提示用户，value和它之间只能指定一个   | boolean                                               | false  |
| hidden | 隐藏 badge                                              | boolean                                               | false  |

## 12、Avatar头像

​	使用一个 `el-avatar`来创建一个头像标签。

```vue
<el-avatar :size="100"
           shape="square"
           src="../../../static/photo.jpg"></el-avatar>
```

<font color=red>属性：
</font>

| 属性名 | 解释                                           | 类型                                                 | 默认值 |
| ------ | ---------------------------------------------- | ---------------------------------------------------- | ------ |
| src    | 图片的路径                                     | string                                               |        |
| shape  | 图片的呈现方式是圆还是方块                     | string（circle / square）                            | circle |
| size   | 指定头像的大小                                 | number、string（ large / medium / small）            | large  |
| icon   | 设置头像图标                                   | string                                               |        |
| alt    | 图像的代替文本                                 | string                                               |        |
| fit    | 当展示类型为图片的时候，设置图片如何适应容器框 | string（fill / contain / cover / none / scale-down） | cover  |

## 13、alert 警告

​	使用 `el-alert` 创建一个警告标签。

```vue
<el-alert
    title="操作成功!"
    type="success">
</el-alert>
```

<font color=red>属性：
</font>

| 属性名      | 解释                                                         | 类型                                 | 默认值 |
| ----------- | ------------------------------------------------------------ | ------------------------------------ | ------ |
| title       | 提示的文字信息                                               | string                               |        |
| type        | 指定提示的类型                                               | string（success/warning/info/error） | info   |
| effect      | 指定提示的主题                                               | string（light/dark）                 | light  |
| closable    | 设置提示是否可以被关闭                                       | boolean                              | true   |
| close-text  | 设置关闭按钮的文字                                           | string                               |        |
| show-icon   | 显示 Alert 的 icon                                           | boolean                              | false  |
| center      | 设置文本居中                                                 | boolean                              | false  |
| description | 设置辅助性文字。辅助性文字只能存放单行文本，会自动换行显示。 | string                               |        |

## 14、message 提示消息

​	在js代码中通过 `this.$message('这是一条消息提示');`提示一条消息。

```js
clickAction(){
    this.$message("这是一条提示消息");
}
```

==消息框除了可以传入一个字符串作为提示消息，也可以传入一个对象来配置这个消息。==

```js
this.$message({
    message: "这是一条提示消息!",  // 消息的正文
    type: "success",   // 消息的类型 success/warning/info/error，默认是info
    showClose: true,   // 设置消息可否手动关闭，不设置手动关闭，默认3秒后自动关闭，默认不能关闭
    duration: 1000,     // 设置自动关闭的时间，单位是毫秒
    center: true,        // 设置提示文字是否居中
    // iconClass: "el-icon-delete-solid" 自定义图标的类名，会覆盖 type
    offset: 500,        // 设置消息图窗口顶部的偏移量，默认是20
    onClose: (message)=>{       // 当消息提示关闭的回调函数，参数是message这个实例
        console.log(message);
        alert("消息被关闭"+message);
    }
});
```

## 15、message提示框

​	调用`$alert`方法即可打开消息提示，它模拟了系统的 `alert`，无法通过按下 ESC 或点击框外关闭。此例中接收了两个参数，`message`和`title`。值得一提的是，窗口被关闭后，它默认会返回一个`Promise`对象便于进行后续操作的处理。若不确定浏览器是否支持`Promise`，可自行引入第三方 polyfill 或像本例一样使用回调进行后续处理。

```js
// 参数一：消息的正文
// 参数二：消息的标题
// 参数三：一个对象，用于配置消息框
this.$alert("消息提示","提示",{
    confirmButtonText: "确定"
});
```

​	调用`$confirm`方法即可打开消息提示，它模拟了系统的 `confirm`。Message Box 组件也拥有极高的定制性，我们可以传入`options`作为第三个参数，它是一个字面量对象。`type`字段表明消息类型，可以为`success`，`error`，`info`和`warning`，无效的设置将会被忽略。注意，第二个参数`title`必须定义为`String`类型，如果是`Object`，会被理解为`options`。在这里我们用了 Promise 来处理后续响应。

```js
this.$confirm("确定进行操作吗?","提示",{
    confirmButtonText: "确定",
    cancelButtonText: "取消",
    type: "success",
    center: true  // 提示居中显示
}).then(()=>{
    // 点击确定后进行的操作
    this.$message({
        message: "确定操作!",
        type: "success",
        offset: 300,
        center: true
    });
}).catch(()=>{
    // 点击取消进行的操作
    this.$message({
        message: "不操作!",
        type: "error",
        offset: 300,
        center: true
    });
});
```

​	调用`$prompt`方法即可打开消息提示，它模拟了系统的 `prompt`。可以用`inputPattern`字段自己规定匹配模式，或者用`inputValidator`规定校验函数，可以返回`Boolean`或`String`，返回`false`或字符串时均表示校验未通过，同时返回的字符串相当于定义了`inputErrorMessage`字段。此外，可以用`inputPlaceholder`字段来定义输入框的占位符。

```js
this.$prompt("请输入您的邮箱","提示",{
    confirmButtonText: "确定",
    cancelButtonText: "取消",
    // 输入的校验规则，是一个正则表达式对象
    // inputPattern: /[\w!#$%&'*+/=?^_`{|}~-]+(?:\.[\w!#$%&'*+/=?^_`{|}~-]+)*@(?:[\w](?:[\w-]*[\w])?\.)+[\w](?:[\w-]*[\w])?/,
    // 也可以通过inputValidator自定义一个校验规则，参数是输入值，返回true就是校验成功，如果返回字符串就是校验不成功，并且返回的字符串会覆盖inputErrorMessage
    inputValidator: (value)=>{
        let regExp = /[\w!#$%&'*+/=?^_`{|}~-]+(?:\.[\w!#$%&'*+/=?^_`{|}~-]+)*@(?:[\w](?:[\w-]*[\w])?\.)+[\w](?:[\w-]*[\w])?/;
        return regExp.test(value);
    },
    // 校验失败的提示文字
    inputErrorMessage: "邮箱的格式不正确!",
    // 占位符
    inputPlaceholder: "请输入邮箱!"
}).then((({value})=>{
    // 当点击确定并输入正确后的操作
    this.$message({
        message: "输入的邮箱为:"+value,
        type: "success",
        offset: 300,
        center: true
    });
})).catch(()=>{
    // 点击取消的操作
    this.$message({
        message: "取消输入!",
        type: "success",
        offset: 300,
        center: true
    });
})
```

<font color=red>提示框的配置内容：
</font>

| 参数               | 解释                                                         | 类型                                                         | 默认值                              |
| ------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ----------------------------------- |
| callback           | 若不使用 Promise，可以使用此参数指定 MessageBox 关闭后的回调 | function(action, instance)，action 的值为'confirm', 'cancel'或'close', instance 为 MessageBox 实例，可以通过它访问实例上的属性和方法 |                                     |
| beforeClose        | MessageBox 关闭前的回调，会暂停实例的关闭                    | function(action, instance, done)，action 的值为'confirm', 'cancel'或'close'；instance 为 MessageBox 实例，可以通过它访问实例上的属性和方法；done 用于关闭 MessageBox 实例 |                                     |
| lockScroll         | 是否在 MessageBox 出现时将 body 滚动锁定                     | boolean                                                      | true                                |
| closeOnClickModal  | 是否可通过点击遮罩关闭 MessageBox                            | boolean                                                      | true（以 alert 方式调用时为 false） |
| closeOnPressEscape | 是否可通过按下 ESC 键关闭 MessageBox                         | boolean                                                      | true（以 alert 方式调用时为 false） |
| inputType          | 输入框的类型                                                 | string                                                       | text                                |

## 16、Notification 通知

```js
this.$notify({
    title: "标题",
    message: "这是一条通知",
    duration: 1000, // 通知默认可以自动关闭，默认时间为4500就关闭，如果设置duration为0就不会自动关闭
    type: "success",  // 指定通知的类型
    position: "top-left", // 指定通知弹出的位置，top-right、top-left、bottom-right、bottom-left，默认为top-right
    // 可以使弹出的消息距屏幕边缘偏移一段距离。注意在同一时刻，所有的 Notification 实例应当具有一个相同的偏移量，根据第一个指定的位置偏移
    // 例如top-left就是一top进行偏移
    offset: 300,
    showClose: false, // 设置是否隐藏关闭按钮
})
```

## 17、NavMenu 导航菜单

​	使用el-menu创建一个导航菜单。

```vue
<!--
    el-menu 是一级菜单，
    el-submenu 创建一个存在二级菜单的菜单
    el-menu的属性：
        default-active: 当前激活菜单的 index，string
        mode:   指定菜单的模式，string（horizontal水平 / vertical垂直），默认是垂直的
        collapse：是否水平折叠菜单，boolean，默认false，仅在 mode 为 vertical 时可用
        background-color: 指定菜单的北京颜色，string
        active-text-color: 指定选中的菜单项对应的文字颜色，string
        text-color:   指定菜单的文字颜色，string
        unique-opened：  是否保持一个子菜单展开，boolean，默认false
        menu-trigger： 子菜单触发的方式，string（hover，click），只在 mode 为 horizontal 时有效)，默认hover
        router：是否使用 vue-router 的模式，启用该模式会在激活导航时以 index 作为 path 进行路由跳转，boolean，false，
        这个只是开启了路由跳转，需要在每一个菜单项中指定一个router属性，来进行跳转。
        collapse-transition：指定是否开启折叠动画，默认开启，boolean
    el-submenu的属性：
        template：自定义一个菜单的标题，可以使用i标签来指定一个图标
        show-timeout：展开菜单项的延时时间，number，默认是300
        hide-timeout：收起的延时时间，number，默认300
 -->
<!--
    el-menu的方法:
        open:展开指定的含有子菜单的菜单项，参数是菜单项的index
        close：收起指定的含有子菜单的菜单项，蚕食的菜单项的index
-->
<!--
    el-menu的事件：
        select：菜单激活时，参数是index: 选中菜单项的 index, indexPath: 选中菜单项的 index path
        open: sub-menu 展开的回调,index: 打开的 sub-menu 的 index， indexPath: 打开的 sub-menu 的 index path
        close:sub-menu 收起的回调index: 打开的 sub-menu 的 index， indexPath: 打开的 sub-menu 的 index path
 -->
<router-link to="/t">点击我</router-link>
<el-radio-group v-model="isCollapse">
    <el-radio-button :label="false">展开</el-radio-button>
    <el-radio-button :label="true">折叠</el-radio-button>
</el-radio-group>
<el-menu
         background-color="#bfa"
         class="el-menu-demo"
         active-text-color="red"
         unique-opened
         :collapse="isCollapse"
         :collapse-transition="false"
         router>
    <el-menu-item
        route="/t"
        index="/t">菜单一
    </el-menu-item>
    <el-submenu
        index="2">
        <template slot="title">
            <i class="el-icon-location"></i>
            <span>菜单二</span>
        </template>
        <el-menu-item index="2-1">菜单二-1</el-menu-item>
        <el-menu-item index="2-2">菜单二-2</el-menu-item>
        <el-menu-item index="2-3">菜单二-3</el-menu-item>
        <el-submenu index="2-4">
            <template slot="title">菜单二-4</template>
            <el-menu-item index="2-4-1">菜单二-4-1</el-menu-item>
            <el-menu-item index="2-4-2">菜单二-4-2</el-menu-item>
            <el-menu-item index="2-4-3">菜单二-4-3</el-menu-item>
        </el-submenu>
    </el-submenu>
    <el-submenu index="3">
        <template slot="title">
            <i class="el-icon-location"></i>
            <span>菜单三</span>
        </template>
        <el-menu-item index="3-1">菜单三-1</el-menu-item>
        <el-menu-item index="3-2">菜单三-2</el-menu-item>
        <el-menu-item index="3-3">菜单三-3</el-menu-item>
    </el-submenu>
</el-menu>
<router-view/>
```

## 18、tabs 标签页

​	使用 `el-tabs`创建一个标签页。

```vue
<template>
    <!--
        el-tabs的属性：
            v-model：绑定值name对应的属性值。
            type：指定标签的类型，card卡片类型，border-card卡片优化版
            tabPosition：指定标签的位置，left|right|top|bottom
            closable：设置标签是否显示关闭按钮，boolean，默认false
            addable：设置标签是否可增加，boolean，默认false
            editable：设置标签是否可编辑，增加和删除，boolean，默认false
            stretch： 标签的宽度是否自撑开，boolean，默认false
    -->
    <!--
        el-tabs的事件：
            tab-click: 当标签被选中时，参数是选中的tab实例
            tab-remove：当移除标签时，参数是删除标签的name
            tab-add：当增加标签时
            edit：当增加或删除标签时，参数是tab实例和事件add（添加）和remove（移除）
    -->
    <el-tabs
        type="card"
        closable
        @tab-click="tabClickAction"
        @edit="editAction"
        v-model="activeName">
        <el-tab-pane
            v-for="(item,index) in items"
            :key="item.name"
            :label="item.label"
            :name="item.name">
            {{item.content}}</el-tab-pane>
    </el-tabs>
</template>

<script>
export default {
    name: "Demo01",
    data(){
        return{
            activeName: "fourth",
            items: [
                {
                    label: "用户管理",
                    name: "first",
                    content: "用户管理"
                },
                {
                    label: "配置管理",
                    name: "second",
                    content: "配置管理"
                },
                {
                    label: "商品管理",
                    name: "third",
                    content: "商品管理"
                },
                {
                    label: "销售管理",
                    name: "fourth",
                    content: "销售管理"
                }
            ]
        }
    },
    methods: {
        tabClickAction(tab,event){
            console.log(tab);
        },
        // 修改标签
        editAction(name,action){
            // 如果是移除事件
            if (action === "remove"){
                // 如果删除当前选中的标签
                if (name === this.activeName){
                    this.items.forEach((item,index)=>{
                        if (name === item.name){
                            // 找到下一个或者上一个标签
                            let next = this.items[index+1] || this.items[index-1];
                            // 切换到相邻的标签
                            if (next){
                                console.log(next);
                                this.activeName = next.name;
                            }
                        }
                    });
                }
                // 如果不是当前标签就直接冲items中删除
                this.items = this.items.filter((item)=>{
                    return item.name !== name;
                });
            }else{    // 如果添加事件

            }
        }
    }
}
</script>
```

## 19、Breadcrumb 面包屑

​	使用 `el-breadcrumb`，可以创建一个面包屑导航栏。

```vue
<template>
    <div>
        <!--
            el-breadcrumb的属性：
                separator:面包屑的分隔符，默认是/
                separator-class: 使用图片作为一个分隔符，这使separator失效
         -->
        <!--
            el-breadcrumb的属性：
                to: 一个对象，路由跳转对象，同 vue-router 的 to
                replace：是否在history中添加记录，boolean，默认false，默认是加记录的
        -->
        <el-breadcrumb separator="/">
            <el-breadcrumb-item
                replace
                to="/x">首页</el-breadcrumb-item>
            <el-breadcrumb-item>
                <router-link to="/x">day02-demo01</router-link>
            </el-breadcrumb-item>
        </el-breadcrumb>
        <router-view/>
    </div>
</template>
```

## 20、PageHeader 页头

​	使用 `el-page-header` 标签创建一个页头。

```vue
<!--
content：页头的内容
@back：点击返回的时的回调函数，没有参数。
-->
<el-page-header
           @back="backAction"
           content="返回首页"></el-page-header>
```

## 21、Dropdown 下拉菜单

​	使用 `el-dropdown`创建一个下拉菜单。

```vue
<!--
    trigger: 设置下来菜单的触发方式，默认是hover，可以设置为click
    hide-on-click: 下拉菜单在点击后默认隐藏下来菜单，把这个属性设置为true点击后就不会隐藏
    @command: 事件，当选中不同的菜单项时，就指定对应的函数，参数是每一个菜单项中绑定的command值
    placement: 菜单弹出位置，string（top/top-start/top-end/bottom/bottom-start/bottom-end），默认是bottom-end
    show-timeout: 展开下拉菜单的延时（仅在 trigger 为 hover 时有效）默认250
    hide-timeout: 隐藏下拉菜单的延时（仅在 trigger 为 hover 时有效）默认250
-->
<el-dropdown
    placement="top-end"
    :hide-on-click="false"
    :show-timeout="250"
    :hide-timeout="150"
    @command="commanddAction">
  <span class="el-dropdown-link">
    下拉菜单<i class="el-icon-arrow-down el-icon--right"></i>
  </span>
    <!-- command: 设置为唯一-->
    <el-dropdown-menu
        icon="el-icon-edit"
        slot="dropdown">
        <el-dropdown-item command="a">黄金糕</el-dropdown-item>
        <el-dropdown-item command="b">双皮奶</el-dropdown-item>
        <el-dropdown-item command="c">蚵仔煎</el-dropdown-item>
    </el-dropdown-menu>
</el-dropdown>

<!-- 可以使用按钮来表示一个下拉菜单-->
<el-dropdown>
    <el-button type="primary">下拉菜单<i class="el-icon-arrow-down el-icon--right"></i></el-button>
    <el-dropdown-menu slot="dropdown">
        <el-dropdown-item>黄金糕</el-dropdown-item>
        <el-dropdown-item >双皮奶</el-dropdown-item>
        <el-dropdown-item >蚵仔煎</el-dropdown-item>
    </el-dropdown-menu>
</el-dropdown>
```

## 22、Steps 步骤条

​	设置`active`属性，接受一个`Number`，表明步骤的 index，从 0 开始。需要定宽的步骤条时，设置`space`属性即可，它接受`Number`，单位为`px`，如果不设置，则为自适应。设置`finish-status`属性可以改变已经完成的步骤的状态。

```vue
<!--
    el-steps的属性：
        active：绑定一个完成的进度，步骤1对应是0，number
        space: 指定每一个之间的间隔距离，number
        finish-status：设置这步完成时对应的类型，可以是wait / process / finish / error / success
        align-center：设置标题和描述是否居中，boolean，默认不居中
        direction：设置进度条的方向，默认是水平，string（vertical、horizontal）
        simple： 可应用简洁风格，该条件下 align-center / description / direction / space 都将失效。
        process-status: 设置当前步骤的当前状态,还没完成的状态。
        finish-status: 设置当前步骤完成后的状态
 -->
<!--
    el-step的属性：
        title：设置每一步的标题
        description：对这个步骤的描述。
        icon：用图片来代替每一个步骤
        status: 设置当前步骤的状态，不设置则根据 steps 确定状态,wait / process / finish / error / success
-->
<el-button
    @click="clickAction"
    type="primary">下一步</el-button>
<el-steps
    align-center
    :active="active"
    finish-status="success"
    :space="200">
    <el-step
        :status="finishStatus1"
        description="这是关键的第一步"
        title="步骤 1"></el-step>
    <el-step
        description="这是中间的一步"
        :status="finishStatus2"
        title="步骤 2"></el-step>
    <el-step
        description="这是最后一步"
        :status="finishStatus3"
        title="步骤 3"></el-step>
</el-steps>

<script>
export default {
    name: "Demo02",
    data(){
        return{
            active: 0,
            finishStatus1: "",
            finishStatus2: "",
            finishStatus3: "",
        }
    },
    methods: {
        backAction(){
            this.$router.push("/day06/demo02");
        },
        commandAction(value){
            console.log(value)
        },
        clickAction(){
            console.log(this.active)
            if (this.active === 2){
                this.finishStatus3 = "error";
            }
            this.active++;
        }
    }
}
</script>
```

## 23、Dialog 对话框

​	需要设置`visible`属性，它接收`Boolean`，当为`true`时显示 Dialog。Dialog 分为两个部分：`body`和`footer`，`footer`需要具名为`footer`的`slot`。`title`属性用于定义标题，它是可选的，默认值为空。最后，本例还展示了`before-close`的用法。

==在Dialog对话框中可以嵌套自己需要的内容，例如表单，用于修改和登录等，表格用来呈现数据。如果需要在一个 Dialog 内部嵌套另一个 Dialog，需要使用 `append-to-body` 属性。==

```vue
<!--
            el-dialog的属性：
                - title： 设置对话框的题目
                - visible： 设置这个对话框是否显示，boolean，true为显示，false为不显示，默认不显示
                - center：可以使标题和底部居中显示
                - width：设置对话框的宽度，默认是50%，string
                - fullscreen：设置对话框是否全屏，boolean，false
                - top: 设置margin-top的值，string，默认15vh
                - modal: 是否需要遮罩，boolean，true
                - lock-scroll: 出现对话框是否锁住滚动条，boolean，默认true
                - custom-class：自定义对话框的类名，进行样式设计，string
                - before-close：关闭前的回调，会暂停 Dialog 的关闭，function(done)，done 用于关闭 Dialog
        -->
        <!--
            事件：
             - open：对话框打开的回调
             - opened：对话框打开的动画结束后的回调
             - close：关闭的回调
             - closed：关闭动画结束的回调
        -->
        <el-dialog
            title="提示"
            center
            :modal="false"
            :visible.sync="dialogVisible"
            :before-close="closeAction"
            width="30%">
            <span>这是一段信息</span>
            <span slot="footer" class="dialog-footer">
            <el-button type="primary" @click="dialogVisible = false">确 定</el-button>
            <el-button @click="dialogVisible = false">取 消</el-button>
          </span>
        </el-dialog>

<!--        <el-dialog title="外层 Dialog" :visible.sync="outerVisible">-->
<!--            <el-dialog-->
<!--                width="30%"-->
<!--                title="内层 Dialog"-->
<!--                :visible.sync="innerVisible"-->
<!--                append-to-body>-->
<!--            </el-dialog>-->
<!--            <div slot="footer" class="dialog-footer">-->
<!--                <el-button @click="outerVisible = false">取 消</el-button>-->
<!--                <el-button type="primary" @click="innerVisible = true">打开内层 Dialog</el-button>-->
<!--            </div>-->
<!--        </el-dialog>-->
```

## 24、Tooltip 文字提示

​	使用`content`属性来决定`hover`时的提示信息。由`placement`属性决定展示效果：`placement`属性值为：`方向-对齐位置`；四个方向：`top`、`left`、`right`、`bottom`；三种对齐位置：`start`, `end`，默认为空。如`placement="left-end"`，则提示信息出现在目标元素的左侧，且提示信息的底部与目标元素的底部对齐。

```vue
<!--
    el-tooltip的属性：
        - content：提示的内容，string
        - placement： 展示的位置，string，top/top-start/top-end/bottom/bottom-start/bottom-end/
        left/left-start/left-end/right/right-start/right-end
        - effect：设置提示的主题，string（light、dark）
        - offset：出现位置的偏移量，number
 -->
<el-tooltip
    class="item"
    effect="dark"
    content="这是一个按钮"
    placement="top">
    <el-button>按钮</el-button>
</el-tooltip>
```

## 25、走马灯

​	结合使用`el-carousel`和`el-carousel-item`标签就得到了一个走马灯。幻灯片的内容是任意的，需要放在`el-carousel-item`标签中。默认情况下，在鼠标 hover 底部的指示器时就会触发切换。通过设置`trigger`属性为`click`，可以达到点击触发的效果。

```vue
<!--
    el-carousel的属性：
        - height：指定高度，string
        - trigger：触发方式，默认是hover，也可以修改为click
        - indicator-position：设置显示指示器，默认在容器里面，可以设置为outside在容器外面，如果设置为none就不显示指示器
        - arrow：设置切换箭头，默认是hover在显示，可以设置为always，就会一直显示，设置为never就会一直隐藏
        - type：设置走马等的样式，可以设置为card样式。
        - direction：设置走马灯的方向，默认是horizontal水平，也可以设置为vertical垂直
        - autoplay：是否自动切换，默认true
        - interval：设置自动切换的时间，默认3000，number
 -->
<!--
    事件：
        - change：当跑马灯切换时的事件，参数目前激活的幻灯片的索引，原幻灯片的索引
-->
<el-carousel height="150px">
    <el-carousel-item
        v-for="item in 4" :key="item">
        <h3 class="small">{{ item }}</h3>
    </el-carousel-item>
</el-carousel>
```

