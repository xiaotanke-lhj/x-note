# less详解



## 1、简介

​	less是一门css的预处理语言，是一个css的增强版，可以通过less编写更少的代码实现强大的样式。

css中原版是支持变量和函数的，但是存在浏览器兼容问题。 less就是在css基础之上支持相应的功能，添加了很多的新特性，也兼容浏览器。

less相当于一个翻译器，写完less后会自动转换成css格式，less的语法和css大体上是相似的。



## 2、webstorm安装less环境

步骤：

1. 下载node.js：[https://nodejs.org/zh-cn/](https://nodejs.org/zh-cn/)

下载node.js就直接进行傻瓜式安装，一直点next就行，但是要记住安装目录。

2. 下载less：

- 找到下载的node.js目录，一版是`C:\Program Files\nodejs`，根据自己安装的来。
- 打开npx.cmd文件，弹出命令框，输入命令：`npm install less -g`

![image-20210827103523695](https://i.loli.net/2021/08/27/hVIai7fbU59qMBE.png)

- 使用lessc命令检测是否安装完成

![image-20210827103654415](https://i.loli.net/2021/08/27/MXQ9ltLd4q6sNKH.png)

3. webstorm配置less环境：

打开setting-》Tools--》File Watchers

![image-20210827103856008](https://i.loli.net/2021/08/27/WirxwDpCZn179kg.png)

![image-20210827104215197](https://i.loli.net/2021/08/27/oqgzalNAZwhx68P.png)

它会自动检索你电脑中的less安装目录的。

![image-20210827104240148](https://i.loli.net/2021/08/27/MHLmrRSbYEjD4yi.png)

4. 测试：

当你创建一个less文件时，就会自动创建一个css文件，每当你保存的时候，它就会自动编译为css文件。

![image-20210827104501996](https://i.loli.net/2021/08/27/g5LDbPsY87RJU2x.png)



## 3、基本语法

### 1、在父元素的样式中设置子元素的样式：

```html
<div class="box1">
   <div class="box2"></div>
</div>
```

less代码：

```less
.box1{
    height: 100px;
    width: 100px;
    background-color: red;
    .box2{
        width: 50px;
        height: 50px;
        background-color: #0c5460;
    }
}
```

这样可以直接在box1的样式中设置box2的样式，可以使书写的结构更加有可读性。

被编译成css代码后：

```css
.box1 {
  height: 100px;
  width: 100px;
  background-color: red;
}
.box1 .box2 {
  width: 50px;
  height: 50px;
  background-color: #0c5460;
}
```

### 2、注释：

- 单行注释：使用 // 进行注释，注释的内容不会解析到css中
- css中的注释：/*  */，注释的内容会被解析到css中，是一种多行注释。

### 3、变量：

   在变量中可以存储一个任意的值，在需要使用的时候就使用，需要修改的时候直接修改一处就可以。

<font color=red>语法：</font>@变量名:变量值;

- 声明变量：`@w:100px;`

- 使用变量： 

```less
.box1{
    width: @w;
}
```

==注意：变量的值是任意值，可以是任何东西，例如颜色，宽高等。==

- 当变量值是类名的时候，使用变量需要以 `@{变量名}`

```less
@b1:box1;
.@{b1}{
    background-color: #0c5460;
}
```

- 当变量名发生重合的时候，优先使用就近原则，使用较进的变量，变量的声明可以写在后面。

- 可以直接引用样式中重复的值：

```less
.box1{
    width: 500px;
    height: $width;
}
```

上面就是height就是直接引用width的值。

### 4、父元素选择器：

<font color=red>语法：</font>& 符号相当于表示外层父元素

```less
.p1{
    >.p2{
        /*设置.p1的子元素.p2的样式*/
    }
    &:hover{
        /*设置.p1的hover样式*/
    }
    .p3{
        &:hover{
            /*设置.p3的hover样式*/
        }
    }
}
```

### 5、扩展

<font color=red>语法：</font>

```less
.p1{
    width: 100px;
    height: 100px;
}
.p3{
    background-color: #bfa;
}
.p2:extend(.p1,.p3){
    /*扩展了p1的所有属性，也可以设置自己的属性*/
    color: red;
}
```

可以扩展多个类的属性，之间使用逗号隔开。

### 6、混合函数

<font color=red>语法：</font>

```less
.test(){
    width: 100px;
    height: 100px;
}
.p4{
    .test();
    color: red;
}
```

 test就是一个混合函数，其它类可以扩展该函数。

函数也可以传入参数，

```css
.test(@w,@h){
    width: @w;
    height: @h;
}
.p4{
    .test(100px,100px);
    color: red;
}
```

==注意：参数的顺序要保持一致，不然传入的参数会对应错误的位置，参数必须全部都要传入==

- 可以指定参数的默认值，指定了默认值，如果没有传入该参数，就使用默认值。

```less
.test(@w:100px,@h:100px,@c){
    width: @w;
    height: @h;
    color: @c;
}
.p4{
    .test(@c:red);
    color: red;
}
```

![image-20210827154139392](https://i.loli.net/2021/08/27/lwK1C5L6OWQjkrq.png)

### 7、运算

​	在less中，数值可以直接使用 +、-、*、/ 运算。

```less
.p1{
    width: 100px+100px;
    height: 10%+20%;
}
```

### 8、引入

​	在less中，我们可以引入其它的less文件，相当于，把其它的less复制到当前文件中。

```less
@import "test.less";
```

这样就引入了test.less文件，这样就可以进行模块开发，一个模块用来写变量，一个模块用来写混合函数，一个模块来整合。







