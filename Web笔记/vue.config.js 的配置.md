# vue.config.js 的配置



## 1、基本配置

~~~js
const path = require('path')
function resolve (dir) {
  return path.join(__dirname, './', dir)
}
module.exports = {
    // 部署应用包时的基本 URL，你可以在不同环境下部署不同的url路径
  	publicPath:  process.env.NODE_ENV, 
    
    
    
    
    
    
    
    
  	outputDir: 'dist',
  	assetsDir: '', // 相对于outputDir的静态资源(js、css、img、fonts)目录
  	runtimeCompiler: true, // 是否使用包含运行时编译器的 Vue 构建版本
  	productionSourceMap: false, // 生产环境的 source map
  	configureWebpack: config => {},
  	chainWebpack: config => {}
}
~~~









