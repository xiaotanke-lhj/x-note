# MySQL 高级进阶笔记

## 1、Centos 环境安装MySQL

> 安装前检查工作

~~~bash
# 检查 MariaDB 是否存在，会和MySQL冲突
rpm -qa|grep mariadb
# 删除 MariaDB
rpm -e --nodeps mariadb-libs-5.5.68-1.el7.x86_64
rm /etc/my.cnf

# 检查电脑是否安装过MySQL
rpm -qa | grep mysql
rpm -e --nodeps mysql-libs-5.1.73-5.el6_6.x86_64
~~~

> 下载安装包，并上传到服务器，压缩包大概400多MB，不要下错了

官网地址[https://downloads.mysql.com/archives/community/](https://downloads.mysql.com/archives/community/)

![image-20230420130604897](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20230420130604897.png)

> 安装命令

~~~bash
# 解压命令
tar -zxvf 压缩包

# 在/usr/local/mysql下创建一个data文件夹
mkdir -p /usr/local/server/mysql/data
~~~

> 创建MySQL用户组

~~~bash
#进入mysql目录
cd /usr/local/server/mysql
#添加用户组
groupadd mysql
#添加用户，mysql组名，xxx用户名
useradd -g mysql mysql
# 修改mysql文件权限，xxxx用户名，mysql组名
chown -R mysql.mysql /usr/local/server/mysql
~~~

> 配置环境

~~~bash
# 创建配置文件
touch /etc/my.cnf

vim my.cnf
~~~

~~~bash
[mysql]
#设置mysql客户端默认字符集
default-character-set=utf8
[mysqld]
# 设置3306端口
port = 3306
# 设置mysql的安装目录
basedir=/usr/local/server/mysql
# 设置mysql数据库的数据的存放目录
datadir=/usr/local/server/mysql/data
# 允许最大连接数
max_connections=200
# 服务端使用的字符集默认为8比特编码的latin1字符集
character-set-server=utf8
# 创建新表时将使用的默认存储引擎
default-storage-engine=INNODB
lower_case_table_names=1
max_allowed_packet=16M
socket=/usr/local/server/mysql/mysql.sock
symbolic-links=0
[client]
port=3306
socket=/usr/local/server/mysql/mysql.sock
!includedir /etc/my.cnf.d
~~~

> 初始化

~~~bash
mkdir /etc/my.cnf.d
# 进入到mysql安装目录下的bin文件夹下
./mysqld --defaults-file=/etc/my.cnf --basedir=/usr/local/server/mysql/ --datadir=/usr/local/server/mysql/data/ --user=mysql --initialize
~~~

![image-20230421102642834](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20230421102642834.png)

> 加入系统服务

~~~bash
cp /usr/local/server/mysql/support-files/mysql.server /etc/init.d/mysql

# 启动mysql
systemctl start mysql
~~~

> 遇见错误

- ```Failed to start LSB: start and stop MySQL```

~~~bash
# 权限不住，需要指定的用户是mysql，并且在MySQL的安装目录下给mysql权限
chown -R mysql.mysql /usr/local/server/mysql

# 输入mysql命令不存在，可以是没有配置环境变量
vim /etc/profile
# 加入下面一句话
export PATH=$PATH:mysql的bin路径
# 刷新环境变量
source /etc/profile
~~~

> 修改root密码

~~~bash
# 修改密码命令
ALTER user 'root'@'localhost' IDENTIFIED BY 'xxxx';
#刷新权限
flush privileges;
~~~

> 配置远程登录

~~~bash
# 使用这个数据库
use mysql;
select user,host,plugin,authentication_string from user;
# mysql8认证方式改了,mysql_native_password这个才能远程连接mysql
# localhost表示本地连接，%，所有ip都可以连接
update user set host='%' where user='root';
flush privileges;
~~~

## 2、MySQL常用设置

### 2.1、字符集设置

> 在 MySQL8.0 之前它默认的编码集是 Latinl ，如何去插入一些中文汉字就会出现乱码的请求，在8之前就需要去手动指定数据库的编码格式，一般是utf-8。在8之后它默认的编码集就是 utf8mb4，从而避免了乱码问题。

~~~bash
# 查看默认字符集
show variables like '%character%';
show variables like '%char%';

# 查看数据库支持字符集
show charset;
show character set;
~~~

![image-20230423131729076](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20230423131729076.png)

- character_set_client：服务器解码请求时的字符集。
- character_set_connection：服务器请求时会把`character_set_client`的字符串用自己的字符集转换一下。
- character_set_results：服务器向客户端返回数据时的字符集。

~~~bash
# 修改数据库默认字符集，如果在创建数据库时没有指定字符集就会使用默认字符集
vim /etc/my.cnf
# 在配置文件中添加下面一句话，然后重启mysql
character_set_server=xxx字符集
~~~

==之前创建数据库的字符集不会改变，还是修改字符集之前的字符集。==

~~~sql
# 修改已有数据库字符集
alter database xxx character set '字符集';
# 修改已有表字符集
alter table xxx convert to character set '字符集';
~~~

> MySQL 字符级别：

- 服务器级别：这个级别最高，如果没有设置其它字符集，就是用服务器级别。(character_set_server)
- 数据库级别：在创建数据库时可以指定数据库字符集。(character_set_database)
- 表级别：在创建数据库表时可以指定表字符集。
- 列级别：在创建表字段时，可以在字段的后面指定字符集。

> utf-8字符集：表示一个字符需要1~4个字节，但是日常的字符3个字节就可以，但是一些特殊字符需要4个字节，例如表情。

- utf8mb3：一个字符用3个字节表示。
- utf8mb4：一个字符用4个字节表示。

> 比较规则：字符串排序时使用的一种规则。

| 后缀 | 作用             |
| ---- | ---------------- |
| _ai  | 不区分重音       |
| _as  | 区分重因         |
| _ci  | 不区分大小写     |
| _cs  | 区分大小写       |
| _bin | 以二进制数据比较 |

~~~bash
# 查看某一个字符集的比较规则
show collation like '字符集'
~~~

![image-20230424130844481](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20230424130844481.png)

> utf8常用比较规则：

- utf8_unicode_ci：校准速度慢，准度高。
- utf8_general_ci：校准速度快，准度差。

==如果数据库中的数据存在德语、法语、俄语，一定要使用utf8_unicode_ci。==

### 2.2、SQL大小写

> Windows下对于一些表名、字段名不区分大小写，但是在Linux环境下是要区分大小写的，但是对于SQL中的关键字、函数都是不区分大小写。

~~~sql
# 查看是否区分大小写，1：不区分，0：区分
show variables like '%lower_case_table_name%';
~~~

![image-20230424134021438](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20230424134021438.png)

> Linux下设置大小写规则：在配置文件中的[mysqld]中加入`lower_case_table_names=1`，然后重启服务器。

~~~bash
[mysqld]
lower_case_table_names=1
~~~

==这个设置方式只适用于5.7，但是在8下设置重启 MySQL 服务会恢复默认的大小写配置。如果在8下修改的话，需要将数据库现有的数据库数据文件夹删除，然后重启服务，不建议修改这个参数。==

> SQL编写建议：

- 关键字、函数名全部大写。
- 数据库名、表名、字段名、字段别名全部小写。
- SQL语句必须以分号 `;` 结尾。

### 2.3、sql_mode

> sql_mode：会影响 MySQL 支持的SQL语法以及它执行的SQL校验，可以完成不同程度的数据校验，它有两种模式(宽松模式、严格模式)。

- 宽松模式：在写SQL语句时，即使发生了错误也会被接受，例如在插入一条数据，数据长度超过了字段指定的长度，在宽松模式下会截取对应长度保存下来，它不会报错。
- 严格模式：严格检查编写的SQL语句，也会对数据进行校验，如果发现错误就直接报错不会进行对应的操作。

~~~sql
# 查看当前sql_mode
# 当前会话
select @@session.sql_mode;
# 全局
select @@global.sql_mode;
~~~

![image-20230425131458180](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20230425131458180.png)

~~~sql
# 设置sql_mode
# 当前会话
set SESSION sql_mode = '值';
# 全局
set GLOBAL sql_mode = '值';
~~~

==设置会话的值在会话关闭后就失效，设置全局值会在MySQL服务重启后失效。==

~~~sql
# 永久设置方式：在配置文件中[mysqld]下面加一句话，然后重启服务
[mysqld]
sql_mode=值
~~~

### 2.4、MySQL数据目录

> MySQL默认数据库：

- mysql：核心数据库，存储了用户账户信息、权限信息、存储过程、事件定义信息、运行日志信息等。
- information_schema：存储MySQL中其它数据库信息，例如某个数据库的表、索引、视图、触发器等。
- sys：通过视图方式将`information_schema`和`performance_schema`结合起来，帮助管理员和开发人员监控数据库性能。

> MySQL把表数据存放在文件系统中，就是在配置文件中指定路径，`datadir=/usr/local/server/mysql/data`。不同的存储引擎在存储时数据的表结构不一样，不同的版本数据存储结构也是不一样的。(可以通过命令：`show variables like 'datadir';`查看数据存储路径)

<font color=red>InnoDB存储引擎：</font>

- MySQL5.7：
    - db.opt：存放数据库信息，例如字符集、比较规则等。
    - emp1.frm：存储emp1表的表结构，例如字段信息、类型、约束等。
    - emp1.ibd：存储emp1表的数据。

![image-20230426131730832](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20230426131730832.png)

- MySQL8.0：
    - gen_table.ibd：存储了gen_table表的所有数据，数据库信息、表结构、表数据都是存放在这里里面，相当于5.7版本opt、frm、ibd和起来。

![image-20230426132406724](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20230426132406724.png)

<font color=red>MyISAM存储引擎：</font>

- MySQL5.7：
    - db.opt：存放数据库信息，例如字符集、比较规则等。
    - frm：存储表结构信息。
    - MYD：存储表数据。
    - MYI：存储表索引信息。

![image-20230426133257808](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20230426133257808.png)

- MySQL8.0：
    - sdi：相当于frm，存放表结构信息。
    - MYD：存储表数据。
    - MYI：存储表索引信息。

![image-20230426133444263](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20230426133444263.png)

### 2.5、用户管理

​		MySQL 用户分为root和普通用户，root是超级用户拥有所有的权限(用户创建、删除、修改等)。普通用户只能使用被赋予的权限。

> 用户操作命令：

~~~sql
# 登录MySQL，-h 数据库的地址(默认localhost)，-P 登录端口(默认3306)，-u 登录账户(默认root) -p 登录密码
mysql -h xxx.xxx.xxx.xx -P port -u xxx -p xxxx

# 创建用户
CREATE user '用户名'@'登录地址' identified by '登录密码';
# 例如，不指定登录地址默认是%
CREATE user 'zhangsan'@'localhost' identified by 'zhangsan';


# 修改用户
UPDATE mysql.user SET user='新账号' WHERE user = '旧帐号';
# 例如
UPDATE mysql.user SET user='zhangsan' WHERE user = 'lisi';
# 修改mysql.user表后必须执行下面命令
FLUSH PRIVILEGES;


# 删除用户
# 方式一：不指定连接地址默认是%（推荐方式）
DROP user '用户名'@'连接地址';
# 方式二：直接删除mysql.user表中的数据，这种方式可能在系统中有残留信息
DELETE FROM mysql.user WHERE user = '用户名';
FLUSH PRIVILEGES;
~~~

> 修改密码：root有很高的权限不仅可以修改自己密码，还可以修改其他人的密码。

~~~sql
# 修改自己登录密码
# 方式一
ALTER user user() identified by '新密码';
# 方式二
SET PASSWORD = '新密码';

# 修改其它用户密码，这个需要root权限才可以，不指定连接地址，默认%
# 方式一
ALTER user '用户名'@'连接地址' identified by '新密码';
# 方式二
SET PASSWORD for '用户名'@'连接地址' = '新密码';
~~~

### 2.6、权限管理

​		MySQL数据库中通过一些权限操作赋予某些用户可以在数据库进行得操作。

~~~sql
# 查看数据库有哪些权限
show privileges;
~~~

![image-20230428131400404](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20230428131400404.png)







































