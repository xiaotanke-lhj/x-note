> 初始化data文件

~~~bash 
mysqld --initialize-insecure
~~~

> 服务：需要已管理员身份运行

~~~bash
# 创建
sc create 服务名 exe文件路径
# 删除
sc delete 服务名
~~~

> 初始化管理员密码：在mysql安装目录下的bin文件夹下，使用

~~~bash
# 临时跳过权限认证，使用这个命令完后需要重启mysql服务
mysqld --skip-grant-tables
# 跳过认证修改密码
mysql -u root
# 修改密码
ALTER USER 'root'@'localhost' IDENTIFIED BY '你的新密码';
# 重启mysql服务
~~~

