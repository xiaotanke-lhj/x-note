# 算法题集

## 1、位运算

~~~java
/**
 *
 * 位运算：
 *  - （1 & 0 = 0） 、（1 & 1 = 1）、（1 | 0 = 1） 
 *  - （1 ^ 1 = 0） 、（1 ^ 0 = 1）
 */
~~~

### 1.1、唯一数

~~~java
/**
 * 数组中存在只有唯一的一个数，其它数都是重复的两个数，找出这个唯一的数（不开辟辅助空间）
 * @param array 数组
 * @return 唯一数
 */
public static int test1(int[] array){
    // 方式一：开辟辅助空间，用一个map(元素值为key，个数为value)，遍历数组。

    // 方式二：将数组的所有元素进行 ^ 操作，剩下的就是唯一元素，因为重复的两个元素 ^ 为0。
    for (int i = 1; i < array.length; i++) {
        // 所有元素进行异或，最后array[0]就是唯一数
        array[0] = array[0] ^ array[i];
    }
    return array[0];
}
~~~

### 1.2、重复数

```java
/**
 * 长度为N+1的数组，里面存放1~N的值和一个重复元素，请找出这个重复元素(不开辟辅助空间)
 * @param array 数组
 * @return 重复元素
 */
public static int test2(int[] array){
    // 方式一：开辟辅助空间，一个长度N的数组记录每一个元素的个数

    // 方式二：把数组元素、1~N进行异或，例如(1^2^3^2）^ (1^2^3) = 2就是重复的元素
    int x = 0;
    // 1~N的 ^
    for (int i=1;i<=array.length-1;i++){
        x = x ^ i;
    }
    for (int j : array) {
        x = x ^ j;
    }
    // 重复元素
    return x;
}
```

### 1.3、二进制中1的个数

```java
/**
 * 输入一个整数，返回用二进制表示中1的个数
 * @param n 整数
 * @return 二进制中1的个数
 */
```

1. 方式一：（1101 & 1 = 1）、（1101 & 10 = 0）（1101 & 100 = 100），整数最多32位，可以循环32次，然后1每次向左移一位，如果N对应位是1，则结果就是1，反之就是0。

```java
public static int test1(int n){
    int count = 0;
    for (int i=0;i<32;i++){
        // 移位的1
        int x = 1<<i;
        // 与
        if ((n&x) == x){
            count++;
        }
    }
    return count;
}
```

2. 方式二：（1101 & 1 = 1）、（110 & 1 = 0）、（11 & 1 = 1），让N向右移位，然后和1与算，如果等于1则该位是1，反之。

```java
public static int test2(int n){
    int count = 0;
    for (int i=0;i<32;i++){
        // n向右移i位，然后和1与算
        if (((n>>>i)&1) == 1){
            count++;
        }
    }
    return count;
}
```

3. 方式三：使用N每次去减1，个位上不够减就会去借位。【（1101-1 = 1100）& 1101】 = 1100，这样就会消掉最低位的1，相当于是（n-1）& n 消掉了最低位的1，如果当n等于0就将1消除完了。

```java
public static int test3(int n){
    int count = 0;
    // 终止条件就是所有位都是0
    while (n!=0){
        // 进行消除最低位的1
        n = (n-1) & n;
        count++;
    }
    return count;
}
```

### 1.4、判断整数是不是2的整数次方

```java
/**
 * 判断整数是不是2的整数次方
 * @param n 整数
 */
public static boolean test4(int n){
    // 方式一：n循环除以2，最后结果是1则是

    // 方式二：整数是不是2的整数次方，表示二进制中只有一个1，直接消除低位1，看是否等于0
    return ((n-1) & n) == 0;
}
```

### 1.5、二进制奇偶位交换

```java
/**
 * 将整数二进制的奇数位和偶数位相互调换，返回调换后的值，例如9（1001）调换后是6（0110）
 * @param n 整数
 * @return 调换后的值
 */
public static int test5(int n){
    /**
     * 思路：将奇偶位上的数据分别取出来，然后偶数位向右移一位，奇数位向左移一位，然后将两个数进行 ^ 操作
     *    &与运算，1保留，0消除
     *    1001 & 0101 = 0001(奇数位) ===> 左移：0010
     *    1001 & 1010 = 1000(偶数位) ===> 右移：0100
     *    异或：0010 ^ 0100 = 0110 = 6
     */
    // 整数最高32位
    int x = n & 0xaaaaaaaa; // 保留偶数位
    int y = n & 0x55555555; // 保留奇数位
    // 奇数左移，偶数右移，然后进行异或
    return ((x>>1) ^ (y<<1));
}
```

### 1.6、出现K次和1次

```java
/**
 * 在一个数组中，存在一个唯一元素，其它元素出现k次
 * @param array 数组
 * @param k 重复元素出现次数
 * @return 唯一元素
 */
public static int test6(int[] array,int k){
    /**
     * 思路：
     *      k个k进制元素做不进位加法，相加结果为 0
     *      把所有元素都换成k进制，然后做不进位加法，所有元素相加就是唯一元素的k进制表示
     * 例如：
     *      10+10 = 00(二进制，不进位加法)
     *      21 + 21 + 21 = 00(三进制，不进位加法)
     */
    // 将数组元素转换换成k进制
    char[][] charA = new char[array.length][];

    // 记录转换后的最长长度，以便后续进行加法
    int maxLength = 0;
    for (int i=0;i<array.length;i++) {
        // 转换k进制
        StringBuilder builder = new StringBuilder(Integer.toString(array[i], k));
        // 将低位与高位反转，这样能对齐
        String s = builder.reverse().toString();
        if (s.length()>maxLength){
            maxLength = s.length();
        }
        charA[i] = s.toCharArray();
    }

    int[] res = new int[maxLength];
    // 进行加法
    for (char[] chars : charA) {
        for (int j = 0; j < chars.length; j++) {
            res[j] += (chars[j] - '0');
        }
    }

    // 进行不进位处理
    int x = 0;
    for (int i = 0; i < res.length; i++) {
        // %k就是进行不进位处理，然后转换成10进制
        x += ((res[i]%k) * Math.pow(k,i));
    }
    return x;
}
```

## 2、递归

> 递归：一个方法中自己调用自己就是一个递归，是一个用空间换时间的算法。

<font color=red>解题思路：
</font>

- 找重复问题，重复需要解决的问题。
- 找重复问题中的参数变量。
- 找重复参数变量的变化趋势，并设计出口逻辑。

### 2.1、m、n最大公约数

```java
/**
 * 求m、n的最大公约数
 */
public static int test1(int m,int n){
    // 需要确定数值大小
    int temp;
    if (m<n){
        temp = n;
        n = m;
        m = temp;
    }
    // 递归的结束条件
    int k = m % n;
    if (k==0){
        return n;
    }
    // 递归
    return test1(n,k);
}
```

### 2.2、插入排序(递归)

```java
/**
 * 使用递归的方式完成插入排序
 */
public static void test2(int[] array,int index){
    // 终止条件
    if (index==0){
        return;
    }
    // 将index前面的元素进行排序
    test2(array,index-1);

    // 将index的元素进行插入，注意这点有一个index=0的临界值需要注意，不能超过
    int temp = array[index];
    while ((--index)!=-1 && temp<array[index]){
        array[index+1] = array[index];
    }

    // 插入位置换乘最后一个元素
    array[++index] = temp;
}
```

































