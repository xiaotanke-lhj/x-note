# Tomcat 服务器学习笔记



## 1、下载及环境配置

1. 下载：

![image-20211108182623147](../../%E7%B4%A0%E6%9D%90/%E5%9B%BE%E7%89%87%E7%B4%A0%E6%9D%90/CSDN/738043a4948b3f7c4bd7b9ee72d30c06.png)

2. 配置环境

- 确定自己的`java`环境配置好，不然tomcat启动会出现闪退的情况。
- 将下载的zip文件解压到自己需要保存的目录中。

步骤：

1. 配置`CATALINA_HOME`环境，打开电脑系统的环境变量。

![image-20211108183745747](../../%E7%B4%A0%E6%9D%90/%E5%9B%BE%E7%89%87%E7%B4%A0%E6%9D%90/CSDN/f7b5508533fbbafdf52035725261275a.png)

2. 配置Path环境

![image-20211108184016114](https://i.loli.net/2021/11/08/qukK85xyNdlQnoz.png)

3. 测试环境，使用`cmd`窗口，输入`startup.bat`命令

![img](../../%E7%B4%A0%E6%9D%90/%E5%9B%BE%E7%89%87%E7%B4%A0%E6%9D%90/CSDN/68f1e17ce56e908da32c5b3f4c6f4dcb.png)



## 2、tomcat的基本操作

### 2.1、启动tomcat

==可以直接通过tomcat的`cmd`进行启动tomcat，`startup.bat`启动tomcat，弹出tomcat的窗口就是启动成功了，关闭窗口就是关闭tomcat。也可以通过下面方式启动tomcat。==

![img](../../%E7%B4%A0%E6%9D%90/%E5%9B%BE%E7%89%87%E7%B4%A0%E6%9D%90/CSDN/93d9e08136eff4b1ef350e2878923c93.png)

### 2.2、tomcat 的文件结构

![image-20211108190730135](https://i.loli.net/2021/11/08/nwJGvWc8QMIRa7x.png)

### 2.3、tomcat 的常用配置

<font color=red>修改端口号：
</font>tomcat默认的端口是8080，但是我们可以认为去修改它的端口号。在`config`目录下的`server.xml`中修改，大约在69行。

![image-20211108191339705](https://i.loli.net/2021/11/08/XmYJu8BcrzS9psy.png)

<font color=red>tomcat启动乱码问题：
</font>因为window的默认编码是`GBK`，但是tomcat的日志输出是`UTF-8`。在`config`目录下修改`logging.properties`文件，大约在51行。

![image-20211108192122861](https://i.loli.net/2021/11/08/RzEveqpF6U3Hibm.png)

<font color=red>修改主机名：
</font>也只在`server.xml`配置文件中，大约在152行。

![image-20211108194524716](https://i.loli.net/2021/11/08/MUSF9CipOlysA8X.png)



## 3、理解如何对一个网站进行访问？

1. 在浏览器中输入一个URL地址后，首先会在自己的电脑上的 `C:\Windows\System32\drivers\etc`这个目录的`hosts`文件下进行`DNS`解析。

![image-20211108192649563](https://i.loli.net/2021/11/08/AYlQev14y7LjT2u.png)

==如果添加自己定义的域名，需要将前面的 # 去掉，但是不建议修改这个文件。在`win7`中不能直接修改该文件，要先要把文件复制出到其它盘中，修改后在复制到刚才的目录。==

![image-20211108192923121](https://i.loli.net/2021/11/08/kvGHxREUdA8h7nL.png)

==如果tomcat修改了主机的名称就需要在这个文件中映射一个`ip`地址。==

2. 在自己电脑上的这个文件中没有找到需要解析的域名，例如www.baidu.com这个域名在我的电脑上没有，就会去互联网的`DNS`解析，找到这个解析就返回解析的`IP`地址，没有找到就返回没有这个资源。



## 4、tomcat发布第一个网站

==直接将自己的web项目放入到`webapps`这个目录下就可以直接访问里面的资源。==

![image-20211108195255394](https://i.loli.net/2021/11/08/6y9GUHfdvi5wbSM.png)

==访问方式：`http://localhost:端口号/项目名/资源`，上面就可以是`http://localhost:8080/test/index.html`。WEB-INF这个目录是安全目录，是放`java`代码的目录，不能直接访问。==

最近发现除了腾讯云和阿里云之外的一种好用的云服务器，那就是三丰云云服务器，它拥有众多的功能，其中一个就是可以免费试用一款云服务器，下面介绍它的使用方式。

[官方地址:https://www.sanfengyun.com/](https://www.sanfengyun.com/)

![image-20230307102210797](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20230307102210797-16781568307221-16836794045841.png)

然后进行一个实名认证和微信的绑定就可以申请一个 1c1g的免费服务器。

![image-20230307102330457](https://jx-image-storage.oss-cn-hangzhou.aliyuncs.com/image/image-20230307102330457-16781568307223-16836794045843.png)



三丰云是北京太极三丰云计算有限公司旗下网络服务品牌，十八年IDC老兵团队蛰伏三年后投资千万于2018年10月1日创建。公司致力于为大众提供优质的互联网基础服务和物联网服务，包括：域名注册、虚拟主机、云服务器、主机托管租用、CDN网站加速、物联网应用等服务。以帮助客户轻松、 高速、高效的应用互联网/物联网，提高企业竞争能力。，它拥有众多的功能，其中一个就是可以免费试用一款云服务器，下面介绍它的使用方式。

[官方地址:https://www.sanfengyun.com/](https://www.sanfengyun.com/)





