# 1、简介

>  git是当前最流行的版本控制器。
>
> 版本控制：是一种在开发过程中用于管理我们文件、目录或工程等内容的修改历史，方便查看更改历史记录，备份以便恢复以前的版本的软件工程技术。就是版本迭代，旧项目版本不能删除，新的版本继续开发。

- 实现跨区域多人协同开发
- 追踪一个或多个文件的历史记录
- 组织和保护你的源代码和文档

- 统计工作
- 并行开发，提高开发效率

**版本控制的分类：**

- 本地版本控制：记录文件每次更新，可以对每个版本做一个快照，或是记录补丁文件，适合个人使用。
- 集中版本控制：所有版本数据都保存在服务器上，协同开发者从服务器上同步更新或上传自己的修改。（`SVN`等）
- 分布式版本控制：每个人都拥有全部的代码，所有版本信息仓库全部同步到本地的每一个用户，这样就可以在本地查看所有版本历史，可以离线在本地提交。只需在连网后提交到相应的服务器上。由于每个用户那里都保存的都是所有版本数据，只要一个用户的设备没有问题就可以恢复到所有的数据。（`Git`）

> `Git`和`SVN`的区别：
>
> `SVN`：是集中版本控制，版本库是放在中央服务器上的，而工作的时候用的都是自己的电脑，所以必须首先从服务器上得到最新的版本，工作完后将自己的代码提交到服务器上。必须连网。
>
> `Git`：是分布式版本控制，没有中央服务器，每一个人的电脑都是一个完整的版本库，工作的时候不需要连网。协同的方法就是，当A修改了test文件，B也修改了test文件，这时，他们只需把自己修改后的文件推送给对方就可以了，这样对方就可以看见对方修改的内容。

#  2、Git的安装和环境搭配

**安装：**

方式一： [Git官网：https://git-scm.com/](https://git-scm.com/)下载对应的版本，但是官网下载非常慢

方式二：[淘宝镜像下载：https://npm.taobao.org/mirrors/git-for-windows/](https://npm.taobao.org/mirrors/git-for-windows/)

​	<img src="https://s2.loli.net/2022/03/15/SfVKgb7lZyDc1hE.png" alt="image-20210807233241975" style="zoom: 67%;" />

下载 后然后进行无脑安装。安装成功后，在开始菜单栏中就会存在。鼠标右键桌面也会有Git Bash和Git GUI这两个选项。

![image-20210807233937336](https://s2.loli.net/2022/03/15/GJCz2ZUWO9dBRyX.png)

`Git Bash：Unix和Linux风格的命令行，使用最多，推荐使用`

`Git CMD：windows风格的命令行`

`Git GUI：图形界面的Git。`

![image-20210807234341497](https://s2.loli.net/2022/03/15/YiR9LrhD5dZWo3E.png)

> 拓展：Linux常用命令
>
> - `cd`：进入到某个目录中
> - `cd ..`：返回到上一级目录
> - `pwd` ：显示当前目录路径
> - `ls`：列出当前目录下的文件
> - `ll`：列出当前目录下的文件，比ls更加详细
> - `touch`：新建一个文件，后面跟一个文件名，例如`touch index.html`
> - `rm`:删除一个文件，后面跟文件文件名，例如`rm index.html`
> - `mkdir`:新建一个文件夹，后面跟文件夹的名称，例如`mkdir test`
> - `rm -r：强制删除一个文件夹，后面跟文件夹名，例如`rm -r test`
> - `rm -rf /`：删除在Linux根目录下的所有文件，相当于清空系统。
> - `mv 移动文件 目标文件夹`：将移动文件，移动到指定的目标文件夹下。
> - `reset`：重启终端
> - `clear`：清除终端屏幕
> - `history`：查看历史使用的命令
> - `exit`：退出终端
> - ‘#’ 表示注释

# 3、Git的配置

1. 查看全局配置命令：`git config -l`

![image-20220315173320848](https://s2.loli.net/2022/03/15/5MuCd23DNlUWLtA.png)

2. 查看系统配置命令：`git config --system --list`

![image-20220315173427280](https://s2.loli.net/2022/03/15/EPRkfd2FKJBUpO8.png)

3. 查看用户配置命令：`git config --global --list`

![image-20220315173539611](https://s2.loli.net/2022/03/15/bEgeuaXYNwMOPAD.png)

4. 设置用户name命令：`git config --global user.name 用户名`

5. 设置用户邮箱email命令，建议使用真实邮箱：`git config --global user.email LHJ160414@163.com`

==Git的配置中第4、5是必须都要配置的，否则导致后面无法上传代码。==

> Git相关配置文件：
>
> - `gitconfig`：在安装目录下的 `/etc/gitconfig`，这是系统的配置文件。
>
> ![image-20220315174257529](https://s2.loli.net/2022/03/15/L4OAtR1vZnU2qmi.png)
>
> - `.gitconfig`：在`C:\Users\LHJ16\.gitconfig`，这是用户配置文件，配置的name和email也可以在这里面配置。
>
> ![image-20220315174512936](https://s2.loli.net/2022/03/15/1iaY463wDKVkU7P.png)

# 4、Git基础理论

> 工作区：Git一共有4个工作区，3个本地的工作区（工作目录、暂存区、资源库），1个远程的工作区（远程仓库）。

![image-20220315182734862](https://s2.loli.net/2022/03/15/j8bdtqFgcuPzh9H.png)

- 工作目录(Working Directory)：本地存放代码的文件，编写代码的目录文件。
- 暂存区(Stage)：可以临时存在你修改的文件，本质就是一个文件，在工作区通过 `git add .`将所有修改的文件加入的暂存区中。
- 资源库(History/Repository)：本地仓库，安全存放数据的位置，如果在暂存区中添加了新的文件，可以通过`git commit`将暂存区的文件提交到资源库中。
- 远程仓库(Remote Directory)：远程仓库，托管代码的服务器，可以通过 `git push` 将本地仓库的文件提交到远程仓库中。

> 常用命令：
>
> - `git add .`：将文件放入到暂存区
> - `git commit`：将暂存区的文件提交的本地仓库
> - `git push`：将本地仓库的文件上传到远程仓库
> - `git pull`：将远程仓库的文件下载到本地
> - `git reset`：如果将文件从暂存区提交到本地仓库后想回滚，就可以使用这个命令
> - `git checkout`：将暂存去还未提交到本地仓库的文件放入到本地工作区。

![image-20220315184057781](https://s2.loli.net/2022/03/15/ntq1HLIzZT8gb9A.png)

# 5、Git项目搭建和克隆

<font color=red>搭建：
</font>使用 `git init` 命令来初始化一个git项目，需要进入创建git项目的目录下。

![image-20220315204115663](https://s2.loli.net/2022/03/15/d2nZNiTJxyE6BFp.png)

<font color=red>克隆：</font>使用 `git clone url`命令从远程仓库中克隆项目到本地，`url`是远程仓库的地址，一般是`gitee`和`github`。

![image-20220315204601785](https://s2.loli.net/2022/03/15/HJAxbdPED4l59uy.png)

![image-20220315204726710](https://s2.loli.net/2022/03/15/drgyXkvuaJMFbLz.png)

# 6、Git基本操作

> Git的文件4中状态：版本控制就是文件的控制，对文件的修改、添加、提交就是版本的控制。

- `Untracked`：未跟踪状态，在工作目录中存在，但是没有加入到其他工作区。
- `Unmodify`：文件已经在本地仓库中，但是未修改与版本的快照一样，这类文件可以进行修改，也可以使用`git rm`移除版本控制。
- `Modify`：文件已经修改，但是没有进行其他操作，这类文件可以`git add`到暂存区中，也可以使用`git checkout`恢复到之前未修改的状态及`Unmodify`状态。
- `Staged`：暂存状态，可以使用`git commit`提交到本地仓库中，这样文件就变成了`UnModify`状态，也可以使用`git reset HEAD filename` 把文件取消暂存，修改为`Modify`状态。

> Git常见命令：

~~~bash
git status [filename]    # 查看文件状态。
git add .  # 将文件存放到暂存区
git commit -m [message]  # 将文件提交到本地仓库，message是提交携带的信息
~~~

> 文件忽略：在项目中一些文件不需要提交到仓库中，需要手动去配置忽略这些文件。需要在主目录中创建`.gitignore`文件，在这个文件中进行配置，下面是一些配置文件忽略的规则。

- 忽略文件中的空行或以#开头的行。
- 可以使用Linux通配符：`*` 表示匹配所有，`？`表示一个字符，`[abc]`表示可选字符的范围，`{str1,str2,....}`表示可选字符串的范围。
- 如果名称前存在 ！表示这个文件将不会被忽略。
- 如果名称前面添加 / 表示忽略的文件在此目录下，但子目录下的文件不忽略。
- 如果名臣后面添加 / 表示忽略这个目录下子目录的文件。

~~~bash
*.txt  # 获取所有.txt结尾的文件
!test.txt  # 表示不忽略test.txt文件
/temp  # 表示忽略的文件在temp目录下的文件，子目录下的文件不忽略
test/  # 表示获取test/目录下的所有文件
test/*.txt  # 表示会忽略test/目录下的所有.txt结尾的文件，其他文件不忽略
~~~

~~~xml
# 常见忽略文件

*.class
*.log
*.lock

*.jar
*.war
*.war
target/

.idea/
*.imi

*velocity.log*

.apt_generated
.factorypath
.springBeans

*ipr
*iws
.idea
.classpath
.project
.settings/
bin/

*.log
tmp/

*rebel.xml*
~~~

# 7、`Gitee`

> `Gitee`： 基于Git的代码托管和研发协作平台。

1. 注册`Gitee`账号：[Gitee官网:https://gitee.com/](https://gitee.com/)
2. 配置本地SSH公钥

- 生成SSH公钥：进入`C:\Users\LHJ16\.ssh`目录下，默认这个目录是空文件夹，使用`ssh-keygen`命令生成公钥。

<img src="https://s2.loli.net/2022/03/15/gcrkvi3OwEdVjIs.png" alt="image-20220315223742838" style="zoom:67%;" />

- 在码云中设置的安全设置中，将公钥复制到里面去

<img src="https://s2.loli.net/2022/03/15/NegLQf3JnMmSjwD.png" alt="image-20220315223928044" style="zoom:67%;" />

3. 连接`Gitee`远程仓库：在上面步骤的基础下，并配置了`user.name`和`user.email`。

<font color=red>克隆方式：</font>通过克隆一个远程仓库，然后将生成的`.git`文件夹放入到自己的工作目录下就可以使用命令进行版本控制。

<font color=red>初始化方式：</font>通过`git init`初始化一个本地仓库，然后通过以下命令连接远程仓库。

~~~bash
# 选择下面一种方式即可
git remote add origin 仓库的SSH
git remote add origin 仓库的url
~~~

![image-20220316104831154](https://s2.loli.net/2022/03/16/FKVWLIkBvcDE8T6.png)

# 8、Git 分支

> 分支就是从主分支上分离出来一个分支，不影响主分支中的任务，子分支也不受主分支的影响，当子分支工作完成后，可以合并到主分支上，Git默认的主分支是master。

<font color=red>Git常用分支命令：
</font>

~~~bash
git branch  # 查看本地分支
git branch -a # 查看本地和远程的所有分支
git branch -r # 查看远程仓库的所有分支
git branch [新分支名] # 新建一个分支，但是依然在当前的分支
git checkout [分支名] # 切换到某个分支
git checkout -b [分支名]  # 新建一个分支，并切换到新分支下
git merge [分支名] # 将这个分支的代码合并到主分支上
git branch -d [分支名] # 删除某个分支
git push origin [本地分支名]:[远程分支名] # 新建远程分支，本地分支和远程分支名称建议相同，但是也可以修改
git push origin --delete [分支名] # 删除远程上的分支
~~~

> 分支合并：两个分支进行合并时，如果分支中同一份文件的代码发生冲突时，可以选择保存哪一个分支上的代码

# 9、拓展

## 9.1、Git插件

- 浏览器插件：[https://gitee.com/oschina/GitCodeTree](https://gitee.com/oschina/GitCodeTree)

![image-20220316163753463](https://s2.loli.net/2022/03/16/FV7dA8DCgfxkmb5.png)

- idea的`Gitee`插件：在idea的插件市场中搜索`gitee`，输入`gitee`账号后，可以克隆这个用户的所有项目，也可以直接在idea中直接push代码，默认是`https`方式。

<img src="https://s2.loli.net/2022/03/16/BIyRPtNdxQSovuZ.png" alt="image-20220316164025035" style="zoom:67%;" />

<img src="https://s2.loli.net/2022/03/16/3mUiTb8wgLXdqEK.png" alt="image-20220316164631747" style="zoom:67%;" />

![image-20220316165022919](https://s2.loli.net/2022/03/16/PdXoiDNr3G2f5Zg.png)

















